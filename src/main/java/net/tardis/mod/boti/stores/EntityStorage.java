package net.tardis.mod.boti.stores;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.UUID;
import java.util.function.Function;

public class EntityStorage extends AbstractEntityStorage {

    public EntityStorageTypes storeType;
    public double prevPosX, prevPosY, prevPosZ;
    public double posX, posY, posZ;
    public float yaw, pitch, limbSwing = 0, limbSwingAmount= 0;
    public ResourceLocation type;
    public UUID id;
    public CompoundNBT data;
    public boolean isSneaking;
    public boolean isAlive;


    private int lastUpdatedTime = 40;
    private boolean removed = false;

    Entity entity;

    public EntityStorage(Entity e) {

        if(!e.isAlive()){
            this.isAlive = false;
            this.id = e.getUniqueID();
            this.entity = null;
            return;
        }

        posX = e.getPosX();
        posY = e.getPosY();
        posZ = e.getPosZ();

        yaw = e.rotationYaw;
        pitch = e.rotationPitch;

        if(e instanceof LivingEntity){
            LivingEntity livingEntity = (LivingEntity) e;
            limbSwing = livingEntity.limbSwing;
            limbSwingAmount = livingEntity.limbSwingAmount;
        }

        id = e.getUniqueID();
        type = e.getType().getRegistryName();
        data = e.serializeNBT();
        this.isSneaking = e.isSneaking();
        this.isAlive = e.isAlive();
        this.entity = e;
    }

    public EntityStorage(PacketBuffer buf) {
        this.decode(buf);
    }

    private EntityStorage(UUID id){
        this.id = id;
        this.isAlive = false;
        this.entity = null;
    }

    public void setStoreType(EntityStorageTypes type){
        this.storeType = type;
    }

    @Override
    public void encode(PacketBuffer buf) {

        buf.writeUniqueId(id);
        buf.writeBoolean(this.isAlive);

        if(!this.isAlive)
            return;

        buf.writeDouble(posX);
        buf.writeDouble(posY);
        buf.writeDouble(posZ);


        buf.writeFloat(yaw);
        buf.writeFloat(pitch);
        buf.writeFloat(limbSwing);
        buf.writeFloat(limbSwingAmount);

        buf.writeResourceLocation(type);
        buf.writeCompoundTag(data);
        buf.writeBoolean(this.isSneaking);
    }

    @Override
    public void decode(PacketBuffer buf) {

        id = buf.readUniqueId();
        this.isAlive = buf.readBoolean();

        if(!this.isAlive)
            return;

        posX = buf.readDouble();
        posY = buf.readDouble();
        posZ = buf.readDouble();

        yaw = buf.readFloat();
        pitch = buf.readFloat();
        limbSwing = buf.readFloat();
        limbSwingAmount = buf.readFloat();

        type = buf.readResourceLocation();
        data = buf.readCompoundTag();
        this.isSneaking = buf.readBoolean();
    }

    public void updateEntity(EntityStorage store) {

        combine(store);

        updateEntity();
        this.lastUpdatedTime = 40;
    }

    public void updateEntity(){
        if(this.entity != null){

            if(this.data != null)
                entity.deserializeNBT(this.data);

            entity.lastTickPosX = entity.prevPosX = this.prevPosX;
            entity.lastTickPosY = entity.prevPosY = this.prevPosY;
            entity.lastTickPosZ = entity.prevPosZ = this.prevPosZ;

            entity.setPosition(posX, posY, posZ);

            entity.prevRotationYaw = this.yaw;
            entity.prevRotationPitch = this.pitch;
            entity.rotationYaw = this.yaw;
            entity.rotationPitch = this.pitch;
            entity.setSneaking(this.isSneaking);
        }
    }

    public void combine(EntityStorage store){
        this.type = store.type;
        this.data = store.data;
        this.id = store.id;
        this.pitch = store.pitch;
        this.yaw = store.yaw;
        this.posX = store.posX;
        this.posY = store.posY;
        this.posZ = store.posZ;
        this.isSneaking = store.isSneaking;
        this.isAlive = store.isAlive;

    }

    @OnlyIn(Dist.CLIENT)
    public Entity getOrCreateEntity(World world){

        if(this.entity != null)
            return getEntity();

        return create(world);
    }

    @OnlyIn(Dist.CLIENT)
    public Entity create(World world){
        try{
            EntityType<?> type = ForgeRegistries.ENTITIES.getValue(this.type);
            if(type != null) {
                this.entity = type.create(world);
                this.entity.deserializeNBT(this.data);
                return this.entity;
            }
        }
        catch(Exception e){}
        return null;
    }

    @Nullable
    public Entity getEntity(){
        return this.entity;
    }

    public Vector3d createPosVec(){
        return new Vector3d(this.posX, this.posY, this.posZ);
    }

    public void tick(){
        if(this.entity != null){
            this.entity.tick();
        }

        this.prevPosX = posX;
        this.prevPosY = posY;
        this.prevPosZ = posZ;
        //Comment this out for now as the player no longer gets captured by boti
//        if(this.lastUpdatedTime > 0) {
//            --this.lastUpdatedTime;
//            if(this.lastUpdatedTime == 0 && !this.entity.forceSpawn)
//                this.removed = true;
//        }
    }

    public enum EntityStorageTypes{
        NORMAL(buf -> new EntityStorage(buf)),
        PLAYER(buf -> new PlayerEntityStorage(buf));

        Function<PacketBuffer, EntityStorage> factory;

        EntityStorageTypes(Function<PacketBuffer, EntityStorage> factory){
            this.factory = factory;
        }

        public EntityStorage create(PacketBuffer buf){
            return this.factory.apply(buf);
        }

    }

    public boolean isRemoved(){
        return this.removed;
    }

    public EntityStorageTypes getStoreType(){
        if(this.storeType == null)
            this.storeType = EntityStorageTypes.NORMAL;

        return this.storeType;
    }

    public static EntityStorage createDeadStore(UUID id){
        return new EntityStorage(id);
    }
}
