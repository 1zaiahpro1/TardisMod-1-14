package net.tardis.mod.tileentities.exteriors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.registries.DisguiseRegistry;
import net.tardis.mod.subsystem.ChameleonSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class DisguiseExteriorTile extends ExteriorTile{

	public Disguise disguise = DisguiseRegistry.STONE_PILLAR.get();
	
	private float particleRot = 0F;
	
	public DisguiseExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public DisguiseExteriorTile() {
		super(TTiles.EXTERIOR_DISGUISE.get());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		if(world != null && world.getBlockState(getPos()).hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
			switch(world.getBlockState(this.getPos()).get(BlockStateProperties.HORIZONTAL_FACING)) {
				case EAST: return new AxisAlignedBB(0.5, -1, 0, 1.1, 1, 1);
				case SOUTH: return new AxisAlignedBB(0, -1, 0.5, 1, 1, 1.1);
				case WEST: return new AxisAlignedBB(-0.1, -1, 0, 0.5, 1, 1);
				default: return new AxisAlignedBB(0, 0, -0.1, 1, 2, 0.5);
			}
		}
		return new AxisAlignedBB(0, 0, 0, 0, 0, 0);
		
	}
	
	@Override
    public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(this.getPos()).grow(2, 5, 2);
	}

	@Override
	public void deleteExteriorBlocks() {
		super.deleteExteriorBlocks(); //Need to call super so we will delete the top and bottom blocks
		this.deleteDisguiseBlocks();
	}

	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		if(compound.contains("disguise")) {
			this.disguise = DisguiseRegistry.DISGUISE_REGISTRY.get().getValue(new ResourceLocation(compound.getString("disguise")));
		}
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		if(this.disguise != null) {
			compound.putString("disguise", this.disguise.getRegistryName().toString());
		}
		return super.write(compound);
	}

	@Override
	public void tick() {
		super.tick();
		
		if(world.isRemote && this.getMatterState() != EnumMatterState.SOLID) {
			
			int width = 1;
			int height = 2;
			
			if(this.disguise != null) {
				width = this.disguise.getWidth() + 1;
				height = this.disguise.getHeight() + 1;
			}
			
			this.particleRot = (this.particleRot + 10) % (360.0F * height);
			this.spawnParticle(Math.sin(Math.toRadians(particleRot % 360.0F)) * width,
					this.particleRot / 360.0F,
					Math.cos(Math.toRadians(particleRot % 360.0F)) * width);
		}
		
	}
	
	public void spawnParticle(double x, double y, double z) {
		
		BlockState state = world.getBlockState(this.getPos().down(2));
		if(this.disguise != null)
			state = this.disguise.getBottomState();
		
		world.addParticle(new BlockParticleData(ParticleTypes.BLOCK, state).setPos(this.getPos()),
				(this.getPos().getX() + 0.5) + x, this.getPos().getY() + y, (this.getPos().getZ() + 0.5) + z,
				0, 1, 0);
	}

	@Override
	public void placeExteriorBlocks() {
		super.placeExteriorBlocks();
		this.findDisguise();
		this.placeDisguiseBlocks();
	}
	
	protected void deleteDisguiseBlocks() {
		if(this.disguise != null) {
			if (!this.world.isRemote()) {
				BlockPos extPos = this.getPos().toImmutable();
				if (this.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
					Direction facing = this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING).getOpposite();
					for(Entry<BlockPos, BlockState> entry : this.disguise.getOtherBlocks().entrySet()) {
						BlockPos offset = entry.getKey();
						BlockPos rotated = WorldHelper.rotateBlockPos(offset, facing);
						BlockPos rotatedOffset = extPos.add(rotated);
							world.setBlockState(rotatedOffset, Blocks.AIR.getDefaultState(), 3);
					}
				}
			}
		}
	}
	
	protected void placeDisguiseBlocks() {
		if(this.disguise != null) {
			if (!this.world.isRemote()) {
				BlockPos extPos = this.getPos().toImmutable();
				if (this.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
					Direction oppositeFacing = this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING).getOpposite();
					for(Entry<BlockPos, BlockState> entry : this.disguise.getOtherBlocks().entrySet()) {
						BlockPos offset = entry.getKey();
						BlockPos rotated = WorldHelper.rotateBlockPos(offset, oppositeFacing);
						BlockPos rotatedOffset = extPos.add(rotated);
						boolean canPlace = world.getBlockState(rotatedOffset).isAir();
						if (canPlace) {
							if (entry.getValue().hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
								if (entry.getValue().get(BlockStateProperties.HORIZONTAL_FACING) != oppositeFacing)
								     world.setBlockState(rotatedOffset, entry.getValue().with(BlockStateProperties.HORIZONTAL_FACING, oppositeFacing), 3);
							}
							else if (entry.getValue().hasProperty(BlockStateProperties.FACING)) {
								if (entry.getValue().get(BlockStateProperties.HORIZONTAL_FACING) != oppositeFacing)
								    world.setBlockState(rotatedOffset, entry.getValue().with(BlockStateProperties.FACING, oppositeFacing), 3);
							}
							else
							    world.setBlockState(rotatedOffset, entry.getValue(), 3);
						}
					}
			    }
				BlockPos bottomPos = extPos.down();
				if (world.getBlockState(bottomPos).getMaterial().isReplaceable())
					world.setBlockState(bottomPos, TBlocks.bottom_exterior.get().getDefaultState()); //Setting an actual block fixes the lighting issue
		    }
			
	    }
	}
	
	@Override
	public void copyConsoleData(ConsoleTile console) {
		super.copyConsoleData(console);
		if(console.getExteriorManager().getDisguise() != null)
			this.disguise = console.getExteriorManager().getDisguise();
	}

	public void findDisguise() {
		if (!world.isRemote()) {
			
			//Stop changes if Chameleon circuit doesn't have enough health
			ConsoleTile console = TardisHelper.getConsole(world.getServer(), this.getInteriorDimensionKey()).orElse(null);
			if(console != null){
				ChameleonSubsystem sys = console.getSubsystem(ChameleonSubsystem.class).orElse(null);
				if(sys != null && !sys.isActiveCamo(true))
					return;
			}
			
			List<Disguise> valid = new ArrayList<>();
			for(Disguise d : DisguiseRegistry.DISGUISE_REGISTRY.get().getValues()) {
				if(d.getIsValid(world, getPos()))
					valid.add(d);
			}
			
			Iterator<Disguise> it = valid.iterator();
			while(it.hasNext()) {
				Disguise d = it.next();
				for(BlockPos pos : d.getOtherBlocks().keySet()) {
					BlockPos next = this.getPos().add(pos);
					if(!world.getBlockState(next).getMaterial().isReplaceable()) {
						it.remove();
						break;
					}
				}
			}
			
			this.disguise = valid.size() > 0 ? valid.get(world.rand.nextInt(valid.size())) : DisguiseRegistry.STONE_PILLAR.get();
			TardisHelper.getConsole(world.getServer(), this.getInteriorDimensionKey()).ifPresent(tile -> tile.getExteriorManager().setDisguise(disguise));
			
		}
		
	}
}
