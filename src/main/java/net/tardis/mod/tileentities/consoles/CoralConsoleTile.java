package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.texturevariants.ConsoleTextureVariants;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

/**
 * Created by Swirtzly
 * on 01/05/2020 @ 12:18
 */
public class CoralConsoleTile extends ConsoleTile {
	
    public CoralConsoleTile() {
        super(TTiles.CONSOLE_CORAL.get());
        this.registerControlEntry(ControlRegistry.MONITOR.get());
        this.variants = ConsoleTextureVariants.CORAL;
    }
    public CoralConsoleTile(TileEntityType<?> type) {
    	super(type);
    }

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return new AxisAlignedBB(this.getPos()).expand(2, 4, 2);
    }
}
