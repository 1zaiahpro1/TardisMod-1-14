package net.tardis.mod.tileentities;

import net.minecraft.block.BlockState;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.tardis.mod.blocks.VortexDetectorBlock;
import net.tardis.mod.helper.TardisHelper;

public class VortexDetectorTile extends TileEntity implements ITickableTileEntity{

	public VortexDetectorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public VortexDetectorTile() {
		this(TTiles.VORTEX_DETECTOR.get());
	}

	@Override
	public void tick() {
		if(!world.isRemote) {
			
			boolean shouldBeOn = this.shouldBeOn();
			if(shouldBeOn != this.getOn())
				this.setOn(shouldBeOn);
			
		}
	}
	
	private boolean shouldBeOn() {
		ConsoleTile tile = TardisHelper.getConsoleInWorld(getWorld()).orElse(null);
		if(tile != null) {
			if(this.getInverted())
				return !tile.isInFlight();
			return tile.isInFlight();
		}
		return false;
	}
	
	private boolean getInverted() {
		BlockState state = world.getBlockState(getPos());
		if(state.hasProperty(VortexDetectorBlock.INVERTED))
			return state.get(VortexDetectorBlock.INVERTED);
		return false;
	}
	
	private void setOn(boolean on) {
		BlockState state = world.getBlockState(getPos());
		if(state.hasProperty(VortexDetectorBlock.POWERED))
			world.setBlockState(getPos(), state.with(VortexDetectorBlock.POWERED, on), 3);
	}
	
	private boolean getOn() {
		BlockState state = world.getBlockState(getPos());
		if(state.hasProperty(VortexDetectorBlock.POWERED))
			return state.get(VortexDetectorBlock.POWERED);
		return false;
	}

}
