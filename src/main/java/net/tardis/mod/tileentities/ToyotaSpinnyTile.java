package net.tardis.mod.tileentities;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.sounds.TSounds;

public class ToyotaSpinnyTile extends MultiblockMasterTile implements ITickableTileEntity{

	
	public static final int TAKE_OFF_TIME = 120;
	private ConsoleTile tile;
	
	public float rotation = 0;
	public float prevRotation = 0;
	
	private AxisAlignedBB renderBox;
	
	public ToyotaSpinnyTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public ToyotaSpinnyTile() {
		super(TTiles.TOYOTA_SPIN.get());
	}

	@Override
	public void tick() {
		
		if(tile == null || tile.isRemoved())
			TardisHelper.getConsoleInWorld(world).ifPresent(con -> this.tile = con);
		
		this.prevRotation = this.rotation;
		
		if(tile != null && tile.isInFlight()) {
			this.playFlightStartSound(tile);
			this.playFlyLoop(tile, 41);
			this.playLandSound(tile);
			this.rotation = (this.rotation + 1) % 360.0F;
		}
		
		if(this.renderBox == null)
			this.renderBox = new AxisAlignedBB(this.getPos()).grow(5);
		
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return renderBox == null ? new AxisAlignedBB(this.getPos()) : renderBox;
	}
	
	public void playFlightStartSound(ConsoleTile console) {
		if(!console.getWorld().isRemote) {
			if (console.flightTicks == 2) {
				console.getWorld().playSound(null, console.getPos(), TSounds.ROTOR_START.get(), SoundCategory.BLOCKS, 0.175F, 1F);
			}
		}
	}
	
	public void playFlyLoop(ConsoleTile console, int loopTime) {
		if (!console.getWorld().isRemote()) {
			boolean isPlayingTakeoff = console.flightTicks < TAKE_OFF_TIME; //Play before shutting down sound
			boolean isPlayingLand = console.getLandTime() == 0 ? false : console.getLandTime() - console.flightTicks < TAKE_OFF_TIME;
			if(console.flightTicks % loopTime == 0 && !(isPlayingTakeoff || isPlayingLand)) {
				console.getWorld().playSound(null, this.tile.getPos(), TSounds.ROTOR_TICK.get(), SoundCategory.BLOCKS, 0.175F, 1F);
			}
		}
	}
	
	public void playLandSound(ConsoleTile console) {
		if(!console.getWorld().isRemote && (console.getLandTime() > 0 && console.getLandTime() - console.flightTicks == TAKE_OFF_TIME)) { //Delay for 6 seconds after remat sound has started, so this sound will stop in sync with remat sound
			console.getWorld().playSound(null, this.tile.getPos(), TSounds.ROTOR_END.get(), SoundCategory.BLOCKS, 0.175F, 1F);
		}	
	}

}
