package net.tardis.mod.tileentities.console.misc;

import net.minecraft.util.SoundEvent;

public interface IAlarmType{
	SoundEvent getLoopSound();
	int getLoopType();
}