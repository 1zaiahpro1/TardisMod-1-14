package net.tardis.mod.tileentities.monitors;

import java.text.DecimalFormat;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.IMonitor;
import net.tardis.mod.tileentities.TTiles;
import net.tardis.mod.tileentities.ConsoleTile;

public class MonitorTile extends TileEntity implements ITickableTileEntity, IMonitor{

	protected String[] info = new String[0];
	public double startX = 0;
	public double startY = 0;
	public double startZ = 0;
	public double width = 0, height = 0;
	public MonitorView view = MonitorView.NORTH_WEST;
	private ConsoleTile console;
	
	public static DecimalFormat format = new DecimalFormat("###");
	
	public MonitorTile() {
		this(TTiles.STEAMPUNK_MONITOR.get());
	}
	
	public MonitorTile(TileEntityType<?> tile) {
		super(tile);
	}

	@Override
	public void tick() {
		ConsoleTile console = this.getConsole();
		
		if(console == null)
			return;
			
		this.info = Helper.getConsoleText(console);
	}
	
	@Nullable
	private ConsoleTile getConsole() {
		if(console == null || console.isRemoved())
			TardisHelper.getConsoleInWorld(world).ifPresent(tile -> this.console = tile);
		return console;
	}
	
	public void setGuiXYZ(double x, double y, double z, double width, double height) {
		this.startX = x;
		this.startY = y;
		this.startZ = z;
		this.width = width;
		this.height = height;
	}
	
	public String[] getInfo() {
		return info;
	}
	
	public MonitorView getView() {
		return this.view;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.getUpdateTag());
	}
	
	public static enum MonitorView{
		PANORAMIC,
		SOUTH_EAST,
		NORTH_WEST;
		
		Supplier<Float> angle;
		
		public void setAngle(Supplier<Float> sup) {
			this.angle = sup;
		}
		
		public float getAngle() {
			return this.angle.get();
		}
	}

	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		this.view = MonitorView.values()[compound.getInt("view")];
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putInt("view", this.view.ordinal());
		return super.write(compound);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	public void setView(MonitorView view) {
		this.view = view;
		this.markDirty();
		if(!world.isRemote)
			world.markAndNotifyBlock(getPos(), world.getChunkAt(this.getPos()), this.getBlockState(), this.getBlockState(), 3, 512);
	}

}
