package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.XionConsoleModel;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class XionConsoleRenderer extends TileEntityRenderer<XionConsoleTile> {
    
    public static final XionConsoleModel MODEL = new XionConsoleModel(tex -> RenderType.getEntityCutout(tex));
    public static final WorldText TEXT = new WorldText(0.44F, 0.25F, 0.003F, 0xFFFFFF);
    
    public XionConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    
    @Override
    public void render(XionConsoleTile console, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrixStackIn.push();
        matrixStackIn.translate(0.5, 0.335,  0.5);
        matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180)); //Rotate 180 degrees so model isn't upside down
        ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/xion.png");
        //Monitor text
        console.getControl(MonitorControl.class).ifPresent(monitor -> {
            matrixStackIn.push();
            matrixStackIn.rotate(Vector3f.YN.rotationDegrees(210));
            matrixStackIn.rotate(Vector3f.XP.rotationDegrees(5));
            matrixStackIn.translate(-0.222, -1.235, -7.55 / 16.0);
            TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, Helper.getConsoleText(console));
            matrixStackIn.pop();
        });
        
        if(console.getVariant() != null)
            texture = console.getVariant().getTexture();
        
        float scale = 0.225F;
        matrixStackIn.scale(scale, scale, scale); //Use a local variable to scale console easier
        MODEL.render(console, scale, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F); //Scale down by 4
        
         //Sonics
        matrixStackIn.push();
        matrixStackIn.translate(-1.9, -3, 2.44);
        matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(25));
        matrixStackIn.rotate(Vector3f.XN.rotationDegrees(12.5F));
        float sonic_scale = 1.25F;
        matrixStackIn.scale(sonic_scale, sonic_scale, sonic_scale);
        Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();
        
        matrixStackIn.pop();
    }
}
