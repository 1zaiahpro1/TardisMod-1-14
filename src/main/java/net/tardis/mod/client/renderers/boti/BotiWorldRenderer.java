package net.tardis.mod.client.renderers.boti;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderTypeBuffers;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.world.DimensionType;
import net.tardis.mod.boti.BotiWorld;

public class BotiWorldRenderer extends WorldRenderer {

    private DimensionType type;

    public BotiWorldRenderer(Minecraft mcIn, RenderTypeBuffers rainTimeBuffersIn) {
        super(mcIn, rainTimeBuffersIn);
    }

    public void setDimensionType(DimensionType dimensionType, BotiWorld world){
        this.setWorldAndLoadRenderers(world);
        this.type = dimensionType;
    }

    public DimensionType getDimensionType(){
        return this.type;
    }

}
