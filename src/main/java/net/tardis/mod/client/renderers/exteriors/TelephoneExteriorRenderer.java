package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.models.exteriors.TelephoneExteriorModel;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.TelephoneExteriorTile;

public class TelephoneExteriorRenderer extends ExteriorRenderer<TelephoneExteriorTile> {

    public TelephoneExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/telephone_red.png");
    public static TelephoneExteriorModel MODEL = new TelephoneExteriorModel();
    public static WorldText TEXT = new WorldText(0.87F, 0.125F, 0.015F, 0x000000);
    
    @Override
    public void renderExterior(TelephoneExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
        matrixStackIn.push();
        matrixStackIn.translate(0, -1, 0);
        ResourceLocation texture = TEXTURE;
        if(tile.getVariant() != null)
            texture = tile.getVariant().getTexture();
        MODEL.render(tile, 1.0F, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(texture)), combinedLightIn, combinedOverlayIn, alpha);
        
        matrixStackIn.push();
        matrixStackIn.translate(-7 / 16.0, -15.5 / 16.0, -(8.6 / 16.0));
        
        TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
        
        matrixStackIn.pop();
        
        matrixStackIn.pop();
    }

}
