package net.tardis.mod.client.guis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.lwjgl.glfw.GLFW;

import java.util.SortedMap;
import java.util.TreeMap;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TRenderHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SendTardisDistressSignal;

public class TardisSendDistressScreen extends Screen implements INeedTardisNames{

    public static final StringTextComponent TITLE = new StringTextComponent("");
    private final TranslationTextComponent message_desc = new TranslationTextComponent(TardisConstants.Strings.GUI + "distress_signal.message_desc");
    private final TranslationTextComponent select_ship = new TranslationTextComponent(TardisConstants.Strings.GUI + "distress_signal.select_ship");
    
    private static final int MAX_PER_PAGE = 4;
    private int index = 0;
    
    private TreeMap<ResourceLocation, String> names = new TreeMap<>();
    private List<SortedMap<ResourceLocation, String>> totalPages = new ArrayList<>();
    private TextFieldWidget message;

    public TardisSendDistressScreen() {
        super(TITLE);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        drawCenteredString(matrixStack, this.font, message_desc.getString(), width / 2, height / 2 - 60, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, select_ship.getString(), width / 2, height / 2 - 10, 0xFFFFFF);

        int index = 1;
        for (int i = 0; i < this.buttons.size(); ++i) {
            if (this.buttons.get(i).isHovered()) {
                index = i + 1;
                break;
            }
        }

        RenderSystem.disableTexture();
        TRenderHelper.renderSineWave(width / 2 - 70, height / 2 - 80, 200, 10, 0.4 * index);
        RenderSystem.enableTexture();
        super.render(matrixStack, mouseX, mouseY, partialTicks);

    }

    @Override
    protected void init() {
        super.init();

        this.buttons.clear();

        int x = width / 2 - 75, y = height / 2 - 40;
        this.message = this.addButton(new TextFieldWidget(this.font, x, y, 150, this.font.FONT_HEIGHT + 8, new TranslationTextComponent("")));

        this.setupDistressButtons();
        
        this.addButton(new Button(width / 2 + 50, height / 2 + 75, 20, 20, new StringTextComponent(">"), but -> modIndex(1)));
        this.addButton(new Button(width / 2 - 80, height / 2 + 75, 20, 20, new StringTextComponent("<"), but -> modIndex(-1)));
        
        this.message.setFocused2(true);
    }

     @Override
     public void setNamesFromServer(Map<ResourceLocation, String> nameMap) {
          this.names.clear();
          this.names.putAll(nameMap);
          this.init();
     }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        message.mouseClicked(mouseX, mouseY, button);
        return super.mouseClicked(mouseX, mouseY, button);
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
    	if (this.message.isFocused()) {
            boolean pressed = message.keyPressed(keyCode, scanCode, modifiers);
            return pressed;
        }
        return super.keyPressed(keyCode, scanCode, modifiers);
    }

    @Override
    public boolean charTyped(char codePoint, int modifiers) {
        return this.message.charTyped(codePoint, modifiers);
    }
     
    private void setupDistressButtons() {
         int initialPages = (int)((double)this.names.size()/(double)MAX_PER_PAGE);
          
         //Account for any leftover entries. If true, add an extra page
         int numPages = (int)((double)this.names.size() % (double)MAX_PER_PAGE) == 0 ? initialPages : initialPages + 1;
 
         SortedMap<ResourceLocation, String> currentNames = this.names;
          
         if (numPages > 0) {
             List<ResourceLocation> keys = new ArrayList<>();
             
             this.names.keySet().forEach(entry -> keys.add(entry));
               
             for (int i = 0; i < numPages; i++) {
                 int startIndex = i * MAX_PER_PAGE;
                 int endIndex = startIndex + MAX_PER_PAGE;
                  
                 ResourceLocation startKey = keys.get(startIndex >= keys.size() ? 0 : startIndex);
                 ResourceLocation endKey = keys.get(endIndex >= keys.size() ? keys.size() - 1 : endIndex);
                  
                 SortedMap<ResourceLocation, String> partitionedMap;
                  
                 //If we are on the last page, include the ending key as well so we can get all of them
                 if (i == numPages - 1) {
                     partitionedMap = this.names.subMap(startKey, true, endKey, true);
                 }
                 else {
                     partitionedMap = this.names.subMap(startKey, endKey);
                 }
                  
                 this.totalPages.add(partitionedMap);
             }
               
             currentNames = this.totalPages.get(this.index);
        }
          
          
        int x2 = width / 2 - 50, y2 = height / 2 + 10;

        int spacingIndex = 1;

        this.addButton(new TextButton(x2, y2, new TranslationTextComponent(TardisConstants.Translations.ALL).getString(), but -> {
            Network.sendToServer(new SendTardisDistressSignal(this.message.getText(), null));
            closeScreen();
        }));

        for(Entry<ResourceLocation, String> e : currentNames.entrySet()) {
             this.addButton(new TextButton(x2, y2 + ((this.font.FONT_HEIGHT + 2) * spacingIndex), e.getValue(), but -> {
                Network.sendToServer(new SendTardisDistressSignal(this.message.getText(), e.getKey()));
                closeScreen();
            }));
            ++spacingIndex;
        }
        
     }
     
     private void modIndex(int mod) {
          int size = this.totalPages.size();
          if (this.index + mod > size)
            this.index = 0;
        else if (this.index + mod < 0)
            this.index = size - 1;
        else this.index += mod;

        this.init();
     }

}
