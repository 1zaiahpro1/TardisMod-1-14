package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.WaypointOpenMessage;
import net.tardis.mod.network.packets.WaypointSaveMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class WaypointNewMonitorScreen extends MonitorScreen {

    private final TranslationTextComponent title = new TranslationTextComponent(TardisConstants.Strings.GUI + "waypoint.new.title");
    private final TranslationTextComponent desc = new TranslationTextComponent(TardisConstants.Strings.GUI + "waypoint.new.desc");
    private final TranslationTextComponent suggestion = new TranslationTextComponent(TardisConstants.Strings.GUI + "waypoint.new.suggestion");
    private TextFieldWidget waypointName;

    public WaypointNewMonitorScreen(IMonitorGui mon) {
        super(mon, "waypoint_new");
    }

    @Override
    protected void init() {
        super.init();
        this.buttons.clear();
        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), TardisConstants.Translations.GUI_CANCEL,
                but -> {Minecraft.getInstance().displayGuiScreen(new WaypointMonitorScreen(this.parent, "waypoints")); Network.sendToServer(new WaypointOpenMessage(null));}));

        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), TardisConstants.Translations.GUI_SAVE, but -> {
            Network.sendToServer(new WaypointSaveMessage(this.waypointName.getText().isEmpty() ? "EmptyName" : this.waypointName.getText()));
            Minecraft.getInstance().displayGuiScreen(null);
        }));

        int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
        int width = 150;
        this.addButton(this.waypointName = new TextFieldWidget(this.font, centerX - width / 2, this.parent.getMaxY() + 40, width, 20, new TranslationTextComponent("Test")));
        this.waypointName.setSuggestion(suggestion.getString());
    }

    @Override
    public void render(MatrixStack matrixStack, int p_render_1_, int p_render_2_, float p_render_3_) {
        super.render(matrixStack, p_render_1_, p_render_2_, p_render_3_);
        int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
        drawCenteredString(matrixStack, this.font, title.getString(), centerX, this.parent.getMaxY() + 10, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, desc.getString(), centerX, this.parent.getMaxY() + 30, 0xFFFFFF);
        if (this.waypointName.isFocused()) {
            this.waypointName.setSuggestion("");
        } else if (this.waypointName.getText().isEmpty()) {
            this.waypointName.setSuggestion(suggestion.getString());
        }
        TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
        if (te instanceof ConsoleTile && te != null) {
            ConsoleTile con = (ConsoleTile) te;
            int width = 150;
            this.font.drawString(matrixStack, TardisConstants.Translations.LOCATION.getString() + WorldHelper.formatBlockPos(con.getCurrentLocation()),
                    centerX - width / 2, this.parent.getMaxY() + 70, 0xFFFFFF);

            this.font.drawString(matrixStack, TardisConstants.Translations.DIMENSION.getString() + WorldHelper.formatDimName(con.getCurrentDimension()),
                    centerX - width / 2, this.parent.getMaxY() + 80, 0xFFFFFF);
            
            this.font.drawString(matrixStack, TardisConstants.Translations.FACING.getString() + con.getExteriorFacingDirection().getName2(), centerX - width / 2, this.parent.getMaxY() + 90, 0xFFFFFF);
        }

    }
}
