package net.tardis.mod.client.guis.minigame.circuit;

import net.minecraft.client.renderer.BufferBuilder;

public abstract class Piece {

	int x, y;
	int width, height;
	
	float minU, minV, maxU, maxV;
	
	public Piece(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void setTextureData(int u, int v) {
		this.minU = u / 256.0F;
		this.minV = v / 256.0F;
		
		this.maxU = this.minU + (width / 256.0F);
		this.maxV = this.minV + (height / 256.0F);
	}
	
	public abstract boolean connectsTo(Connection connect);
	
	public void render(BufferBuilder bb) {
		bb.pos(x, y, 0).tex(this.minU, this.minV).endVertex();
		bb.pos(x, y + height, 0).tex(this.minU, this.maxV).endVertex();
		bb.pos(x + width, y + height, 0).tex(this.maxU, this.maxV).endVertex();
		bb.pos(x + width, y, 0).tex(this.maxU, this.minV).endVertex();
	}
	
	public static enum Connection{
		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT,
		CENTER;
	}
}
