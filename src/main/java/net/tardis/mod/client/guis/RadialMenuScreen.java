package net.tardis.mod.client.guis;


import java.util.List;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.client.guis.radial.IRadialAction;
import net.tardis.mod.client.guis.widgets.RadialButton;
import net.tardis.mod.sounds.TSounds;

public abstract class RadialMenuScreen extends Screen {

    private final List<IRadialAction> radialActions = Lists.newArrayList();

    private float prevRotation = 0F;
    private float rotation = 0F;

    public RadialMenuScreen(ITextComponent titleIn) {
        super(titleIn);
    }

    @Override
    public void init() {
        super.init();
        this.buttons.clear();
        this.radialActions.clear();

        this.addRadialButtons();

        //Add the actual buttons

        int centerX = this.width / 2, centerY = this.height / 2;
        double length = 41;

        float step = 360.0F / (float) this.radialActions.size();

        for (int i = 0; i < this.radialActions.size(); ++i) {

            float angle = (float) Math.toRadians(step * i);

            IRadialAction action = this.radialActions.get(i);
            this.addButton(new RadialButton(centerX + (int) (Math.sin(angle) * length),
                    centerY + (int) (-Math.cos(angle) * length), this.getTexture(),
                    action));
        }
        Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.GADGET_MENU_OPEN.get(), 1.0F));

    }

    public void addMenuButton(IRadialAction action) {
        this.radialActions.add(action);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {

        Minecraft.getInstance().textureManager.bindTexture(this.getTexture());

        //Render background
        this.blit(matrixStack, this.width / 2 - 143 / 2, this.height / 2 - 143 / 2, 0, 0, 143, 143);
        this.renderInnerCircle(matrixStack);

        for (Widget widget : buttons) {
            if (widget instanceof RadialButton) {
                RadialButton radialButton = (RadialButton) widget;
                if (radialButton.isHovered()) {
                    renderTooltip(matrixStack, radialButton.getAction().getTextComponent(), mouseX, mouseY);
                }
            }
        }

        super.render(matrixStack, mouseX, mouseY, partialTicks);
    }

    public void renderInnerCircle(MatrixStack matrixStack) {
        matrixStack.push();
        matrixStack.translate(width / 2, height / 2, 0);
        matrixStack.rotate(Vector3f.ZP.rotationDegrees(MathHelper.lerp(Minecraft.getInstance().getRenderPartialTicks(), this.prevRotation, this.rotation)));
        matrixStack.translate(-width / 2, -height / 2, 0);
        this.blit(matrixStack, width / 2 - 113 / 2, height / 2 - 113 / 2, 143, 0, 113, 112);
        matrixStack.pop();
    }

    @Override
    public void tick() {
        super.tick();

        this.prevRotation = this.rotation;
        this.rotation = rotation + 1 % 360.0F;
    }

    public abstract ResourceLocation getTexture();

    public abstract void addRadialButtons();
}
