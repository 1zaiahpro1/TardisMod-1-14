package net.tardis.mod.client.guis;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.gui.widget.Widget;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.ars.ARSPieceCategory;
import net.tardis.mod.client.guis.ars.ARSCategoryButton;
import net.tardis.mod.client.guis.ars.ARSPieceButton;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;

/** Seperate Screen class to split up functions and fix issue where resizing screen resets the GUI to the very beginning*/
public class ARSTabletCategoryScreen extends ARSTabletScreen{
	private static final int MAX_PER_PAGE = 6;
	private int categoryIndex = 0;
	private ARSPieceCategory category;
	
	public ARSTabletCategoryScreen() {
		super();
	}
	
	public ARSTabletCategoryScreen(ARSPieceCategory category) {
        this();
        this.category = category;
    }
	
	@Override
	protected void init() {
		this.addButtons(this.category);
	}
	
	public void addButtons(ARSPieceCategory category) {

		for(Widget w : this.buttons){
            w.active = w.visible = false;
            w.changeFocus(false);
        }
        this.buttons.clear();
        int x = width / 2 - 100, y = height / 2 - 20;
        
        this.addSubCategoriesAndOrPieces(category, x, y);

        int maxPageIndex = this.calculateMaxPageIndex(category);
        //Add default buttons like next, previous etc.
        this.addDefaultButtons(x, maxPageIndex);
	}

    @Override
	public void modIndex(int mod, int size) {
        if (this.categoryIndex + mod > size)
            this.categoryIndex = 0;
        else if (this.categoryIndex + mod < 0)
            this.categoryIndex = size;
        else this.categoryIndex += mod;

        this.init();
    }
    
    @Override
    protected void addDefaultButtons(int x, int maxIndex) {
        this.addButton(new TextButton(x, height / 2 + 45, TardisConstants.Translations.GUI_NEXT.getString(), but -> this.modIndex(1, maxIndex)));
        this.addButton(new TextButton(x, height / 2 + 55, TardisConstants.Translations.GUI_PREV.getString(), but -> this.modIndex(-1, maxIndex)));
    }

	/** Calculate the index number for our number of pages
     * <p> E.g. Max Index 2 = the pages consistent of {0, 1, 2} meaning 3 pages in total.*/
    private int calculateMaxPageIndex(ARSPieceCategory category) {
        int maxPageIndex = 0;
        
        if (!category.getChildren().isEmpty() && category.getPieces().isEmpty()) {
        	if (category.getChildren().size() % MAX_PER_PAGE == 0) {
            	maxPageIndex = category.getChildren().size() / MAX_PER_PAGE - 1;
            }
        	else {
        		maxPageIndex = category.getChildren().size() / MAX_PER_PAGE;
        	}
        }
        //If this category contains pieces, set the number of pages to account for piece size
        else if (!category.getPieces().isEmpty() && category.getChildren().isEmpty()) {
        	//If the number of pieces is divisible by our limit, minus one so we have exact number of pages
        	if (category.getPieces().size() % MAX_PER_PAGE == 0) {
        		maxPageIndex = category.getPieces().size() / MAX_PER_PAGE - 1;
        	}
        	else {
        		//If has any remainders, allow an extra page to be made to include the remaining pieces
        		maxPageIndex = category.getPieces().size() / MAX_PER_PAGE;
        	}
        }
        else { //If we have a mixture of both categories and pieces
        	int mergedNoPages = category.getChildren().size() + category.getPieces().size();
        	if (mergedNoPages % MAX_PER_PAGE == 0) {
        		maxPageIndex = mergedNoPages / MAX_PER_PAGE - 1;
        	}
        	else {
        		maxPageIndex = mergedNoPages / MAX_PER_PAGE;
        	}
        }
        return maxPageIndex;
    }

    /** Add either a mixture of both categories and ars piece buttons if we have both, or only one or the other
     * <p> Solves edge cases where users add a category within a subcategory
     * <br> E.g. Two pieces called room/copper/pool and room/lab buttons would reside in the same category screen. 
     * <br> We want to account for cases where there can be both categories and pieces*/
    private void addSubCategoriesAndOrPieces(ARSPieceCategory category, int x, int y) {
    	int startIndex = categoryIndex * MAX_PER_PAGE;
    	if (!category.getChildren().isEmpty() && category.getPieces().isEmpty()) {
    		this.addCategories(category, startIndex, x, y);
    	}
    	else if (!category.getPieces().isEmpty() && category.getChildren().isEmpty()) {
    		this.addPieces(category, startIndex, x, y);
    	}
    	else { //If there are both categories and pieces in here
    		this.mergeAndAddCategoriesAndPieces(category, startIndex, x, y);
    	}
        
    }
    
    private void addCategories(ARSPieceCategory category, int startIndex, int x, int y) {
    	int textSpacing = 0;
    	ArrayList<ARSPieceCategory> allCategories = Lists.newArrayList(category.getChildren());
        if (startIndex < allCategories.size()) {
        	int lastIndex = startIndex + MAX_PER_PAGE;
        	lastIndex = lastIndex >= allCategories.size() ? allCategories.size() : lastIndex;

            List<ARSPieceCategory> categoryPage = allCategories.subList(startIndex, lastIndex);
            for(ARSPieceCategory cat : categoryPage) {
                this.addButton(new ARSCategoryButton(x, y + (textSpacing * (this.font.FONT_HEIGHT + 2)), cat, this));
                ++textSpacing;
            }
        } else categoryIndex = 0; //If the index exceeds the total size, reset the page index to 0
    }
    
    private void addPieces(ARSPieceCategory category, int startIndex, int x, int y) {
    	int textSpacing = 0;
    	ArrayList<ARSPiece> all = Lists.newArrayList(category.getPieces());
    	all.sort((one, two) -> {
            return one.getRegistryName().getPath()
                    .compareToIgnoreCase(two.getRegistryName().getPath());
        });
        if (startIndex < all.size()) {
        	int lastIndex = startIndex + MAX_PER_PAGE;
        	lastIndex = lastIndex >= all.size() ? all.size() : lastIndex;

            List<ARSPiece> page = all.subList(startIndex, lastIndex);
            
        	for(ARSPiece piece : page){
        		if (piece != null) {
        			this.addButton(new ARSPieceButton(x, y + (textSpacing * (this.font.FONT_HEIGHT + 2)), piece));
        		}
                ++textSpacing;
            }
        } else categoryIndex = 0; //If the index exceeds the total size, reset the page index to 0
    }
    
    private void mergeAndAddCategoriesAndPieces(ARSPieceCategory category, int startIndex, int x, int y) {
    	int textSpacing = 0;
    	int totalSize = category.getChildren().size() + category.getPieces().size();
    	
    	if (startIndex < totalSize) {
    		int lastIndex = startIndex + MAX_PER_PAGE;
    		
    		ArrayList<ARSPiece> allPieces = Lists.newArrayList(category.getPieces());
    		ArrayList<ARSPieceCategory> allCategories = Lists.newArrayList(category.getChildren());
    		lastIndex = lastIndex >= allCategories.size() ? allCategories.size() : lastIndex;

    		List<ARSPieceCategory> subCategoryList = Lists.newArrayList();
    		if (startIndex < lastIndex) {
    			subCategoryList = allCategories.subList(startIndex, lastIndex);
    		}
    		
    		//Make the last index for pieces be limited to total page limit
    		int lastPieceIndex = (startIndex + MAX_PER_PAGE) - (startIndex >= MAX_PER_PAGE ? 0 : lastIndex);
    		lastPieceIndex = lastPieceIndex >= allPieces.size() ? allPieces.size() : lastPieceIndex;
    		List<ARSPiece> pieceList = Lists.newArrayList();
    		if (startIndex < lastPieceIndex) {
    			pieceList = allPieces.subList(startIndex, lastPieceIndex);
    		}

			//Add the category buttons
			for(ARSPieceCategory cat : subCategoryList) {
				this.addButton(new ARSCategoryButton(x, y + (textSpacing * (this.font.FONT_HEIGHT + 2)), cat, this));
                ++textSpacing;
            }
			
			//Add piece buttons
			for(ARSPiece piece : pieceList){
        		if (piece != null) {
        			this.addButton(new ARSPieceButton(x, y + (textSpacing * (this.font.FONT_HEIGHT + 2)), piece));
        		}
                ++textSpacing;
            }
    	}
    }
}