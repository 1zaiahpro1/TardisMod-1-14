package net.tardis.mod.client.guis.radial;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

/** IRadialAction implementation for displaying a string in the center of the button
 *  <br> Does not need OnlyIn Annotation, that annotation is only for Vanilla classes*/
public class StringRadialAction implements IRadialAction {

    private FontRenderer fr;

    public Runnable action;
    public ITextComponent text;

    public StringRadialAction(Runnable action, ITextComponent text){
        this.action = action;
        this.text = text;
        this.fr = Minecraft.getInstance().fontRenderer;
    }

    @Override
    public void render(MatrixStack matrix, int x, int y, float partialTicks){

        matrix.push();


        float scale = this.getScale();

        matrix.translate(x + 2, y + this.calcYOffset(), 0);
        matrix.scale(scale, scale, scale);
        this.fr.drawText(matrix, this.text, 0, 0, 0xFFFFFF);

        matrix.pop();
    }

    public float calcYOffset(){
        return 37.0F / 2.0F;
    }

    public float getScale(){
        return MathHelper.clamp(30.0F / (float)fr.getStringWidth(this.text.getString()), 0.0F, 1.0F);
    }

    @Override
    public void run() {
        this.action.run();
    }

    @Override
    public ITextComponent getTextComponent() {
        return text;
    }
}
