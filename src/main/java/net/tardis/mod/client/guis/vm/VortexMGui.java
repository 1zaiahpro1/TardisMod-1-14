package net.tardis.mod.client.guis.vm;


import org.lwjgl.glfw.GLFW;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.DisabledTextFieldWidget;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMFunctionMessage;
import net.tardis.mod.registries.VortexMFunctionCategories;
import net.tardis.mod.vm.AbstractVortexMFunction;
import net.tardis.mod.vm.VortexMFunctionCategory;

/**
 * Created by 50ap5ud5 12/09/2019
 * Standalone Vortex Manipulator Gui
 **/

public class VortexMGui extends Screen implements IVortexMScreen {

    public static final TranslationTextComponent title = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "base");
    public static final TranslationTextComponent locked = new TranslationTextComponent("message.tardis.vm.function_locked");
    private static final ResourceLocation BACKGROUND = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui.png");
    private final TranslationTextComponent selectFunc = new TranslationTextComponent("tooltip.gui.vm.select");
    private final TranslationTextComponent closeGui = new TranslationTextComponent("tooltip.gui.vm.close");
    private final TranslationTextComponent dec_func = new TranslationTextComponent("tooltip.gui.vm.dec_func");
    private final TranslationTextComponent inc_func = new TranslationTextComponent("tooltip.gui.vm.inc_func");
    private final TranslationTextComponent inc_subFunc = new TranslationTextComponent("tooltip.gui.vm.inc_subfunc");
    private final TranslationTextComponent dec_subFunc = new TranslationTextComponent("tooltip.gui.vm.dec_subfunc");
    private final TranslationTextComponent selected = new TranslationTextComponent("gui.vm.selected_func");
    private TextFieldWidget currentFunction;
    private ImageButton select;
    private ImageButton fnCycleLeft;
    private ImageButton fnCycleRight;
    private ImageButton catCycleUp;
    private ImageButton catCycleDown;
    private ImageButton closeBtn;
    private Minecraft mc = Minecraft.getInstance();
    private VortexMFunctionCategory category = VortexMFunctionCategories.TELEPORT.get();

    private int index = 0;
    private int subIndex = 0;

    public VortexMGui() {
        this(title);
    }

    public VortexMGui(ITextComponent title) {
        super(title);
    }

    @Override
    public void renderScreen(MatrixStack matrixStack) {
        this.renderBackground(matrixStack);
    }

    @Override
    public int getMinY() {
        return this.height / 2 + 74;
    }

    @Override
    public int getMinX() {
        return this.width / 2 - 68;
    }

    @Override
    public int getMaxX() {
        return this.getMinX() + 138;
    }

    @Override
    public int getMaxY() {
        return this.getMinY() - 146;
    }

    @Override
    public void renderBackground(MatrixStack matrixStack) {
        Minecraft.getInstance().getTextureManager().bindTexture(BACKGROUND);
        this.blit(matrixStack, this.width / 2 - this.texWidth() / 2, this.height / 2 - this.texHeight() / 2, 0, 0, this.texWidth(), this.texHeight());
        drawCenteredString(matrixStack, this.font, selected.getString(), this.getMinX() + 70, this.getMinY() - 37, 0xFFFFFF);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        if (this.select.isMouseOver(mouseX, mouseY)) {
            this.renderTooltip(matrixStack, selectFunc, mouseX, mouseY);
        }
        if (this.closeBtn.isMouseOver(mouseX, mouseY)) {
            this.renderTooltip(matrixStack, closeGui, mouseX, mouseY);
        }
        if (this.fnCycleLeft.isMouseOver(mouseX, mouseY)) {
            this.renderTooltip(matrixStack, dec_func, mouseX, mouseY);
        }
        if (this.fnCycleRight.isMouseOver(mouseX, mouseY)) {
            this.renderTooltip(matrixStack, inc_func, mouseX, mouseY);
        }
        if (this.catCycleUp.isMouseOver(mouseX, mouseY)) {
            this.renderTooltip(matrixStack, inc_subFunc, mouseX, mouseY);
        }
        if (this.catCycleDown.isMouseOver(mouseX, mouseY)) {
            this.renderTooltip(matrixStack, dec_subFunc, mouseX, mouseY);
        }
    }

    @Override
    public void init() {
        super.init();
        this.category = VortexMFunctionCategories.getCategoryFromIndex(index);
        currentFunction = new DisabledTextFieldWidget(this.font, this.getMinX() + 25, this.getMinY() - 25, 90, this.font.FONT_HEIGHT + 2, this.category.getValueFromIndex(this.subIndex).getDisplayName());
        //Left Function
        fnCycleLeft = addFunctionButton(this.getMinX() + 22, this.getMaxY() + 63, 77, -1);
        //Right Function
        fnCycleRight = addFunctionButton(this.getMinX() + 38, this.getMaxY() + 63, 88, 1);
        //Up Subfunction
        catCycleUp = addSubFunctionButton(this.getMinX() + 26, this.getMaxY() + 59, 42, 1);
        //Down SubFunction
        catCycleDown = addSubFunctionButton(this.getMinX() + 26, this.getMaxY() + 75, 57, -1);
        
        select = new ImageButton(this.getMinX() + 42, this.getMaxY() + 48, 14, 14, 14, 175, 16, BACKGROUND, but -> {
            AbstractVortexMFunction function = this.category.getValueFromIndex(this.subIndex);
            if (!function.stateComplete()) {
                currentFunction.setText(locked.getString());
            }
            else {
            	switch(function.getLogicalSide()) {
				case BOTH:
					function.sendActionOnClient(mc.world, mc.player);
            		Network.sendToServer(new VMFunctionMessage(function.getRegistryName(), function.getLogicalSide()));
					break;
				case CLIENT:
					function.sendActionOnClient(mc.world, mc.player);
					break;
				case SERVER:
					Network.sendToServer(new VMFunctionMessage(function.getRegistryName(), function.getLogicalSide()));
					break;
				default:
					Network.sendToServer(new VMFunctionMessage(function.getRegistryName(), function.getLogicalSide()));
					break;
            	}
            }
        });
        closeBtn = new ImageButton(this.getMinX() + 79, this.getMaxY() + 48, 14, 14, 14, 175, 16, BACKGROUND, but ->  {
             closeScreen();
        });


        this.buttons.clear();
        this.addButton(select);
        this.addButton(fnCycleLeft);
        this.addButton(fnCycleRight);
        this.addButton(catCycleUp);
        this.addButton(catCycleDown);
        this.addButton(currentFunction);
        this.addButton(closeBtn);
        currentFunction.setEnabled(false);
        currentFunction.setText(new StringTextComponent(this.category.getValueFromIndex(this.subIndex).getDisplayName().getString()).getString()); //Set default function name on init
        currentFunction.setDisabledTextColour(TextFormatting.BLUE.getColor());
        currentFunction.setCursorPositionZero();
    }

    @Override
    public void closeScreen() {
    	Minecraft.getInstance().displayGuiScreen((Screen)null);
        PlayerHelper.closeVMModel(this.minecraft.player); //set item model to close
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int bitmaskModifier) {
        final int rightKey = GLFW.GLFW_KEY_RIGHT;
        final int leftKey = GLFW.GLFW_KEY_LEFT;
        final int upKey = GLFW.GLFW_KEY_UP;
        final int downKey = GLFW.GLFW_KEY_DOWN;
        final int escKey = GLFW.GLFW_KEY_ESCAPE;
        final int enterKey = GLFW.GLFW_KEY_ENTER;

        switch (keyCode) {
            case rightKey: //Right arrow key
                modFunction(1);
                return true;
            case leftKey: //Left arrow key
                modFunction(-1);
                return true;
            case upKey: //Up arrow key
                modSubFunction(1);
                return true;
            case downKey: //Down arrow key
                modSubFunction(-1);
                return true;
            case escKey: //Close Button, for some reason this overrides ShouldCloseOnEsc()
                closeScreen();
                return true;
            case enterKey:
                select.onPress();
                return true;
            default:
                return super.keyPressed(keyCode, scanCode, bitmaskModifier);
        }
    }

    public void modFunction(int amount) {
        if (this.index + amount > VortexMFunctionCategories.FUNCTION_CATEGORY_REGISTRY.get().getEntries().size() - 1)
            index = 0;
        else if (this.index + amount < 0)
            this.index = VortexMFunctionCategories.FUNCTION_CATEGORY_REGISTRY.get().getEntries().size() - 1;
        else this.index += amount;
        this.category = VortexMFunctionCategories.getCategoryFromIndex(index);
        this.subIndex = 0;
        currentFunction.setText(category.getValueFromIndex(this.subIndex).getDisplayName().mergeStyle(TextFormatting.BLUE).getString());
        currentFunction.setCursorPositionZero();
        
    }

    public void modSubFunction(int amount) {
        
        if (this.subIndex + amount > this.category.getListSize() - 1)
            subIndex = 0;
        else if (this.subIndex + amount < 0) {
            this.subIndex = this.category.getListSize() - 1;
            if (this.category.getListSize() == 0)
                subIndex = 0;
        } else this.subIndex += amount;
        AbstractVortexMFunction function = this.category.getValueFromIndex(this.subIndex);
        currentFunction.setText(function.getDisplayName().mergeStyle(TextFormatting.BLUE).getString());
        currentFunction.setCursorPositionZero();
    }
    
    public ImageButton addFunctionButton(int x, int y, int uOffset, int modIndex) {
    	 return new ImageButton(x, y, 4, 12, uOffset, 175, 16, BACKGROUND, but -> {
    		 modFunction(modIndex);
    	 });
    }
    
    public ImageButton addSubFunctionButton(int x, int y, int uOffset, int modIndex) {
   	      return new ImageButton(x, y, 12, 4, uOffset, 175, 7, BACKGROUND, but -> {
   		     modSubFunction(modIndex);
   	     });
    }
    
    @Override
    public int texWidth() {
        return 138;
    }

    @Override
    public int texHeight() {
        return 146;
    }

}
