package net.tardis.mod.client.models.interiordoors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.entity.DoorRenderer;
import net.tardis.mod.client.renderers.exteriors.FortuneExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.tileentities.ConsoleTile;

public class FortuneInteriorModel extends AbstractInteriorDoorModel{
    private final ModelRenderer door;
    private final ModelRenderer latch;
    private final ModelRenderer frame;
    private final ModelRenderer latch2;
    private final ModelRenderer boti;

    public FortuneInteriorModel() {
        textureWidth = 512;
        textureHeight = 512;

        door = new ModelRenderer(this);
        door.setRotationPoint(-24.0F, 24.0F, 28.0F);
        door.setTextureOffset(253, 106).addBox(40.0F, -154.0F, 0.0F, 8.0F, 148.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(179, 110).addBox(2.0F, -151.6F, 2.0F, 44.0F, 144.0F, 1.0F, 0.0F, false);
        door.setTextureOffset(256, 108).addBox(0.0F, -154.0F, 0.0F, 8.0F, 148.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(224, 241).addBox(8.0F, -18.0F, 0.0F, 32.0F, 12.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(292, 125).addBox(8.0F, -154.0F, 0.0F, 32.0F, 12.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(139, 105).addBox(8.0F, -112.4F, 0.0F, 32.0F, 4.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(139, 105).addBox(8.0F, -54.0F, 0.0F, 32.0F, 4.0F, 4.0F, 0.0F, false);

        latch = new ModelRenderer(this);
        latch.setRotationPoint(0.0F, 0.0F, 0.0F);
        door.addChild(latch);
        latch.setTextureOffset(166, 8).addBox(42.0F, -88.0F, -2.0F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        latch.setTextureOffset(110, 16).addBox(42.0F, -78.0F, 1.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        frame = new ModelRenderer(this);
        frame.setRotationPoint(-24.0F, 24.0F, 21.0F);
        frame.setTextureOffset(256, 108).addBox(-4.0F, -154.0F, 4.75F, 4.0F, 148.0F, 6.0F, 0.0F, false);
        frame.setTextureOffset(256, 108).addBox(48.0F, -154.0F, 4.75F, 4.0F, 148.0F, 6.0F, 0.0F, false);
        frame.setTextureOffset(224, 241).addBox(-2.0F, -8.0F, 3.0F, 52.0F, 2.0F, 4.0F, 0.0F, false);
        frame.setTextureOffset(339, 131).addBox(-4.0F, -158.25F, 4.5F, 56.0F, 4.0F, 6.0F, 0.0F, false);

        latch2 = new ModelRenderer(this);
        latch2.setRotationPoint(0.0F, 0.0F, 0.0F);
        frame.addChild(latch2);
        

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 22.75F, 7.0F);
        boti.setTextureOffset(1, 97).addBox(-24.0F, -153.0F, 25.0F, 48.0F, 149.0F, 4.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        matrixStack.push();
        matrixStack.translate(0, 1.2, -0.05);
        matrixStack.rotate(Vector3f.YP.rotationDegrees(180));
        matrixStack.scale(0.25F, 0.25F, 0.25F);
        
        this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.FORTUNE.getRotationForState(door.getOpenState()));
        this.renderDoorWhenClosed(door, matrixStack, buffer, packedLight, packedOverlay, this.door);
        frame.render(matrixStack, buffer, packedLight, packedOverlay);
        //boti.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
    }

    @Override
    public void renderBoti(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        if(Minecraft.getInstance().world != null && door.getOpenState() != EnumDoorState.CLOSED){ 
            Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
                
                PortalInfo info = new PortalInfo();
                
                info.setPosition(door.getPositionVec());
                info.setWorldShell(data.getBotiWorld());
                
                //translate
                info.setTranslate(matrix -> {
                    DoorRenderer.applyTranslations(matrix, door.rotationYaw - 180, door.getHorizontalFacing());
                    matrixStack.translate(0, 1.2, -0.05);
                });
                
                info.setTranslatePortal(matrix -> {
                    matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                    matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(data.getBotiWorld().getPortalDirection())));
                    matrix.translate(-0.5, -0.25, -0.5);
                });
                
                //Renderer
                info.setRenderDoor((matrix, buf) -> {
                    matrix.push();
                    matrix.translate(0, 0, -0.05);
                    matrix.rotate(Vector3f.YP.rotationDegrees(180));
                    matrix.scale(0.25F, 0.25F, 0.25F);
                    this.door.render(matrix, buf.getBuffer(RenderType.getEntityCutout(getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });
                info.setRenderPortal((matrix, buf) -> {
                    matrix.push();
                    matrix.rotate(Vector3f.YP.rotationDegrees(180));
                    matrix.scale(0.25F, 0.25F, 0.25F);
    
                    this.boti.render(matrix, buf.getBuffer(RenderType.getEntityCutout(getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });
                
                BOTIRenderer.addPortal(info);
                
            });
        }
    }

    @Override
    public ResourceLocation getTexture() {
        ConsoleTile tile = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
        if (tile != null) {
            int index = tile.getExteriorManager().getExteriorVariant();
            TexVariant[] vars = tile.getExteriorType().getVariants();
            if (vars != null && index < vars.length)
                return vars[index].getTexture();
        }
        return FortuneExteriorRenderer.TEXTURE;
    }

    @Override
    public boolean doesDoorOpenIntoBotiWindow() {
        return true;
    }
    
    
}