package net.tardis.mod.client.models;

import com.mojang.blaze3d.matrix.MatrixStack;
/** If a Console Model has a Monitor Control*/
public interface IHaveMonitor {
    /** Translates the model renderers to the matrix stack used by the tile renderer*/
	public void translateMonitorPos(MatrixStack stack);
}
