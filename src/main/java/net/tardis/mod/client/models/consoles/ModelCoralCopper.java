//package net.tardis.mod.client.models.consoles;// Made with Blockbench
//// Paste this code into your mod.
//// Make sure to generate all required imports
//
//import net.minecraft.client.Minecraft;
//import net.minecraft.client.renderer.model.Model;
//import net.minecraft.client.renderer.model.ModelBox;
//import net.minecraft.client.renderer.model.ModelRenderer;
//import net.tardis.mod.controls.HandbrakeControl;
//import net.tardis.mod.controls.IncModControl;
//import net.tardis.mod.controls.RandomiserControl;
//import net.tardis.mod.controls.ThrottleControl;
//import net.tardis.mod.tileentities.ConsoleTile;
//
//public class ModelCoralCopper extends Model implements IConsoleModel {
//    private final ModelRenderer all;
//    private final ModelRenderer Panelcomponents;
//    private final ModelRenderer NorthWest;
//    private final ModelRenderer Monitor2;
//    private final ModelRenderer screen2;
//    private final ModelRenderer Fuellink4;
//    private final ModelRenderer bone;
//    private final ModelRenderer bone8;
//    private final ModelRenderer randomiser;
//    private final ModelRenderer Smallbuttons;
//    private final ModelRenderer FastReturn;
//    private final ModelRenderer NorthEast;
//    private final ModelRenderer stablizer2;
//    private final ModelRenderer keyboard3;
//    private final ModelRenderer West;
//    private final ModelRenderer landingtype2;
//    private final ModelRenderer Dimensioncontrol2;
//    private final ModelRenderer Fuellink2;
//    private final ModelRenderer SouthWest;
//    private final ModelRenderer handbreak2;
//    private final ModelRenderer bone10;
//    private final ModelRenderer Lever2;
//    private final ModelRenderer Lever;
//    private final ModelRenderer Incrementincrease;
//    private final ModelRenderer xyzcontrols2;
//    private final ModelRenderer xcontrol2;
//    private final ModelRenderer ycontrol2;
//    private final ModelRenderer zcontrol2;
//    private final ModelRenderer sonicport2;
//    private final ModelRenderer SouthEast;
//    private final ModelRenderer misc;
//    private final ModelRenderer Telepathiccircuit2;
//    private final ModelRenderer circlesection19;
//    private final ModelRenderer bone13;
//    private final ModelRenderer East;
//    private final ModelRenderer Fuellink3;
//    private final ModelRenderer Refueler2;
//    private final ModelRenderer Refueler3;
//    private final ModelRenderer throttle3;
//    private final ModelRenderer bone11;
//    private final ModelRenderer throtlelever2;
//    private final ModelRenderer Panels;
//    private final ModelRenderer North_panel;
//    private final ModelRenderer circlesection73;
//    private final ModelRenderer circlesection74;
//    private final ModelRenderer circlesection75;
//    private final ModelRenderer North_panel2;
//    private final ModelRenderer circlesection2;
//    private final ModelRenderer circlesection3;
//    private final ModelRenderer circlesection4;
//    private final ModelRenderer North_panel3;
//    private final ModelRenderer circlesection5;
//    private final ModelRenderer circlesection6;
//    private final ModelRenderer circlesection7;
//    private final ModelRenderer North_panel4;
//    private final ModelRenderer circlesection8;
//    private final ModelRenderer circlesection9;
//    private final ModelRenderer circlesection10;
//    private final ModelRenderer North_panel5;
//    private final ModelRenderer circlesection11;
//    private final ModelRenderer circlesection12;
//    private final ModelRenderer circlesection25;
//    private final ModelRenderer North_panel6;
//    private final ModelRenderer circlesection26;
//    private final ModelRenderer circlesection27;
//    private final ModelRenderer circlesection28;
//    private final ModelRenderer underpanels;
//    private final ModelRenderer bone2;
//    private final ModelRenderer bone3;
//    private final ModelRenderer bone4;
//    private final ModelRenderer bone5;
//    private final ModelRenderer bone6;
//    private final ModelRenderer bone7;
//    private final ModelRenderer bone12;
//    private final ModelRenderer edges;
//    private final ModelRenderer North_leg6;
//    private final ModelRenderer circlesection144;
//    private final ModelRenderer circlesection145;
//    private final ModelRenderer circlesection146;
//    private final ModelRenderer circlesection147;
//    private final ModelRenderer circlesection148;
//    private final ModelRenderer circlesection149;
//    private final ModelRenderer circlesection150;
//    private final ModelRenderer North_leg2;
//    private final ModelRenderer circlesection29;
//    private final ModelRenderer circlesection30;
//    private final ModelRenderer circlesection31;
//    private final ModelRenderer circlesection32;
//    private final ModelRenderer circlesection33;
//    private final ModelRenderer circlesection34;
//    private final ModelRenderer circlesection35;
//    private final ModelRenderer North_leg3;
//    private final ModelRenderer circlesection36;
//    private final ModelRenderer circlesection37;
//    private final ModelRenderer circlesection38;
//    private final ModelRenderer circlesection39;
//    private final ModelRenderer circlesection40;
//    private final ModelRenderer circlesection41;
//    private final ModelRenderer circlesection42;
//    private final ModelRenderer North_leg4;
//    private final ModelRenderer circlesection43;
//    private final ModelRenderer circlesection44;
//    private final ModelRenderer circlesection45;
//    private final ModelRenderer circlesection46;
//    private final ModelRenderer circlesection47;
//    private final ModelRenderer circlesection48;
//    private final ModelRenderer circlesection49;
//    private final ModelRenderer North_leg5;
//    private final ModelRenderer circlesection50;
//    private final ModelRenderer circlesection51;
//    private final ModelRenderer circlesection52;
//    private final ModelRenderer circlesection53;
//    private final ModelRenderer circlesection54;
//    private final ModelRenderer circlesection55;
//    private final ModelRenderer circlesection56;
//    private final ModelRenderer North_leg7;
//    private final ModelRenderer circlesection57;
//    private final ModelRenderer circlesection58;
//    private final ModelRenderer circlesection59;
//    private final ModelRenderer circlesection60;
//    private final ModelRenderer circlesection61;
//    private final ModelRenderer circlesection62;
//    private final ModelRenderer circlesection63;
//    private final ModelRenderer Centre;
//    private final ModelRenderer circlesection13;
//    private final ModelRenderer circlesection14;
//    private final ModelRenderer circlesection15;
//    private final ModelRenderer circlesection16;
//    private final ModelRenderer circlesection17;
//    private final ModelRenderer circlesection18;
//    private final ModelRenderer TRANSPARENT_BEAMS;
//    private final ModelRenderer circlesection20;
//    private final ModelRenderer circlesection21;
//    private final ModelRenderer circlesection22;
//    private final ModelRenderer circlesection23;
//    private final ModelRenderer circlesection24;
//    private final ModelRenderer Rotar;
//    private final ModelRenderer rotarupper;
//    private final ModelRenderer Rotarlower;
//
//    public ModelCoralCopper() {
//        textureWidth = 164;
//        textureHeight = 164;
//
//        all = new ModelRenderer(this);
//        all.setRotationPoint(0.0F, 24.0F, 0.0F);
//
//        Panelcomponents = new ModelRenderer(this);
//        Panelcomponents.setRotationPoint(0.0F, 0.0F, 60.0F);
//        all.addChild(Panelcomponents);
//
//        NorthWest = new ModelRenderer(this);
//        NorthWest.setRotationPoint(0.0F, 0.0F, -60.0F);
//        setRotationAngle(NorthWest, 0.0F, -0.5236F, 0.0F);
//        Panelcomponents.addChild(NorthWest);
//
//        Monitor2 = new ModelRenderer(this);
//        Monitor2.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(Monitor2, 0.0F, 0.5236F, 0.0F);
//        NorthWest.addChild(Monitor2);
//        Monitor2.cubeList.add(new ModelBox(Monitor2, 3, 34, -1.0F, -37.75F, -20.2F, 2, 3, 12, 0.0F, false));
//        Monitor2.cubeList.add(new ModelBox(Monitor2, 3, 34, -1.0F, -42.0F, -18.95F, 2, 3, 11, 0.0F, false));
//
//        screen2 = new ModelRenderer(this);
//        screen2.setRotationPoint(-48.0F, -35.6F, -18.3F);
//        setRotationAngle(screen2, -0.384F, 0.0F, 0.0F);
//        Monitor2.addChild(screen2);
//        screen2.cubeList.add(new ModelBox(screen2, 24, 135, 40.4F, -9.7364F, -2.7127F, 15, 13, 1, 0.0F, false));
//        screen2.cubeList.add(new ModelBox(screen2, 3, 31, 41.4F, -9.2199F, -3.3121F, 13, 10, 1, 0.0F, false));
//        screen2.cubeList.add(new ModelBox(screen2, 24, 135, 40.4F, 1.2636F, -4.7127F, 15, 2, 2, 0.0F, false));
//        screen2.cubeList.add(new ModelBox(screen2, 24, 135, 40.4F, -9.7364F, -1.7127F, 15, 2, 3, 0.0F, false));
//
//        Fuellink4 = new ModelRenderer(this);
//        Fuellink4.setRotationPoint(-9.9192F, -58.5736F, -32.7617F);
//        setRotationAngle(Fuellink4, 0.6109F, 0.0F, 0.0F);
//        NorthWest.addChild(Fuellink4);
//        Fuellink4.cubeList.add(new ModelBox(Fuellink4, 3, 32, 5.3593F, 30.8857F, -2.8823F, 3, 2, 3, 0.0F, true));
//        Fuellink4.cubeList.add(new ModelBox(Fuellink4, 3, 32, 7.9163F, 31.384F, -1.736F, 8, 2, 1, 0.0F, true));
//        Fuellink4.cubeList.add(new ModelBox(Fuellink4, 3, 32, 5.9043F, 31.3501F, -6.7293F, 2, 2, 4, 0.0F, true));
//        Fuellink4.cubeList.add(new ModelBox(Fuellink4, 3, 32, 1.9311F, 31.3509F, -6.7532F, 4, 2, 2, 0.0F, true));
//
//        bone = new ModelRenderer(this);
//        bone.setRotationPoint(0.0F, -27.5F, -31.0F);
//        setRotationAngle(bone, 0.8727F, 0.0F, 0.0F);
//        NorthWest.addChild(bone);
//        bone.cubeList.add(new ModelBox(bone, 46, 150, -7.0F, 6.75F, 2.0F, 2, 2, 4, 0.0F, false));
//        bone.cubeList.add(new ModelBox(bone, 46, 150, -4.0F, 6.75F, 2.0F, 2, 2, 4, 0.0F, false));
//        bone.cubeList.add(new ModelBox(bone, 46, 150, 1.0F, 6.5F, 3.0F, 4, 2, 2, 0.0F, false));
//
//        bone8 = new ModelRenderer(this);
//        bone8.setRotationPoint(0.0F, 0.0F, -2.0F);
//        NorthWest.addChild(bone8);
//        bone8.cubeList.add(new ModelBox(bone8, 4, 110, 1.0F, -31.0F, -19.5F, 4, 4, 4, 0.0F, false));
//        bone8.cubeList.add(new ModelBox(bone8, 4, 107, 5.0F, -31.0F, -19.0F, 1, 4, 3, 0.0F, false));
//        bone8.cubeList.add(new ModelBox(bone8, 4, 107, 0.0F, -31.0F, -19.0F, 1, 4, 3, 0.0F, false));
//
//        randomiser = new ModelRenderer(this);
//        randomiser.setRotationPoint(3.0F, -33.0F, -17.5F);
//        setRotationAngle(randomiser, 0.0F, 0.8727F, 0.0F);
//        bone8.addChild(randomiser);
//        randomiser.cubeList.add(new ModelBox(randomiser, 73, 67, -2.0F, -2.0F, -2.0F, 4, 4, 4, 0.0F, false));
//
//        Smallbuttons = new ModelRenderer(this);
//        Smallbuttons.setRotationPoint(1.9971F, -26.1896F, -33.9977F);
//        setRotationAngle(Smallbuttons, 0.4363F, 0.0F, 0.0F);
//        NorthWest.addChild(Smallbuttons);
//        Smallbuttons.cubeList.add(new ModelBox(Smallbuttons, 4, 109, -3.75F, 2.0F, 22.5F, 2, 2, 1, 0.0F, true));
//        Smallbuttons.cubeList.add(new ModelBox(Smallbuttons, 4, 109, -1.25F, 1.75F, 21.5F, 2, 2, 1, 0.0F, true));
//
//        FastReturn = new ModelRenderer(this);
//        FastReturn.setRotationPoint(-8.9843F, -16.3682F, -25.7158F);
//        setRotationAngle(FastReturn, 0.733F, 0.2094F, -0.0349F);
//        NorthWest.addChild(FastReturn);
//        FastReturn.cubeList.add(new ModelBox(FastReturn, 52, 34, -1.4274F, -6.5624F, -1.6664F, 3, 19, 3, 0.0F, false));
//        FastReturn.cubeList.add(new ModelBox(FastReturn, 42, 136, -1.0933F, -7.8927F, -0.8955F, 2, 3, 2, 0.0F, false));
//        FastReturn.cubeList.add(new ModelBox(FastReturn, 37, 40, -1.0933F, -8.8927F, -0.8955F, 2, 1, 2, 0.0F, false));
//
//        NorthEast = new ModelRenderer(this);
//        NorthEast.setRotationPoint(0.0F, 0.0F, -60.0F);
//        Panelcomponents.addChild(NorthEast);
//
//        stablizer2 = new ModelRenderer(this);
//        stablizer2.setRotationPoint(-11.9804F, -26.7604F, -20.4705F);
//        setRotationAngle(stablizer2, 0.925F, 0.5236F, 0.0F);
//        NorthEast.addChild(stablizer2);
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 72, 91, -6.655F, -0.601F, 0.1548F, 3, 1, 3, 0.0F, false));
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 72, 109, -6.6773F, -0.8533F, -3.0371F, 3, 1, 3, 0.0F, false));
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 28, 42, -3.3301F, -0.6874F, 0.0985F, 3, 1, 3, 0.0F, false));
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 28, 42, 0.1679F, -0.7075F, 0.0432F, 3, 1, 3, 0.0F, false));
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 28, 42, 3.5294F, -0.7151F, 0.0223F, 3, 1, 3, 0.0F, false));
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 28, 42, -3.3292F, -0.5025F, -3.0584F, 3, 1, 3, 0.0F, false));
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 28, 42, 0.1689F, -0.5226F, -3.1137F, 3, 1, 3, 0.0F, false));
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 28, 42, 3.5304F, -0.5302F, -3.1346F, 3, 1, 3, 0.0F, false));
//        stablizer2.cubeList.add(new ModelBox(stablizer2, 28, 42, -6.7818F, 0.2084F, -3.2826F, 14, 2, 7, 0.0F, false));
//
//        keyboard3 = new ModelRenderer(this);
//        keyboard3.setRotationPoint(-74.7564F, -29.6007F, -14.0656F);
//        setRotationAngle(keyboard3, -0.5411F, -2.618F, 0.0F);
//        NorthEast.addChild(keyboard3);
//        keyboard3.cubeList.add(new ModelBox(keyboard3, 6, 112, -61.1522F, 14.8898F, -31.8876F, 7, 5, 5, 0.0F, false));
//        keyboard3.cubeList.add(new ModelBox(keyboard3, 6, 112, -61.1522F, 13.8898F, -32.8876F, 7, 1, 5, 0.0F, false));
//
//        West = new ModelRenderer(this);
//        West.setRotationPoint(0.0F, 0.0F, -51.0F);
//        Panelcomponents.addChild(West);
//
//        landingtype2 = new ModelRenderer(this);
//        landingtype2.setRotationPoint(14.8852F, -34.0749F, -17.5239F);
//        setRotationAngle(landingtype2, 0.3054F, 0.4363F, 0.6109F);
//        West.addChild(landingtype2);
//        landingtype2.cubeList.add(new ModelBox(landingtype2, 25, 134, -5.5007F, -0.7341F, -2.051F, 11, 4, 4, 0.0F, false));
//        landingtype2.cubeList.add(new ModelBox(landingtype2, 17, 38, -2.9993F, -1.7659F, -0.949F, 6, 1, 2, 0.0F, false));
//
//        Dimensioncontrol2 = new ModelRenderer(this);
//        Dimensioncontrol2.setRotationPoint(-45.8F, -26.6F, -10.4F);
//        setRotationAngle(Dimensioncontrol2, 0.0F, 0.0F, 0.925F);
//        West.addChild(Dimensioncontrol2);
//        Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 15, 133, 38.2012F, -55.3489F, -5.7F, 6, 2, 14, 0.0F, false));
//        Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 2, 111, 39.0831F, -56.1823F, -4.2F, 4, 2, 11, 0.0F, false));
//
//        Fuellink2 = new ModelRenderer(this);
//        Fuellink2.setRotationPoint(-50.2211F, -29.2899F, -10.5214F);
//        setRotationAngle(Fuellink2, 1.1694F, -1.5533F, 0.8203F);
//        West.addChild(Fuellink2);
//        Fuellink2.cubeList.add(new ModelBox(Fuellink2, 4, 107, 1.4776F, -54.703F, 26.4028F, 2, 1, 3, 0.0F, false));
//        Fuellink2.cubeList.add(new ModelBox(Fuellink2, 4, 107, -1.0886F, -58.7569F, 27.4292F, 2, 1, 3, 0.0F, false));
//        Fuellink2.cubeList.add(new ModelBox(Fuellink2, 6, 39, -5.2816F, -63.0962F, 25.0764F, 8, 2, 4, 0.0F, false));
//        Fuellink2.cubeList.add(new ModelBox(Fuellink2, 6, 39, 2.7184F, -63.0962F, 25.0764F, 2, 7, 4, 0.0F, false));
//        Fuellink2.cubeList.add(new ModelBox(Fuellink2, 6, 39, 4.7184F, -58.0962F, 25.0764F, 4, 2, 4, 0.0F, false));
//
//        SouthWest = new ModelRenderer(this);
//        SouthWest.setRotationPoint(17.0F, 0.0F, -73.0F);
//        setRotationAngle(SouthWest, 0.0F, -1.0472F, 0.0F);
//        Panelcomponents.addChild(SouthWest);
//
//        handbreak2 = new ModelRenderer(this);
//        handbreak2.setRotationPoint(3.867F, -29.4056F, 12.7515F);
//        setRotationAngle(handbreak2, 0.0F, 0.5236F, 0.0F);
//        SouthWest.addChild(handbreak2);
//
//        bone10 = new ModelRenderer(this);
//        bone10.setRotationPoint(-1.0F, 1.25F, 8.0F);
//        setRotationAngle(bone10, 0.0F, 0.0F, 0.5236F);
//        handbreak2.addChild(bone10);
//        bone10.cubeList.add(new ModelBox(bone10, 0, 64, 8.2F, -12.8428F, -3.7148F, 9, 2, 3, 0.0F, false));
//        bone10.cubeList.add(new ModelBox(bone10, 0, 64, 7.7673F, -11.1973F, -4.4589F, 9, 3, 7, 0.0F, false));
//        bone10.cubeList.add(new ModelBox(bone10, 66, 40, 11.2068F, -13.6756F, 1.4449F, 3, 3, 2, 0.0F, false));
//        bone10.cubeList.add(new ModelBox(bone10, 0, 64, 7.7673F, -11.1973F, -4.7089F, 9, 3, 7, 0.0F, false));
//        bone10.cubeList.add(new ModelBox(bone10, 0, 64, 8.1595F, -12.804F, -0.3077F, 9, 2, 2, 0.0F, false));
//
//        Lever2 = new ModelRenderer(this);
//        Lever2.setRotationPoint(13.0234F, -9.8006F, -3.5079F);
//        setRotationAngle(Lever2, 0.0F, 0.0F, -0.192F);
//        bone10.addChild(Lever2);
//        Lever2.cubeList.add(new ModelBox(Lever2, 3, 134, -2.1928F, -5.983F, -0.5282F, 2, 6, 1, 0.0F, false));
//        Lever2.cubeList.add(new ModelBox(Lever2, 4, 109, -2.1928F, -7.983F, -0.5282F, 2, 2, 3, 0.0F, false));
//
//        Lever = new ModelRenderer(this);
//        Lever.setRotationPoint(14.5138F, -10.745F, -0.6015F);
//        setRotationAngle(Lever, 0.0F, 0.0F, 0.576F);
//        bone10.addChild(Lever);
//        Lever.cubeList.add(new ModelBox(Lever, 49, 134, -2.1928F, -5.983F, -0.5282F, 2, 6, 1, 0.0F, false));
//        Lever.cubeList.add(new ModelBox(Lever, 4, 109, -2.1928F, -7.983F, -0.5282F, 2, 2, 3, 0.0F, false));
//
//        Incrementincrease = new ModelRenderer(this);
//        Incrementincrease.setRotationPoint(12.6117F, -12.5321F, 3.7493F);
//        setRotationAngle(Incrementincrease, -0.0175F, -0.0175F, 1.6581F);
//        bone10.addChild(Incrementincrease);
//        Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 48, 133, 1.4296F, -2.3298F, -0.7877F, 2, 5, 1, 0.0F, false));
//        Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 48, 133, -3.5704F, -2.3298F, -0.7877F, 2, 5, 1, 0.0F, false));
//        Incrementincrease.cubeList.add(new ModelBox(Incrementincrease, 46, 134, -2.0704F, -3.3298F, -0.7627F, 4, 7, 1, 0.0F, false));
//
//        xyzcontrols2 = new ModelRenderer(this);
//        xyzcontrols2.setRotationPoint(7.3422F, -30.627F, 0.3147F);
//        setRotationAngle(xyzcontrols2, -0.9599F, -1.5708F, 0.0F);
//        SouthWest.addChild(xyzcontrols2);
//        xyzcontrols2.cubeList.add(new ModelBox(xyzcontrols2, 7, 111, 19.3573F, 5.8138F, -7.8271F, 2, 2, 3, 0.0F, false));
//        xyzcontrols2.cubeList.add(new ModelBox(xyzcontrols2, 7, 111, 17.291F, 10.3677F, -7.8535F, 2, 1, 3, 0.0F, false));
//        xyzcontrols2.cubeList.add(new ModelBox(xyzcontrols2, 3, 42, 10.089F, 14.4934F, -7.4735F, 10, 2, 4, 0.0F, false));
//        xyzcontrols2.cubeList.add(new ModelBox(xyzcontrols2, 3, 42, 20.089F, 9.4934F, -7.4735F, 2, 7, 4, 0.0F, false));
//        xyzcontrols2.cubeList.add(new ModelBox(xyzcontrols2, 3, 42, 22.089F, 9.4934F, -7.4735F, 6, 2, 4, 0.0F, false));
//
//        xcontrol2 = new ModelRenderer(this);
//        xcontrol2.setRotationPoint(19.9826F, 17.7276F, -7.048F);
//        xyzcontrols2.addChild(xcontrol2);
//        xcontrol2.cubeList.add(new ModelBox(xcontrol2, 16, 109, 2.6945F, -4.9002F, -0.9235F, 4, 2, 2, 0.0F, false));
//
//        ycontrol2 = new ModelRenderer(this);
//        ycontrol2.setRotationPoint(24.6766F, 16.5162F, -7.1302F);
//        setRotationAngle(ycontrol2, 1.7453F, 0.0F, 0.0F);
//        xyzcontrols2.addChild(ycontrol2);
//        ycontrol2.cubeList.add(new ModelBox(ycontrol2, 13, 110, -2.0F, -0.75F, -1.0F, 4, 2, 2, 0.0F, false));
//
//        zcontrol2 = new ModelRenderer(this);
//        zcontrol2.setRotationPoint(19.8578F, 16.827F, -8.0147F);
//        setRotationAngle(zcontrol2, 0.4363F, 0.0F, 0.0F);
//        xyzcontrols2.addChild(zcontrol2);
//        zcontrol2.cubeList.add(new ModelBox(zcontrol2, 10, 107, 2.7033F, 1.626F, 0.2121F, 4, 2, 2, 0.0F, false));
//
//        sonicport2 = new ModelRenderer(this);
//        sonicport2.setRotationPoint(10.7015F, 17.7016F, -8.6457F);
//        setRotationAngle(sonicport2, 0.4363F, 0.0F, 0.0F);
//        xyzcontrols2.addChild(sonicport2);
//        sonicport2.cubeList.add(new ModelBox(sonicport2, 2, 63, 3.2033F, -0.074F, 0.2121F, 4, 4, 3, 0.0F, false));
//        sonicport2.cubeList.add(new ModelBox(sonicport2, 40, 33, 4.2033F, 0.926F, -0.7879F, 2, 2, 2, 0.0F, false));
//
//        SouthEast = new ModelRenderer(this);
//        SouthEast.setRotationPoint(0.0F, 0.0F, -60.0F);
//        setRotationAngle(SouthEast, 0.0F, -0.5236F, 0.0F);
//        Panelcomponents.addChild(SouthEast);
//
//        misc = new ModelRenderer(this);
//        misc.setRotationPoint(-0.2316F, -27.4338F, 19.8459F);
//        setRotationAngle(misc, -0.6109F, 0.0F, 0.0F);
//        SouthEast.addChild(misc);
//        misc.cubeList.add(new ModelBox(misc, 7, 109, -2.7486F, -1.6132F, 3.9357F, 6, 2, 2, 0.0F, false));
//        misc.cubeList.add(new ModelBox(misc, 7, 109, -2.7486F, -1.4213F, -10.3126F, 6, 2, 2, 0.0F, false));
//        misc.cubeList.add(new ModelBox(misc, 7, 109, 6.5014F, -1.5783F, 0.436F, 2, 2, 3, 0.0F, false));
//        misc.cubeList.add(new ModelBox(misc, 7, 109, -7.9986F, -1.5783F, 0.436F, 2, 2, 3, 0.0F, false));
//
//        Telepathiccircuit2 = new ModelRenderer(this);
//        Telepathiccircuit2.setRotationPoint(0.0043F, -30.4F, 21.425F);
//        setRotationAngle(Telepathiccircuit2, -0.5934F, 0.0F, 0.0F);
//        SouthEast.addChild(Telepathiccircuit2);
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 16, 110, -3.4845F, 1.7891F, -7.1871F, 7, 2, 2, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 22, 110, -1.9055F, 1.8299F, -4.0008F, 4, 2, 4, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 37, 39, -1.4055F, 1.0799F, -3.5008F, 3, 2, 3, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 23, 37, -4.2345F, 1.2891F, 0.8129F, 2, 2, 2, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 23, 37, -4.2345F, 1.2891F, -6.1871F, 2, 2, 2, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 23, 37, 2.5155F, 1.2891F, 0.8129F, 2, 2, 2, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 23, 37, 2.5155F, 1.2891F, -6.1871F, 2, 2, 2, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 16, 110, -5.2345F, 1.7891F, -5.1871F, 2, 2, 7, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 16, 110, 3.2655F, 1.7891F, -5.1871F, 2, 2, 7, 0.0F, false));
//        Telepathiccircuit2.cubeList.add(new ModelBox(Telepathiccircuit2, 16, 110, -3.4845F, 1.7891F, 1.8129F, 7, 2, 2, 0.0F, false));
//
//        circlesection19 = new ModelRenderer(this);
//        circlesection19.setRotationPoint(-13.0F, -14.0F, -44.0F);
//        setRotationAngle(circlesection19, 0.0F, 0.5236F, 0.0F);
//        SouthEast.addChild(circlesection19);
//
//        bone13 = new ModelRenderer(this);
//        bone13.setRotationPoint(-2.0F, 4.1495F, 5.6872F);
//        setRotationAngle(bone13, -0.5236F, 0.0F, 0.0F);
//        circlesection19.addChild(bone13);
//        bone13.cubeList.add(new ModelBox(bone13, 27, 133, -10.7589F, -51.7549F, 33.1492F, 4, 3, 10, 0.0F, false));
//        bone13.cubeList.add(new ModelBox(bone13, 5, 50, -10.7589F, -51.7549F, 43.1492F, 4, 3, 3, 0.0F, false));
//        bone13.cubeList.add(new ModelBox(bone13, 7, 45, -10.7589F, -51.7549F, 30.1492F, 4, 3, 3, 0.0F, false));
//        bone13.cubeList.add(new ModelBox(bone13, 3, 63, -11.7904F, -48.9445F, 33.2991F, 6, 4, 12, 0.0F, false));
//
//        East = new ModelRenderer(this);
//        East.setRotationPoint(0.0F, 0.0F, -60.0F);
//        Panelcomponents.addChild(East);
//
//        Fuellink3 = new ModelRenderer(this);
//        Fuellink3.setRotationPoint(-81.0562F, -31.2285F, -1.8F);
//        setRotationAngle(Fuellink3, 1.5708F, -0.7854F, -0.5411F);
//        East.addChild(Fuellink3);
//        Fuellink3.cubeList.add(new ModelBox(Fuellink3, 12, 38, 40.1575F, -39.0297F, -34.5082F, 4, 4, 2, 0.0F, false));
//        Fuellink3.cubeList.add(new ModelBox(Fuellink3, 12, 38, 34.8142F, -38.212F, -35.1571F, 6, 2, 2, 0.0F, false));
//        Fuellink3.cubeList.add(new ModelBox(Fuellink3, 12, 38, 40.5763F, -35.4686F, -35.1233F, 2, 4, 2, 0.0F, false));
//        Fuellink3.cubeList.add(new ModelBox(Fuellink3, 12, 38, 42.5763F, -33.4686F, -35.1233F, 2, 2, 2, 0.0F, false));
//        Fuellink3.cubeList.add(new ModelBox(Fuellink3, 12, 38, 34.8142F, -43.212F, -35.1571F, 2, 5, 2, 0.0F, false));
//
//        Refueler2 = new ModelRenderer(this);
//        Refueler2.setRotationPoint(-22.5904F, -25.7132F, 3.45F);
//        setRotationAngle(Refueler2, 1.5708F, 0.0F, -0.9599F);
//        East.addChild(Refueler2);
//        Refueler2.cubeList.add(new ModelBox(Refueler2, 5, 66, -1.9835F, -1.0F, -0.7209F, 4, 4, 2, 0.0F, false));
//        Refueler2.cubeList.add(new ModelBox(Refueler2, 74, 118, -1.6296F, -0.5F, -0.1463F, 3, 3, 2, 0.0F, false));
//        Refueler2.cubeList.add(new ModelBox(Refueler2, 76, 98, 12.0689F, -5.0F, -6.407F, 1, 3, 2, 0.0F, false));
//        Refueler2.cubeList.add(new ModelBox(Refueler2, 76, 98, 2.565F, -4.7F, -1.6039F, 1, 3, 2, 0.0F, false));
//        Refueler2.cubeList.add(new ModelBox(Refueler2, 74, 106, 2.565F, 1.2F, -1.6039F, 1, 3, 2, 0.0F, false));
//        Refueler2.cubeList.add(new ModelBox(Refueler2, 76, 89, 2.565F, -11.2F, -1.6039F, 1, 3, 2, 0.0F, false));
//
//        Refueler3 = new ModelRenderer(this);
//        Refueler3.setRotationPoint(-22.5904F, -25.7132F, -7.55F);
//        setRotationAngle(Refueler3, 1.5708F, 0.0F, -0.9599F);
//        East.addChild(Refueler3);
//        Refueler3.cubeList.add(new ModelBox(Refueler3, 5, 66, -1.9835F, 0.0F, -0.7209F, 4, 4, 2, 0.0F, false));
//        Refueler3.cubeList.add(new ModelBox(Refueler3, 74, 118, -1.6296F, 0.5F, -0.1463F, 3, 3, 2, 0.0F, false));
//
//        throttle3 = new ModelRenderer(this);
//        throttle3.setRotationPoint(-14.6F, -33.8F, -10.4F);
//        setRotationAngle(throttle3, 0.0F, -0.5236F, 0.0F);
//        East.addChild(throttle3);
//
//        bone11 = new ModelRenderer(this);
//        bone11.setRotationPoint(0.3179F, 4.4029F, 2.3543F);
//        setRotationAngle(bone11, 0.0F, 0.0F, -0.6109F);
//        throttle3.addChild(bone11);
//        bone11.cubeList.add(new ModelBox(bone11, 22, 42, -7.4556F, -3.8511F, -3.3413F, 11, 4, 5, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 22, 136, -7.2848F, -4.7812F, -1.6737F, 11, 4, 1, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 75, 99, -3.3593F, -5.6201F, -1.5266F, 3, 2, 1, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 75, 109, -6.079F, -5.5687F, -1.5008F, 3, 2, 1, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 75, 90, -0.8396F, -5.6715F, -1.5524F, 3, 2, 1, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 22, 136, -7.7723F, -5.1981F, -3.6166F, 1, 3, 6, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 22, 136, 3.6832F, -5.3052F, -3.5866F, 1, 3, 6, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 22, 136, -7.3168F, -4.3052F, 1.4134F, 11, 2, 1, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 22, 136, -7.3168F, -4.3052F, -3.5866F, 11, 2, 1, 0.0F, false));
//        bone11.cubeList.add(new ModelBox(bone11, 24, 155, -2.8718F, -4.2955F, -0.5574F, 2, 2, 2, 0.0F, false));
//
//        throtlelever2 = new ModelRenderer(this);
//        throtlelever2.setRotationPoint(-1.7996F, -3.6759F, 0.6114F);
//        setRotationAngle(throtlelever2, 0.0F, 0.0F, 1.5184F);
//        bone11.addChild(throtlelever2);
//        throtlelever2.cubeList.add(new ModelBox(throtlelever2, 30, 136, -0.4709F, -4.8162F, -0.6615F, 1, 5, 1, 0.0F, false));
//        throtlelever2.cubeList.add(new ModelBox(throtlelever2, 60, 35, -0.9614F, -5.3802F, -1.1081F, 2, 2, 2, 0.0F, false));
//
//        Panels = new ModelRenderer(this);
//        Panels.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(Panels, 0.0F, -0.5236F, 0.0F);
//        all.addChild(Panels);
//
//        North_panel = new ModelRenderer(this);
//        North_panel.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_panel, 0.0F, -1.5708F, 0.0F);
//        Panels.addChild(North_panel);
//        North_panel.cubeList.add(new ModelBox(North_panel, 0, 0, 8.0504F, -33.0F, -4.9F, 4, 2, 10, 0.0F, false));
//        North_panel.cubeList.add(new ModelBox(North_panel, 0, 132, 22.6694F, -23.0957F, -11.9F, 4, 6, 24, 0.0F, false));
//
//        circlesection73 = new ModelRenderer(this);
//        circlesection73.setRotationPoint(-10.5F, 0.0F, -12.5F);
//        setRotationAngle(circlesection73, 0.0F, 0.0F, 0.4363F);
//        North_panel.addChild(circlesection73);
//        circlesection73.cubeList.add(new ModelBox(circlesection73, 0, 0, 6.4912F, -39.4384F, 5.6F, 4, 2, 14, 0.0F, false));
//
//        circlesection74 = new ModelRenderer(this);
//        circlesection74.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection74, 0.0F, 0.0F, 0.1745F);
//        circlesection73.addChild(circlesection74);
//        circlesection74.cubeList.add(new ModelBox(circlesection74, 0, 0, 7.4835F, -40.661F, 3.1F, 4, 2, 19, 0.0F, false));
//        circlesection74.cubeList.add(new ModelBox(circlesection74, 0, 0, 3.4835F, -40.661F, 4.6F, 4, 2, 16, 0.0F, false));
//
//        circlesection75 = new ModelRenderer(this);
//        circlesection75.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection75, 0.0F, 0.0F, 0.5236F);
//        circlesection74.addChild(circlesection75);
//        circlesection75.cubeList.add(new ModelBox(circlesection75, 0, 0, -10.3855F, -40.9552F, 2.1F, 4, 2, 21, 0.0F, false));
//
//        North_panel2 = new ModelRenderer(this);
//        North_panel2.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_panel2, 0.0F, -0.5236F, 0.0F);
//        Panels.addChild(North_panel2);
//        North_panel2.cubeList.add(new ModelBox(North_panel2, 0, 0, 8.0504F, -33.0F, -4.9F, 4, 2, 10, 0.0F, false));
//        North_panel2.cubeList.add(new ModelBox(North_panel2, 0, 132, 22.6694F, -23.0957F, -11.9F, 4, 6, 24, 0.0F, false));
//
//        circlesection2 = new ModelRenderer(this);
//        circlesection2.setRotationPoint(-10.5F, 0.0F, -12.5F);
//        setRotationAngle(circlesection2, 0.0F, 0.0F, 0.4363F);
//        North_panel2.addChild(circlesection2);
//        circlesection2.cubeList.add(new ModelBox(circlesection2, 0, 0, 6.4912F, -39.4384F, 5.6F, 4, 2, 14, 0.0F, false));
//
//        circlesection3 = new ModelRenderer(this);
//        circlesection3.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection3, 0.0F, 0.0F, 0.1745F);
//        circlesection2.addChild(circlesection3);
//        circlesection3.cubeList.add(new ModelBox(circlesection3, 0, 0, 7.4835F, -40.661F, 3.1F, 4, 2, 19, 0.0F, false));
//        circlesection3.cubeList.add(new ModelBox(circlesection3, 0, 0, 3.4835F, -40.661F, 4.6F, 4, 2, 16, 0.0F, false));
//
//        circlesection4 = new ModelRenderer(this);
//        circlesection4.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection4, 0.0F, 0.0F, 0.5236F);
//        circlesection3.addChild(circlesection4);
//        circlesection4.cubeList.add(new ModelBox(circlesection4, 0, 0, -10.3855F, -40.9552F, 2.1F, 4, 2, 21, 0.0F, false));
//
//        North_panel3 = new ModelRenderer(this);
//        North_panel3.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_panel3, 0.0F, 0.5236F, 0.0F);
//        Panels.addChild(North_panel3);
//        North_panel3.cubeList.add(new ModelBox(North_panel3, 0, 0, 8.0504F, -33.0F, -4.9F, 4, 2, 10, 0.0F, false));
//        North_panel3.cubeList.add(new ModelBox(North_panel3, 0, 132, 22.6694F, -23.0957F, -11.9F, 4, 6, 24, 0.0F, false));
//
//        circlesection5 = new ModelRenderer(this);
//        circlesection5.setRotationPoint(-10.5F, 0.0F, -12.5F);
//        setRotationAngle(circlesection5, 0.0F, 0.0F, 0.4363F);
//        North_panel3.addChild(circlesection5);
//        circlesection5.cubeList.add(new ModelBox(circlesection5, 0, 0, 6.4912F, -39.4384F, 5.6F, 4, 2, 14, 0.0F, false));
//
//        circlesection6 = new ModelRenderer(this);
//        circlesection6.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection6, 0.0F, 0.0F, 0.1745F);
//        circlesection5.addChild(circlesection6);
//        circlesection6.cubeList.add(new ModelBox(circlesection6, 0, 0, 7.4835F, -40.661F, 3.1F, 4, 2, 19, 0.0F, false));
//        circlesection6.cubeList.add(new ModelBox(circlesection6, 0, 0, 3.4835F, -40.661F, 4.6F, 4, 2, 16, 0.0F, false));
//
//        circlesection7 = new ModelRenderer(this);
//        circlesection7.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection7, 0.0F, 0.0F, 0.5236F);
//        circlesection6.addChild(circlesection7);
//        circlesection7.cubeList.add(new ModelBox(circlesection7, 0, 0, -10.3855F, -40.9552F, 2.1F, 4, 2, 21, 0.0F, false));
//
//        North_panel4 = new ModelRenderer(this);
//        North_panel4.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_panel4, 0.0F, 1.5708F, 0.0F);
//        Panels.addChild(North_panel4);
//        North_panel4.cubeList.add(new ModelBox(North_panel4, 0, 0, 8.0504F, -33.0F, -4.9F, 4, 2, 10, 0.0F, false));
//        North_panel4.cubeList.add(new ModelBox(North_panel4, 0, 132, 22.6694F, -23.0957F, -11.9F, 4, 6, 24, 0.0F, false));
//
//        circlesection8 = new ModelRenderer(this);
//        circlesection8.setRotationPoint(-10.5F, 0.0F, -12.5F);
//        setRotationAngle(circlesection8, 0.0F, 0.0F, 0.4363F);
//        North_panel4.addChild(circlesection8);
//        circlesection8.cubeList.add(new ModelBox(circlesection8, 0, 0, 6.4912F, -39.4384F, 5.6F, 4, 2, 14, 0.0F, false));
//
//        circlesection9 = new ModelRenderer(this);
//        circlesection9.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection9, 0.0F, 0.0F, 0.1745F);
//        circlesection8.addChild(circlesection9);
//        circlesection9.cubeList.add(new ModelBox(circlesection9, 0, 0, 7.4835F, -40.661F, 3.1F, 4, 2, 19, 0.0F, false));
//        circlesection9.cubeList.add(new ModelBox(circlesection9, 0, 0, 3.4835F, -40.661F, 4.6F, 4, 2, 16, 0.0F, false));
//
//        circlesection10 = new ModelRenderer(this);
//        circlesection10.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection10, 0.0F, 0.0F, 0.5236F);
//        circlesection9.addChild(circlesection10);
//        circlesection10.cubeList.add(new ModelBox(circlesection10, 0, 0, -10.3855F, -40.9552F, 2.1F, 4, 2, 21, 0.0F, false));
//
//        North_panel5 = new ModelRenderer(this);
//        North_panel5.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_panel5, 0.0F, 2.618F, 0.0F);
//        Panels.addChild(North_panel5);
//        North_panel5.cubeList.add(new ModelBox(North_panel5, 0, 0, 8.0504F, -33.0F, -4.9F, 4, 2, 10, 0.0F, false));
//        North_panel5.cubeList.add(new ModelBox(North_panel5, 0, 132, 22.6694F, -23.0957F, -11.9F, 4, 6, 24, 0.0F, false));
//
//        circlesection11 = new ModelRenderer(this);
//        circlesection11.setRotationPoint(-10.5F, 0.0F, -12.5F);
//        setRotationAngle(circlesection11, 0.0F, 0.0F, 0.4363F);
//        North_panel5.addChild(circlesection11);
//        circlesection11.cubeList.add(new ModelBox(circlesection11, 0, 0, 6.4912F, -39.4384F, 5.6F, 4, 2, 14, 0.0F, false));
//
//        circlesection12 = new ModelRenderer(this);
//        circlesection12.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection12, 0.0F, 0.0F, 0.1745F);
//        circlesection11.addChild(circlesection12);
//        circlesection12.cubeList.add(new ModelBox(circlesection12, 0, 0, 7.4835F, -40.661F, 3.1F, 4, 2, 19, 0.0F, false));
//        circlesection12.cubeList.add(new ModelBox(circlesection12, 0, 0, 3.4835F, -40.661F, 4.6F, 4, 2, 16, 0.0F, false));
//
//        circlesection25 = new ModelRenderer(this);
//        circlesection25.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection25, 0.0F, 0.0F, 0.5236F);
//        circlesection12.addChild(circlesection25);
//        circlesection25.cubeList.add(new ModelBox(circlesection25, 0, 0, -10.3855F, -40.9552F, 2.1F, 4, 2, 21, 0.0F, false));
//
//        North_panel6 = new ModelRenderer(this);
//        North_panel6.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_panel6, 0.0F, -2.618F, 0.0F);
//        Panels.addChild(North_panel6);
//        North_panel6.cubeList.add(new ModelBox(North_panel6, 0, 0, 8.0504F, -33.0F, -4.9F, 4, 2, 10, 0.0F, false));
//        North_panel6.cubeList.add(new ModelBox(North_panel6, 0, 132, 22.6694F, -23.0957F, -11.9F, 4, 6, 24, 0.0F, false));
//
//        circlesection26 = new ModelRenderer(this);
//        circlesection26.setRotationPoint(-10.5F, 0.0F, -12.5F);
//        setRotationAngle(circlesection26, 0.0F, 0.0F, 0.4363F);
//        North_panel6.addChild(circlesection26);
//        circlesection26.cubeList.add(new ModelBox(circlesection26, 0, 0, 6.4912F, -39.4384F, 5.6F, 4, 2, 14, 0.0F, false));
//
//        circlesection27 = new ModelRenderer(this);
//        circlesection27.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection27, 0.0F, 0.0F, 0.1745F);
//        circlesection26.addChild(circlesection27);
//        circlesection27.cubeList.add(new ModelBox(circlesection27, 0, 0, 7.4835F, -40.661F, 3.1F, 4, 2, 19, 0.0F, false));
//        circlesection27.cubeList.add(new ModelBox(circlesection27, 0, 0, 3.4835F, -40.661F, 4.6F, 4, 2, 16, 0.0F, false));
//
//        circlesection28 = new ModelRenderer(this);
//        circlesection28.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection28, 0.0F, 0.0F, 0.5236F);
//        circlesection27.addChild(circlesection28);
//        circlesection28.cubeList.add(new ModelBox(circlesection28, 0, 0, -10.3855F, -40.9552F, 2.1F, 4, 2, 21, 0.0F, false));
//
//        underpanels = new ModelRenderer(this);
//        underpanels.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(underpanels, 0.0F, -0.5236F, 0.0F);
//        all.addChild(underpanels);
//
//        bone2 = new ModelRenderer(this);
//        bone2.setRotationPoint(0.0F, 0.0F, -1.5F);
//        underpanels.addChild(bone2);
//        bone2.cubeList.add(new ModelBox(bone2, 0, 35, -10.6F, -17.2568F, 21.1672F, 21, 4, 4, 0.0F, false));
//        bone2.cubeList.add(new ModelBox(bone2, 0, 35, -7.6F, -9.2568F, 12.1672F, 15, 4, 7, 0.0F, false));
//        bone2.cubeList.add(new ModelBox(bone2, 0, 35, -8.6F, -13.2568F, 19.1672F, 17, 4, 2, 0.0F, false));
//
//        bone3 = new ModelRenderer(this);
//        bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(bone3, 0.0F, -1.0472F, 0.0F);
//        underpanels.addChild(bone3);
//        bone3.cubeList.add(new ModelBox(bone3, 0, 35, -10.6F, -17.2568F, 19.6672F, 21, 4, 4, 0.0F, false));
//        bone3.cubeList.add(new ModelBox(bone3, 0, 35, -7.6F, -9.2568F, 10.6672F, 15, 4, 7, 0.0F, false));
//        bone3.cubeList.add(new ModelBox(bone3, 0, 35, -8.6F, -13.2568F, 17.6672F, 17, 4, 2, 0.0F, false));
//
//        bone4 = new ModelRenderer(this);
//        bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(bone4, 0.0F, -2.0944F, 0.0F);
//        underpanels.addChild(bone4);
//        bone4.cubeList.add(new ModelBox(bone4, 0, 35, -10.6F, -17.2568F, 19.6672F, 21, 4, 4, 0.0F, false));
//        bone4.cubeList.add(new ModelBox(bone4, 0, 35, -7.6F, -9.2568F, 10.6672F, 15, 4, 7, 0.0F, false));
//        bone4.cubeList.add(new ModelBox(bone4, 0, 35, -8.6F, -13.2568F, 17.6672F, 17, 4, 2, 0.0F, false));
//
//        bone5 = new ModelRenderer(this);
//        bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(bone5, 0.0F, 3.1416F, 0.0F);
//        underpanels.addChild(bone5);
//        bone5.cubeList.add(new ModelBox(bone5, 0, 35, -10.6F, -17.2568F, 19.6672F, 21, 4, 4, 0.0F, false));
//        bone5.cubeList.add(new ModelBox(bone5, 0, 35, -7.6F, -9.2568F, 10.6672F, 15, 4, 7, 0.0F, false));
//        bone5.cubeList.add(new ModelBox(bone5, 0, 35, -8.6F, -13.2568F, 17.6672F, 17, 4, 2, 0.0F, false));
//
//        bone6 = new ModelRenderer(this);
//        bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(bone6, 0.0F, 2.0944F, 0.0F);
//        underpanels.addChild(bone6);
//        bone6.cubeList.add(new ModelBox(bone6, 0, 35, -10.6F, -17.2568F, 19.6672F, 21, 4, 4, 0.0F, false));
//        bone6.cubeList.add(new ModelBox(bone6, 0, 35, -7.6F, -9.2568F, 10.6672F, 15, 4, 7, 0.0F, false));
//        bone6.cubeList.add(new ModelBox(bone6, 0, 35, -8.6F, -13.2568F, 17.6672F, 17, 4, 2, 0.0F, false));
//
//        bone7 = new ModelRenderer(this);
//        bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(bone7, 0.0F, 1.0472F, 0.0F);
//        underpanels.addChild(bone7);
//        bone7.cubeList.add(new ModelBox(bone7, 0, 35, -10.6F, -17.2568F, 19.6672F, 21, 4, 4, 0.0F, false));
//        bone7.cubeList.add(new ModelBox(bone7, 0, 35, -7.6F, -9.2568F, 10.6672F, 15, 4, 7, 0.0F, false));
//        bone7.cubeList.add(new ModelBox(bone7, 0, 35, -8.6F, -13.2568F, 17.6672F, 17, 4, 2, 0.0F, false));
//
//        bone12 = new ModelRenderer(this);
//        bone12.setRotationPoint(0.0F, -5.0F, 0.0F);
//        setRotationAngle(bone12, 0.0F, 0.5236F, 0.0F);
//        underpanels.addChild(bone12);
//        bone12.cubeList.add(new ModelBox(bone12, 0, 35, -11.0F, -1.0F, -11.0F, 22, 0, 22, 0.0F, false));
//
//        edges = new ModelRenderer(this);
//        edges.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(edges, 0.0F, -0.5236F, 0.0F);
//        all.addChild(edges);
//
//        North_leg6 = new ModelRenderer(this);
//        North_leg6.setRotationPoint(0.0F, 0.0F, 0.0F);
//        edges.addChild(North_leg6);
//        North_leg6.cubeList.add(new ModelBox(North_leg6, 0, 62, 7.5504F, -36.2F, -3.0F, 4, 4, 6, 0.0F, false));
//
//        circlesection144 = new ModelRenderer(this);
//        circlesection144.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection144, 0.0F, 0.0F, 0.4363F);
//        North_leg6.addChild(circlesection144);
//        circlesection144.cubeList.add(new ModelBox(circlesection144, 0, 62, 11.3043F, 1.3968F, -5.6F, 4, 4, 6, 0.0F, false));
//
//        circlesection145 = new ModelRenderer(this);
//        circlesection145.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection145, 0.0F, 0.0F, 0.6109F);
//        North_leg6.addChild(circlesection145);
//        circlesection145.cubeList.add(new ModelBox(circlesection145, 0, 62, 15.3144F, -1.282F, -5.6F, 12, 3, 6, 0.0F, false));
//
//        circlesection146 = new ModelRenderer(this);
//        circlesection146.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection146, 0.0F, 0.0F, 1.1345F);
//        North_leg6.addChild(circlesection146);
//        circlesection146.cubeList.add(new ModelBox(circlesection146, 0, 62, 23.014F, -14.7674F, -5.6F, 4, 3, 6, 0.0F, false));
//
//        circlesection147 = new ModelRenderer(this);
//        circlesection147.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        North_leg6.addChild(circlesection147);
//        circlesection147.cubeList.add(new ModelBox(circlesection147, 0, 62, 22.4005F, 18.242F, -5.6F, 5, 8, 6, 0.0F, false));
//
//        circlesection148 = new ModelRenderer(this);
//        circlesection148.setRotationPoint(1.8956F, -34.2434F, 2.6F);
//        setRotationAngle(circlesection148, 0.0F, 0.0F, 2.3562F);
//        North_leg6.addChild(circlesection148);
//        circlesection148.cubeList.add(new ModelBox(circlesection148, 0, 62, -6.476F, -32.2741F, -5.6F, 14, 5, 6, 0.0F, false));
//
//        circlesection149 = new ModelRenderer(this);
//        circlesection149.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        setRotationAngle(circlesection149, 0.0F, 0.0F, 0.7854F);
//        North_leg6.addChild(circlesection149);
//        circlesection149.cubeList.add(new ModelBox(circlesection149, 0, 62, 15.5061F, 17.292F, -5.3557F, 8, 7, 6, 0.0F, false));
//
//        circlesection150 = new ModelRenderer(this);
//        circlesection150.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        North_leg6.addChild(circlesection150);
//        circlesection150.cubeList.add(new ModelBox(circlesection150, 0, 62, -0.5557F, 29.7983F, -5.3557F, 6, 4, 6, 0.0F, false));
//
//        North_leg2 = new ModelRenderer(this);
//        North_leg2.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_leg2, 0.0F, 1.0472F, 0.0F);
//        edges.addChild(North_leg2);
//        North_leg2.cubeList.add(new ModelBox(North_leg2, 0, 62, 7.5504F, -36.2F, -3.0F, 4, 4, 6, 0.0F, false));
//
//        circlesection29 = new ModelRenderer(this);
//        circlesection29.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection29, 0.0F, 0.0F, 0.4363F);
//        North_leg2.addChild(circlesection29);
//        circlesection29.cubeList.add(new ModelBox(circlesection29, 0, 62, 11.3043F, 1.3968F, -5.6F, 4, 4, 6, 0.0F, false));
//
//        circlesection30 = new ModelRenderer(this);
//        circlesection30.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection30, 0.0F, 0.0F, 0.6109F);
//        North_leg2.addChild(circlesection30);
//        circlesection30.cubeList.add(new ModelBox(circlesection30, 0, 62, 15.3144F, -1.282F, -5.6F, 12, 3, 6, 0.0F, false));
//
//        circlesection31 = new ModelRenderer(this);
//        circlesection31.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection31, 0.0F, 0.0F, 1.1345F);
//        North_leg2.addChild(circlesection31);
//        circlesection31.cubeList.add(new ModelBox(circlesection31, 0, 62, 23.014F, -14.7674F, -5.6F, 4, 3, 6, 0.0F, false));
//
//        circlesection32 = new ModelRenderer(this);
//        circlesection32.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        North_leg2.addChild(circlesection32);
//        circlesection32.cubeList.add(new ModelBox(circlesection32, 0, 62, 22.4005F, 18.242F, -5.6F, 5, 8, 6, 0.0F, false));
//
//        circlesection33 = new ModelRenderer(this);
//        circlesection33.setRotationPoint(1.8956F, -34.2434F, 2.6F);
//        setRotationAngle(circlesection33, 0.0F, 0.0F, 2.3562F);
//        North_leg2.addChild(circlesection33);
//        circlesection33.cubeList.add(new ModelBox(circlesection33, 0, 62, -6.476F, -32.2741F, -5.6F, 14, 5, 6, 0.0F, false));
//
//        circlesection34 = new ModelRenderer(this);
//        circlesection34.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        setRotationAngle(circlesection34, 0.0F, 0.0F, 0.7854F);
//        North_leg2.addChild(circlesection34);
//        circlesection34.cubeList.add(new ModelBox(circlesection34, 0, 62, 15.5061F, 17.292F, -5.3557F, 8, 7, 6, 0.0F, false));
//
//        circlesection35 = new ModelRenderer(this);
//        circlesection35.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        North_leg2.addChild(circlesection35);
//        circlesection35.cubeList.add(new ModelBox(circlesection35, 0, 62, -0.5557F, 29.7983F, -5.3557F, 6, 4, 6, 0.0F, false));
//
//        North_leg3 = new ModelRenderer(this);
//        North_leg3.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_leg3, 0.0F, 2.0944F, 0.0F);
//        edges.addChild(North_leg3);
//        North_leg3.cubeList.add(new ModelBox(North_leg3, 0, 62, 7.5504F, -36.2F, -3.0F, 4, 4, 6, 0.0F, false));
//
//        circlesection36 = new ModelRenderer(this);
//        circlesection36.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection36, 0.0F, 0.0F, 0.4363F);
//        North_leg3.addChild(circlesection36);
//        circlesection36.cubeList.add(new ModelBox(circlesection36, 0, 62, 11.3043F, 1.3968F, -5.6F, 4, 4, 6, 0.0F, false));
//
//        circlesection37 = new ModelRenderer(this);
//        circlesection37.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection37, 0.0F, 0.0F, 0.6109F);
//        North_leg3.addChild(circlesection37);
//        circlesection37.cubeList.add(new ModelBox(circlesection37, 0, 62, 15.3144F, -1.282F, -5.6F, 12, 3, 6, 0.0F, false));
//
//        circlesection38 = new ModelRenderer(this);
//        circlesection38.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection38, 0.0F, 0.0F, 1.1345F);
//        North_leg3.addChild(circlesection38);
//        circlesection38.cubeList.add(new ModelBox(circlesection38, 0, 62, 23.014F, -14.7674F, -5.6F, 4, 3, 6, 0.0F, false));
//
//        circlesection39 = new ModelRenderer(this);
//        circlesection39.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        North_leg3.addChild(circlesection39);
//        circlesection39.cubeList.add(new ModelBox(circlesection39, 0, 62, 22.4005F, 18.242F, -5.6F, 5, 8, 6, 0.0F, false));
//
//        circlesection40 = new ModelRenderer(this);
//        circlesection40.setRotationPoint(1.8956F, -34.2434F, 2.6F);
//        setRotationAngle(circlesection40, 0.0F, 0.0F, 2.3562F);
//        North_leg3.addChild(circlesection40);
//        circlesection40.cubeList.add(new ModelBox(circlesection40, 0, 62, -6.476F, -32.2741F, -5.6F, 14, 5, 6, 0.0F, false));
//
//        circlesection41 = new ModelRenderer(this);
//        circlesection41.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        setRotationAngle(circlesection41, 0.0F, 0.0F, 0.7854F);
//        North_leg3.addChild(circlesection41);
//        circlesection41.cubeList.add(new ModelBox(circlesection41, 0, 62, 15.5061F, 17.292F, -5.3557F, 8, 7, 6, 0.0F, false));
//
//        circlesection42 = new ModelRenderer(this);
//        circlesection42.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        North_leg3.addChild(circlesection42);
//        circlesection42.cubeList.add(new ModelBox(circlesection42, 0, 62, -0.5557F, 29.7983F, -5.3557F, 6, 4, 6, 0.0F, false));
//
//        North_leg4 = new ModelRenderer(this);
//        North_leg4.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_leg4, 0.0F, 3.1416F, 0.0F);
//        edges.addChild(North_leg4);
//        North_leg4.cubeList.add(new ModelBox(North_leg4, 0, 62, 7.5504F, -36.2F, -3.0F, 4, 4, 6, 0.0F, false));
//
//        circlesection43 = new ModelRenderer(this);
//        circlesection43.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection43, 0.0F, 0.0F, 0.4363F);
//        North_leg4.addChild(circlesection43);
//        circlesection43.cubeList.add(new ModelBox(circlesection43, 0, 62, 11.3043F, 1.3968F, -5.6F, 4, 4, 6, 0.0F, false));
//
//        circlesection44 = new ModelRenderer(this);
//        circlesection44.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection44, 0.0F, 0.0F, 0.6109F);
//        North_leg4.addChild(circlesection44);
//        circlesection44.cubeList.add(new ModelBox(circlesection44, 0, 62, 15.3144F, -1.282F, -5.6F, 12, 3, 6, 0.0F, false));
//
//        circlesection45 = new ModelRenderer(this);
//        circlesection45.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection45, 0.0F, 0.0F, 1.1345F);
//        North_leg4.addChild(circlesection45);
//        circlesection45.cubeList.add(new ModelBox(circlesection45, 0, 62, 23.014F, -14.7674F, -5.6F, 4, 3, 6, 0.0F, false));
//
//        circlesection46 = new ModelRenderer(this);
//        circlesection46.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        North_leg4.addChild(circlesection46);
//        circlesection46.cubeList.add(new ModelBox(circlesection46, 0, 62, 22.4005F, 18.242F, -5.6F, 5, 8, 6, 0.0F, false));
//
//        circlesection47 = new ModelRenderer(this);
//        circlesection47.setRotationPoint(1.8956F, -34.2434F, 2.6F);
//        setRotationAngle(circlesection47, 0.0F, 0.0F, 2.3562F);
//        North_leg4.addChild(circlesection47);
//        circlesection47.cubeList.add(new ModelBox(circlesection47, 0, 62, -6.476F, -32.2741F, -5.6F, 14, 5, 6, 0.0F, false));
//
//        circlesection48 = new ModelRenderer(this);
//        circlesection48.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        setRotationAngle(circlesection48, 0.0F, 0.0F, 0.7854F);
//        North_leg4.addChild(circlesection48);
//        circlesection48.cubeList.add(new ModelBox(circlesection48, 0, 62, 15.5061F, 17.292F, -5.3557F, 8, 7, 6, 0.0F, false));
//
//        circlesection49 = new ModelRenderer(this);
//        circlesection49.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        North_leg4.addChild(circlesection49);
//        circlesection49.cubeList.add(new ModelBox(circlesection49, 0, 62, -0.5557F, 29.7983F, -5.3557F, 6, 4, 6, 0.0F, false));
//
//        North_leg5 = new ModelRenderer(this);
//        North_leg5.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_leg5, 0.0F, -2.0944F, 0.0F);
//        edges.addChild(North_leg5);
//        North_leg5.cubeList.add(new ModelBox(North_leg5, 0, 62, 7.5504F, -36.2F, -3.0F, 4, 4, 6, 0.0F, false));
//
//        circlesection50 = new ModelRenderer(this);
//        circlesection50.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection50, 0.0F, 0.0F, 0.4363F);
//        North_leg5.addChild(circlesection50);
//        circlesection50.cubeList.add(new ModelBox(circlesection50, 0, 62, 11.3043F, 1.3968F, -5.6F, 4, 4, 6, 0.0F, false));
//
//        circlesection51 = new ModelRenderer(this);
//        circlesection51.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection51, 0.0F, 0.0F, 0.6109F);
//        North_leg5.addChild(circlesection51);
//        circlesection51.cubeList.add(new ModelBox(circlesection51, 0, 62, 15.3144F, -1.282F, -5.6F, 12, 3, 6, 0.0F, false));
//
//        circlesection52 = new ModelRenderer(this);
//        circlesection52.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection52, 0.0F, 0.0F, 1.1345F);
//        North_leg5.addChild(circlesection52);
//        circlesection52.cubeList.add(new ModelBox(circlesection52, 0, 62, 23.014F, -14.7674F, -5.6F, 4, 3, 6, 0.0F, false));
//
//        circlesection53 = new ModelRenderer(this);
//        circlesection53.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        North_leg5.addChild(circlesection53);
//        circlesection53.cubeList.add(new ModelBox(circlesection53, 0, 62, 22.4005F, 18.242F, -5.6F, 5, 8, 6, 0.0F, false));
//
//        circlesection54 = new ModelRenderer(this);
//        circlesection54.setRotationPoint(1.8956F, -34.2434F, 2.6F);
//        setRotationAngle(circlesection54, 0.0F, 0.0F, 2.3562F);
//        North_leg5.addChild(circlesection54);
//        circlesection54.cubeList.add(new ModelBox(circlesection54, 0, 62, -6.476F, -32.2741F, -5.6F, 14, 5, 6, 0.0F, false));
//
//        circlesection55 = new ModelRenderer(this);
//        circlesection55.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        setRotationAngle(circlesection55, 0.0F, 0.0F, 0.7854F);
//        North_leg5.addChild(circlesection55);
//        circlesection55.cubeList.add(new ModelBox(circlesection55, 0, 62, 15.5061F, 17.292F, -5.3557F, 8, 7, 6, 0.0F, false));
//
//        circlesection56 = new ModelRenderer(this);
//        circlesection56.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        North_leg5.addChild(circlesection56);
//        circlesection56.cubeList.add(new ModelBox(circlesection56, 0, 62, -0.5557F, 29.7983F, -5.3557F, 6, 4, 6, 0.0F, false));
//
//        North_leg7 = new ModelRenderer(this);
//        North_leg7.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(North_leg7, 0.0F, -1.0472F, 0.0F);
//        edges.addChild(North_leg7);
//        North_leg7.cubeList.add(new ModelBox(North_leg7, 0, 62, 7.5504F, -36.2F, -3.0F, 4, 4, 6, 0.0F, false));
//
//        circlesection57 = new ModelRenderer(this);
//        circlesection57.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection57, 0.0F, 0.0F, 0.4363F);
//        North_leg7.addChild(circlesection57);
//        circlesection57.cubeList.add(new ModelBox(circlesection57, 0, 62, 11.3043F, 1.3968F, -5.6F, 4, 4, 6, 0.0F, false));
//
//        circlesection58 = new ModelRenderer(this);
//        circlesection58.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection58, 0.0F, 0.0F, 0.6109F);
//        North_leg7.addChild(circlesection58);
//        circlesection58.cubeList.add(new ModelBox(circlesection58, 0, 62, 15.3144F, -1.282F, -5.6F, 12, 3, 6, 0.0F, false));
//
//        circlesection59 = new ModelRenderer(this);
//        circlesection59.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        setRotationAngle(circlesection59, 0.0F, 0.0F, 1.1345F);
//        North_leg7.addChild(circlesection59);
//        circlesection59.cubeList.add(new ModelBox(circlesection59, 0, 62, 23.014F, -14.7674F, -5.6F, 4, 3, 6, 0.0F, false));
//
//        circlesection60 = new ModelRenderer(this);
//        circlesection60.setRotationPoint(1.8956F, -42.2434F, 2.6F);
//        North_leg7.addChild(circlesection60);
//        circlesection60.cubeList.add(new ModelBox(circlesection60, 0, 62, 22.4005F, 18.242F, -5.6F, 5, 8, 6, 0.0F, false));
//
//        circlesection61 = new ModelRenderer(this);
//        circlesection61.setRotationPoint(1.8956F, -34.2434F, 2.6F);
//        setRotationAngle(circlesection61, 0.0F, 0.0F, 2.3562F);
//        North_leg7.addChild(circlesection61);
//        circlesection61.cubeList.add(new ModelBox(circlesection61, 0, 62, -6.476F, -32.2741F, -5.6F, 14, 5, 6, 0.0F, false));
//
//        circlesection62 = new ModelRenderer(this);
//        circlesection62.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        setRotationAngle(circlesection62, 0.0F, 0.0F, 0.7854F);
//        North_leg7.addChild(circlesection62);
//        circlesection62.cubeList.add(new ModelBox(circlesection62, 0, 62, 15.5061F, 17.292F, -5.3557F, 8, 7, 6, 0.0F, false));
//
//        circlesection63 = new ModelRenderer(this);
//        circlesection63.setRotationPoint(15.8934F, -34.2434F, 2.3557F);
//        North_leg7.addChild(circlesection63);
//        circlesection63.cubeList.add(new ModelBox(circlesection63, 0, 62, -0.5557F, 29.7983F, -5.3557F, 6, 4, 6, 0.0F, false));
//
//        Centre = new ModelRenderer(this);
//        Centre.setRotationPoint(0.0F, 2.0F, 0.0F);
//        all.addChild(Centre);
//
//        circlesection13 = new ModelRenderer(this);
//        circlesection13.setRotationPoint(0.0F, 0.0F, 0.0F);
//        Centre.addChild(circlesection13);
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 0, 38, 4.954F, -47.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 18, 76, -0.046F, -106.0F, -4.0032F, 7, 15, 8, 0.0F, false));
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 0, 38, 4.954F, -43.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 0, 63, 6.635F, -49.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 0, 82, 6.635F, -104.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 0, 82, 6.635F, -91.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 0, 82, 6.635F, -97.5F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 0, 63, 6.635F, -45.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection13.cubeList.add(new ModelBox(circlesection13, 0, 63, 5.85F, -41.0F, -4.5032F, 2, 9, 9, 0.0F, false));
//
//        circlesection14 = new ModelRenderer(this);
//        circlesection14.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection14, 0.0F, -1.0472F, 0.0F);
//        Centre.addChild(circlesection14);
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 0, 38, 4.954F, -47.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 18, 76, -0.046F, -106.0F, -4.0032F, 7, 15, 8, 0.0F, false));
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 0, 38, 4.954F, -43.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 0, 82, 6.635F, -49.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 0, 82, 6.635F, -104.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 0, 82, 6.635F, -91.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 0, 82, 6.635F, -97.5F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 0, 82, 6.635F, -45.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection14.cubeList.add(new ModelBox(circlesection14, 0, 82, 5.85F, -41.0F, -4.5032F, 2, 9, 9, 0.0F, false));
//
//        circlesection15 = new ModelRenderer(this);
//        circlesection15.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection15, 0.0F, -2.0944F, 0.0F);
//        Centre.addChild(circlesection15);
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 0, 38, 4.954F, -47.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 18, 76, -0.046F, -106.0F, -4.0032F, 7, 15, 8, 0.0F, false));
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 0, 38, 4.954F, -43.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 0, 82, 6.635F, -49.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 0, 82, 6.635F, -104.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 0, 82, 6.635F, -91.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 0, 82, 6.635F, -97.5F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 0, 82, 6.635F, -45.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection15.cubeList.add(new ModelBox(circlesection15, 0, 82, 5.85F, -41.0F, -4.5032F, 2, 9, 9, 0.0F, false));
//
//        circlesection16 = new ModelRenderer(this);
//        circlesection16.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection16, 0.0F, 3.1416F, 0.0F);
//        Centre.addChild(circlesection16);
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 0, 38, 4.954F, -47.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 18, 76, -0.046F, -106.0F, -4.0032F, 7, 15, 8, 0.0F, false));
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 0, 38, 4.954F, -43.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 0, 82, 6.635F, -49.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 0, 82, 6.635F, -104.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 0, 82, 6.635F, -91.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 0, 82, 6.635F, -97.5F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 0, 82, 6.635F, -45.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection16.cubeList.add(new ModelBox(circlesection16, 0, 82, 5.85F, -41.0F, -4.5032F, 2, 9, 9, 0.0F, false));
//
//        circlesection17 = new ModelRenderer(this);
//        circlesection17.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection17, 0.0F, 2.0944F, 0.0F);
//        Centre.addChild(circlesection17);
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 0, 38, 4.954F, -47.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 18, 76, -0.046F, -106.0F, -4.0032F, 7, 15, 8, 0.0F, false));
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 0, 38, 4.954F, -43.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 0, 82, 6.635F, -49.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 0, 82, 6.635F, -104.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 0, 82, 6.635F, -91.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 0, 82, 6.635F, -97.5F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 0, 82, 6.635F, -45.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection17.cubeList.add(new ModelBox(circlesection17, 0, 82, 5.85F, -41.0F, -4.5032F, 2, 9, 9, 0.0F, false));
//
//        circlesection18 = new ModelRenderer(this);
//        circlesection18.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection18, 0.0F, 1.0472F, 0.0F);
//        Centre.addChild(circlesection18);
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 0, 38, 4.954F, -47.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 18, 76, -0.046F, -106.0F, -4.0032F, 7, 15, 8, 0.0F, false));
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 0, 38, 4.954F, -43.0F, -4.0032F, 2, 2, 8, 0.0F, false));
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 0, 82, 6.635F, -49.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 0, 82, 6.635F, -104.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 0, 82, 6.635F, -91.0F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 0, 82, 6.635F, -97.5F, -5.0032F, 2, 3, 10, 0.0F, false));
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 0, 82, 6.635F, -45.0F, -5.0032F, 2, 2, 10, 0.0F, false));
//        circlesection18.cubeList.add(new ModelBox(circlesection18, 0, 82, 5.85F, -41.0F, -4.5032F, 2, 9, 9, 0.0F, false));
//
//        TRANSPARENT_BEAMS = new ModelRenderer(this);
//        TRANSPARENT_BEAMS.setRotationPoint(0.0F, -2.0F, 0.0F);
//        setRotationAngle(TRANSPARENT_BEAMS, 0.0F, -0.5236F, 0.0F);
//        Centre.addChild(TRANSPARENT_BEAMS);
//        TRANSPARENT_BEAMS.cubeList.add(new ModelBox(TRANSPARENT_BEAMS, 46, 63, -8.635F, -86.0F, -1.5F, 1, 39, 3, 0.0F, false));
//
//        circlesection20 = new ModelRenderer(this);
//        circlesection20.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection20, 0.0F, -1.0472F, 0.0F);
//        TRANSPARENT_BEAMS.addChild(circlesection20);
//        circlesection20.cubeList.add(new ModelBox(circlesection20, 46, 63, -8.635F, -86.0F, -1.5F, 1, 39, 3, 0.0F, false));
//
//        circlesection21 = new ModelRenderer(this);
//        circlesection21.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection21, 0.0F, -1.0472F, 0.0F);
//        circlesection20.addChild(circlesection21);
//        circlesection21.cubeList.add(new ModelBox(circlesection21, 46, 63, -8.635F, -86.0F, -1.5F, 1, 39, 3, 0.0F, false));
//
//        circlesection22 = new ModelRenderer(this);
//        circlesection22.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection22, 0.0F, -1.0472F, 0.0F);
//        circlesection21.addChild(circlesection22);
//        circlesection22.cubeList.add(new ModelBox(circlesection22, 46, 63, -8.635F, -86.0F, -1.5F, 1, 39, 3, 0.0F, false));
//
//        circlesection23 = new ModelRenderer(this);
//        circlesection23.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection23, 0.0F, -1.0472F, 0.0F);
//        circlesection22.addChild(circlesection23);
//        circlesection23.cubeList.add(new ModelBox(circlesection23, 46, 63, -8.635F, -86.0F, -1.5F, 1, 39, 3, 0.0F, false));
//
//        circlesection24 = new ModelRenderer(this);
//        circlesection24.setRotationPoint(0.0F, 0.0F, 0.0F);
//        setRotationAngle(circlesection24, 0.0F, -1.0472F, 0.0F);
//        circlesection23.addChild(circlesection24);
//        circlesection24.cubeList.add(new ModelBox(circlesection24, 46, 63, -8.635F, -86.0F, -1.5F, 1, 39, 3, 0.0F, false));
//
//        Rotar = new ModelRenderer(this);
//        Rotar.setRotationPoint(-1.0F, 3.0F, -0.95F);
//        setRotationAngle(Rotar, 0.0F, -0.5236F, 0.0F);
//        all.addChild(Rotar);
//
//        rotarupper = new ModelRenderer(this);
//        rotarupper.setRotationPoint(0.4F, -34.2F, 0.0F);
//        Rotar.addChild(rotarupper);
//        rotarupper.cubeList.add(new ModelBox(rotarupper, 0, 0, -0.4F, -59.0F, -4.8F, 2, 23, 2, 0.0F, false));
//        rotarupper.cubeList.add(new ModelBox(rotarupper, 0, 0, -5.05F, -59.0F, 0.0F, 2, 23, 2, 0.0F, false));
//        rotarupper.cubeList.add(new ModelBox(rotarupper, 0, 0, 4.4F, -59.0F, 0.0F, 2, 23, 2, 0.0F, false));
//        rotarupper.cubeList.add(new ModelBox(rotarupper, 0, 0, -0.4F, -59.0F, 4.05F, 2, 23, 2, 0.0F, false));
//        rotarupper.cubeList.add(new ModelBox(rotarupper, 0, 0, -3.984F, -41.8F, -3.7866F, 9, 3, 9, 0.0F, false));
//        rotarupper.cubeList.add(new ModelBox(rotarupper, 0, 0, -3.984F, -50.8F, -3.7866F, 9, 3, 9, 0.0F, false));
//
//        Rotarlower = new ModelRenderer(this);
//        Rotarlower.setRotationPoint(0.4F, -13.2F, 0.0F);
//        Rotar.addChild(Rotarlower);
//        Rotarlower.cubeList.add(new ModelBox(Rotarlower, 0, 0, -0.4F, -57.0F, -4.8F, 2, 23, 2, 0.0F, false));
//        Rotarlower.cubeList.add(new ModelBox(Rotarlower, 0, 0, -5.05F, -57.0F, 0.0F, 2, 23, 2, 0.0F, false));
//        Rotarlower.cubeList.add(new ModelBox(Rotarlower, 0, 0, -3.984F, -38.8F, -3.7866F, 9, 3, 9, 0.0F, false));
//        Rotarlower.cubeList.add(new ModelBox(Rotarlower, 0, 0, -3.984F, -53.8F, -3.7866F, 9, 3, 9, 0.0F, false));
//        Rotarlower.cubeList.add(new ModelBox(Rotarlower, 0, 0, 4.4F, -57.0F, 0.0F, 2, 23, 2, 0.0F, false));
//        Rotarlower.cubeList.add(new ModelBox(Rotarlower, 0, 0, -0.4F, -57.0F, 4.05F, 2, 23, 2, 0.0F, false));
//    }
//
//
//    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
//        modelRenderer.rotateAngleX = x;
//        modelRenderer.rotateAngleY = y;
//        modelRenderer.rotateAngleZ = z;
//    }
//
//    @Override
//    public void render(ConsoleTile console, float scale) {
//
//        HandbrakeControl hand = console.getControl(HandbrakeControl.class);
//        if (hand != null) {
//            this.Lever.rotateAngleZ = (float) Math.toRadians(hand.isFree() ? 45 : -45);
//            this.Lever2.rotateAngleZ = (float) Math.toRadians(hand.isFree() ? -45 : 45);
//        }
//
//        float pixel = 0.06125F * 3;
//        this.rotarupper.offsetY = (float) Math.cos(console.flightTicks * 0.1) * 0.7F - pixel;
//        this.Rotarlower.offsetY = -(float) Math.cos(console.flightTicks * 0.1) * 0.7F - pixel;
//
//        IncModControl inc = console.getControl(IncModControl.class);
//        if (inc != null) {
//            Incrementincrease.rotateAngleZ = (float) Math.toRadians(inc.getAnimationTicks() * 18);
//        }
//
//        //Throttle
//        ThrottleControl throttle = console.getControl(ThrottleControl.class);
//        if (throttle != null) {
//            this.throtlelever2.rotateAngleX = 0;
//            this.throtlelever2.rotateAngleY = 0;
//            this.throtlelever2.rotateAngleZ = (float) Math.toRadians(100 - (float) 150 * throttle.getAmount() - 15);
//        }
//
//        RandomiserControl randomizer = console.getControl(RandomiserControl.class);
//        if (randomizer != null)
//            this.randomiser.rotateAngleY = (float) Math.toRadians((randomizer.getAnimationTicks() * 36) + Minecraft.getInstance().getRenderPartialTicks() % 360.0);
//
//
//        all.render(scale);
//    }
//}