package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.entity.DoorRenderer;
import net.tardis.mod.client.renderers.exteriors.JapanExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

// Made with Blockbench 3.8.2
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class JapanInteriorModel extends AbstractInteriorDoorModel {
    private final ModelRenderer Posts;
    private final ModelRenderer cube_r1;
    private final ModelRenderer Signage;
    private final ModelRenderer panels;
    private final ModelRenderer door;
    private final ModelRenderer boti;

    public JapanInteriorModel() {
        textureWidth = 256;
        textureHeight = 256;

        Posts = new ModelRenderer(this);
        Posts.setRotationPoint(-12.0F, 20.0F, 10.0F);
        Posts.setTextureOffset(131, 31).addBox(22.0F, -37.0F, -18.0F, 3.0F, 41.0F, 3.0F, 0.0F, false);

        cube_r1 = new ModelRenderer(this);
        cube_r1.setRotationPoint(12.0F, 4.0F, -5.0F);
        Posts.addChild(cube_r1);
        setRotationAngle(cube_r1, 0.0F, 1.5708F, 0.0F);
        cube_r1.setTextureOffset(131, 31).addBox(10.0F, -41.0F, -13.0F, 3.0F, 41.0F, 3.0F, 0.0F, false);

        Signage = new ModelRenderer(this);
        Signage.setRotationPoint(14.0F, -18.0F, -14.0F);
        Signage.setTextureOffset(32, 78).addBox(-26.0F, 3.0F, 5.0F, 24.0F, 4.0F, 3.0F, 0.0F, false);

        panels = new ModelRenderer(this);
        panels.setRotationPoint(9.0F, 20.0F, -12.0F);
        panels.setTextureOffset(118, 87).addBox(-19.0F, -31.0F, 5.0F, 10.0F, 35.0F, 1.0F, 0.0F, false);

        door = new ModelRenderer(this);
        door.setRotationPoint(0.0F, 20.0F, 10.0F);
        door.setTextureOffset(96, 87).addBox(0.0F, -31.0F, -16.0F, 10.0F, 35.0F, 1.0F, 0.0F, false);

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, 0.0F);
        boti.setTextureOffset(209, 35).addBox(-11.0F, -35.0F, -9.0F, 22.0F, 35.0F, 1.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
    
    @Override
    public void renderBones(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        
        matrixStack.push();
        
        matrixStack.push();
        matrixStack.translate(EnumDoorType.JAPAN.getRotationForState(door.getOpenState()), 0, 0);
        this.renderDoorWhenClosed(door, matrixStack, buffer, packedLight, packedOverlay, this.door);
        matrixStack.pop();
        
        Posts.render(matrixStack, buffer, packedLight, packedOverlay);
        Signage.render(matrixStack, buffer, packedLight, packedOverlay);
        panels.render(matrixStack, buffer, packedLight, packedOverlay);
        
        matrixStack.pop();
    }

    @Override
    public void renderBoti(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        if(Minecraft.getInstance().world != null && door.getOpenState() != EnumDoorState.CLOSED){
            Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
                PortalInfo info = new PortalInfo();
                info.setWorldShell(data.getBotiWorld());
                info.setPosition(door.getPositionVec());

                //Translations
                info.setTranslate(matrix -> {
                    DoorRenderer.applyTranslations(matrix, door.rotationYaw - 180, door.getHorizontalFacing());
                    matrix.translate(0, 0.75, 0);
                    matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                });
                info.setTranslatePortal((matrix) -> {
                    matrix.translate(0, -0.75, 0);
                    matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(data.getBotiWorld().getPortalDirection())));
                    matrix.translate(-0.5, 0, -0.5);
                });

                //Renderers

                info.setRenderPortal((matrix, buf) -> {
                    matrix.push();
                    this.boti.render(matrix, buf.getBuffer(TRenderTypes.getTardis(this.getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });
                info.setRenderDoor((matrix, buf) -> {
                    matrix.push();
                    matrix.rotate(Vector3f.ZP.rotationDegrees(180));
                    matrix.translate(0, 1.5, 0);
                    matrix.scale(0.25F, 0.25F, 0.25F);
                    this.door.render(matrix, buf.getBuffer(RenderType.getEntityTranslucent(getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });
                BOTIRenderer.addPortal(info);
            });
        }
    }

    @Override
    public ResourceLocation getTexture() {
        return JapanExteriorRenderer.TEXTURE;
    }

    @Override
    public boolean doesDoorOpenIntoBotiWindow() {
        return true;
    }
    
    
}