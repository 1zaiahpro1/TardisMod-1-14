package net.tardis.mod.client.models.interiordoors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.entity.DoorRenderer;
import net.tardis.mod.client.renderers.exteriors.SteamExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.ConsoleTile;

public class SteamInteriorModel extends AbstractInteriorDoorModel{
    private final ModelRenderer glow_front_window;
    private final ModelRenderer door;
    private final ModelRenderer hinge;
    private final ModelRenderer inner_frame;
    private final ModelRenderer floorboard;
    private final ModelRenderer roof_front;
    private final ModelRenderer peak;
    private final ModelRenderer wroughtiron_front;
    private final ModelRenderer panel_front_top;
    private final ModelRenderer outer_frame;
    private final ModelRenderer boti;

    public SteamInteriorModel() {
        textureWidth = 1024;
        textureHeight = 1024;

        glow_front_window = new ModelRenderer(this);
        glow_front_window.setRotationPoint(0.0F, 23.0556F, 0.4167F);
        glow_front_window.setTextureOffset(516, 150).addBox(6.0F, -179.0556F, -18.1667F, 12.0F, 12.0F, 7.0F, 0.0F, false);
        glow_front_window.setTextureOffset(470, 146).addBox(-18.0F, -179.0556F, -18.1667F, 12.0F, 12.0F, 7.0F, 0.0F, false);

        door = new ModelRenderer(this);
        door.setRotationPoint(26.5F, -5.0F, -14.5F);
        door.setTextureOffset(163, 372).addBox(-50.5F, -3.0F, -2.25F, 48.0F, 20.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(258, 27).addBox(-50.5F, -123.0F, -1.25F, 48.0F, 140.0F, 0.0F, 0.0F, false);
        door.setTextureOffset(150, 269).addBox(-50.5F, -123.0F, -2.25F, 48.0F, 8.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(150, 269).addBox(-10.5F, -115.0F, -2.25F, 8.0F, 112.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(150, 269).addBox(-50.5F, -115.0F, -2.25F, 8.0F, 112.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(150, 269).addBox(-40.5F, -113.0F, -2.25F, 28.0F, 4.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(151, 370).addBox(-40.5F, -13.0F, -2.25F, 28.0F, 8.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(148, 283).addBox(-40.5F, -109.0F, -2.25F, 4.0F, 96.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(171, 289).addBox(-16.5F, -109.0F, -2.25F, 4.0F, 96.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(253, 291).addBox(-34.5F, -107.0F, -2.25F, 16.0F, 92.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(489, 45).addBox(-48.5F, -1.0F, -3.25F, 44.0F, 16.0F, 4.0F, 0.0F, false);
        door.setTextureOffset(498, 41).addBox(-48.5F, -68.0F, -4.25F, 4.0F, 16.0F, 8.0F, 0.0F, false);

        hinge = new ModelRenderer(this);
        hinge.setRotationPoint(-0.6532F, -56.6667F, 1.1099F);
        door.addChild(hinge);
        setRotationAngle(hinge, 0.0F, 0.7854F, 0.0F);
        hinge.setTextureOffset(521, 45).addBox(-2.0F, -59.3333F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        hinge.setTextureOffset(521, 45).addBox(-2.0F, -11.3333F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        hinge.setTextureOffset(521, 45).addBox(-2.0F, 52.6667F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);

        inner_frame = new ModelRenderer(this);
        inner_frame.setRotationPoint(0.0F, 23.0556F, 0.4167F);
        inner_frame.setTextureOffset(144, 391).addBox(-32.0F, -11.0556F, -18.1667F, 64.0F, 8.0F, 4.0F, 0.0F, false);
        inner_frame.setTextureOffset(143, 207).addBox(-32.0F, -183.0556F, -18.1667F, 8.0F, 172.0F, 4.0F, 0.0F, false);
        inner_frame.setTextureOffset(234, 212).addBox(24.0F, -183.0556F, -18.1667F, 8.0F, 172.0F, 4.0F, 0.0F, false);
        inner_frame.setTextureOffset(146, 265).addBox(-32.0F, -191.0556F, -18.1667F, 64.0F, 8.0F, 4.0F, 0.0F, false);
        inner_frame.setTextureOffset(150, 273).addBox(-24.0F, -163.0556F, -18.1667F, 48.0F, 12.0F, 4.0F, 0.0F, false);
        inner_frame.setTextureOffset(151, 282).addBox(-32.0F, -191.0556F, -20.1667F, 64.0F, 4.0F, 8.0F, 0.0F, false);
        inner_frame.setTextureOffset(459, 41).addBox(-20.0F, -161.0556F, -17.1667F, 40.0F, 8.0F, 4.0F, 0.0F, false);
        inner_frame.setTextureOffset(185, 215).addBox(-29.0F, -187.0556F, -17.1667F, 4.0F, 184.0F, 4.0F, 0.0F, false);
        inner_frame.setTextureOffset(269, 215).addBox(25.0F, -187.0556F, -17.1667F, 4.0F, 184.0F, 4.0F, 0.0F, false);
        inner_frame.setTextureOffset(144, 211).addBox(-32.0F, -187.0556F, -20.1667F, 4.0F, 184.0F, 8.0F, 0.0F, false);
        inner_frame.setTextureOffset(279, 213).addBox(28.0F, -187.0556F, -20.1667F, 4.0F, 184.0F, 8.0F, 0.0F, false);
        inner_frame.setTextureOffset(143, 280).addBox(-28.0F, -159.0556F, -20.1667F, 56.0F, 4.0F, 4.0F, 0.0F, false);

        floorboard = new ModelRenderer(this);
        floorboard.setRotationPoint(0.0F, 0.0F, 0.0F);
        inner_frame.addChild(floorboard);
        floorboard.setTextureOffset(238, 389).addBox(-32.0F, -3.0556F, -25.1667F, 64.0F, 4.0F, 16.0F, 0.0F, false);

        roof_front = new ModelRenderer(this);
        roof_front.setRotationPoint(0.0F, -47.0F, -12.0F);
        inner_frame.addChild(roof_front);
        

        peak = new ModelRenderer(this);
        peak.setRotationPoint(0.0F, 0.0F, 4.0F);
        roof_front.addChild(peak);
        

        wroughtiron_front = new ModelRenderer(this);
        wroughtiron_front.setRotationPoint(0.0F, 47.9444F, 11.5833F);
        roof_front.addChild(wroughtiron_front);
        

        panel_front_top = new ModelRenderer(this);
        panel_front_top.setRotationPoint(0.0F, 0.0F, 0.0F);
        inner_frame.addChild(panel_front_top);
        panel_front_top.setTextureOffset(78, 223).addBox(-24.0F, -183.0556F, -17.1667F, 48.0F, 20.0F, 4.0F, 0.0F, false);

        outer_frame = new ModelRenderer(this);
        outer_frame.setRotationPoint(0.0F, 24.0F, 0.0F);
        outer_frame.setTextureOffset(64, 205).addBox(-40.0F, -192.0F, -17.75F, 8.0F, 192.0F, 12.0F, 0.0F, false);
        outer_frame.setTextureOffset(45, 205).addBox(32.0F, -192.0F, -17.75F, 8.0F, 192.0F, 12.0F, 0.0F, false);
        outer_frame.setTextureOffset(91, 256).addBox(-40.0F, -200.0F, -17.75F, 80.0F, 8.0F, 12.0F, 0.0F, false);

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, 0.0F);
        boti.setTextureOffset(12, 14).addBox(-36.0F, -164.0F, -23.75F, 72.0F, 160.0F, 8.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
        matrixStack.push();
        this.translateModel(matrixStack);
        
        this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.STEAM.getRotationForState(door.getOpenState()));
        
        glow_front_window.render(matrixStack, buffer, packedLight, packedOverlay);
        this.door.render(matrixStack, buffer, packedLight, packedOverlay);
        inner_frame.render(matrixStack, buffer, packedLight, packedOverlay);
        outer_frame.render(matrixStack, buffer, packedLight, packedOverlay);
        //boti.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();

    }

    @Override
    public void renderBoti(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        if(Minecraft.getInstance().world != null && door.getOpenState() != EnumDoorState.CLOSED){
            Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data ->{

                PortalInfo info = new PortalInfo();
                info.setPosition(door.getPositionVec());
                info.setWorldShell(data.getBotiWorld());

                //Translators
                info.setTranslate(matrix -> {
                    DoorRenderer.applyTranslations(matrix, door);
                });
                info.setTranslatePortal(matrix -> {
                    matrix.translate(0, 1.25, 0.1);
                    matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                    matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(data.getBotiWorld().getPortalDirection())));
                    matrix.translate(-0.5, 0, -0.5);
                });

                //Renderers
                info.setRenderPortal((matrix, buf) -> {
                    matrix.push();
                    this.translateModel(matrix);
                    matrix.translate(0, 0, -0.03);
                    this.boti.render(matrix, buf.getBuffer(RenderType.getEntityCutout(this.getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });

                BOTIRenderer.addPortal(info);
            });
        }
    }

    @Override
    public ResourceLocation getTexture() {
        ConsoleTile tile = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
        if (tile != null) {
            int index = tile.getExteriorManager().getExteriorVariant();
            if (tile.getExteriorType().getVariants() != null && index < tile.getExteriorType().getVariants().length)
                return tile.getExteriorType().getVariants()[index].getTexture();
        }
        return SteamExteriorRenderer.TEXTURE;
    }

    public void translateModel(MatrixStack stack) {
        stack.translate(0, 1.12, -0.2);
        stack.scale(0.25F, 0.25F, 0.25F);
    }
}