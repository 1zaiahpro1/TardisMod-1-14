package net.tardis.mod.client.models.entity.dalek;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;

public class DalekModel extends EntityModel<DalekEntity> implements IDalekModel {
    private final ModelRenderer deco_dalek;
    private final ModelRenderer dome;
    private final ModelRenderer cap;
    private final ModelRenderer stalk_frame;
    private final ModelRenderer eyestalk;
    private final LightModelRenderer glow_eye;
    private final ModelRenderer lamp_l;
    private final ModelRenderer lamp_r;
    private final LightModelRenderer glow_headlamp;
    private final ModelRenderer lamp_l2;
    private final ModelRenderer lamp_r2;
    private final ModelRenderer neck;
    private final ModelRenderer ring;
    private final ModelRenderer ring2;
    private final ModelRenderer ring3;
    private final ModelRenderer grill;
    private final ModelRenderer struts;
    private final ModelRenderer shoulders;
    private final ModelRenderer plats;
    private final ModelRenderer ring4;
    private final ModelRenderer ring5;
    private final ModelRenderer bone;
    private final ModelRenderer mount;
    private final ModelRenderer bone4;
    private final ModelRenderer bone17;
    private final ModelRenderer laser;
    private final ModelRenderer plunger;
    private final ModelRenderer cup;
    private final ModelRenderer skirt;
    private final ModelRenderer bone5;
    private final ModelRenderer bone6;
    private final ModelRenderer bone10;
    private final ModelRenderer bone3;
    private final ModelRenderer bone8;
    private final ModelRenderer bone7;
    private final ModelRenderer bone9;
    private final ModelRenderer bone11;
    private final ModelRenderer bone13;
    private final ModelRenderer bone12;
    private final ModelRenderer bone14;
    private final ModelRenderer bone15;
    private final ModelRenderer bone16;
    private final ModelRenderer bone2;
    private final LightModelRenderer glow_hover;
    private final ModelRenderer bone18;
    private DalekEntity dalek;

    public DalekModel() {
        textureWidth = 128;
        textureHeight = 128;

        deco_dalek = new ModelRenderer(this);
        deco_dalek.setRotationPoint(0.0F, 23.5F, 0.0F);


        dome = new ModelRenderer(this);
        dome.setRotationPoint(-0.0625F, -29.2917F, 3.5226F);
        deco_dalek.addChild(dome);


        cap = new ModelRenderer(this);
        cap.setRotationPoint(0.0625F, 28.2917F, -1.5226F);
        dome.addChild(cap);
        cap.setTextureOffset(14, 61).addBox(-5.0F, -29.75F, -2.5F, 10.0F, 2.0F, 9.0F, 0.0F, false);
        cap.setTextureOffset(14, 61).addBox(-4.5F, -31.0F, -1.5F, 9.0F, 2.0F, 7.0F, 0.0F, false);
        cap.setTextureOffset(14, 61).addBox(-4.0F, -30.75F, -3.0F, 8.0F, 3.0F, 10.0F, 0.0F, false);
        cap.setTextureOffset(16, 60).addBox(-3.0F, -32.5F, -1.5F, 6.0F, 2.0F, 7.0F, 0.0F, false);
        cap.setTextureOffset(14, 61).addBox(-3.5F, -31.75F, -2.5F, 7.0F, 1.0F, 9.0F, 0.0F, false);

        stalk_frame = new ModelRenderer(this);
        stalk_frame.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap.addChild(stalk_frame);
        stalk_frame.setTextureOffset(30, 105).addBox(1.0F, -32.25F, -3.5F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        stalk_frame.setTextureOffset(30, 105).addBox(-2.0F, -32.25F, -3.5F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        stalk_frame.setTextureOffset(30, 105).addBox(-1.0F, -29.25F, -3.5F, 2.0F, 1.0F, 3.0F, 0.0F, false);

        eyestalk = new ModelRenderer(this);
        eyestalk.setRotationPoint(0.0625F, -3.4583F, -4.2726F);
        dome.addChild(eyestalk);
        eyestalk.setTextureOffset(108, 99).addBox(-0.5F, -0.25F, -8.25F, 1.0F, 1.0F, 8.0F, 0.0F, false);
        eyestalk.setTextureOffset(68, 83).addBox(-1.0F, -0.75F, -1.25F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        eyestalk.setTextureOffset(68, 83).addBox(-0.5F, -1.0F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);
        eyestalk.setTextureOffset(68, 83).addBox(-1.0F, -0.75F, -2.4063F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        eyestalk.setTextureOffset(68, 83).addBox(-1.0F, -0.75F, -8.3438F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        eyestalk.setTextureOffset(68, 83).addBox(-1.5F, -1.25F, -8.0938F, 3.0F, 3.0F, 1.0F, 0.0F, false);
        eyestalk.setTextureOffset(68, 83).addBox(-1.0F, -0.75F, -4.25F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        eyestalk.setTextureOffset(68, 83).addBox(-1.0F, -0.75F, -4.75F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        eyestalk.setTextureOffset(68, 83).addBox(-1.0F, -0.75F, -5.25F, 2.0F, 2.0F, 0.0F, 0.0F, false);

        glow_eye = new LightModelRenderer(this);
        glow_eye.setRotationPoint(0.0F, 33.25F, 0.75F);
        eyestalk.addChild(glow_eye);
        glow_eye.setTextureOffset(72, 20).addBox(-0.5F, -33.5F, -9.25F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        lamp_l = new ModelRenderer(this);
        lamp_l.setRotationPoint(-3.3451F, -2.6517F, 0.4774F);
        dome.addChild(lamp_l);
        setRotationAngle(lamp_l, 0.0F, 0.0F, 0.6981F);
        lamp_l.setTextureOffset(10, 62).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        lamp_r = new ModelRenderer(this);
        lamp_r.setRotationPoint(3.4701F, -2.6517F, 0.4774F);
        dome.addChild(lamp_r);
        setRotationAngle(lamp_r, 0.0F, 0.0F, -0.6981F);
        lamp_r.setTextureOffset(10, 62).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        glow_headlamp = new LightModelRenderer(this);
        glow_headlamp.setRotationPoint(0.0625F, 29.7917F, -3.5226F);
        dome.addChild(glow_headlamp);


        lamp_l2 = new ModelRenderer(this);
        lamp_l2.setRotationPoint(-3.4076F, -32.4434F, 4.0F);
        glow_headlamp.addChild(lamp_l2);
        setRotationAngle(lamp_l2, 0.0F, 0.0F, 0.6981F);
        lamp_l2.setTextureOffset(103, 11).addBox(-2.25F, -0.5F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

        lamp_r2 = new ModelRenderer(this);
        lamp_r2.setRotationPoint(3.4076F, -32.4434F, 4.0F);
        glow_headlamp.addChild(lamp_r2);
        setRotationAngle(lamp_r2, 0.0F, 0.0F, -0.6981F);
        lamp_r2.setTextureOffset(104, 13).addBox(0.25F, -0.5F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

        neck = new ModelRenderer(this);
        neck.setRotationPoint(0.0F, 0.0F, 2.0F);
        deco_dalek.addChild(neck);


        ring = new ModelRenderer(this);
        ring.setRotationPoint(0.0F, -2.5F, 0.0F);
        neck.addChild(ring);
        ring.setTextureOffset(85, 52).addBox(1.5F, -26.25F, -2.0F, 4.0F, 1.0F, 8.0F, 0.0F, false);
        ring.setTextureOffset(85, 52).addBox(-5.5F, -26.25F, -2.0F, 4.0F, 1.0F, 8.0F, 0.0F, false);
        ring.setTextureOffset(85, 52).addBox(-5.0938F, -26.25F, 6.0F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        ring.setTextureOffset(85, 52).addBox(-5.0938F, -26.25F, -3.0F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        ring.setTextureOffset(89, 56).addBox(-4.0F, -26.25F, -4.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        ring.setTextureOffset(84, 58).addBox(-4.0F, -26.25F, 7.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);

        ring2 = new ModelRenderer(this);
        ring2.setRotationPoint(0.0F, -0.75F, 0.0F);
        neck.addChild(ring2);
        ring2.setTextureOffset(90, 53).addBox(4.5F, -26.5F, -2.0F, 1.0F, 1.0F, 8.0F, 0.0F, false);
        ring2.setTextureOffset(85, 52).addBox(-5.5F, -26.5F, -2.0F, 1.0F, 1.0F, 8.0F, 0.0F, false);
        ring2.setTextureOffset(85, 58).addBox(-5.0938F, -26.5F, 6.0F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        ring2.setTextureOffset(87, 59).addBox(-5.0938F, -26.5F, -3.0F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        ring2.setTextureOffset(87, 59).addBox(-4.0F, -26.5F, -4.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        ring2.setTextureOffset(89, 58).addBox(-4.0F, -26.5F, 7.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);

        ring3 = new ModelRenderer(this);
        ring3.setRotationPoint(0.0F, 1.0313F, 0.0F);
        neck.addChild(ring3);
        ring3.setTextureOffset(85, 56).addBox(4.5F, -26.5F, -2.0F, 1.0F, 1.0F, 8.0F, 0.0F, false);
        ring3.setTextureOffset(85, 53).addBox(-5.5F, -26.5F, -2.0F, 1.0F, 1.0F, 8.0F, 0.0F, false);
        ring3.setTextureOffset(88, 60).addBox(-5.0938F, -26.5F, 6.0F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        ring3.setTextureOffset(90, 60).addBox(-5.0938F, -26.5F, -3.0F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        ring3.setTextureOffset(87, 60).addBox(-4.0F, -26.5F, -4.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        ring3.setTextureOffset(88, 61).addBox(-4.0F, -26.5F, 7.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);

        grill = new ModelRenderer(this);
        grill.setRotationPoint(0.0F, -0.25F, 0.0F);
        neck.addChild(grill);
        grill.setTextureOffset(69, 76).addBox(-5.0F, -27.75F, -1.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        grill.setTextureOffset(69, 76).addBox(4.0F, -27.75F, -1.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        grill.setTextureOffset(69, 76).addBox(3.5F, -27.75F, 4.5F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        grill.setTextureOffset(69, 76).addBox(3.5F, -27.75F, -2.5F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        grill.setTextureOffset(69, 76).addBox(-4.5F, -27.75F, -2.5F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        grill.setTextureOffset(69, 76).addBox(-4.5F, -27.75F, 4.5F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        grill.setTextureOffset(69, 76).addBox(-3.5F, -27.75F, 6.25F, 7.0F, 4.0F, 1.0F, 0.0F, false);
        grill.setTextureOffset(69, 76).addBox(-3.5F, -27.75F, -3.25F, 7.0F, 4.0F, 1.0F, 0.0F, false);

        struts = new ModelRenderer(this);
        struts.setRotationPoint(0.0F, -0.25F, 0.0F);
        neck.addChild(struts);
        struts.setTextureOffset(98, 61).addBox(-2.75F, -27.75F, -3.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        struts.setTextureOffset(98, 61).addBox(-2.75F, -27.75F, 6.6563F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        struts.setTextureOffset(98, 61).addBox(-5.25F, -27.75F, 3.6563F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        struts.setTextureOffset(98, 61).addBox(-5.25F, -27.75F, -0.3438F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        struts.setTextureOffset(98, 61).addBox(4.25F, -27.75F, -0.3438F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        struts.setTextureOffset(98, 61).addBox(4.25F, -27.75F, 3.1563F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        struts.setTextureOffset(98, 61).addBox(1.75F, -27.75F, -3.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        struts.setTextureOffset(98, 61).addBox(1.75F, -27.75F, 6.75F, 1.0F, 4.0F, 1.0F, 0.0F, false);

        shoulders = new ModelRenderer(this);
        shoulders.setRotationPoint(0.0F, -18.0F, 4.25F);
        deco_dalek.addChild(shoulders);


        plats = new ModelRenderer(this);
        plats.setRotationPoint(0.0F, 21.25F, -2.25F);
        shoulders.addChild(plats);
        plats.setTextureOffset(119, 61).addBox(-0.5F, -26.75F, 7.25F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-2.25F, -26.75F, -4.25F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(1.25F, -26.75F, -4.25F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-0.5F, -26.75F, -4.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-2.5F, -26.75F, 7.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-4.25F, -26.75F, 7.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-5.75F, -26.75F, 5.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(3.25F, -26.75F, 7.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(4.75F, -26.75F, 5.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(1.5F, -26.75F, 7.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-5.75F, -26.75F, -2.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(4.75F, -26.75F, -2.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-5.75F, -26.75F, 1.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(4.75F, -26.75F, 1.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-5.75F, -26.75F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(4.75F, -26.75F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(-5.75F, -26.75F, 3.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        plats.setTextureOffset(119, 61).addBox(4.75F, -26.75F, 3.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

        ring4 = new ModelRenderer(this);
        ring4.setRotationPoint(0.0F, 20.25F, -2.25F);
        shoulders.addChild(ring4);
        ring4.setTextureOffset(22, 112).addBox(4.5F, -26.25F, -2.0F, 1.0F, 4.0F, 8.0F, 0.0F, false);
        ring4.setTextureOffset(22, 112).addBox(-5.5F, -26.25F, -2.0F, 1.0F, 4.0F, 8.0F, 0.0F, false);
        ring4.setTextureOffset(22, 112).addBox(-5.0938F, -26.25F, 6.0F, 10.0F, 4.0F, 1.0F, 0.0F, false);
        ring4.setTextureOffset(22, 112).addBox(-5.0938F, -26.25F, -3.0F, 10.0F, 4.0F, 1.0F, 0.0F, false);
        ring4.setTextureOffset(22, 112).addBox(-4.0F, -26.25F, -4.0F, 8.0F, 4.0F, 1.0F, 0.0F, false);
        ring4.setTextureOffset(22, 112).addBox(-4.0F, -26.25F, 7.0F, 8.0F, 4.0F, 1.0F, 0.0F, false);

        ring5 = new ModelRenderer(this);
        ring5.setRotationPoint(0.0F, 24.25F, -2.25F);
        shoulders.addChild(ring5);


        bone = new ModelRenderer(this);
        bone.setRotationPoint(0.0F, 0.0F, 0.0F);
        ring5.addChild(bone);


        mount = new ModelRenderer(this);
        mount.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone.addChild(mount);
        mount.setTextureOffset(69, 83).addBox(-5.0313F, -28.75F, -6.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        mount.setTextureOffset(71, 82).addBox(2.9688F, -28.75F, -6.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        bone4 = new ModelRenderer(this);
        bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone.addChild(bone4);
        bone4.setTextureOffset(25, 86).addBox(-3.0F, -26.25F, 8.0F, 6.0F, 14.0F, 1.0F, 0.0F, false);
        bone4.setTextureOffset(26, 58).addBox(1.0F, -26.5F, 7.25F, 1.0F, 14.0F, 2.0F, 0.0F, false);
        bone4.setTextureOffset(26, 58).addBox(-2.0F, -26.5F, 7.25F, 1.0F, 14.0F, 2.0F, 0.0F, false);
        bone4.setTextureOffset(25, 86).addBox(5.5F, -26.25F, -2.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        bone4.setTextureOffset(25, 86).addBox(-6.5F, -26.25F, -2.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        bone4.setTextureOffset(25, 86).addBox(-5.5938F, -26.25F, 5.25F, 11.0F, 15.0F, 3.0F, 0.0F, false);
        bone4.setTextureOffset(25, 86).addBox(-6.0938F, -25.25F, -4.0F, 12.0F, 5.0F, 2.0F, 0.0F, false);
        bone4.setTextureOffset(26, 116).addBox(2.4688F, -29.25F, -5.5F, 3.0F, 7.0F, 4.0F, 0.0F, false);
        bone4.setTextureOffset(24, 117).addBox(-5.5F, -29.25F, -5.5F, 3.0F, 7.0F, 4.0F, 0.0F, false);
        bone4.setTextureOffset(25, 86).addBox(-4.0F, -26.25F, -5.0F, 8.0F, 6.0F, 1.0F, 0.0F, false);

        bone17 = new ModelRenderer(this);
        bone17.setRotationPoint(-1.0F, 1.0F, -1.5F);
        bone4.addChild(bone17);
        bone17.setTextureOffset(95, 34).addBox(1.5F, -26.25F, 9.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone17.setTextureOffset(95, 34).addBox(-1.5F, -26.25F, 9.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone17.setTextureOffset(95, 34).addBox(1.5F, -23.25F, 9.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone17.setTextureOffset(95, 34).addBox(-1.5F, -23.25F, 9.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone17.setTextureOffset(95, 34).addBox(1.5F, -20.25F, 9.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone17.setTextureOffset(95, 34).addBox(-1.5F, -20.25F, 9.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone17.setTextureOffset(95, 34).addBox(1.5F, -17.25F, 9.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone17.setTextureOffset(95, 34).addBox(-1.5F, -17.25F, 9.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        laser = new ModelRenderer(this);
        laser.setRotationPoint(3.9688F, -27.75F, -5.3333F);
        ring5.addChild(laser);
        laser.setTextureOffset(110, 100).addBox(-0.5F, -0.5F, -7.1667F, 1.0F, 1.0F, 7.0F, 0.0F, false);
        laser.setTextureOffset(114, 101).addBox(0.0F, -1.0F, -6.4167F, 0.0F, 2.0F, 6.0F, 0.0F, false);
        laser.setTextureOffset(109, 102).addBox(-1.0F, 0.0F, -6.4167F, 2.0F, 0.0F, 6.0F, 0.0F, false);

        plunger = new ModelRenderer(this);
        plunger.setRotationPoint(-3.9844F, -27.8438F, -5.5F);
        ring5.addChild(plunger);
        plunger.setTextureOffset(68, 100).addBox(-0.5469F, -0.4063F, -7.0F, 1.0F, 1.0F, 7.0F, 0.0F, false);

        cup = new ModelRenderer(this);
        cup.setRotationPoint(3.9844F, 27.8438F, 5.5F);
        plunger.addChild(cup);
        cup.setTextureOffset(70, 84).addBox(-4.5313F, -28.25F, -13.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        cup.setTextureOffset(70, 84).addBox(-5.0625F, -28.8438F, -14.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        cup.setTextureOffset(70, 84).addBox(-5.4063F, -28.8438F, -14.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        cup.setTextureOffset(70, 84).addBox(-5.0313F, -29.25F, -14.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        cup.setTextureOffset(70, 84).addBox(-3.6563F, -28.8438F, -14.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        cup.setTextureOffset(70, 84).addBox(-5.0313F, -27.5F, -14.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

        skirt = new ModelRenderer(this);
        skirt.setRotationPoint(0.0F, -4.25F, -4.0F);
        deco_dalek.addChild(skirt);
        setRotationAngle(skirt, -0.1745F, 0.0F, 0.0F);
        skirt.setTextureOffset(18, 89).addBox(-7.375F, -15.2818F, 2.8845F, 3.0F, 14.0F, 8.0F, 0.0F, false);
        skirt.setTextureOffset(17, 91).addBox(4.25F, -15.2818F, 2.8845F, 3.0F, 14.0F, 8.0F, 0.0F, false);

        bone5 = new ModelRenderer(this);
        bone5.setRotationPoint(0.0F, 13.4057F, 1.9858F);
        skirt.addChild(bone5);
        setRotationAngle(bone5, -0.0873F, 0.0F, 0.0F);
        bone5.setTextureOffset(16, 91).addBox(-3.0F, -26.0615F, -9.6835F, 6.0F, 15.0F, 7.0F, 0.0F, false);
        bone5.setTextureOffset(68, 81).addBox(-3.5F, -11.5615F, -10.1835F, 7.0F, 1.0F, 6.0F, 0.0F, false);
        bone5.setTextureOffset(69, 81).addBox(-3.0F, -11.0615F, -10.6835F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone5.setTextureOffset(69, 81).addBox(3.0F, -11.0615F, -10.6835F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        bone5.setTextureOffset(69, 81).addBox(-4.0F, -11.0615F, -10.6835F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        bone5.setTextureOffset(28, 60).addBox(-2.0F, -26.0615F, -9.9335F, 1.0F, 15.0F, 1.0F, 0.0F, false);
        bone5.setTextureOffset(28, 60).addBox(1.0F, -26.0615F, -9.9335F, 1.0F, 15.0F, 1.0F, 0.0F, false);
        bone5.setTextureOffset(9, 113).addBox(-7.0F, -15.7993F, -2.9362F, 14.0F, 3.0F, 9.0F, 0.0F, false);

        bone6 = new ModelRenderer(this);
        bone6.setRotationPoint(-1.5F, 0.0F, 0.0F);
        bone5.addChild(bone6);
        bone6.setTextureOffset(96, 35).addBox(-1.0F, -14.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone6.setTextureOffset(96, 35).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone6.setTextureOffset(96, 35).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone6.setTextureOffset(96, 35).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        bone10 = new ModelRenderer(this);
        bone10.setRotationPoint(1.25F, 0.0F, 0.0F);
        bone5.addChild(bone10);
        bone10.setTextureOffset(96, 35).addBox(-0.75F, -14.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone10.setTextureOffset(96, 35).addBox(-0.75F, -17.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone10.setTextureOffset(96, 35).addBox(-0.75F, -20.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone10.setTextureOffset(96, 35).addBox(-0.75F, -23.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        bone3 = new ModelRenderer(this);
        bone3.setRotationPoint(0.0F, 14.0391F, -0.8156F);
        skirt.addChild(bone3);
        setRotationAngle(bone3, -0.6109F, 0.0F, 0.0F);
        bone3.setTextureOffset(68, 79).addBox(-1.0F, -10.3964F, -10.8536F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        bone3.setTextureOffset(68, 79).addBox(-9.0F, -21.224F, -0.1144F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        bone3.setTextureOffset(69, 79).addBox(7.0F, -21.224F, -0.1144F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        bone8 = new ModelRenderer(this);
        bone8.setRotationPoint(0.0F, 11.75F, 4.5F);
        skirt.addChild(bone8);
        bone8.setTextureOffset(18, 84).addBox(-4.25F, -25.75F, -8.5F, 2.0F, 15.0F, 12.0F, 0.0F, false);
        bone8.setTextureOffset(18, 84).addBox(2.25F, -25.75F, -8.5F, 2.0F, 15.0F, 12.0F, 0.0F, false);
        bone8.setTextureOffset(18, 91).addBox(-8.25F, -23.5903F, -6.5304F, 4.0F, 13.0F, 8.0F, 0.0F, false);
        bone8.setTextureOffset(69, 73).addBox(-8.5625F, -10.8403F, -7.2804F, 2.0F, 1.0F, 13.0F, 0.0F, false);
        bone8.setTextureOffset(69, 73).addBox(6.4375F, -10.8403F, -7.2804F, 2.0F, 1.0F, 13.0F, 0.0F, false);
        bone8.setTextureOffset(68, 82).addBox(-6.5625F, -10.8403F, -9.2804F, 13.0F, 1.0F, 3.0F, 0.0F, false);
        bone8.setTextureOffset(18, 89).addBox(3.25F, -23.5903F, -6.5304F, 5.0F, 13.0F, 8.0F, 0.0F, false);

        bone7 = new ModelRenderer(this);
        bone7.setRotationPoint(6.75F, 1.3039F, 3.125F);
        bone8.addChild(bone7);
        bone7.setTextureOffset(96, 35).addBox(-1.0F, -14.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone7.setTextureOffset(96, 35).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone7.setTextureOffset(96, 35).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone7.setTextureOffset(96, 35).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        bone9 = new ModelRenderer(this);
        bone9.setRotationPoint(-6.75F, 1.3039F, 3.125F);
        bone8.addChild(bone9);
        bone9.setTextureOffset(96, 35).addBox(-1.0F, -14.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone9.setTextureOffset(96, 35).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone9.setTextureOffset(96, 35).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        bone9.setTextureOffset(96, 35).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        bone11 = new ModelRenderer(this);
        bone11.setRotationPoint(-7.75F, 1.2385F, 4.9135F);
        bone8.addChild(bone11);
        bone11.setTextureOffset(97, 34).addBox(-1.0F, -14.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone11.setTextureOffset(97, 34).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone11.setTextureOffset(97, 34).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone11.setTextureOffset(97, 34).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        bone13 = new ModelRenderer(this);
        bone13.setRotationPoint(7.75F, 1.2385F, 4.9135F);
        bone8.addChild(bone13);
        bone13.setTextureOffset(93, 34).addBox(-1.0F, -14.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone13.setTextureOffset(93, 34).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone13.setTextureOffset(93, 34).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone13.setTextureOffset(93, 34).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        bone12 = new ModelRenderer(this);
        bone12.setRotationPoint(-7.75F, 1.1873F, 8.2226F);
        bone8.addChild(bone12);
        bone12.setTextureOffset(97, 34).addBox(-1.0F, -14.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone12.setTextureOffset(97, 34).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone12.setTextureOffset(97, 34).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone12.setTextureOffset(97, 34).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        bone14 = new ModelRenderer(this);
        bone14.setRotationPoint(7.75F, 1.1873F, 8.2226F);
        bone8.addChild(bone14);
        bone14.setTextureOffset(93, 34).addBox(-1.0F, -14.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone14.setTextureOffset(93, 34).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone14.setTextureOffset(93, 34).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone14.setTextureOffset(93, 34).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        bone15 = new ModelRenderer(this);
        bone15.setRotationPoint(6.75F, -2.0139F, 13.1027F);
        bone8.addChild(bone15);
        bone15.setTextureOffset(93, 34).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone15.setTextureOffset(93, 34).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone15.setTextureOffset(93, 34).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        bone16 = new ModelRenderer(this);
        bone16.setRotationPoint(-6.75F, -2.0139F, 13.1027F);
        bone8.addChild(bone16);
        bone16.setTextureOffset(97, 34).addBox(-1.0F, -17.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone16.setTextureOffset(97, 34).addBox(-1.0F, -20.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        bone16.setTextureOffset(97, 34).addBox(-1.0F, -23.6174F, -10.158F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        bone2 = new ModelRenderer(this);
        bone2.setRotationPoint(0.5F, -15.2385F, -1.8017F);
        skirt.addChild(bone2);
        setRotationAngle(bone2, 0.6109F, 0.0F, 0.0F);
        bone2.setTextureOffset(18, 84).addBox(-3.0F, -0.5523F, -3.9931F, 5.0F, 3.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(17, 56).addBox(-2.5F, -0.713F, -4.1846F, 1.0F, 3.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(17, 56).addBox(0.5F, -0.713F, -4.1846F, 1.0F, 3.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(18, 84).addBox(-4.625F, -0.2463F, -2.5111F, 2.0F, 3.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(18, 84).addBox(1.625F, -0.2463F, -2.5111F, 2.0F, 3.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(19, 88).addBox(3.625F, 2.6525F, -2.1365F, 3.0F, 5.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(19, 88).addBox(-7.75F, 2.6525F, -2.1365F, 3.0F, 5.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(26, 82).addBox(-7.9688F, 15.6525F, 3.7385F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        bone2.setTextureOffset(24, 80).addBox(4.625F, 15.6525F, 3.7385F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        bone2.setTextureOffset(117, 100).addBox(-7.4688F, 9.9025F, 6.4885F, 1.0F, 6.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(118, 99).addBox(5.2813F, 9.9025F, 6.4885F, 1.0F, 6.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(18, 84).addBox(4.5313F, 10.1525F, 5.7385F, 2.0F, 3.0F, 2.0F, 0.0F, false);
        bone2.setTextureOffset(18, 84).addBox(-7.6563F, 10.1525F, 5.7385F, 2.0F, 3.0F, 2.0F, 0.0F, false);
        bone2.setTextureOffset(30, 107).addBox(-8.875F, 15.9477F, -0.9931F, 2.0F, 5.0F, 8.0F, 0.0F, false);
        bone2.setTextureOffset(51, 110).addBox(-9.875F, 16.9477F, 0.0069F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(21, 108).addBox(5.7813F, 15.9477F, -0.9931F, 2.0F, 5.0F, 8.0F, 0.0F, false);
        bone2.setTextureOffset(1, 110).addBox(7.75F, 16.9477F, 0.0069F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(69, 81).addBox(-7.0F, 16.1977F, 5.5069F, 14.0F, 4.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(68, 83).addBox(-9.5F, 17.6977F, 5.5069F, 18.0F, 1.0F, 2.0F, 0.0F, false);
        bone2.setTextureOffset(11, 117).addBox(-8.5F, 15.4477F, 0.0069F, 16.0F, 4.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(11, 117).addBox(-8.5F, 15.4477F, 0.0069F, 16.0F, 4.0F, 6.0F, 0.0F, false);

        glow_hover = new LightModelRenderer(this);
        glow_hover.setRotationPoint(0.0F, 4.75F, 4.0F);
        skirt.addChild(glow_hover);


        bone18 = new ModelRenderer(this);
        bone18.setRotationPoint(0.0F, -0.2109F, -0.0656F);
        glow_hover.addChild(bone18);
        setRotationAngle(bone18, -0.7854F, 0.0F, 0.0F);
        bone18.setTextureOffset(68, 18).addBox(-8.5F, -9.38F, 2.4766F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        bone18.setTextureOffset(68, 18).addBox(7.5F, -9.38F, 2.4766F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        bone18.setTextureOffset(68, 16).addBox(-0.5F, 1.4034F, -9.3068F, 1.0F, 4.0F, 4.0F, 0.0F, false);
    }

    @Override
    public void setRotationAngles(DalekEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        this.eyestalk.rotateAngleX = headPitch * ((float) Math.PI / 180F);
        this.dome.rotateAngleY = netHeadYaw * ((float) Math.PI / 180F);

   /*     if (entity.world.rand.nextInt(50) == 5) {
            this.plunger.rotateAngleY = (float) Math.toDegrees(entity.world.rand.nextFloat() * entity.world.rand.nextInt(3) /  ((float) Math.PI / 180F));
            this.plunger.rotateAngleZ = (float) Math.toDegrees(entity.world.rand.nextFloat() * entity.world.rand.nextInt(3) / ((float) Math.PI / 180F));
            this.laser.rotateAngleZ = (float) Math.toDegrees(entity.world.rand.nextFloat() * entity.world.rand.nextInt(3)/ ((float) Math.PI / 180F));
            this.laser.rotateAngleY = (float) Math.toDegrees(entity.world.rand.nextFloat() * entity.world.rand.nextInt(3)/ ((float) Math.PI / 180F));
        }*/

        float motion = 1.0F;

        motion = (float) entity.getMotion().lengthSquared();
        motion = motion / 0.2F;
        motion = motion * motion * motion;

        if (motion < 1.0F) {
            motion = 1.0F;
        }

        this.laser.rotateAngleY = (MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 2.0F * limbSwingAmount * 0.5F / motion) / 2;
        this.laser.rotateAngleX = (MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 2.0F * limbSwingAmount * 0.5F / motion) / 2;

        this.plunger.rotateAngleY = -(MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 2.0F * limbSwingAmount * 0.5F / motion) / 1.4F;
        this.plunger.rotateAngleX = -(MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 2.0F * limbSwingAmount * 0.5F / motion) / 1.2F;
    }


    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        float light = 1.0F;
        DalekEntity dalekEntity = getDalekEntity();
        boolean isRusty = dalekEntity.getDalekType().getRegistryName().toString().equals(TardisConstants.DalekTypes.DALEK_RUSTY.toString());
        if(isRusty && dalekEntity.world.rand.nextBoolean()){
            light = getDalekEntity().world.rand.nextFloat();
        }

        this.glow_eye.setBright(light);
        this.glow_headlamp.setBright(light);
        this.glow_hover.setBright(light);
        deco_dalek.render(matrixStack, buffer, packedLight, packedOverlay);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void setDalek(DalekEntity dalek) {
        this.dalek = dalek;
    }

    @Override
    public DalekEntity getDalekEntity() {
        return dalek;
    }

    @Override
    public ModelRenderer getLaser() {
        return laser;
    }
}