package net.tardis.mod.client.models.consoles;

import java.util.function.Function;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.controls.CommunicatorControl;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.TelepathicControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.tileentities.consoles.SteamConsoleTile;

public class SteamConsoleModel extends AbstractConsoleRenderTypedModel<SteamConsoleTile> {
    private final LightModelRenderer glow;
    private final LightModelRenderer glow_meterglass_a1;
    private final LightModelRenderer glow_lamp_a1;
    private final LightModelRenderer glow_lamp_a2;
    private final LightModelRenderer glow_glass_b1;
    private final LightModelRenderer glow_glass_b2;
    private final LightModelRenderer glow_lamp_c1;
    private final LightModelRenderer glow_lamp_c2;
    private final LightModelRenderer glow_panel_e;
    private final LightModelRenderer glow_glass_e2;
    private final LightModelRenderer glow_glass_e3;
    private final LightModelRenderer glow_glass_e1;
    private final LightModelRenderer redlamp_e3;
    private final LightModelRenderer redlamp_e4;
    private final LightModelRenderer glow_core;
    private final LightModelRenderer glow_radio;
    private final ModelRenderer rotor;
    private final ModelRenderer core;
    private final ModelRenderer underbit_1;
    private final ModelRenderer underbit_2;
    private final ModelRenderer underbit_3;
    private final ModelRenderer spinner_slide_y;
    private final ModelRenderer hourflip_rotate_x;
    private final LightModelRenderer glass;
    private final ModelRenderer center;
    private final ModelRenderer top_ring;
    private final ModelRenderer under_ring;
    private final ModelRenderer strut_1;
    private final ModelRenderer strut_2;
    private final ModelRenderer controls;
    private final ModelRenderer contolset_a;
    private final ModelRenderer door_switch;
    private final ModelRenderer door_crank_rotate_y;
    private final ModelRenderer sonic_port;
    private final ModelRenderer meter_a1;
    private final ModelRenderer needle_a1_rotate_y;
    private final ModelRenderer button_set_a1;
    private final ModelRenderer baseplate_a1;
    private final ModelRenderer baseplate_a2;
    private final ModelRenderer baseplate_a3;
    private final ModelRenderer controlset_b;
    private final ModelRenderer throttle;
    private final LightModelRenderer glow_throttle;
    private final LightModelRenderer glow_green_throttle;
    private final LightModelRenderer glow_yellow_throttle;
    private final LightModelRenderer glow_red_throttle;
    private final ModelRenderer leaver_b1_rotate_z;
    private final ModelRenderer leaver_b1_jig;
    private final ModelRenderer x_switch;
    private final ModelRenderer y_switch;
    private final ModelRenderer z_switch;
    private final ModelRenderer cordselect_inc;
    private final ModelRenderer cord_slider_rotate_z;
    private final ModelRenderer nixietube_b1;
    private final ModelRenderer nixietube_b2;
    private final ModelRenderer baseplate_b1;
    private final ModelRenderer baseplate_b2;
    private final ModelRenderer baseplate_b3;
    private final ModelRenderer readout_b1;
    private final ModelRenderer baseplate_b4;
    private final ModelRenderer readout_b2;
    private final ModelRenderer controlset_c;
    private final ModelRenderer communications;
    private final ModelRenderer radio;
    private final ModelRenderer radio_dial;
    private final ModelRenderer radio_dial_sub;
    private final ModelRenderer radio_needle_rotate_y;
    private final ModelRenderer speakers;
    private final ModelRenderer radio_body;
    private final ModelRenderer radio_dial_base;
    private final ModelRenderer refuler;
    private final LightModelRenderer refueler_button_rotate_z;
    private final ModelRenderer baseplate_c1;
    private final ModelRenderer baseplate_c2;
    private final ModelRenderer slider_c1;
    private final ModelRenderer sliderknob_c1_rotate_z;
    private final ModelRenderer slider_track_c1;
    private final ModelRenderer slider_c2;
    private final ModelRenderer slider_track_c2;
    private final ModelRenderer sliderknob_c2_rotate_z;
    private final ModelRenderer slider_c3;
    private final ModelRenderer slider_track_c3;
    private final ModelRenderer sliderknob_c3_rotate_z;
    private final ModelRenderer switch_c1;
    private final ModelRenderer toggle_c1;
    private final ModelRenderer switch_c2;
    private final ModelRenderer toggle_c2;
    private final ModelRenderer switch_c3;
    private final ModelRenderer toggle_c3;
    private final ModelRenderer switch_c4;
    private final ModelRenderer toggle_c4;
    private final ModelRenderer controlset_d;
    private final ModelRenderer waypoint_selector;
    private final ModelRenderer type_arm_rotate_x;
    private final ModelRenderer keys;
    private final ModelRenderer type_body;
    private final ModelRenderer type_body_r1;
    private final ModelRenderer tumbler;
    private final ModelRenderer dimention_selector;
    private final ModelRenderer tilt_d1;
    private final ModelRenderer baseplate_d1;
    private final ModelRenderer baseplate_d2;
    private final ModelRenderer baseplate_d3;
    private final ModelRenderer controlset_e;
    private final ModelRenderer telepathic_circuits;
    private final ModelRenderer talking_board;
    private final ModelRenderer talkingboard_text;
    private final ModelRenderer talkingboard_corners;
    private final ModelRenderer scrying_glass_rotate_y;
    private final LightModelRenderer glow_frame;
    private final ModelRenderer scrying_glass_frame_e1;
    private final ModelRenderer scrying_glass_frame_e3;
    private final ModelRenderer scrying_glass_frame_e2;
    private final ModelRenderer post_e1;
    private final ModelRenderer nixietube_e1;
    private final ModelRenderer nixietube_e2;
    private final ModelRenderer nixietube_e3;
    private final ModelRenderer baseplate_e1;
    private final ModelRenderer baseplate_e2;
    private final ModelRenderer switch_e1;
    private final ModelRenderer toggle_e1;
    private final ModelRenderer switch_e2;
    private final ModelRenderer toggle_e2;
    private final ModelRenderer controlset_f;
    private final ModelRenderer baseplate_f1;
    private final ModelRenderer switch_f1;
    private final ModelRenderer toggle_f1;
    private final ModelRenderer switch_f2;
    private final ModelRenderer toggle_f2;
    private final ModelRenderer switch_f3;
    private final ModelRenderer toggle_f3;
    private final ModelRenderer switch_f4;
    private final ModelRenderer toggle_f4;
    private final ModelRenderer switch_f5;
    private final ModelRenderer toggle_f5;
    private final ModelRenderer switch_f6;
    private final ModelRenderer toggle_f6;
    private final ModelRenderer handbreak;
    private final ModelRenderer lever_f1_rotate_z;
    private final ModelRenderer barrel;
    private final ModelRenderer randomize_cords;
    private final LightModelRenderer globe_rotate_y;
    private final LightModelRenderer glow_globe;
    private final LightModelRenderer globe_mount;
    private final ModelRenderer rotation_selector;
    private final ModelRenderer rotation_crank_rotate_y;
    private final ModelRenderer structure;
    private final ModelRenderer side_1;
    private final ModelRenderer skin;
    private final ModelRenderer floor_skin;
    private final ModelRenderer floorboards;
    private final ModelRenderer leg_skin;
    private final ModelRenderer knee_skin;
    private final ModelRenderer thigh_skin;
    private final ModelRenderer belly_skin;
    private final ModelRenderer rimtop_skin;
    private final ModelRenderer panel_skin;
    private final ModelRenderer skelly;
    private final ModelRenderer rib;
    private final ModelRenderer rib_bolts_1;
    private final ModelRenderer foot;
    private final ModelRenderer footslope;
    private final ModelRenderer foot_r1;
    private final ModelRenderer foot_r2;
    private final ModelRenderer leg;
    private final ModelRenderer thigh;
    private final ModelRenderer floorpipes;
    private final ModelRenderer front_rail;
    private final ModelRenderer front_railbolt;
    private final ModelRenderer rail_topbevel;
    private final ModelRenderer rail_underbevel;
    private final ModelRenderer rotor_gasket;
    private final ModelRenderer floor_trim;
    private final ModelRenderer side_2;
    private final ModelRenderer skin2;
    private final ModelRenderer floor_skin2;
    private final ModelRenderer floorboards2;
    private final ModelRenderer leg_skin2;
    private final ModelRenderer knee_skin2;
    private final ModelRenderer thigh_skin2;
    private final ModelRenderer belly_skin2;
    private final ModelRenderer rimtop_skin2;
    private final ModelRenderer panel_skin2;
    private final ModelRenderer skelly2;
    private final ModelRenderer rib2;
    private final ModelRenderer rib_bolts_2;
    private final ModelRenderer foot2;
    private final ModelRenderer footslope2;
    private final ModelRenderer foot_r3;
    private final ModelRenderer foot_r4;
    private final ModelRenderer leg2;
    private final ModelRenderer thigh2;
    private final ModelRenderer floorpipes2;
    private final ModelRenderer front_rail2;
    private final ModelRenderer front_railbolt2;
    private final ModelRenderer rail_topbevel2;
    private final ModelRenderer rail_underbevel2;
    private final ModelRenderer rotor_gasket2;
    private final ModelRenderer floor_trim2;
    private final ModelRenderer side_3;
    private final ModelRenderer skin3;
    private final ModelRenderer floor_skin3;
    private final ModelRenderer floorboards3;
    private final ModelRenderer leg_skin3;
    private final ModelRenderer knee_skin3;
    private final ModelRenderer thigh_skin3;
    private final ModelRenderer belly_skin3;
    private final ModelRenderer rimtop_skin3;
    private final ModelRenderer panel_skin3;
    private final ModelRenderer skelly3;
    private final ModelRenderer rib3;
    private final ModelRenderer rib_bolts_3;
    private final ModelRenderer foot3;
    private final ModelRenderer footslope3;
    private final ModelRenderer foot_r5;
    private final ModelRenderer foot_r6;
    private final ModelRenderer leg3;
    private final ModelRenderer thigh3;
    private final ModelRenderer floorpipes3;
    private final ModelRenderer front_rail3;
    private final ModelRenderer front_railbolt3;
    private final ModelRenderer rail_topbevel3;
    private final ModelRenderer rail_underbevel3;
    private final ModelRenderer rotor_gasket3;
    private final ModelRenderer floor_trim3;
    private final ModelRenderer side_4;
    private final ModelRenderer skin4;
    private final ModelRenderer floor_skin4;
    private final ModelRenderer floorboards4;
    private final ModelRenderer leg_skin4;
    private final ModelRenderer knee_skin4;
    private final ModelRenderer thigh_skin4;
    private final ModelRenderer belly_skin4;
    private final ModelRenderer rimtop_skin4;
    private final ModelRenderer panel_skin4;
    private final ModelRenderer skelly4;
    private final ModelRenderer rib4;
    private final ModelRenderer rib_bolts_4;
    private final ModelRenderer foot4;
    private final ModelRenderer footslope4;
    private final ModelRenderer foot_r7;
    private final ModelRenderer foot_r8;
    private final ModelRenderer leg4;
    private final ModelRenderer thigh4;
    private final ModelRenderer floorpipes4;
    private final ModelRenderer front_rail4;
    private final ModelRenderer front_railbolt4;
    private final ModelRenderer rail_topbevel4;
    private final ModelRenderer rail_underbevel4;
    private final ModelRenderer rotor_gasket4;
    private final ModelRenderer floor_trim4;
    private final ModelRenderer side_5;
    private final ModelRenderer skin5;
    private final ModelRenderer floor_skin5;
    private final ModelRenderer floorboards5;
    private final ModelRenderer leg_skin5;
    private final ModelRenderer knee_skin5;
    private final ModelRenderer thigh_skin5;
    private final ModelRenderer belly_skin5;
    private final ModelRenderer rimtop_skin5;
    private final ModelRenderer panel_skin5;
    private final ModelRenderer skelly5;
    private final ModelRenderer rib5;
    private final ModelRenderer rib_bolts_5;
    private final ModelRenderer foot5;
    private final ModelRenderer footslope5;
    private final ModelRenderer foot_r9;
    private final ModelRenderer foot_r10;
    private final ModelRenderer leg5;
    private final ModelRenderer thigh5;
    private final ModelRenderer floorpipes5;
    private final ModelRenderer front_rail5;
    private final ModelRenderer front_railbolt5;
    private final ModelRenderer rail_topbevel5;
    private final ModelRenderer rail_underbevel5;
    private final ModelRenderer rotor_gasket5;
    private final ModelRenderer floor_trim5;
    private final ModelRenderer side_6;
    private final ModelRenderer skin6;
    private final ModelRenderer floor_skin6;
    private final ModelRenderer floorboards6;
    private final ModelRenderer leg_skin6;
    private final ModelRenderer knee_skin6;
    private final ModelRenderer thigh_skin6;
    private final ModelRenderer belly_skin6;
    private final ModelRenderer rimtop_skin6;
    private final ModelRenderer panel_skin6;
    private final ModelRenderer skelly6;
    private final ModelRenderer rib6;
    private final ModelRenderer rib_bolts_6;
    private final ModelRenderer foot6;
    private final ModelRenderer footslope6;
    private final ModelRenderer foot_r11;
    private final ModelRenderer foot_r12;
    private final ModelRenderer leg6;
    private final ModelRenderer thigh6;
    private final ModelRenderer floorpipes6;
    private final ModelRenderer front_rail6;
    private final ModelRenderer front_railbolt6;
    private final ModelRenderer rail_topbevel6;
    private final ModelRenderer rail_underbevel6;
    private final ModelRenderer rotor_gasket6;
    private final ModelRenderer floor_trim6;

    public SteamConsoleModel(Function<ResourceLocation, RenderType> function) {
        super(function);
        textureWidth = 512;
        textureHeight = 512;

        glow = new LightModelRenderer(this);
        glow.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(glow, 0.0F, 1.0472F, 0.0F);
        

        glow_meterglass_a1 = new LightModelRenderer(this);
        glow_meterglass_a1.setRotationPoint(0.0F, -58.0F, 26.0F);
        glow.addChild(glow_meterglass_a1);
        setRotationAngle(glow_meterglass_a1, -0.3491F, 0.0F, 0.0F);
        glow_meterglass_a1.setTextureOffset(426, 330).addBox(-3.0F, -2.4F, -2.6F, 6.0F, 4.0F, 6.0F, 0.0F, false);

        glow_lamp_a1 = new LightModelRenderer(this);
        glow_lamp_a1.setRotationPoint(-11.0F, -58.8F, 38.0F);
        glow.addChild(glow_lamp_a1);
        setRotationAngle(glow_lamp_a1, -0.3491F, 0.0F, 0.0F);
        glow_lamp_a1.setTextureOffset(490, 450).addBox(-2.0F, -3.0F, -2.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);

        glow_lamp_a2 = new LightModelRenderer(this);
        glow_lamp_a2.setRotationPoint(11.0F, -59.0F, 37.0F);
        glow.addChild(glow_lamp_a2);
        setRotationAngle(glow_lamp_a2, -0.3491F, 0.0F, 0.0F);
        glow_lamp_a2.setTextureOffset(490, 450).addBox(-2.0F, -3.0F, -1.0F, 4.0F, 6.0F, 4.0F, 0.0F, false);

        glow_glass_b1 = new LightModelRenderer(this);
        glow_glass_b1.setRotationPoint(17.0667F, -64.0F, 14.5333F);
        glow.addChild(glow_glass_b1);
        setRotationAngle(glow_glass_b1, -0.0873F, -0.5236F, 0.1745F);
        glow_glass_b1.setTextureOffset(240, 460).addBox(-0.0667F, 1.6F, -0.5333F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        glow_glass_b1.setTextureOffset(240, 460).addBox(-0.6667F, -2.4F, -0.9333F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        glow_glass_b1.setTextureOffset(240, 460).addBox(-0.0667F, -3.4F, -0.5333F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        glow_glass_b2 = new LightModelRenderer(this);
        glow_glass_b2.setRotationPoint(22.0667F, -64.0F, 6.9333F);
        glow.addChild(glow_glass_b2);
        setRotationAngle(glow_glass_b2, 0.1745F, -2.0944F, 0.0F);
        glow_glass_b2.setTextureOffset(240, 460).addBox(-0.4667F, 2.0F, -0.5333F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        glow_glass_b2.setTextureOffset(240, 460).addBox(-1.0667F, -2.0F, -0.9333F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        glow_glass_b2.setTextureOffset(240, 460).addBox(-0.4667F, -3.0F, -0.5333F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        glow_lamp_c1 = new LightModelRenderer(this);
        glow_lamp_c1.setRotationPoint(40.0F, -60.4F, -9.8F);
        glow.addChild(glow_lamp_c1);
        setRotationAngle(glow_lamp_c1, 0.2618F, -1.0472F, 0.0F);
        glow_lamp_c1.setTextureOffset(280, 420).addBox(-2.6F, -3.8F, -2.2F, 1.0F, 8.0F, 2.0F, 0.0F, false);
        glow_lamp_c1.setTextureOffset(280, 420).addBox(-1.6F, -3.8F, -3.2F, 2.0F, 8.0F, 1.0F, 0.0F, false);
        glow_lamp_c1.setTextureOffset(280, 420).addBox(-1.6F, -3.8F, -0.2F, 2.0F, 8.0F, 1.0F, 0.0F, false);
        glow_lamp_c1.setTextureOffset(280, 420).addBox(-1.6F, -4.8F, -2.2F, 2.0F, 8.0F, 2.0F, 0.0F, false);
        glow_lamp_c1.setTextureOffset(280, 420).addBox(0.2F, -3.8F, -2.2F, 1.0F, 8.0F, 2.0F, 0.0F, false);

        glow_lamp_c2 = new LightModelRenderer(this);
        glow_lamp_c2.setRotationPoint(29.0F, -60.4F, -29.0F);
        glow.addChild(glow_lamp_c2);
        setRotationAngle(glow_lamp_c2, 0.2618F, -1.0472F, 0.0F);
        glow_lamp_c2.setTextureOffset(280, 420).addBox(0.6F, -3.8F, -1.8F, 1.0F, 8.0F, 2.0F, 0.0F, false);
        glow_lamp_c2.setTextureOffset(280, 420).addBox(-1.4F, -3.8F, -2.8F, 2.0F, 8.0F, 1.0F, 0.0F, false);
        glow_lamp_c2.setTextureOffset(280, 420).addBox(-1.4F, -3.8F, 0.2F, 2.0F, 8.0F, 1.0F, 0.0F, false);
        glow_lamp_c2.setTextureOffset(280, 420).addBox(-1.4F, -4.8F, -1.8F, 2.0F, 8.0F, 2.0F, 0.0F, false);
        glow_lamp_c2.setTextureOffset(280, 420).addBox(-2.4F, -3.8F, -1.8F, 1.0F, 8.0F, 2.0F, 0.0F, false);

        glow_panel_e = new LightModelRenderer(this);
        glow_panel_e.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow.addChild(glow_panel_e);
        setRotationAngle(glow_panel_e, 0.0F, -0.5236F, 0.0F);
        

        glow_glass_e2 = new LightModelRenderer(this);
        glow_glass_e2.setRotationPoint(34.0F, -58.0F, -2.0F);
        glow_panel_e.addChild(glow_glass_e2);
        glow_glass_e2.setTextureOffset(240, 460).addBox(-57.8F, -5.2F, -4.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        glow_glass_e2.setTextureOffset(240, 460).addBox(-58.4F, -9.2F, -4.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        glow_glass_e2.setTextureOffset(240, 460).addBox(-57.8F, -10.2F, -4.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        glow_glass_e3 = new LightModelRenderer(this);
        glow_glass_e3.setRotationPoint(34.0F, -58.0F, -2.0F);
        glow_panel_e.addChild(glow_glass_e3);
        glow_glass_e3.setTextureOffset(240, 460).addBox(-55.4F, -5.2F, 1.6F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        glow_glass_e3.setTextureOffset(240, 460).addBox(-56.0F, -9.2F, 1.2F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        glow_glass_e3.setTextureOffset(240, 460).addBox(-55.4F, -10.2F, 1.6F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        glow_glass_e1 = new LightModelRenderer(this);
        glow_glass_e1.setRotationPoint(34.0F, -58.0F, -2.0F);
        glow_panel_e.addChild(glow_glass_e1);
        glow_glass_e1.setTextureOffset(240, 460).addBox(-57.8F, -5.2F, 6.8F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        glow_glass_e1.setTextureOffset(240, 460).addBox(-58.4F, -9.2F, 6.4F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        glow_glass_e1.setTextureOffset(240, 460).addBox(-57.8F, -10.2F, 6.8F, 1.0F, 1.0F, 1.0F, 0.0F, false);

        redlamp_e3 = new LightModelRenderer(this);
        redlamp_e3.setRotationPoint(-52.0F, -54.0F, -19.0F);
        glow_panel_e.addChild(redlamp_e3);
        setRotationAngle(redlamp_e3, 0.0F, 0.0F, -0.3491F);
        redlamp_e3.setTextureOffset(390, 440).addBox(-2.0F, 1.6F, -1.8F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        redlamp_e4 = new LightModelRenderer(this);
        redlamp_e4.setRotationPoint(-52.0F, -54.0F, -19.0F);
        glow_panel_e.addChild(redlamp_e4);
        setRotationAngle(redlamp_e4, 0.0F, 0.0F, -0.3491F);
        redlamp_e4.setTextureOffset(390, 440).addBox(-2.0F, 1.6F, 35.2F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        glow_core = new LightModelRenderer(this);
        glow_core.setRotationPoint(0.0F, 4.0F, 0.0F);
        glow.addChild(glow_core);
        glow_core.setTextureOffset(266, 327).addBox(-16.4F, -54.0F, -12.0F, 32.0F, 16.0F, 24.0F, 0.0F, false);

        glow_radio = new LightModelRenderer(this);
        glow_radio.setRotationPoint(25.4F, -58.0F, -14.0F);
        glow.addChild(glow_radio);
        setRotationAngle(glow_radio, -0.1047F, 0.5236F, -0.2094F);
        glow_radio.setTextureOffset(420, 340).addBox(-1.0F, -2.8F, -8.52F, 4.0F, 4.0F, 16.0F, 0.0F, false);

        rotor = new ModelRenderer(this);
        rotor.setRotationPoint(0.0F, 28.0F, 0.0F);
        

        core = new ModelRenderer(this);
        core.setRotationPoint(0.0F, 0.0F, 0.0F);
        rotor.addChild(core);
        setRotationAngle(core, 0.0F, 0.5236F, 0.0F);
        

        underbit_1 = new ModelRenderer(this);
        underbit_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        core.addChild(underbit_1);
        underbit_1.setTextureOffset(80, 83).addBox(-20.4F, -9.0F, -12.0F, 40.0F, 2.0F, 24.0F, 0.0F, false);

        underbit_2 = new ModelRenderer(this);
        underbit_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        core.addChild(underbit_2);
        setRotationAngle(underbit_2, 0.0F, 1.0472F, 0.0F);
        underbit_2.setTextureOffset(80, 83).addBox(-20.4F, -10.0F, -12.0F, 40.0F, 4.0F, 24.0F, 0.0F, false);

        underbit_3 = new ModelRenderer(this);
        underbit_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        core.addChild(underbit_3);
        setRotationAngle(underbit_3, 0.0F, 2.0944F, 0.0F);
        underbit_3.setTextureOffset(80, 83).addBox(-20.4F, -10.4F, -12.0F, 40.0F, 4.0F, 24.0F, 0.0F, false);

        spinner_slide_y = new ModelRenderer(this);
        spinner_slide_y.setRotationPoint(0.0F, -101.4F, 0.0F);
        rotor.addChild(spinner_slide_y);
        

        hourflip_rotate_x = new ModelRenderer(this);
        hourflip_rotate_x.setRotationPoint(0.0F, 0.15F, -0.25F);
        spinner_slide_y.addChild(hourflip_rotate_x);
        

        glass = new LightModelRenderer(this);
        glass.setRotationPoint(0.0F, 101.25F, 0.25F);
        hourflip_rotate_x.addChild(glass);
        glass.setTextureOffset(296, 280).addBox(-6.0F, -119.0F, -6.0F, 12.0F, 12.0F, 12.0F, 0.0F, false);
        glass.setTextureOffset(295, 330).addBox(-4.0F, -109.0F, -4.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        glass.setTextureOffset(295, 330).addBox(-3.0F, -107.0F, -3.0F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        glass.setTextureOffset(295, 330).addBox(-2.0F, -104.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        glass.setTextureOffset(295, 330).addBox(-3.0F, -100.0F, -3.0F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        glass.setTextureOffset(295, 330).addBox(-4.0F, -98.0F, -4.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        glass.setTextureOffset(291, 362).addBox(-6.0F, -96.0F, -6.0F, 12.0F, 12.0F, 12.0F, 0.0F, false);

        center = new ModelRenderer(this);
        center.setRotationPoint(0.0F, 101.25F, 0.25F);
        hourflip_rotate_x.addChild(center);
        center.setTextureOffset(377, 108).addBox(6.25F, -111.575F, -1.0F, 1.0F, 9.0F, 2.0F, 0.0F, false);
        center.setTextureOffset(375, 86).addBox(6.25F, -100.6F, -1.0F, 1.0F, 9.0F, 2.0F, 0.0F, false);
        center.setTextureOffset(302, 78).addBox(6.0F, -102.6F, -1.0F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        center.setTextureOffset(348, 78).addBox(-7.25F, -111.6F, -1.0F, 1.0F, 9.0F, 2.0F, 0.0F, false);
        center.setTextureOffset(317, 77).addBox(-7.25F, -100.6F, -1.0F, 1.0F, 9.0F, 2.0F, 0.0F, false);
        center.setTextureOffset(302, 78).addBox(-12.0F, -102.6F, -1.0F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        center.setTextureOffset(140, 420).addBox(7.4F, -103.6F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        center.setTextureOffset(140, 420).addBox(-9.4F, -103.6F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        top_ring = new ModelRenderer(this);
        top_ring.setRotationPoint(0.0F, 101.25F, 0.25F);
        hourflip_rotate_x.addChild(top_ring);
        top_ring.setTextureOffset(303, 170).addBox(-7.0F, -118.0F, -7.0F, 14.0F, 2.0F, 14.0F, 0.0F, false);
        top_ring.setTextureOffset(303, 84).addBox(6.0F, -112.2F, -6.0F, 1.0F, 2.0F, 12.0F, 0.0F, false);
        top_ring.setTextureOffset(318, 79).addBox(-7.0F, -112.2F, -6.0F, 1.0F, 2.0F, 12.0F, 0.0F, false);
        top_ring.setTextureOffset(331, 81).addBox(-6.0F, -112.2F, 6.0F, 12.0F, 2.0F, 1.0F, 0.0F, false);
        top_ring.setTextureOffset(306, 78).addBox(-6.0F, -112.2F, -7.0F, 12.0F, 2.0F, 1.0F, 0.0F, false);

        under_ring = new ModelRenderer(this);
        under_ring.setRotationPoint(0.0F, 101.25F, 0.25F);
        hourflip_rotate_x.addChild(under_ring);
        under_ring.setTextureOffset(306, 176).addBox(-7.0F, -87.0F, -7.0F, 14.0F, 2.0F, 14.0F, 0.0F, false);
        under_ring.setTextureOffset(351, 98).addBox(6.0F, -92.8F, -6.0F, 1.0F, 2.0F, 12.0F, 0.0F, false);
        under_ring.setTextureOffset(346, 78).addBox(-7.0F, -92.8F, -6.0F, 1.0F, 2.0F, 12.0F, 0.0F, false);
        under_ring.setTextureOffset(359, 90).addBox(-6.0F, -92.8F, 6.0F, 12.0F, 2.0F, 1.0F, 0.0F, false);
        under_ring.setTextureOffset(334, 89).addBox(-6.0F, -92.8F, -7.0F, 12.0F, 2.0F, 1.0F, 0.0F, false);

        strut_1 = new ModelRenderer(this);
        strut_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        rotor.addChild(strut_1);
        strut_1.setTextureOffset(306, 37).addBox(-9.5F, -104.0F, -4.0F, 2.0F, 50.0F, 2.0F, 0.0F, false);
        strut_1.setTextureOffset(306, 37).addBox(-9.5F, -104.0F, 2.0F, 2.0F, 50.0F, 2.0F, 0.0F, false);
        strut_1.setTextureOffset(304, 43).addBox(-9.5F, -106.0F, -4.0F, 2.0F, 2.0F, 8.0F, 0.0F, false);

        strut_2 = new ModelRenderer(this);
        strut_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        rotor.addChild(strut_2);
        strut_2.setTextureOffset(304, 44).addBox(7.5F, -104.0F, -4.0F, 2.0F, 50.0F, 2.0F, 0.0F, false);
        strut_2.setTextureOffset(304, 44).addBox(7.5F, -104.0F, 2.0F, 2.0F, 50.0F, 2.0F, 0.0F, false);
        strut_2.setTextureOffset(299, 46).addBox(7.5F, -106.0F, -4.0F, 2.0F, 2.0F, 8.0F, 0.0F, false);

        controls = new ModelRenderer(this);
        controls.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(controls, 0.0F, -0.5236F, 0.0F);
        

        contolset_a = new ModelRenderer(this);
        contolset_a.setRotationPoint(50.0F, -50.0F, 14.0F);
        controls.addChild(contolset_a);
        setRotationAngle(contolset_a, 0.0F, 0.0F, -2.7925F);
        

        door_switch = new ModelRenderer(this);
        door_switch.setRotationPoint(0.0F, 0.0F, 0.0F);
        contolset_a.addChild(door_switch);
        setRotationAngle(door_switch, 0.0F, 0.0F, 0.5236F);
        door_switch.setTextureOffset(379, 60).addBox(-2.0F, -2.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        door_crank_rotate_y = new ModelRenderer(this);
        door_crank_rotate_y.setRotationPoint(0.0F, 6.1F, 4.0F);
        door_switch.addChild(door_crank_rotate_y);
        door_crank_rotate_y.setTextureOffset(149, 423).addBox(-1.0F, -7.1F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(379, 60).addBox(-1.0F, -2.1F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(379, 60).addBox(-1.0F, -2.1F, 1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(379, 60).addBox(-3.0F, -2.1F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(379, 60).addBox(1.0F, -2.1F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(379, 60).addBox(3.0F, -2.1F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(379, 60).addBox(-5.0F, -2.1F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(379, 60).addBox(-3.0F, -2.1F, 3.0F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(379, 60).addBox(-3.0F, -2.1F, -5.0F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        door_crank_rotate_y.setTextureOffset(86, 427).addBox(3.0F, -0.1F, -1.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);

        sonic_port = new ModelRenderer(this);
        sonic_port.setRotationPoint(0.0F, 0.0F, 0.0F);
        contolset_a.addChild(sonic_port);
        sonic_port.setTextureOffset(379, 60).addBox(4.0F, -2.0F, -18.0F, 8.0F, 4.0F, 2.0F, 0.0F, false);
        sonic_port.setTextureOffset(379, 60).addBox(4.0F, -2.0F, -12.0F, 8.0F, 4.0F, 2.0F, 0.0F, false);
        sonic_port.setTextureOffset(78, 441).addBox(6.0F, -3.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(379, 60).addBox(10.0F, -2.0F, -16.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(379, 60).addBox(4.0F, -2.0F, -16.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(379, 60).addBox(2.0F, -3.0F, -16.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(379, 60).addBox(12.0F, -3.0F, -16.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(379, 60).addBox(6.0F, -3.0F, -10.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);
        sonic_port.setTextureOffset(379, 60).addBox(6.0F, -3.0F, -20.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);

        meter_a1 = new ModelRenderer(this);
        meter_a1.setRotationPoint(0.0F, 16.3333F, -14.0F);
        contolset_a.addChild(meter_a1);
        meter_a1.setTextureOffset(69, 431).addBox(21.0F, -19.3333F, -4.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);

        needle_a1_rotate_y = new ModelRenderer(this);
        needle_a1_rotate_y.setRotationPoint(23.2F, -15.3333F, -0.1F);
        meter_a1.addChild(needle_a1_rotate_y);
        setRotationAngle(needle_a1_rotate_y, 0.0F, 0.5236F, 0.0F);
        needle_a1_rotate_y.setTextureOffset(82, 433).addBox(-0.2F, -2.0F, -0.5F, 4.0F, 4.0F, 1.0F, 0.0F, false);

        button_set_a1 = new ModelRenderer(this);
        button_set_a1.setRotationPoint(0.0F, 0.0F, 0.0F);
        contolset_a.addChild(button_set_a1);
        button_set_a1.setTextureOffset(70, 420).addBox(17.0F, -3.0F, -19.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        button_set_a1.setTextureOffset(70, 420).addBox(17.0F, -3.0F, -11.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        button_set_a1.setTextureOffset(70, 420).addBox(17.0F, -3.0F, -15.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);

        baseplate_a1 = new ModelRenderer(this);
        baseplate_a1.setRotationPoint(0.0F, 0.0F, 0.0F);
        contolset_a.addChild(baseplate_a1);
        baseplate_a1.setTextureOffset(379, 60).addBox(30.0F, -4.0F, -20.0F, 2.0F, 4.0F, 12.0F, 0.0F, false);
        baseplate_a1.setTextureOffset(379, 60).addBox(18.0F, -4.0F, -22.0F, 12.0F, 4.0F, 16.0F, 0.0F, false);
        baseplate_a1.setTextureOffset(379, 60).addBox(16.0F, -4.0F, -20.0F, 2.0F, 4.0F, 12.0F, 0.0F, false);

        baseplate_a2 = new ModelRenderer(this);
        baseplate_a2.setRotationPoint(24.0F, -2.0F, -14.0F);
        contolset_a.addChild(baseplate_a2);
        baseplate_a2.setTextureOffset(379, 60).addBox(-13.0F, 0.0F, 8.0F, 6.0F, 4.0F, 6.0F, 0.0F, false);

        baseplate_a3 = new ModelRenderer(this);
        baseplate_a3.setRotationPoint(24.0F, -2.0F, -14.0F);
        contolset_a.addChild(baseplate_a3);
        baseplate_a3.setTextureOffset(379, 60).addBox(-13.0F, 0.0F, -14.0F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        baseplate_a3.setTextureOffset(379, 60).addBox(-25.0F, -1.0F, -12.0F, 12.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_a3.setTextureOffset(379, 60).addBox(-25.0F, -1.0F, -32.0F, 2.0F, 4.0F, 20.0F, 0.0F, false);
        baseplate_a3.setTextureOffset(276, 93).addBox(-26.0F, -4.0F, -13.0F, 4.0F, 8.0F, 4.0F, 0.0F, false);

        controlset_b = new ModelRenderer(this);
        controlset_b.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controlset_b);
        setRotationAngle(controlset_b, 0.0F, 1.0472F, 0.0F);
        

        throttle = new ModelRenderer(this);
        throttle.setRotationPoint(52.0F, -62.0F, 0.0F);
        controlset_b.addChild(throttle);
        setRotationAngle(throttle, 0.0F, 0.0F, 0.1745F);
        throttle.setTextureOffset(379, 60).addBox(-10.0F, 10.0F, 10.0F, 16.0F, 4.0F, 8.0F, 0.0F, false);
        throttle.setTextureOffset(75, 436).addBox(-8.0F, 6.0F, 11.0F, 13.0F, 7.0F, 6.0F, 0.0F, false);
        throttle.setTextureOffset(379, 60).addBox(-0.8F, 11.6F, 18.0F, 2.0F, 4.0F, 8.0F, 0.0F, false);

        glow_throttle = new LightModelRenderer(this);
        glow_throttle.setRotationPoint(-52.0F, 62.0F, 0.0F);
        throttle.addChild(glow_throttle);

        glow_green_throttle = new LightModelRenderer(this);
        glow_green_throttle.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_throttle.addChild(glow_green_throttle);
        glow_green_throttle.setTextureOffset(489, 415).addBox(47.0F, -57.0F, 13.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        glow_yellow_throttle = new LightModelRenderer(this);
        glow_yellow_throttle.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_throttle.addChild(glow_yellow_throttle);
        glow_yellow_throttle.setTextureOffset(450, 422).addBox(50.0F, -57.0F, 13.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        glow_red_throttle = new LightModelRenderer(this);
        glow_red_throttle.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow_throttle.addChild(glow_red_throttle);
        glow_red_throttle.setTextureOffset(400, 411).addBox(53.0F, -57.0F, 13.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        leaver_b1_rotate_z = new ModelRenderer(this);
        leaver_b1_rotate_z.setRotationPoint(-2.0F, 8.0F, 14.0F);
        throttle.addChild(leaver_b1_rotate_z);
        setRotationAngle(leaver_b1_rotate_z, 0.0F, 0.0F, 0.5236F);
        leaver_b1_rotate_z.setTextureOffset(379, 60).addBox(-1.0F, -1.0F, -5.0F, 2.0F, 2.0F, 10.0F, 0.0F, false);
        leaver_b1_rotate_z.setTextureOffset(379, 60).addBox(0.0F, -6.0F, 3.0F, 2.0F, 8.0F, 1.0F, 0.0F, false);
        leaver_b1_rotate_z.setTextureOffset(213, 104).addBox(-0.5F, -11.5F, -1.0F, 3.0F, 3.0F, 8.0F, 0.0F, false);
        leaver_b1_rotate_z.setTextureOffset(379, 60).addBox(0.0F, -10.6F, 7.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);

        leaver_b1_jig = new ModelRenderer(this);
        leaver_b1_jig.setRotationPoint(1.0F, -6.0F, 4.5F);
        leaver_b1_rotate_z.addChild(leaver_b1_jig);
        setRotationAngle(leaver_b1_jig, -1.1345F, 0.0F, 0.0F);
        leaver_b1_jig.setTextureOffset(379, 60).addBox(-1.0F, -3.0F, -0.5F, 2.0F, 4.0F, 1.0F, 0.0F, false);

        x_switch = new ModelRenderer(this);
        x_switch.setRotationPoint(31.0F, -58.0F, -5.0F);
        controlset_b.addChild(x_switch);
        setRotationAngle(x_switch, 0.0F, 0.0F, 0.7854F);
        x_switch.setTextureOffset(87, 436).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        x_switch.setTextureOffset(379, 60).addBox(-2.0F, -1.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        y_switch = new ModelRenderer(this);
        y_switch.setRotationPoint(31.0F, -58.0F, 0.0F);
        controlset_b.addChild(y_switch);
        setRotationAngle(y_switch, 0.0F, 0.0F, 0.7854F);
        y_switch.setTextureOffset(86, 436).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        y_switch.setTextureOffset(363, 59).addBox(-2.0F, -1.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        z_switch = new ModelRenderer(this);
        z_switch.setRotationPoint(31.0F, -58.0F, 5.0F);
        controlset_b.addChild(z_switch);
        setRotationAngle(z_switch, 0.0F, 0.0F, 0.7854F);
        z_switch.setTextureOffset(95, 437).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        z_switch.setTextureOffset(379, 60).addBox(-2.0F, -1.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        cordselect_inc = new ModelRenderer(this);
        cordselect_inc.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_b.addChild(cordselect_inc);
        setRotationAngle(cordselect_inc, 0.0F, 0.0F, 0.2618F);
        cordselect_inc.setTextureOffset(83, 431).addBox(11.0F, 2.0F, -12.0F, 8.0F, 4.0F, 1.0F, 0.0F, false);
        cordselect_inc.setTextureOffset(379, 60).addBox(11.0F, 2.0F, -14.0F, 8.0F, 4.0F, 2.0F, 0.0F, false);
        cordselect_inc.setTextureOffset(379, 60).addBox(11.0F, 2.0F, -11.0F, 8.0F, 4.0F, 2.0F, 0.0F, false);
        cordselect_inc.setTextureOffset(379, 60).addBox(19.0F, 2.0F, -14.0F, 2.0F, 4.0F, 5.0F, 0.0F, false);
        cordselect_inc.setTextureOffset(379, 60).addBox(9.0F, 2.0F, -14.0F, 2.0F, 4.0F, 5.0F, 0.0F, false);

        cord_slider_rotate_z = new ModelRenderer(this);
        cord_slider_rotate_z.setRotationPoint(14.5F, 58.0F, -11.6F);
        cordselect_inc.addChild(cord_slider_rotate_z);
        setRotationAngle(cord_slider_rotate_z, 0.0F, 0.0F, 0.0873F);
        cord_slider_rotate_z.setTextureOffset(78, 439).addBox(-1.5F, -57.0F, -2.0F, 3.0F, 1.0F, 4.0F, 0.0F, false);

        nixietube_b1 = new ModelRenderer(this);
        nixietube_b1.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_b.addChild(nixietube_b1);
        nixietube_b1.setTextureOffset(77, 428).addBox(-13.0F, -3.0F, 5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        nixietube_b2 = new ModelRenderer(this);
        nixietube_b2.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_b.addChild(nixietube_b2);
        nixietube_b2.setTextureOffset(78, 435).addBox(-13.0F, -3.0F, -4.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        baseplate_b1 = new ModelRenderer(this);
        baseplate_b1.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_b.addChild(baseplate_b1);
        setRotationAngle(baseplate_b1, 0.0F, 0.0F, 0.2618F);
        baseplate_b1.setTextureOffset(379, 60).addBox(-5.0F, 1.0F, -8.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_b1.setTextureOffset(379, 60).addBox(-5.0F, 1.0F, 10.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_b1.setTextureOffset(379, 60).addBox(-6.0F, 1.0F, -6.0F, 8.0F, 4.0F, 16.0F, 0.0F, false);

        baseplate_b2 = new ModelRenderer(this);
        baseplate_b2.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_b.addChild(baseplate_b2);
        setRotationAngle(baseplate_b2, 0.0F, 0.0F, 0.2618F);
        baseplate_b2.setTextureOffset(379, 60).addBox(-12.6F, 1.0F, -1.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);
        baseplate_b2.setTextureOffset(379, 60).addBox(-14.0F, 1.0F, -5.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        baseplate_b2.setTextureOffset(379, 60).addBox(-14.0F, 1.0F, 4.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        baseplate_b3 = new ModelRenderer(this);
        baseplate_b3.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_b.addChild(baseplate_b3);
        setRotationAngle(baseplate_b3, 0.0F, 0.0F, 0.2618F);
        baseplate_b3.setTextureOffset(379, 60).addBox(7.0F, 2.0F, -3.0F, 5.0F, 4.0F, 10.0F, 0.0F, false);
        baseplate_b3.setTextureOffset(379, 60).addBox(7.0F, 1.0F, -4.0F, 5.0F, 4.0F, 1.0F, 0.0F, false);
        baseplate_b3.setTextureOffset(379, 60).addBox(7.0F, 1.0F, 7.0F, 5.0F, 4.0F, 1.0F, 0.0F, false);
        baseplate_b3.setTextureOffset(379, 60).addBox(8.0F, 2.0F, 8.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);
        baseplate_b3.setTextureOffset(379, 60).addBox(8.0F, 2.0F, -5.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);

        readout_b1 = new ModelRenderer(this);
        readout_b1.setRotationPoint(9.0F, 2.0F, 2.0F);
        baseplate_b3.addChild(readout_b1);
        setRotationAngle(readout_b1, 0.0F, 0.0F, 0.6109F);
        readout_b1.setTextureOffset(73, 435).addBox(-1.0F, -0.8F, -5.0F, 4.0F, 4.0F, 10.0F, 0.0F, false);
        readout_b1.setTextureOffset(214, 438).addBox(-0.6F, -1.0F, -5.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        readout_b1.setTextureOffset(214, 438).addBox(-0.6F, -1.0F, 1.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        readout_b1.setTextureOffset(214, 438).addBox(-0.6F, -1.0F, -0.52F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        baseplate_b4 = new ModelRenderer(this);
        baseplate_b4.setRotationPoint(40.0F, -56.0F, -2.0F);
        controlset_b.addChild(baseplate_b4);
        setRotationAngle(baseplate_b4, 0.0F, 0.0F, 0.2618F);
        baseplate_b4.setTextureOffset(379, 60).addBox(7.0F, 2.0F, -3.0F, 5.0F, 4.0F, 10.0F, 0.0F, false);
        baseplate_b4.setTextureOffset(379, 60).addBox(7.0F, 1.0F, -4.0F, 5.0F, 4.0F, 1.0F, 0.0F, false);
        baseplate_b4.setTextureOffset(379, 60).addBox(7.0F, 1.0F, 7.0F, 5.0F, 4.0F, 1.0F, 0.0F, false);
        baseplate_b4.setTextureOffset(379, 60).addBox(8.0F, 2.0F, 8.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);
        baseplate_b4.setTextureOffset(379, 60).addBox(8.0F, 2.0F, -5.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);

        readout_b2 = new ModelRenderer(this);
        readout_b2.setRotationPoint(9.0F, 2.0F, 2.0F);
        baseplate_b4.addChild(readout_b2);
        setRotationAngle(readout_b2, 0.0F, 0.0F, 0.6109F);
        readout_b2.setTextureOffset(75, 440).addBox(-1.0F, -0.8F, -5.0F, 4.0F, 4.0F, 10.0F, 0.0F, false);
        readout_b2.setTextureOffset(208, 435).addBox(-0.6F, -1.0F, -5.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        readout_b2.setTextureOffset(208, 435).addBox(-0.6F, -1.0F, 1.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        readout_b2.setTextureOffset(208, 435).addBox(-0.6F, -1.0F, -0.52F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        controlset_c = new ModelRenderer(this);
        controlset_c.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controlset_c);
        setRotationAngle(controlset_c, 0.0F, 2.0944F, 0.0F);
        

        communications = new ModelRenderer(this);
        communications.setRotationPoint(0.0F, 0.0F, 0.0F);
        controlset_c.addChild(communications);
        

        radio = new ModelRenderer(this);
        radio.setRotationPoint(25.0F, -64.0F, 0.0F);
        communications.addChild(radio);
        setRotationAngle(radio, 0.0F, 0.0F, -0.2618F);
        

        radio_dial = new ModelRenderer(this);
        radio_dial.setRotationPoint(13.0F, 15.0F, 13.0F);
        radio.addChild(radio_dial);
        setRotationAngle(radio_dial, 0.0F, 0.0F, 0.6109F);
        radio_dial.setTextureOffset(78, 430).addBox(-7.0F, -3.2F, -14.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        radio_dial.setTextureOffset(78, 430).addBox(-8.0F, -2.2F, -15.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        radio_dial_sub = new ModelRenderer(this);
        radio_dial_sub.setRotationPoint(-6.0F, -3.0F, -13.0F);
        radio_dial.addChild(radio_dial_sub);
        setRotationAngle(radio_dial_sub, 0.0F, -0.7854F, 0.0F);
        radio_dial_sub.setTextureOffset(78, 430).addBox(-2.0F, 1.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        radio_needle_rotate_y = new ModelRenderer(this);
        radio_needle_rotate_y.setRotationPoint(-26.0F, 6.5F, 0.5F);
        radio.addChild(radio_needle_rotate_y);
        radio_needle_rotate_y.setTextureOffset(391, 411).addBox(22.0F, -1.5F, -0.5F, 11.0F, 3.0F, 1.0F, 0.0F, false);

        speakers = new ModelRenderer(this);
        speakers.setRotationPoint(0.0F, 0.0F, 0.0F);
        radio.addChild(speakers);
        speakers.setTextureOffset(77, 431).addBox(-2.6F, -4.0F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
        speakers.setTextureOffset(77, 431).addBox(-2.6F, -7.0F, -2.0F, 8.0F, 2.0F, 4.0F, 0.0F, false);
        speakers.setTextureOffset(77, 431).addBox(-2.6F, 0.0F, -5.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        speakers.setTextureOffset(77, 431).addBox(-2.6F, -7.0F, -5.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        speakers.setTextureOffset(77, 431).addBox(-2.6F, -7.0F, 3.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        speakers.setTextureOffset(77, 431).addBox(-2.6F, -4.0F, 3.0F, 8.0F, 3.0F, 2.0F, 0.0F, false);
        speakers.setTextureOffset(77, 431).addBox(-2.6F, 0.0F, 3.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        speakers.setTextureOffset(77, 431).addBox(-2.6F, -4.0F, -5.0F, 8.0F, 3.0F, 2.0F, 0.0F, false);

        radio_body = new ModelRenderer(this);
        radio_body.setRotationPoint(0.0F, 0.0F, 0.0F);
        radio.addChild(radio_body);
        radio_body.setTextureOffset(161, 329).addBox(-5.0F, -3.0F, -10.0F, 10.0F, 12.0F, 20.0F, 0.0F, false);
        radio_body.setTextureOffset(161, 329).addBox(-5.0F, -7.0F, -8.0F, 10.0F, 4.0F, 16.0F, 0.0F, false);
        radio_body.setTextureOffset(161, 329).addBox(-5.0F, -9.0F, -6.0F, 10.0F, 2.0F, 12.0F, 0.0F, false);
        radio_body.setTextureOffset(71, 430).addBox(-2.2F, 6.0F, -7.6F, 8.0F, 1.0F, 15.0F, 0.0F, false);

        radio_dial_base = new ModelRenderer(this);
        radio_dial_base.setRotationPoint(13.0F, 15.0F, 3.0F);
        radio.addChild(radio_dial_base);
        setRotationAngle(radio_dial_base, 0.0F, 0.0F, 0.6109F);
        radio_dial_base.setTextureOffset(379, 60).addBox(-9.0F, -1.0F, -6.0F, 6.0F, 4.0F, 6.0F, 0.0F, false);

        refuler = new ModelRenderer(this);
        refuler.setRotationPoint(-50.0F, -50.5F, -8.5F);
        controlset_c.addChild(refuler);
        refuler.setTextureOffset(379, 60).addBox(102.0F, -0.5F, -14.5F, 4.0F, 4.0F, 12.0F, 0.0F, false);

        refueler_button_rotate_z = new LightModelRenderer(this);
        refueler_button_rotate_z.setRotationPoint(1.0F, -0.5F, -8.5F);
        refuler.addChild(refueler_button_rotate_z);
        refueler_button_rotate_z.setTextureOffset(390, 410).addBox(102.0F, -2.0F, -5.0F, 2.0F, 4.0F, 10.0F, 0.0F, false);

        baseplate_c1 = new ModelRenderer(this);
        baseplate_c1.setRotationPoint(38.0F, -52.5F, -10.5F);
        controlset_c.addChild(baseplate_c1);
        setRotationAngle(baseplate_c1, 0.0F, 0.0F, 0.3491F);
        baseplate_c1.setTextureOffset(379, 60).addBox(-3.0F, -3.5F, -3.5F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        baseplate_c1.setTextureOffset(84, 431).addBox(-2.6F, -4.5F, -3.1F, 5.0F, 1.0F, 5.0F, 0.0F, false);

        baseplate_c2 = new ModelRenderer(this);
        baseplate_c2.setRotationPoint(38.0F, -52.5F, 11.5F);
        controlset_c.addChild(baseplate_c2);
        setRotationAngle(baseplate_c2, 0.0F, 0.0F, 0.3491F);
        baseplate_c2.setTextureOffset(379, 60).addBox(-3.0F, -3.5F, -3.5F, 6.0F, 4.0F, 6.0F, 0.0F, false);
        baseplate_c2.setTextureOffset(85, 440).addBox(-2.6F, -4.5F, -3.1F, 5.0F, 1.0F, 5.0F, 0.0F, false);

        slider_c1 = new ModelRenderer(this);
        slider_c1.setRotationPoint(50.0F, -52.5F, -2.5F);
        controlset_c.addChild(slider_c1);
        setRotationAngle(slider_c1, 0.0F, 0.0F, 0.3491F);
        

        sliderknob_c1_rotate_z = new ModelRenderer(this);
        sliderknob_c1_rotate_z.setRotationPoint(-2.1F, 30.85F, -2.2F);
        slider_c1.addChild(sliderknob_c1_rotate_z);
        setRotationAngle(sliderknob_c1_rotate_z, 0.0F, 0.0F, 0.1745F);
        sliderknob_c1_rotate_z.setTextureOffset(70, 420).addBox(-2.8522F, -32.9705F, -1.5F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        sliderknob_c1_rotate_z.setTextureOffset(70, 420).addBox(-2.3522F, -30.9705F, -0.5F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        slider_track_c1 = new ModelRenderer(this);
        slider_track_c1.setRotationPoint(0.0F, 0.0F, 0.0F);
        slider_c1.addChild(slider_track_c1);
        slider_track_c1.setTextureOffset(379, 60).addBox(-7.0F, 0.5F, -3.7F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        slider_track_c1.setTextureOffset(379, 60).addBox(-6.0F, 0.5F, -1.7F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c1.setTextureOffset(379, 60).addBox(-6.0F, 0.5F, -3.7F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c1.setTextureOffset(78, 431).addBox(-6.0F, 1.5F, -2.7F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c1.setTextureOffset(379, 60).addBox(3.0F, 0.5F, -3.7F, 1.0F, 4.0F, 3.0F, 0.0F, false);

        slider_c2 = new ModelRenderer(this);
        slider_c2.setRotationPoint(50.0F, -52.5F, -2.5F);
        controlset_c.addChild(slider_c2);
        setRotationAngle(slider_c2, 0.0F, 0.0F, 0.3491F);
        

        slider_track_c2 = new ModelRenderer(this);
        slider_track_c2.setRotationPoint(0.0F, 0.0F, 0.0F);
        slider_c2.addChild(slider_track_c2);
        slider_track_c2.setTextureOffset(379, 60).addBox(-7.0F, 0.5F, 1.1F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        slider_track_c2.setTextureOffset(379, 60).addBox(-6.0F, 0.5F, 3.1F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c2.setTextureOffset(379, 60).addBox(-6.0F, 0.5F, 1.1F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c2.setTextureOffset(79, 431).addBox(-6.0F, 1.5F, 2.1F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c2.setTextureOffset(379, 60).addBox(3.0F, 0.5F, 1.1F, 1.0F, 4.0F, 3.0F, 0.0F, false);

        sliderknob_c2_rotate_z = new ModelRenderer(this);
        sliderknob_c2_rotate_z.setRotationPoint(-2.1F, 30.85F, 2.55F);
        slider_c2.addChild(sliderknob_c2_rotate_z);
        setRotationAngle(sliderknob_c2_rotate_z, 0.0F, 0.0F, 0.1745F);
        sliderknob_c2_rotate_z.setTextureOffset(70, 420).addBox(-2.8522F, -32.9705F, -1.5F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        sliderknob_c2_rotate_z.setTextureOffset(70, 420).addBox(-2.3522F, -30.9705F, -0.5F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        slider_c3 = new ModelRenderer(this);
        slider_c3.setRotationPoint(50.0F, -52.5F, -2.5F);
        controlset_c.addChild(slider_c3);
        setRotationAngle(slider_c3, 0.0F, 0.0F, 0.3491F);
        

        slider_track_c3 = new ModelRenderer(this);
        slider_track_c3.setRotationPoint(0.0F, 0.0F, 0.0F);
        slider_c3.addChild(slider_track_c3);
        slider_track_c3.setTextureOffset(379, 60).addBox(-7.0F, 0.5F, 5.9F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        slider_track_c3.setTextureOffset(379, 60).addBox(-6.0F, 0.5F, 7.9F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c3.setTextureOffset(379, 60).addBox(-6.0F, 0.5F, 5.9F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c3.setTextureOffset(73, 443).addBox(-6.0F, 1.5F, 6.9F, 9.0F, 4.0F, 1.0F, 0.0F, false);
        slider_track_c3.setTextureOffset(379, 60).addBox(3.0F, 0.5F, 5.9F, 1.0F, 4.0F, 3.0F, 0.0F, false);

        sliderknob_c3_rotate_z = new ModelRenderer(this);
        sliderknob_c3_rotate_z.setRotationPoint(-2.1F, 30.85F, 7.3F);
        slider_c3.addChild(sliderknob_c3_rotate_z);
        setRotationAngle(sliderknob_c3_rotate_z, 0.0F, 0.0F, 0.1745F);
        sliderknob_c3_rotate_z.setTextureOffset(70, 420).addBox(-2.8522F, -32.9705F, -1.5F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        sliderknob_c3_rotate_z.setTextureOffset(70, 420).addBox(-2.3522F, -30.9705F, -0.5F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        switch_c1 = new ModelRenderer(this);
        switch_c1.setRotationPoint(50.0F, -52.5F, -2.5F);
        controlset_c.addChild(switch_c1);
        setRotationAngle(switch_c1, 0.0F, 0.0F, 0.3491F);
        switch_c1.setTextureOffset(379, 60).addBox(0.0F, 0.5F, 11.5F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_c1 = new ModelRenderer(this);
        toggle_c1.setRotationPoint(1.7F, 0.7F, 12.8F);
        switch_c1.addChild(toggle_c1);
        setRotationAngle(toggle_c1, 0.0F, 0.0F, -0.6109F);
        toggle_c1.setTextureOffset(400, 300).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_c1.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_c2 = new ModelRenderer(this);
        switch_c2.setRotationPoint(50.0F, -52.5F, 1.5F);
        controlset_c.addChild(switch_c2);
        setRotationAngle(switch_c2, 0.0F, 0.0F, 0.3491F);
        switch_c2.setTextureOffset(379, 60).addBox(0.0F, 0.5F, 11.5F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_c2 = new ModelRenderer(this);
        toggle_c2.setRotationPoint(1.7F, 0.7F, 12.8F);
        switch_c2.addChild(toggle_c2);
        setRotationAngle(toggle_c2, 0.0F, 0.0F, -0.6109F);
        toggle_c2.setTextureOffset(400, 300).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_c2.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_c3 = new ModelRenderer(this);
        switch_c3.setRotationPoint(50.0F, -52.5F, 5.5F);
        controlset_c.addChild(switch_c3);
        setRotationAngle(switch_c3, 0.0F, 0.0F, 0.3491F);
        switch_c3.setTextureOffset(379, 60).addBox(0.0F, 0.5F, 11.5F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_c3 = new ModelRenderer(this);
        toggle_c3.setRotationPoint(1.7F, 0.7F, 12.8F);
        switch_c3.addChild(toggle_c3);
        setRotationAngle(toggle_c3, 0.0F, 0.0F, -0.6109F);
        toggle_c3.setTextureOffset(400, 300).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_c3.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_c4 = new ModelRenderer(this);
        switch_c4.setRotationPoint(50.0F, -52.5F, 9.5F);
        controlset_c.addChild(switch_c4);
        setRotationAngle(switch_c4, 0.0F, 0.0F, 0.3491F);
        switch_c4.setTextureOffset(379, 60).addBox(0.0F, 0.5F, 11.5F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_c4 = new ModelRenderer(this);
        toggle_c4.setRotationPoint(1.7F, 0.7F, 12.8F);
        switch_c4.addChild(toggle_c4);
        setRotationAngle(toggle_c4, 0.0F, 0.0F, -0.6109F);
        toggle_c4.setTextureOffset(400, 300).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_c4.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        controlset_d = new ModelRenderer(this);
        controlset_d.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controlset_d);
        

        waypoint_selector = new ModelRenderer(this);
        waypoint_selector.setRotationPoint(-38.0F, -53.0F, 0.0F);
        controlset_d.addChild(waypoint_selector);
        setRotationAngle(waypoint_selector, 0.0F, 0.0262F, -0.2618F);
        

        type_arm_rotate_x = new ModelRenderer(this);
        type_arm_rotate_x.setRotationPoint(-15.0F, -10.0F, 12.0F);
        waypoint_selector.addChild(type_arm_rotate_x);
        setRotationAngle(type_arm_rotate_x, 0.6109F, 0.0F, 1.6581F);
        type_arm_rotate_x.setTextureOffset(379, 60).addBox(-0.2F, -0.4F, -0.8F, 1.0F, 12.0F, 1.0F, 0.0F, false);
        type_arm_rotate_x.setTextureOffset(88, 439).addBox(-0.8F, 8.6F, -0.4F, 2.0F, 4.0F, 1.0F, 0.0F, false);

        keys = new ModelRenderer(this);
        keys.setRotationPoint(-19.6225F, -6.0F, -0.1575F);
        waypoint_selector.addChild(keys);
        setRotationAngle(keys, 0.0F, 0.0F, -0.2618F);
        keys.setTextureOffset(75, 429).addBox(-11.3775F, 1.0F, -7.8425F, 2.0F, 2.0F, 16.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-11.3775F, 1.0F, -10.4425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, -9.2425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, -6.8425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, -2.0425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, -4.4425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, 2.7575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, 0.3575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, 5.1575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, 9.9575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.3775F, 1.0F, 7.5575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-8.7775F, 1.0F, -12.4425F, 5.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, -10.0425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, -7.6425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, -2.8425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, -5.2425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, 1.9575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, -0.4425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, 4.3575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, 9.1575F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-5.7775F, 1.0F, 6.7575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, -11.6425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, -9.2425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, -6.8425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, -2.0425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, -4.4425F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, 2.7575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, 0.3575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, 5.1575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, 9.9575F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        keys.setTextureOffset(75, 429).addBox(-3.3775F, 1.0F, 7.5575F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        type_body = new ModelRenderer(this);
        type_body.setRotationPoint(0.0F, 0.0F, 0.0F);
        waypoint_selector.addChild(type_body);
        type_body.setTextureOffset(154, 315).addBox(-20.0F, -8.0F, -14.0F, 2.0F, 8.0F, 28.0F, 0.0F, false);
        type_body.setTextureOffset(154, 315).addBox(-18.0F, -8.0F, 10.0F, 8.0F, 8.0F, 4.0F, 0.0F, false);
        type_body.setTextureOffset(154, 315).addBox(-18.0F, -8.0F, -14.0F, 8.0F, 8.0F, 4.0F, 0.0F, false);
        type_body.setTextureOffset(154, 315).addBox(-12.0F, -8.0F, -10.2F, 2.0F, 8.0F, 20.0F, 0.0F, false);

        type_body_r1 = new ModelRenderer(this);
        type_body_r1.setRotationPoint(-25.0086F, -1.7166F, 0.0F);
        type_body.addChild(type_body_r1);
        setRotationAngle(type_body_r1, 0.0F, 0.0F, -0.2182F);
        type_body_r1.setTextureOffset(154, 315).addBox(-6.0F, -1.0F, -14.0F, 12.0F, 2.0F, 28.0F, 0.0F, false);

        tumbler = new ModelRenderer(this);
        tumbler.setRotationPoint(-7.95F, -6.95F, 0.0F);
        waypoint_selector.addChild(tumbler);
        setRotationAngle(tumbler, 0.0F, 0.0F, -0.7854F);
        tumbler.setTextureOffset(379, 60).addBox(-5.05F, -9.05F, 10.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);
        tumbler.setTextureOffset(198, 433).addBox(-6.05F, -8.05F, -10.0F, 4.0F, 4.0F, 20.0F, 0.0F, false);
        tumbler.setTextureOffset(379, 60).addBox(-6.05F, -7.05F, -11.0F, 3.0F, 3.0F, 22.0F, 0.0F, false);
        tumbler.setTextureOffset(194, 428).addBox(-2.05F, -4.05F, -10.0F, 7.0F, 1.0F, 20.0F, 0.0F, false);

        dimention_selector = new ModelRenderer(this);
        dimention_selector.setRotationPoint(-36.0F, -54.0F, 0.0F);
        controlset_d.addChild(dimention_selector);
        setRotationAngle(dimention_selector, 0.0F, 0.0F, -0.3491F);
        dimention_selector.setTextureOffset(379, 60).addBox(11.0F, -2.0F, -8.0F, 10.0F, 4.0F, 16.0F, 0.0F, false);

        tilt_d1 = new ModelRenderer(this);
        tilt_d1.setRotationPoint(14.5F, -4.0F, -0.1F);
        dimention_selector.addChild(tilt_d1);
        setRotationAngle(tilt_d1, 0.0F, 0.0F, -0.6109F);
        tilt_d1.setTextureOffset(65, 428).addBox(-5.5F, 1.0F, -7.5F, 11.0F, 5.0F, 15.0F, 0.0F, false);
        tilt_d1.setTextureOffset(430, 360).addBox(-0.5F, -0.2F, -5.7F, 5.0F, 3.0F, 11.0F, 0.0F, false);

        baseplate_d1 = new ModelRenderer(this);
        baseplate_d1.setRotationPoint(-36.0F, -54.0F, 0.0F);
        controlset_d.addChild(baseplate_d1);
        setRotationAngle(baseplate_d1, 0.0F, 0.0F, -0.3491F);
        baseplate_d1.setTextureOffset(379, 60).addBox(3.6F, -2.0F, 10.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(0.6F, -2.0F, 8.0F, 8.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(0.6F, -2.0F, 6.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(0.6F, -2.0F, -8.0F, 2.0F, 4.0F, 16.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(6.6F, -2.0F, -8.0F, 2.0F, 4.0F, 16.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(0.6F, -2.0F, -10.0F, 8.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(3.6F, -2.0F, -12.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(272, 282).addBox(2.6F, -1.2F, -8.0F, 4.0F, 4.0F, 16.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(-15.4F, -2.0F, 1.0F, 16.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(-7.4F, -2.0F, -19.0F, 2.0F, 4.0F, 18.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(-11.4F, -2.0F, -19.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);
        baseplate_d1.setTextureOffset(379, 60).addBox(-5.4F, -2.0F, -3.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);

        baseplate_d2 = new ModelRenderer(this);
        baseplate_d2.setRotationPoint(-36.0F, -54.0F, 0.0F);
        controlset_d.addChild(baseplate_d2);
        setRotationAngle(baseplate_d2, 0.0F, 0.0F, -0.3491F);
        baseplate_d2.setTextureOffset(379, 60).addBox(-19.4F, -2.0F, -22.0F, 8.0F, 4.0F, 6.0F, 0.0F, false);
        baseplate_d2.setTextureOffset(146, 429).addBox(-18.4F, -4.0F, -21.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);

        baseplate_d3 = new ModelRenderer(this);
        baseplate_d3.setRotationPoint(-36.0F, -54.0F, 0.0F);
        controlset_d.addChild(baseplate_d3);
        setRotationAngle(baseplate_d3, 0.0F, 0.0F, -0.3491F);
        baseplate_d3.setTextureOffset(379, 60).addBox(-19.4F, -2.0F, 16.0F, 8.0F, 4.0F, 6.0F, 0.0F, false);
        baseplate_d3.setTextureOffset(152, 433).addBox(-18.4F, -4.0F, 17.0F, 6.0F, 4.0F, 4.0F, 0.0F, false);

        controlset_e = new ModelRenderer(this);
        controlset_e.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controlset_e);
        setRotationAngle(controlset_e, 0.0F, 1.0472F, 0.0F);
        

        telepathic_circuits = new ModelRenderer(this);
        telepathic_circuits.setRotationPoint(-36.0F, -54.0F, 0.0F);
        controlset_e.addChild(telepathic_circuits);
        setRotationAngle(telepathic_circuits, 0.0F, 0.0F, -0.3491F);
        

        talking_board = new ModelRenderer(this);
        talking_board.setRotationPoint(0.0F, 0.0F, 0.0F);
        telepathic_circuits.addChild(talking_board);
        talking_board.setTextureOffset(37, 83).addBox(-15.6F, -2.0F, -14.0F, 19.0F, 4.0F, 27.0F, 0.0F, false);
        talking_board.setTextureOffset(379, 60).addBox(3.0F, -2.0F, 11.8F, 3.0F, 2.0F, 1.0F, 0.0F, false);
        talking_board.setTextureOffset(379, 60).addBox(6.0F, -2.0F, -0.2F, 1.0F, 2.0F, 13.0F, 0.0F, false);
        talking_board.setTextureOffset(379, 60).addBox(7.0F, -2.0F, -0.4F, 8.0F, 2.0F, 1.0F, 0.0F, false);
        talking_board.setTextureOffset(281, 103).addBox(5.6F, -2.5F, -0.8F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        talking_board.setTextureOffset(379, 60).addBox(13.0F, -2.0F, -4.8F, 1.0F, 2.0F, 9.0F, 0.0F, false);
        talking_board.setTextureOffset(280, 109).addBox(12.6F, -2.6F, -0.8F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        talkingboard_text = new ModelRenderer(this);
        talkingboard_text.setRotationPoint(36.2221F, 54.7048F, -0.1283F);
        talking_board.addChild(talkingboard_text);
        talkingboard_text.setTextureOffset(435, 332).addBox(-50.8F, -56.8F, 8.8F, 4.0F, 1.0F, 4.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-50.8F, -56.8F, -13.6F, 4.0F, 1.0F, 4.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-36.4F, -56.8F, -13.6F, 4.0F, 1.0F, 4.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-36.4F, -56.8F, 8.8F, 4.0F, 1.0F, 4.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-43.6F, -56.8F, 6.4F, 2.0F, 1.0F, 4.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-43.6F, -56.8F, -11.6F, 2.0F, 1.0F, 4.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-36.4F, -56.8F, -5.6F, 3.0F, 1.0F, 10.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-47.4F, -56.8F, -7.6F, 2.0F, 1.0F, 14.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-50.4F, -56.8F, -7.6F, 2.0F, 1.0F, 14.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-39.4F, -56.8F, -8.6F, 2.0F, 1.0F, 16.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-40.8F, -56.8F, 7.2F, 2.0F, 1.0F, 4.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-40.8F, -56.8F, -12.4F, 2.0F, 1.0F, 4.0F, 0.0F, false);
        talkingboard_text.setTextureOffset(435, 332).addBox(-42.0F, -56.8F, -7.6F, 2.0F, 1.0F, 14.0F, 0.0F, false);

        talkingboard_corners = new ModelRenderer(this);
        talkingboard_corners.setRotationPoint(36.0F, 54.0F, 0.0F);
        talking_board.addChild(talkingboard_corners);
        talkingboard_corners.setTextureOffset(449, 88).addBox(-35.0F, -57.0F, 10.8F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        talkingboard_corners.setTextureOffset(449, 88).addBox(-35.0F, -57.0F, -14.2F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        talkingboard_corners.setTextureOffset(449, 88).addBox(-52.0F, -57.0F, -14.2F, 3.0F, 2.0F, 3.0F, 0.0F, false);
        talkingboard_corners.setTextureOffset(449, 88).addBox(-52.0F, -57.0F, 10.8F, 3.0F, 2.0F, 3.0F, 0.0F, false);

        scrying_glass_rotate_y = new ModelRenderer(this);
        scrying_glass_rotate_y.setRotationPoint(15.2824F, 63.0681F, -0.2176F);
        telepathic_circuits.addChild(scrying_glass_rotate_y);
        setRotationAngle(scrying_glass_rotate_y, 0.0F, -1.6144F, 0.0F);
        

        glow_frame = new LightModelRenderer(this);
        glow_frame.setRotationPoint(0.1161F, -8.0681F, -0.9369F);
        scrying_glass_rotate_y.addChild(glow_frame);
        

        scrying_glass_frame_e1 = new ModelRenderer(this);
        scrying_glass_frame_e1.setRotationPoint(-3.6F, -59.9F, 26.4F);
        glow_frame.addChild(scrying_glass_frame_e1);
        scrying_glass_frame_e1.setTextureOffset(318, 337).addBox(-3.0F, -0.5F, -1.0F, 6.0F, 1.0F, 2.0F, 0.0F, false);

        scrying_glass_frame_e3 = new ModelRenderer(this);
        scrying_glass_frame_e3.setRotationPoint(0.4F, -59.9F, 25.4F);
        glow_frame.addChild(scrying_glass_frame_e3);
        setRotationAngle(scrying_glass_frame_e3, 0.0F, -1.0996F, 0.0F);
        scrying_glass_frame_e3.setTextureOffset(309, 326).addBox(-8.0F, -0.5F, -1.0F, 8.0F, 1.0F, 2.0F, 0.0F, false);

        scrying_glass_frame_e2 = new ModelRenderer(this);
        scrying_glass_frame_e2.setRotationPoint(-7.6F, -59.9F, 25.4F);
        glow_frame.addChild(scrying_glass_frame_e2);
        setRotationAngle(scrying_glass_frame_e2, 0.0F, 1.117F, 0.0F);
        scrying_glass_frame_e2.setTextureOffset(312, 335).addBox(0.0F, -0.5F, -1.0F, 8.0F, 1.0F, 2.0F, 0.0F, false);

        post_e1 = new ModelRenderer(this);
        post_e1.setRotationPoint(-3.3172F, -66.8681F, 22.1631F);
        scrying_glass_rotate_y.addChild(post_e1);
        post_e1.setTextureOffset(379, 60).addBox(-5.9667F, -2.0F, 1.3F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        post_e1.setTextureOffset(396, 47).addBox(3.0333F, -2.0F, 1.3F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        post_e1.setTextureOffset(379, 60).addBox(-1.5667F, -2.0F, -7.1F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        nixietube_e1 = new ModelRenderer(this);
        nixietube_e1.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_e.addChild(nixietube_e1);
        nixietube_e1.setTextureOffset(70, 420).addBox(-58.4F, -4.2F, 6.4F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        nixietube_e1.setTextureOffset(379, 60).addBox(-59.0F, -3.4F, 5.8F, 3.0F, 2.0F, 3.0F, 0.0F, false);

        nixietube_e2 = new ModelRenderer(this);
        nixietube_e2.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_e.addChild(nixietube_e2);
        nixietube_e2.setTextureOffset(70, 420).addBox(-58.4F, -4.2F, -4.4F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        nixietube_e2.setTextureOffset(379, 60).addBox(-59.0F, -3.4F, -5.0F, 3.0F, 2.0F, 3.0F, 0.0F, false);

        nixietube_e3 = new ModelRenderer(this);
        nixietube_e3.setRotationPoint(34.0F, -58.0F, -2.0F);
        controlset_e.addChild(nixietube_e3);
        nixietube_e3.setTextureOffset(70, 420).addBox(-56.0F, -4.2F, 1.2F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        nixietube_e3.setTextureOffset(379, 60).addBox(-56.6F, -3.4F, 0.6F, 3.0F, 2.0F, 3.0F, 0.0F, false);

        baseplate_e1 = new ModelRenderer(this);
        baseplate_e1.setRotationPoint(-52.0F, -54.0F, -19.0F);
        controlset_e.addChild(baseplate_e1);
        setRotationAngle(baseplate_e1, 0.0F, 0.0F, -0.3491F);
        baseplate_e1.setTextureOffset(379, 60).addBox(-2.6F, 3.0F, -2.4F, 5.0F, 4.0F, 5.0F, 0.0F, false);

        baseplate_e2 = new ModelRenderer(this);
        baseplate_e2.setRotationPoint(-52.0F, -54.0F, -19.0F);
        controlset_e.addChild(baseplate_e2);
        setRotationAngle(baseplate_e2, 0.0F, 0.0F, -0.3491F);
        baseplate_e2.setTextureOffset(379, 60).addBox(-2.6F, 3.0F, 34.6F, 5.0F, 4.0F, 5.0F, 0.0F, false);

        switch_e1 = new ModelRenderer(this);
        switch_e1.setRotationPoint(-32.25F, -26.0F, -6.85F);
        controlset_e.addChild(switch_e1);
        setRotationAngle(switch_e1, 0.0F, 3.1416F, -0.3491F);
        switch_e1.setTextureOffset(379, 60).addBox(-15.75F, -30.0F, -4.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_e1 = new ModelRenderer(this);
        toggle_e1.setRotationPoint(-14.05F, -29.8F, -3.45F);
        switch_e1.addChild(toggle_e1);
        setRotationAngle(toggle_e1, 0.0F, 0.0F, -0.6109F);
        toggle_e1.setTextureOffset(400, 300).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_e1.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_e2 = new ModelRenderer(this);
        switch_e2.setRotationPoint(-32.25F, -26.0F, -10.85F);
        controlset_e.addChild(switch_e2);
        setRotationAngle(switch_e2, 0.0F, 3.1416F, -0.3491F);
        switch_e2.setTextureOffset(379, 60).addBox(-15.75F, -30.0F, -4.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_e2 = new ModelRenderer(this);
        toggle_e2.setRotationPoint(-14.05F, -29.8F, -3.45F);
        switch_e2.addChild(toggle_e2);
        setRotationAngle(toggle_e2, 0.0F, 0.0F, -0.6109F);
        toggle_e2.setTextureOffset(400, 300).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_e2.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        controlset_f = new ModelRenderer(this);
        controlset_f.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(controlset_f);
        setRotationAngle(controlset_f, 0.0F, 2.0944F, 0.0F);
        

        baseplate_f1 = new ModelRenderer(this);
        baseplate_f1.setRotationPoint(-44.8F, -54.0F, 0.0F);
        controlset_f.addChild(baseplate_f1);
        setRotationAngle(baseplate_f1, -0.0087F, 0.0262F, -0.6109F);
        baseplate_f1.setTextureOffset(379, 60).addBox(-9.2F, 1.0F, -9.0F, 17.0F, 4.0F, 19.0F, 0.0F, false);

        switch_f1 = new ModelRenderer(this);
        switch_f1.setRotationPoint(4.15F, 4.2F, -5.85F);
        baseplate_f1.addChild(switch_f1);
        setRotationAngle(switch_f1, 0.0F, 3.1416F, -0.3491F);
        switch_f1.setTextureOffset(379, 60).addBox(1.25F, -6.0F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_f1 = new ModelRenderer(this);
        toggle_f1.setRotationPoint(2.95F, -5.8F, -0.45F);
        switch_f1.addChild(toggle_f1);
        setRotationAngle(toggle_f1, 0.0F, 0.0F, -0.8727F);
        toggle_f1.setTextureOffset(464, 342).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_f1.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_f2 = new ModelRenderer(this);
        switch_f2.setRotationPoint(3.95F, 4.2F, -0.05F);
        baseplate_f1.addChild(switch_f2);
        setRotationAngle(switch_f2, 0.0F, 3.1416F, -0.3491F);
        switch_f2.setTextureOffset(379, 60).addBox(1.25F, -6.0F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_f2 = new ModelRenderer(this);
        toggle_f2.setRotationPoint(2.95F, -5.8F, -0.45F);
        switch_f2.addChild(toggle_f2);
        setRotationAngle(toggle_f2, 0.0F, 0.0F, -0.8727F);
        toggle_f2.setTextureOffset(461, 320).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_f2.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_f3 = new ModelRenderer(this);
        switch_f3.setRotationPoint(3.95F, 4.2F, 6.15F);
        baseplate_f1.addChild(switch_f3);
        setRotationAngle(switch_f3, 0.0F, 3.1416F, -0.3491F);
        switch_f3.setTextureOffset(379, 60).addBox(1.25F, -6.0F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_f3 = new ModelRenderer(this);
        toggle_f3.setRotationPoint(2.95F, -5.8F, -0.45F);
        switch_f3.addChild(toggle_f3);
        setRotationAngle(toggle_f3, 0.0F, 0.0F, -0.8727F);
        toggle_f3.setTextureOffset(440, 329).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_f3.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_f4 = new ModelRenderer(this);
        switch_f4.setRotationPoint(8.55F, 4.6F, -5.85F);
        baseplate_f1.addChild(switch_f4);
        setRotationAngle(switch_f4, 0.0F, 3.1416F, -0.3491F);
        switch_f4.setTextureOffset(379, 60).addBox(1.25F, -6.0F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_f4 = new ModelRenderer(this);
        toggle_f4.setRotationPoint(2.95F, -5.8F, -0.45F);
        switch_f4.addChild(toggle_f4);
        setRotationAngle(toggle_f4, 0.0F, 0.0F, -0.8727F);
        toggle_f4.setTextureOffset(429, 318).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_f4.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_f5 = new ModelRenderer(this);
        switch_f5.setRotationPoint(8.55F, 4.0F, -0.25F);
        baseplate_f1.addChild(switch_f5);
        setRotationAngle(switch_f5, 0.0F, 3.1416F, -0.3491F);
        switch_f5.setTextureOffset(379, 60).addBox(1.25F, -6.0F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_f5 = new ModelRenderer(this);
        toggle_f5.setRotationPoint(2.95F, -5.8F, -0.45F);
        switch_f5.addChild(toggle_f5);
        setRotationAngle(toggle_f5, 0.0F, 0.0F, -0.8727F);
        toggle_f5.setTextureOffset(439, 342).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_f5.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        switch_f6 = new ModelRenderer(this);
        switch_f6.setRotationPoint(8.55F, 4.0F, 6.15F);
        baseplate_f1.addChild(switch_f6);
        setRotationAngle(switch_f6, 0.0F, 3.1416F, -0.3491F);
        switch_f6.setTextureOffset(379, 60).addBox(1.25F, -6.0F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        toggle_f6 = new ModelRenderer(this);
        toggle_f6.setRotationPoint(2.95F, -5.8F, -0.45F);
        switch_f6.addChild(toggle_f6);
        setRotationAngle(toggle_f6, 0.0F, 0.0F, -0.8727F);
        toggle_f6.setTextureOffset(457, 345).addBox(-0.7F, -3.2F, -0.38F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        toggle_f6.setTextureOffset(70, 420).addBox(-1.1F, -1.2F, -0.78F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        handbreak = new ModelRenderer(this);
        handbreak.setRotationPoint(-36.0F, -54.0F, 0.0F);
        controlset_f.addChild(handbreak);
        setRotationAngle(handbreak, 0.0F, 0.0F, -0.3491F);
        handbreak.setTextureOffset(379, 60).addBox(-15.4F, -2.0F, -20.0F, 7.0F, 4.0F, 8.0F, 0.0F, false);

        lever_f1_rotate_z = new ModelRenderer(this);
        lever_f1_rotate_z.setRotationPoint(-11.9F, -3.2F, -17.0F);
        handbreak.addChild(lever_f1_rotate_z);
        setRotationAngle(lever_f1_rotate_z, 0.0F, 0.0F, 3.0543F);
        lever_f1_rotate_z.setTextureOffset(379, 60).addBox(-1.5F, -0.6F, -3.4F, 2.0F, 2.0F, 9.0F, 0.0F, false);
        lever_f1_rotate_z.setTextureOffset(379, 60).addBox(-5.5F, -0.6F, 4.2F, 4.0F, 2.0F, 1.0F, 0.0F, false);
        lever_f1_rotate_z.setTextureOffset(379, 60).addBox(-5.5F, -0.6F, -3.2F, 4.0F, 2.0F, 1.0F, 0.0F, false);
        lever_f1_rotate_z.setTextureOffset(379, 60).addBox(-6.5F, -0.6F, -3.0F, 1.0F, 2.0F, 8.0F, 0.0F, false);
        lever_f1_rotate_z.setTextureOffset(379, 60).addBox(-14.5F, -0.6F, 0.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        lever_f1_rotate_z.setTextureOffset(363, 193).addBox(-13.5F, -1.1F, -0.5F, 6.0F, 3.0F, 3.0F, 0.0F, false);

        barrel = new ModelRenderer(this);
        barrel.setRotationPoint(-13.0F, -3.0F, -16.0F);
        handbreak.addChild(barrel);
        barrel.setTextureOffset(150, 427).addBox(-1.2F, -3.0F, -3.0F, 4.0F, 5.0F, 6.0F, 0.0F, false);

        randomize_cords = new ModelRenderer(this);
        randomize_cords.setRotationPoint(-36.0F, -56.0F, 0.0F);
        controlset_f.addChild(randomize_cords);
        setRotationAngle(randomize_cords, 0.0F, 0.0349F, -0.0873F);
        randomize_cords.setTextureOffset(379, 60).addBox(4.0F, -2.6F, -6.0F, 12.0F, 4.0F, 12.0F, 0.0F, false);

        globe_rotate_y = new LightModelRenderer(this);
        globe_rotate_y.setRotationPoint(9.0F, -15.4F, 1.0F);
        randomize_cords.addChild(globe_rotate_y);
        setRotationAngle(globe_rotate_y, -1.5708F, 2.0944F, 0.0F);
        globe_rotate_y.setTextureOffset(379, 60).addBox(0.8F, -4.2F, -0.6F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        globe_rotate_y.setTextureOffset(379, 60).addBox(-1.0F, -4.2F, 1.8F, 3.0F, 4.0F, 2.0F, 0.0F, false);
        globe_rotate_y.setTextureOffset(379, 60).addBox(-4.2F, -4.2F, 4.8F, 3.0F, 4.0F, 2.0F, 0.0F, false);
        globe_rotate_y.setTextureOffset(379, 60).addBox(-3.8F, 2.8F, 3.0F, 3.0F, 2.0F, 5.0F, 0.0F, false);
        globe_rotate_y.setTextureOffset(379, 60).addBox(-4.2F, -3.2F, 2.8F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        globe_rotate_y.setTextureOffset(379, 60).addBox(0.8F, 2.8F, 1.8F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        globe_rotate_y.setTextureOffset(379, 60).addBox(2.8F, 2.8F, 4.8F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        globe_rotate_y.setTextureOffset(379, 60).addBox(2.2F, 3.2F, 4.2F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        globe_rotate_y.setTextureOffset(379, 60).addBox(-2.2F, -1.4F, -0.8F, 4.0F, 6.0F, 1.0F, 0.0F, false);

        glow_globe = new LightModelRenderer(this);
        glow_globe.setRotationPoint(27.0F, 71.4F, -1.0F);
        globe_rotate_y.addChild(glow_globe);
        glow_globe.setTextureOffset(264, 384).addBox(-30.6F, -75.0F, 0.6F, 8.0F, 8.0F, 8.0F, 0.0F, false);

        globe_mount = new LightModelRenderer(this);
        globe_mount.setRotationPoint(0.0F, 0.0F, 0.0F);
        randomize_cords.addChild(globe_mount);
        globe_mount.setTextureOffset(81, 431).addBox(5.0F, -3.8F, -3.8F, 8.0F, 2.0F, 8.0F, 0.0F, false);
        globe_mount.setTextureOffset(379, 60).addBox(9.0F, -19.0F, -8.0F, 1.0F, 12.0F, 2.0F, 0.0F, false);
        globe_mount.setTextureOffset(379, 60).addBox(9.0F, -7.0F, -8.0F, 1.0F, 2.0F, 8.0F, 0.0F, false);
        globe_mount.setTextureOffset(379, 60).addBox(9.0F, -19.2F, 0.0F, 1.0F, 16.0F, 1.0F, 0.0F, false);
        globe_mount.setTextureOffset(379, 60).addBox(9.0F, -19.0F, -6.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
        globe_mount.setTextureOffset(190, 319).addBox(5.6F, -4.4F, -3.4F, 7.0F, 2.0F, 7.0F, 0.0F, false);

        rotation_selector = new ModelRenderer(this);
        rotation_selector.setRotationPoint(50.0F, -50.0F, 14.0F);
        controlset_f.addChild(rotation_selector);
        rotation_selector.setTextureOffset(379, 60).addBox(-103.0F, -1.0F, 2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        rotation_crank_rotate_y = new ModelRenderer(this);
        rotation_crank_rotate_y.setRotationPoint(-101.0F, -4.9F, 3.8F);
        rotation_selector.addChild(rotation_crank_rotate_y);
        rotation_crank_rotate_y.setTextureOffset(146, 428).addBox(-1.0F, -2.1F, -0.8F, 2.0F, 8.0F, 2.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(379, 60).addBox(-1.0F, -1.1F, -2.8F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(379, 60).addBox(-1.0F, -1.1F, 1.2F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(379, 60).addBox(-3.0F, -1.1F, -0.8F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(379, 60).addBox(1.0F, -1.1F, -0.8F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(379, 60).addBox(3.0F, -1.1F, -2.8F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(379, 60).addBox(-5.0F, -1.1F, -2.8F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(379, 60).addBox(-3.0F, -1.1F, 3.2F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(379, 60).addBox(-3.0F, -1.1F, -4.8F, 6.0F, 2.0F, 2.0F, 0.0F, false);
        rotation_crank_rotate_y.setTextureOffset(90, 433).addBox(3.0F, -5.1F, -0.8F, 2.0F, 4.0F, 2.0F, 0.0F, false);

        structure = new ModelRenderer(this);
        structure.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        side_1 = new ModelRenderer(this);
        side_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        structure.addChild(side_1);
        

        skin = new ModelRenderer(this);
        skin.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_1.addChild(skin);
        setRotationAngle(skin, 0.0F, 0.5236F, 0.0F);
        

        floor_skin = new ModelRenderer(this);
        floor_skin.setRotationPoint(0.0F, 0.0F, 0.0F);
        skin.addChild(floor_skin);
        

        floorboards = new ModelRenderer(this);
        floorboards.setRotationPoint(0.0F, 0.0F, 0.0F);
        floor_skin.addChild(floorboards);
        setRotationAngle(floorboards, 3.1416F, 0.0F, 0.0F);
        floorboards.setTextureOffset(214, 162).addBox(-46.0F, 0.25F, -23.0F, 12.0F, 1.0F, 45.0F, 0.0F, false);
        floorboards.setTextureOffset(318, 76).addBox(-34.0F, 0.5F, -17.0F, 10.0F, 2.0F, 34.0F, 0.0F, false);
        floorboards.setTextureOffset(285, 73).addBox(-24.25F, 1.5F, -13.0F, 3.0F, 4.0F, 26.0F, 0.0F, false);

        leg_skin = new ModelRenderer(this);
        leg_skin.setRotationPoint(-23.0F, -3.0F, 0.0F);
        floor_skin.addChild(leg_skin);
        setRotationAngle(leg_skin, 0.0F, 0.0F, -1.2217F);
        leg_skin.setTextureOffset(319, 178).addBox(0.0F, 0.0F, -12.0F, 20.0F, 1.0F, 24.0F, 0.0F, false);

        knee_skin = new ModelRenderer(this);
        knee_skin.setRotationPoint(16.0F, -4.0F, 0.0F);
        leg_skin.addChild(knee_skin);
        setRotationAngle(knee_skin, 0.0F, 0.0F, 1.2217F);
        knee_skin.setTextureOffset(329, 88).addBox(1.0F, -16.0F, -9.0F, 4.0F, 4.0F, 20.0F, 0.0F, false);
        knee_skin.setTextureOffset(319, 178).addBox(3.0F, -17.0F, -10.0F, 1.0F, 20.0F, 22.0F, 0.0F, false);
        knee_skin.setTextureOffset(320, 57).addBox(-2.0F, 12.0F, -13.0F, 4.0F, 4.0F, 27.0F, 0.0F, false);

        thigh_skin = new ModelRenderer(this);
        thigh_skin.setRotationPoint(1.0F, -14.0F, 0.0F);
        knee_skin.addChild(thigh_skin);
        setRotationAngle(thigh_skin, 0.0F, 0.0F, -0.6981F);
        thigh_skin.setTextureOffset(35, 73).addBox(0.75F, -13.0F, -13.0F, 1.0F, 13.0F, 26.0F, 0.0F, false);

        belly_skin = new ModelRenderer(this);
        belly_skin.setRotationPoint(2.0F, -10.0F, 0.0F);
        thigh_skin.addChild(belly_skin);
        setRotationAngle(belly_skin, 0.0F, 0.0F, -0.8727F);
        belly_skin.setTextureOffset(319, 178).addBox(1.0F, -35.0F, -28.0F, 1.0F, 19.0F, 55.0F, 0.0F, false);
        belly_skin.setTextureOffset(305, 71).addBox(0.5F, -16.0F, -20.0F, 2.0F, 14.0F, 40.0F, 0.0F, false);

        rimtop_skin = new ModelRenderer(this);
        rimtop_skin.setRotationPoint(4.5F, -34.0F, 0.0F);
        belly_skin.addChild(rimtop_skin);
        setRotationAngle(rimtop_skin, 0.0F, 0.0F, 2.9671F);
        rimtop_skin.setTextureOffset(319, 178).addBox(-1.5F, -8.0F, -34.0F, 1.0F, 8.0F, 68.0F, 0.0F, false);

        panel_skin = new ModelRenderer(this);
        panel_skin.setRotationPoint(-6.5F, -4.0F, 1.0F);
        rimtop_skin.addChild(panel_skin);
        setRotationAngle(panel_skin, 0.0F, 0.0F, 3.0718F);
        panel_skin.setTextureOffset(319, 188).addBox(-6.55F, 3.0F, -27.0F, 1.0F, 14.0F, 51.0F, 0.0F, false);
        panel_skin.setTextureOffset(319, 184).addBox(-6.55F, 17.0F, -19.0F, 1.0F, 14.0F, 36.0F, 0.0F, false);
        panel_skin.setTextureOffset(319, 178).addBox(-6.55F, 31.0F, -13.0F, 1.0F, 12.0F, 23.0F, 0.0F, false);

        skelly = new ModelRenderer(this);
        skelly.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_1.addChild(skelly);
        

        rib = new ModelRenderer(this);
        rib.setRotationPoint(-68.0F, -49.0F, -2.0F);
        skelly.addChild(rib);
        setRotationAngle(rib, 0.0F, 0.0F, -0.2618F);
        rib.setTextureOffset(370, 107).addBox(1.0F, -1.0F, -2.0F, 54.0F, 5.0F, 8.0F, 0.0F, false);
        rib.setTextureOffset(325, 77).addBox(0.0F, -3.0F, 1.0F, 55.0F, 4.0F, 2.0F, 0.0F, false);

        rib_bolts_1 = new ModelRenderer(this);
        rib_bolts_1.setRotationPoint(68.0F, 49.0F, 2.0F);
        rib.addChild(rib_bolts_1);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_1.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        foot = new ModelRenderer(this);
        foot.setRotationPoint(-8.0F, 0.0F, -2.0F);
        skelly.addChild(foot);
        foot.setTextureOffset(354, 84).addBox(-47.0F, -5.0F, -3.0F, 31.0F, 4.0F, 10.0F, 0.0F, false);
        foot.setTextureOffset(419, 78).addBox(-52.0F, -6.0F, -4.0F, 7.0F, 6.0F, 12.0F, 0.0F, false);
        foot.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, 5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot.setTextureOffset(302, 0).addBox(-16.0F, -40.0F, -0.5F, 4.0F, 33.0F, 5.0F, 0.0F, false);
        foot.setTextureOffset(333, 84).addBox(-59.0F, -46.0F, -2.0F, 40.0F, 4.0F, 8.0F, 0.0F, false);
        foot.setTextureOffset(349, 84).addBox(-63.5F, -50.25F, -2.5F, 19.0F, 8.0F, 9.0F, 0.0F, false);
        foot.setTextureOffset(353, 72).addBox(-7.75F, -80.0F, -3.0F, 4.0F, 20.0F, 10.0F, 0.0F, false);
        foot.setTextureOffset(339, 74).addBox(-65.0F, -52.0F, 0.5F, 12.0F, 12.0F, 3.0F, 0.0F, false);
        foot.setTextureOffset(160, 75).addBox(-9.0F, -77.4F, -4.0F, 4.0F, 3.0F, 12.0F, 0.0F, false);

        footslope = new ModelRenderer(this);
        footslope.setRotationPoint(8.0F, -2.0F, 1.5F);
        foot.addChild(footslope);
        

        foot_r1 = new ModelRenderer(this);
        foot_r1.setRotationPoint(-41.9844F, -2.6072F, 5.0F);
        footslope.addChild(foot_r1);
        setRotationAngle(foot_r1, 0.0F, 0.0F, -0.2182F);
        foot_r1.setTextureOffset(399, 102).addBox(-18.5F, -5.5F, -6.0F, 38.0F, 6.0F, 3.0F, 0.0F, false);

        foot_r2 = new ModelRenderer(this);
        foot_r2.setRotationPoint(-39.5F, -3.5F, 0.5F);
        footslope.addChild(foot_r2);
        setRotationAngle(foot_r2, 0.0F, 0.0F, -0.2182F);
        foot_r2.setTextureOffset(386, 210).addBox(-14.5F, -3.5F, -4.0F, 12.0F, 5.0F, 8.0F, 0.0F, false);
        foot_r2.setTextureOffset(387, 194).addBox(-2.5F, -3.5F, -4.0F, 25.0F, 8.0F, 8.0F, 0.0F, false);

        leg = new ModelRenderer(this);
        leg.setRotationPoint(-21.0F, -4.0F, 0.0F);
        foot.addChild(leg);
        setRotationAngle(leg, 0.0F, 0.0F, 0.4363F);
        leg.setTextureOffset(430, 61).addBox(0.0F, -12.0F, 0.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        leg.setTextureOffset(51, 93).addBox(-16.0F, -65.0F, 0.0F, 4.0F, 10.0F, 4.0F, 0.0F, false);

        thigh = new ModelRenderer(this);
        thigh.setRotationPoint(-8.0F, -32.0F, 0.0F);
        foot.addChild(thigh);
        setRotationAngle(thigh, 0.0F, 0.0F, -0.7854F);
        thigh.setTextureOffset(46, 91).addBox(-9.0F, -26.0F, 0.0F, 9.0F, 27.0F, 4.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-17.0F, -32.0F, 0.0F, 2.0F, 39.0F, 4.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-15.0F, 0.0F, 1.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-15.0F, -9.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-15.0F, -18.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-20.0F, -23.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-20.0F, -14.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-20.0F, -5.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-20.0F, -30.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh.setTextureOffset(46, 91).addBox(-15.0F, -30.0F, 1.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);

        floorpipes = new ModelRenderer(this);
        floorpipes.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_1.addChild(floorpipes);
        setRotationAngle(floorpipes, 0.0F, 0.5236F, 0.0F);
        floorpipes.setTextureOffset(200, 102).addBox(-31.6F, -4.0F, 0.4F, 6.0F, 2.0F, 13.0F, 0.0F, false);
        floorpipes.setTextureOffset(309, 70).addBox(-31.0F, -43.0F, -2.0F, 2.0F, 20.0F, 2.0F, 0.0F, false);
        floorpipes.setTextureOffset(375, 69).addBox(-31.0F, -23.0F, -2.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        floorpipes.setTextureOffset(317, 372).addBox(-32.0F, -24.0F, 1.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        floorpipes.setTextureOffset(443, 91).addBox(-31.0F, -20.0F, 2.0F, 2.0F, 19.0F, 2.0F, 0.0F, false);
        floorpipes.setTextureOffset(203, 434).addBox(-27.2F, -17.4F, -6.0F, 4.0F, 7.0F, 7.0F, 0.0F, false);
        floorpipes.setTextureOffset(80, 429).addBox(-26.6F, -18.0F, -6.6F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        floorpipes.setTextureOffset(210, 114).addBox(-25.6F, -4.0F, -3.6F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        floorpipes.setTextureOffset(345, 50).addBox(-25.0F, -10.0F, -3.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);

        front_rail = new ModelRenderer(this);
        front_rail.setRotationPoint(-88.0F, 0.0F, 2.0F);
        side_1.addChild(front_rail);
        setRotationAngle(front_rail, 0.0F, 0.5236F, 0.0F);
        front_rail.setTextureOffset(345, 32).addBox(14.7F, -46.85F, 11.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        front_railbolt = new ModelRenderer(this);
        front_railbolt.setRotationPoint(88.0F, 0.0F, -2.0F);
        front_rail.addChild(front_railbolt);
        front_railbolt.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 69.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 17.15F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 44.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 30.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 57.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        rail_topbevel = new ModelRenderer(this);
        rail_topbevel.setRotationPoint(16.0F, -50.0F, 43.0F);
        front_rail.addChild(rail_topbevel);
        setRotationAngle(rail_topbevel, 0.0F, 0.0F, 0.8727F);
        rail_topbevel.setTextureOffset(331, 39).addBox(1.6F, -1.0F, -32.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        rail_underbevel = new ModelRenderer(this);
        rail_underbevel.setRotationPoint(16.0F, -42.0F, 43.0F);
        front_rail.addChild(rail_underbevel);
        setRotationAngle(rail_underbevel, 0.0F, 0.0F, -0.9599F);
        rail_underbevel.setTextureOffset(328, 37).addBox(-0.075F, -1.55F, -35.0F, 2.0F, 4.0F, 69.0F, 0.0F, false);

        rotor_gasket = new ModelRenderer(this);
        rotor_gasket.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_1.addChild(rotor_gasket);
        setRotationAngle(rotor_gasket, 0.0F, 0.5236F, 0.0F);
        rotor_gasket.setTextureOffset(304, 52).addBox(-15.0F, -70.0F, -4.0F, 2.0F, 10.0F, 8.0F, 0.0F, false);
        rotor_gasket.setTextureOffset(352, 63).addBox(-14.0F, -79.0F, -4.0F, 2.0F, 6.0F, 8.0F, 0.0F, false);
        rotor_gasket.setTextureOffset(136, 96).addBox(-17.0F, -76.9F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        rotor_gasket.setTextureOffset(297, 77).addBox(-17.0F, -67.0F, -5.0F, 2.0F, 10.0F, 10.0F, 0.0F, false);

        floor_trim = new ModelRenderer(this);
        floor_trim.setRotationPoint(-86.0F, -0.75F, 44.0F);
        side_1.addChild(floor_trim);
        setRotationAngle(floor_trim, 0.0F, 0.5236F, 0.0F);
        floor_trim.setTextureOffset(292, 18).addBox(43.0F, -2.0F, -22.0F, 8.0F, 2.0F, 54.0F, 0.0F, false);
        floor_trim.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -14.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 13.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -5.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 4.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 21.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);

        side_2 = new ModelRenderer(this);
        side_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        structure.addChild(side_2);
        setRotationAngle(side_2, 0.0F, -1.0472F, 0.0F);
        

        skin2 = new ModelRenderer(this);
        skin2.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_2.addChild(skin2);
        setRotationAngle(skin2, 0.0F, 0.5236F, 0.0F);
        

        floor_skin2 = new ModelRenderer(this);
        floor_skin2.setRotationPoint(0.0F, 0.0F, 0.0F);
        skin2.addChild(floor_skin2);
        

        floorboards2 = new ModelRenderer(this);
        floorboards2.setRotationPoint(0.0F, 0.0F, 0.0F);
        floor_skin2.addChild(floorboards2);
        setRotationAngle(floorboards2, 3.1416F, 0.0F, 0.0F);
        floorboards2.setTextureOffset(214, 162).addBox(-46.0F, 0.25F, -23.0F, 12.0F, 1.0F, 45.0F, 0.0F, false);
        floorboards2.setTextureOffset(318, 76).addBox(-34.0F, 0.5F, -17.0F, 10.0F, 2.0F, 34.0F, 0.0F, false);
        floorboards2.setTextureOffset(285, 73).addBox(-24.25F, 1.5F, -13.0F, 3.0F, 4.0F, 26.0F, 0.0F, false);

        leg_skin2 = new ModelRenderer(this);
        leg_skin2.setRotationPoint(-23.0F, -3.0F, 0.0F);
        floor_skin2.addChild(leg_skin2);
        setRotationAngle(leg_skin2, 0.0F, 0.0F, -1.2217F);
        leg_skin2.setTextureOffset(319, 178).addBox(0.0F, 0.0F, -12.0F, 20.0F, 1.0F, 24.0F, 0.0F, false);

        knee_skin2 = new ModelRenderer(this);
        knee_skin2.setRotationPoint(16.0F, -4.0F, 0.0F);
        leg_skin2.addChild(knee_skin2);
        setRotationAngle(knee_skin2, 0.0F, 0.0F, 1.2217F);
        knee_skin2.setTextureOffset(329, 88).addBox(1.0F, -16.0F, -9.0F, 4.0F, 4.0F, 20.0F, 0.0F, false);
        knee_skin2.setTextureOffset(319, 178).addBox(3.0F, -17.0F, -10.0F, 1.0F, 20.0F, 22.0F, 0.0F, false);
        knee_skin2.setTextureOffset(320, 57).addBox(-2.0F, 12.0F, -13.0F, 4.0F, 4.0F, 27.0F, 0.0F, false);

        thigh_skin2 = new ModelRenderer(this);
        thigh_skin2.setRotationPoint(1.0F, -14.0F, 0.0F);
        knee_skin2.addChild(thigh_skin2);
        setRotationAngle(thigh_skin2, 0.0F, 0.0F, -0.6981F);
        thigh_skin2.setTextureOffset(35, 73).addBox(0.75F, -13.0F, -13.0F, 1.0F, 13.0F, 26.0F, 0.0F, false);

        belly_skin2 = new ModelRenderer(this);
        belly_skin2.setRotationPoint(2.0F, -10.0F, 0.0F);
        thigh_skin2.addChild(belly_skin2);
        setRotationAngle(belly_skin2, 0.0F, 0.0F, -0.8727F);
        belly_skin2.setTextureOffset(319, 178).addBox(1.0F, -35.0F, -28.0F, 1.0F, 19.0F, 55.0F, 0.0F, false);
        belly_skin2.setTextureOffset(305, 71).addBox(0.5F, -16.0F, -20.0F, 2.0F, 14.0F, 40.0F, 0.0F, false);

        rimtop_skin2 = new ModelRenderer(this);
        rimtop_skin2.setRotationPoint(4.5F, -34.0F, 0.0F);
        belly_skin2.addChild(rimtop_skin2);
        setRotationAngle(rimtop_skin2, 0.0F, 0.0F, 2.9671F);
        rimtop_skin2.setTextureOffset(319, 178).addBox(-1.5F, -8.0F, -34.0F, 1.0F, 8.0F, 68.0F, 0.0F, false);

        panel_skin2 = new ModelRenderer(this);
        panel_skin2.setRotationPoint(-6.5F, -4.0F, 1.0F);
        rimtop_skin2.addChild(panel_skin2);
        setRotationAngle(panel_skin2, 0.0F, 0.0F, 3.0718F);
        panel_skin2.setTextureOffset(319, 188).addBox(-6.55F, 3.0F, -27.0F, 1.0F, 14.0F, 51.0F, 0.0F, false);
        panel_skin2.setTextureOffset(319, 184).addBox(-6.55F, 17.0F, -19.0F, 1.0F, 14.0F, 36.0F, 0.0F, false);
        panel_skin2.setTextureOffset(319, 178).addBox(-6.55F, 31.0F, -13.0F, 1.0F, 12.0F, 23.0F, 0.0F, false);

        skelly2 = new ModelRenderer(this);
        skelly2.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_2.addChild(skelly2);
        

        rib2 = new ModelRenderer(this);
        rib2.setRotationPoint(-68.0F, -49.0F, -2.0F);
        skelly2.addChild(rib2);
        setRotationAngle(rib2, 0.0F, 0.0F, -0.2618F);
        rib2.setTextureOffset(370, 107).addBox(1.0F, -1.0F, -2.0F, 54.0F, 5.0F, 8.0F, 0.0F, false);
        rib2.setTextureOffset(325, 77).addBox(0.0F, -3.0F, 1.0F, 55.0F, 4.0F, 2.0F, 0.0F, false);

        rib_bolts_2 = new ModelRenderer(this);
        rib_bolts_2.setRotationPoint(68.0F, 49.0F, 2.0F);
        rib2.addChild(rib_bolts_2);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_2.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        foot2 = new ModelRenderer(this);
        foot2.setRotationPoint(-8.0F, 0.0F, -2.0F);
        skelly2.addChild(foot2);
        foot2.setTextureOffset(354, 84).addBox(-47.0F, -5.0F, -3.0F, 31.0F, 4.0F, 10.0F, 0.0F, false);
        foot2.setTextureOffset(419, 78).addBox(-52.0F, -6.0F, -4.0F, 7.0F, 6.0F, 12.0F, 0.0F, false);
        foot2.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, 5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot2.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot2.setTextureOffset(302, 0).addBox(-16.0F, -40.0F, -0.5F, 4.0F, 33.0F, 5.0F, 0.0F, false);
        foot2.setTextureOffset(333, 84).addBox(-59.0F, -46.0F, -2.0F, 40.0F, 4.0F, 8.0F, 0.0F, false);
        foot2.setTextureOffset(349, 84).addBox(-63.5F, -50.25F, -2.5F, 19.0F, 8.0F, 9.0F, 0.0F, false);
        foot2.setTextureOffset(353, 72).addBox(-7.75F, -80.0F, -3.0F, 4.0F, 20.0F, 10.0F, 0.0F, false);
        foot2.setTextureOffset(339, 74).addBox(-65.0F, -52.0F, 0.5F, 12.0F, 12.0F, 3.0F, 0.0F, false);
        foot2.setTextureOffset(160, 75).addBox(-9.0F, -77.4F, -4.0F, 4.0F, 3.0F, 12.0F, 0.0F, false);

        footslope2 = new ModelRenderer(this);
        footslope2.setRotationPoint(8.0F, -2.0F, 1.5F);
        foot2.addChild(footslope2);
        

        foot_r3 = new ModelRenderer(this);
        foot_r3.setRotationPoint(-41.9844F, -2.6072F, 5.0F);
        footslope2.addChild(foot_r3);
        setRotationAngle(foot_r3, 0.0F, 0.0F, -0.2182F);
        foot_r3.setTextureOffset(399, 102).addBox(-18.5F, -5.5F, -6.0F, 38.0F, 6.0F, 3.0F, 0.0F, false);

        foot_r4 = new ModelRenderer(this);
        foot_r4.setRotationPoint(-39.5F, -3.5F, 0.5F);
        footslope2.addChild(foot_r4);
        setRotationAngle(foot_r4, 0.0F, 0.0F, -0.2182F);
        foot_r4.setTextureOffset(386, 210).addBox(-14.5F, -3.5F, -4.0F, 12.0F, 5.0F, 8.0F, 0.0F, false);
        foot_r4.setTextureOffset(387, 194).addBox(-2.5F, -3.5F, -4.0F, 25.0F, 8.0F, 8.0F, 0.0F, false);

        leg2 = new ModelRenderer(this);
        leg2.setRotationPoint(-21.0F, -4.0F, 0.0F);
        foot2.addChild(leg2);
        setRotationAngle(leg2, 0.0F, 0.0F, 0.4363F);
        leg2.setTextureOffset(430, 61).addBox(0.0F, -12.0F, 0.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        leg2.setTextureOffset(51, 93).addBox(-16.0F, -65.0F, 0.0F, 4.0F, 10.0F, 4.0F, 0.0F, false);

        thigh2 = new ModelRenderer(this);
        thigh2.setRotationPoint(-8.0F, -32.0F, 0.0F);
        foot2.addChild(thigh2);
        setRotationAngle(thigh2, 0.0F, 0.0F, -0.7854F);
        thigh2.setTextureOffset(46, 91).addBox(-9.0F, -26.0F, 0.0F, 9.0F, 27.0F, 4.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-17.0F, -32.0F, 0.0F, 2.0F, 39.0F, 4.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-15.0F, 0.0F, 1.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-15.0F, -9.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-15.0F, -18.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-20.0F, -23.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-20.0F, -14.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-20.0F, -5.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-20.0F, -30.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh2.setTextureOffset(46, 91).addBox(-15.0F, -30.0F, 1.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);

        floorpipes2 = new ModelRenderer(this);
        floorpipes2.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_2.addChild(floorpipes2);
        setRotationAngle(floorpipes2, 0.0F, 0.5236F, 0.0F);
        floorpipes2.setTextureOffset(200, 102).addBox(-31.6F, -4.0F, 0.4F, 6.0F, 2.0F, 13.0F, 0.0F, false);
        floorpipes2.setTextureOffset(58, 67).addBox(-31.0F, -44.0F, 7.0F, 5.0F, 40.0F, 5.0F, 0.0F, false);
        floorpipes2.setTextureOffset(58, 67).addBox(-36.0F, -27.0F, -9.0F, 19.0F, 5.0F, 5.0F, 0.0F, false);
        floorpipes2.setTextureOffset(58, 67).addBox(-41.0F, -46.0F, -9.0F, 5.0F, 19.0F, 5.0F, 0.0F, false);
        floorpipes2.setTextureOffset(204, 15).addBox(-42.0F, -28.0F, -10.0F, 8.0F, 7.0F, 7.0F, 0.0F, false);
        floorpipes2.setTextureOffset(204, 15).addBox(-42.0F, -31.0F, -10.0F, 7.0F, 3.0F, 7.0F, 0.0F, false);
        floorpipes2.setTextureOffset(204, 15).addBox(-34.25F, -28.5F, -10.5F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        floorpipes2.setTextureOffset(204, 15).addBox(-42.5F, -34.0F, -10.5F, 8.0F, 3.0F, 8.0F, 0.0F, false);
        floorpipes2.setTextureOffset(204, 15).addBox(-31.5F, -20.0F, 6.5F, 6.0F, 8.0F, 6.0F, 0.0F, false);
        floorpipes2.setTextureOffset(204, 15).addBox(-32.5F, -17.5F, 5.5F, 8.0F, 3.0F, 8.0F, 0.0F, false);

        front_rail2 = new ModelRenderer(this);
        front_rail2.setRotationPoint(-88.0F, 0.0F, 2.0F);
        side_2.addChild(front_rail2);
        setRotationAngle(front_rail2, 0.0F, 0.5236F, 0.0F);
        front_rail2.setTextureOffset(345, 32).addBox(14.7F, -46.85F, 11.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        front_railbolt2 = new ModelRenderer(this);
        front_railbolt2.setRotationPoint(88.0F, 0.0F, -2.0F);
        front_rail2.addChild(front_railbolt2);
        front_railbolt2.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 69.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt2.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 17.15F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt2.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 44.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt2.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 30.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt2.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 57.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        rail_topbevel2 = new ModelRenderer(this);
        rail_topbevel2.setRotationPoint(16.0F, -50.0F, 43.0F);
        front_rail2.addChild(rail_topbevel2);
        setRotationAngle(rail_topbevel2, 0.0F, 0.0F, 0.8727F);
        rail_topbevel2.setTextureOffset(331, 39).addBox(1.6F, -1.0F, -32.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        rail_underbevel2 = new ModelRenderer(this);
        rail_underbevel2.setRotationPoint(16.0F, -42.0F, 43.0F);
        front_rail2.addChild(rail_underbevel2);
        setRotationAngle(rail_underbevel2, 0.0F, 0.0F, -0.9599F);
        rail_underbevel2.setTextureOffset(328, 37).addBox(-0.075F, -1.55F, -35.0F, 2.0F, 4.0F, 69.0F, 0.0F, false);

        rotor_gasket2 = new ModelRenderer(this);
        rotor_gasket2.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_2.addChild(rotor_gasket2);
        setRotationAngle(rotor_gasket2, 0.0F, 0.5236F, 0.0F);
        rotor_gasket2.setTextureOffset(304, 52).addBox(-15.0F, -70.0F, -4.0F, 2.0F, 10.0F, 8.0F, 0.0F, false);
        rotor_gasket2.setTextureOffset(352, 63).addBox(-14.0F, -79.0F, -4.0F, 2.0F, 6.0F, 8.0F, 0.0F, false);
        rotor_gasket2.setTextureOffset(136, 96).addBox(-17.0F, -76.9F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        rotor_gasket2.setTextureOffset(297, 77).addBox(-17.0F, -67.0F, -5.0F, 2.0F, 10.0F, 10.0F, 0.0F, false);

        floor_trim2 = new ModelRenderer(this);
        floor_trim2.setRotationPoint(-86.0F, -0.75F, 44.0F);
        side_2.addChild(floor_trim2);
        setRotationAngle(floor_trim2, 0.0F, 0.5236F, 0.0F);
        floor_trim2.setTextureOffset(292, 18).addBox(43.0F, -2.0F, -22.0F, 8.0F, 2.0F, 54.0F, 0.0F, false);
        floor_trim2.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -14.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim2.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 13.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim2.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -5.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim2.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 4.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim2.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 21.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);

        side_3 = new ModelRenderer(this);
        side_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        structure.addChild(side_3);
        setRotationAngle(side_3, 0.0F, -2.0944F, 0.0F);
        

        skin3 = new ModelRenderer(this);
        skin3.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_3.addChild(skin3);
        setRotationAngle(skin3, 0.0F, 0.5236F, 0.0F);
        

        floor_skin3 = new ModelRenderer(this);
        floor_skin3.setRotationPoint(0.0F, 0.0F, 0.0F);
        skin3.addChild(floor_skin3);
        

        floorboards3 = new ModelRenderer(this);
        floorboards3.setRotationPoint(0.0F, 0.0F, 0.0F);
        floor_skin3.addChild(floorboards3);
        setRotationAngle(floorboards3, 3.1416F, 0.0F, 0.0F);
        floorboards3.setTextureOffset(214, 162).addBox(-46.0F, 0.25F, -23.0F, 12.0F, 1.0F, 45.0F, 0.0F, false);
        floorboards3.setTextureOffset(318, 76).addBox(-34.0F, 0.5F, -17.0F, 10.0F, 2.0F, 34.0F, 0.0F, false);
        floorboards3.setTextureOffset(285, 73).addBox(-24.25F, 1.5F, -13.0F, 3.0F, 4.0F, 26.0F, 0.0F, false);

        leg_skin3 = new ModelRenderer(this);
        leg_skin3.setRotationPoint(-23.0F, -3.0F, 0.0F);
        floor_skin3.addChild(leg_skin3);
        setRotationAngle(leg_skin3, 0.0F, 0.0F, -1.2217F);
        leg_skin3.setTextureOffset(319, 178).addBox(0.0F, 0.0F, -12.0F, 20.0F, 1.0F, 24.0F, 0.0F, false);

        knee_skin3 = new ModelRenderer(this);
        knee_skin3.setRotationPoint(16.0F, -4.0F, 0.0F);
        leg_skin3.addChild(knee_skin3);
        setRotationAngle(knee_skin3, 0.0F, 0.0F, 1.2217F);
        knee_skin3.setTextureOffset(329, 88).addBox(1.0F, -16.0F, -9.0F, 4.0F, 4.0F, 20.0F, 0.0F, false);
        knee_skin3.setTextureOffset(319, 178).addBox(3.0F, -17.0F, -10.0F, 1.0F, 20.0F, 22.0F, 0.0F, false);
        knee_skin3.setTextureOffset(320, 57).addBox(-2.0F, 12.0F, -13.0F, 4.0F, 4.0F, 27.0F, 0.0F, false);

        thigh_skin3 = new ModelRenderer(this);
        thigh_skin3.setRotationPoint(1.0F, -14.0F, 0.0F);
        knee_skin3.addChild(thigh_skin3);
        setRotationAngle(thigh_skin3, 0.0F, 0.0F, -0.6981F);
        thigh_skin3.setTextureOffset(35, 73).addBox(0.75F, -13.0F, -13.0F, 1.0F, 13.0F, 26.0F, 0.0F, false);

        belly_skin3 = new ModelRenderer(this);
        belly_skin3.setRotationPoint(2.0F, -10.0F, 0.0F);
        thigh_skin3.addChild(belly_skin3);
        setRotationAngle(belly_skin3, 0.0F, 0.0F, -0.8727F);
        belly_skin3.setTextureOffset(319, 178).addBox(1.0F, -35.0F, -28.0F, 1.0F, 19.0F, 55.0F, 0.0F, false);
        belly_skin3.setTextureOffset(305, 71).addBox(0.5F, -16.0F, -20.0F, 2.0F, 14.0F, 40.0F, 0.0F, false);

        rimtop_skin3 = new ModelRenderer(this);
        rimtop_skin3.setRotationPoint(4.5F, -34.0F, 0.0F);
        belly_skin3.addChild(rimtop_skin3);
        setRotationAngle(rimtop_skin3, 0.0F, 0.0F, 2.9671F);
        rimtop_skin3.setTextureOffset(319, 178).addBox(-1.5F, -8.0F, -34.0F, 1.0F, 8.0F, 68.0F, 0.0F, false);

        panel_skin3 = new ModelRenderer(this);
        panel_skin3.setRotationPoint(-6.5F, -4.0F, 1.0F);
        rimtop_skin3.addChild(panel_skin3);
        setRotationAngle(panel_skin3, 0.0F, 0.0F, 3.0718F);
        panel_skin3.setTextureOffset(319, 188).addBox(-6.55F, 3.0F, -27.0F, 1.0F, 14.0F, 51.0F, 0.0F, false);
        panel_skin3.setTextureOffset(319, 184).addBox(-6.55F, 17.0F, -19.0F, 1.0F, 14.0F, 36.0F, 0.0F, false);
        panel_skin3.setTextureOffset(319, 178).addBox(-6.55F, 31.0F, -13.0F, 1.0F, 12.0F, 23.0F, 0.0F, false);

        skelly3 = new ModelRenderer(this);
        skelly3.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_3.addChild(skelly3);
        

        rib3 = new ModelRenderer(this);
        rib3.setRotationPoint(-68.0F, -49.0F, -2.0F);
        skelly3.addChild(rib3);
        setRotationAngle(rib3, 0.0F, 0.0F, -0.2618F);
        rib3.setTextureOffset(370, 107).addBox(1.0F, -1.0F, -2.0F, 54.0F, 5.0F, 8.0F, 0.0F, false);
        rib3.setTextureOffset(325, 77).addBox(0.0F, -3.0F, 1.0F, 55.0F, 4.0F, 2.0F, 0.0F, false);

        rib_bolts_3 = new ModelRenderer(this);
        rib_bolts_3.setRotationPoint(68.0F, 49.0F, 2.0F);
        rib3.addChild(rib_bolts_3);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_3.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        foot3 = new ModelRenderer(this);
        foot3.setRotationPoint(-8.0F, 0.0F, -2.0F);
        skelly3.addChild(foot3);
        foot3.setTextureOffset(354, 84).addBox(-47.0F, -5.0F, -3.0F, 31.0F, 4.0F, 10.0F, 0.0F, false);
        foot3.setTextureOffset(419, 78).addBox(-52.0F, -6.0F, -4.0F, 7.0F, 6.0F, 12.0F, 0.0F, false);
        foot3.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, 5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot3.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot3.setTextureOffset(302, 0).addBox(-16.0F, -40.0F, -0.5F, 4.0F, 33.0F, 5.0F, 0.0F, false);
        foot3.setTextureOffset(333, 84).addBox(-59.0F, -46.0F, -2.0F, 40.0F, 4.0F, 8.0F, 0.0F, false);
        foot3.setTextureOffset(349, 84).addBox(-63.5F, -50.25F, -2.5F, 19.0F, 8.0F, 9.0F, 0.0F, false);
        foot3.setTextureOffset(353, 72).addBox(-7.75F, -80.0F, -3.0F, 4.0F, 20.0F, 10.0F, 0.0F, false);
        foot3.setTextureOffset(339, 74).addBox(-65.0F, -52.0F, 0.5F, 12.0F, 12.0F, 3.0F, 0.0F, false);
        foot3.setTextureOffset(160, 75).addBox(-9.0F, -77.4F, -4.0F, 4.0F, 3.0F, 12.0F, 0.0F, false);

        footslope3 = new ModelRenderer(this);
        footslope3.setRotationPoint(8.0F, -2.0F, 1.5F);
        foot3.addChild(footslope3);
        

        foot_r5 = new ModelRenderer(this);
        foot_r5.setRotationPoint(-41.9844F, -2.6072F, 5.0F);
        footslope3.addChild(foot_r5);
        setRotationAngle(foot_r5, 0.0F, 0.0F, -0.2182F);
        foot_r5.setTextureOffset(399, 102).addBox(-18.5F, -5.5F, -6.0F, 38.0F, 6.0F, 3.0F, 0.0F, false);

        foot_r6 = new ModelRenderer(this);
        foot_r6.setRotationPoint(-39.5F, -3.5F, 0.5F);
        footslope3.addChild(foot_r6);
        setRotationAngle(foot_r6, 0.0F, 0.0F, -0.2182F);
        foot_r6.setTextureOffset(386, 210).addBox(-14.5F, -3.5F, -4.0F, 12.0F, 5.0F, 8.0F, 0.0F, false);
        foot_r6.setTextureOffset(387, 194).addBox(-2.5F, -3.5F, -4.0F, 25.0F, 8.0F, 8.0F, 0.0F, false);

        leg3 = new ModelRenderer(this);
        leg3.setRotationPoint(-21.0F, -4.0F, 0.0F);
        foot3.addChild(leg3);
        setRotationAngle(leg3, 0.0F, 0.0F, 0.4363F);
        leg3.setTextureOffset(430, 61).addBox(0.0F, -12.0F, 0.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        leg3.setTextureOffset(51, 93).addBox(-16.0F, -65.0F, 0.0F, 4.0F, 10.0F, 4.0F, 0.0F, false);

        thigh3 = new ModelRenderer(this);
        thigh3.setRotationPoint(-8.0F, -32.0F, 0.0F);
        foot3.addChild(thigh3);
        setRotationAngle(thigh3, 0.0F, 0.0F, -0.7854F);
        thigh3.setTextureOffset(46, 91).addBox(-9.0F, -26.0F, 0.0F, 9.0F, 27.0F, 4.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-17.0F, -32.0F, 0.0F, 2.0F, 39.0F, 4.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-15.0F, 0.0F, 1.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-15.0F, -9.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-15.0F, -18.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-20.0F, -23.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-20.0F, -14.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-20.0F, -5.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-20.0F, -30.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh3.setTextureOffset(46, 91).addBox(-15.0F, -30.0F, 1.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);

        floorpipes3 = new ModelRenderer(this);
        floorpipes3.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_3.addChild(floorpipes3);
        setRotationAngle(floorpipes3, 0.0F, 0.5236F, 0.0F);
        floorpipes3.setTextureOffset(200, 102).addBox(-31.6F, -4.0F, 0.4F, 6.0F, 2.0F, 13.0F, 0.0F, false);
        floorpipes3.setTextureOffset(309, 70).addBox(-31.0F, -43.0F, -2.0F, 2.0F, 20.0F, 2.0F, 0.0F, false);
        floorpipes3.setTextureOffset(375, 69).addBox(-31.0F, -23.0F, -2.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        floorpipes3.setTextureOffset(317, 372).addBox(-32.0F, -24.0F, 1.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        floorpipes3.setTextureOffset(443, 91).addBox(-31.0F, -20.0F, 2.0F, 2.0F, 19.0F, 2.0F, 0.0F, false);
        floorpipes3.setTextureOffset(58, 67).addBox(-31.0F, -44.0F, 7.0F, 5.0F, 40.0F, 5.0F, 0.0F, false);
        floorpipes3.setTextureOffset(204, 15).addBox(-31.5F, -20.0F, 6.5F, 6.0F, 8.0F, 6.0F, 0.0F, false);
        floorpipes3.setTextureOffset(204, 15).addBox(-32.5F, -17.5F, 5.5F, 8.0F, 3.0F, 8.0F, 0.0F, false);
        floorpipes3.setTextureOffset(203, 434).addBox(-27.2F, -17.4F, -6.0F, 4.0F, 7.0F, 7.0F, 0.0F, false);
        floorpipes3.setTextureOffset(80, 429).addBox(-26.6F, -18.0F, -6.6F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        floorpipes3.setTextureOffset(210, 114).addBox(-25.6F, -4.0F, -3.6F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        floorpipes3.setTextureOffset(345, 50).addBox(-25.0F, -10.0F, -3.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);

        front_rail3 = new ModelRenderer(this);
        front_rail3.setRotationPoint(-88.0F, 0.0F, 2.0F);
        side_3.addChild(front_rail3);
        setRotationAngle(front_rail3, 0.0F, 0.5236F, 0.0F);
        front_rail3.setTextureOffset(345, 32).addBox(14.7F, -46.85F, 11.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        front_railbolt3 = new ModelRenderer(this);
        front_railbolt3.setRotationPoint(88.0F, 0.0F, -2.0F);
        front_rail3.addChild(front_railbolt3);
        front_railbolt3.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 69.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt3.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 17.15F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt3.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 44.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt3.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 30.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt3.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 57.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        rail_topbevel3 = new ModelRenderer(this);
        rail_topbevel3.setRotationPoint(16.0F, -50.0F, 43.0F);
        front_rail3.addChild(rail_topbevel3);
        setRotationAngle(rail_topbevel3, 0.0F, 0.0F, 0.8727F);
        rail_topbevel3.setTextureOffset(331, 39).addBox(1.6F, -1.0F, -32.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        rail_underbevel3 = new ModelRenderer(this);
        rail_underbevel3.setRotationPoint(16.0F, -42.0F, 43.0F);
        front_rail3.addChild(rail_underbevel3);
        setRotationAngle(rail_underbevel3, 0.0F, 0.0F, -0.9599F);
        rail_underbevel3.setTextureOffset(328, 37).addBox(-0.075F, -1.55F, -35.0F, 2.0F, 4.0F, 69.0F, 0.0F, false);

        rotor_gasket3 = new ModelRenderer(this);
        rotor_gasket3.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_3.addChild(rotor_gasket3);
        setRotationAngle(rotor_gasket3, 0.0F, 0.5236F, 0.0F);
        rotor_gasket3.setTextureOffset(304, 52).addBox(-15.0F, -70.0F, -4.0F, 2.0F, 10.0F, 8.0F, 0.0F, false);
        rotor_gasket3.setTextureOffset(352, 63).addBox(-14.0F, -79.0F, -4.0F, 2.0F, 6.0F, 8.0F, 0.0F, false);
        rotor_gasket3.setTextureOffset(136, 96).addBox(-17.0F, -76.9F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        rotor_gasket3.setTextureOffset(297, 77).addBox(-17.0F, -67.0F, -5.0F, 2.0F, 10.0F, 10.0F, 0.0F, false);

        floor_trim3 = new ModelRenderer(this);
        floor_trim3.setRotationPoint(-86.0F, -0.75F, 44.0F);
        side_3.addChild(floor_trim3);
        setRotationAngle(floor_trim3, 0.0F, 0.5236F, 0.0F);
        floor_trim3.setTextureOffset(292, 18).addBox(43.0F, -2.0F, -22.0F, 8.0F, 2.0F, 54.0F, 0.0F, false);
        floor_trim3.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -14.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim3.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 13.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim3.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -5.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim3.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 4.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim3.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 21.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);

        side_4 = new ModelRenderer(this);
        side_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        structure.addChild(side_4);
        setRotationAngle(side_4, 0.0F, 3.1416F, 0.0F);
        

        skin4 = new ModelRenderer(this);
        skin4.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_4.addChild(skin4);
        setRotationAngle(skin4, 0.0F, 0.5236F, 0.0F);
        

        floor_skin4 = new ModelRenderer(this);
        floor_skin4.setRotationPoint(0.0F, 0.0F, 0.0F);
        skin4.addChild(floor_skin4);
        

        floorboards4 = new ModelRenderer(this);
        floorboards4.setRotationPoint(0.0F, 0.0F, 0.0F);
        floor_skin4.addChild(floorboards4);
        setRotationAngle(floorboards4, 3.1416F, 0.0F, 0.0F);
        floorboards4.setTextureOffset(214, 162).addBox(-46.0F, 0.25F, -23.0F, 12.0F, 1.0F, 45.0F, 0.0F, false);
        floorboards4.setTextureOffset(318, 76).addBox(-34.0F, 0.5F, -17.0F, 10.0F, 2.0F, 34.0F, 0.0F, false);
        floorboards4.setTextureOffset(285, 73).addBox(-24.25F, 1.5F, -13.0F, 3.0F, 4.0F, 26.0F, 0.0F, false);

        leg_skin4 = new ModelRenderer(this);
        leg_skin4.setRotationPoint(-23.0F, -3.0F, 0.0F);
        floor_skin4.addChild(leg_skin4);
        setRotationAngle(leg_skin4, 0.0F, 0.0F, -1.2217F);
        leg_skin4.setTextureOffset(319, 178).addBox(0.0F, 0.0F, -12.0F, 20.0F, 1.0F, 24.0F, 0.0F, false);

        knee_skin4 = new ModelRenderer(this);
        knee_skin4.setRotationPoint(16.0F, -4.0F, 0.0F);
        leg_skin4.addChild(knee_skin4);
        setRotationAngle(knee_skin4, 0.0F, 0.0F, 1.2217F);
        knee_skin4.setTextureOffset(329, 88).addBox(1.0F, -16.0F, -9.0F, 4.0F, 4.0F, 20.0F, 0.0F, false);
        knee_skin4.setTextureOffset(319, 178).addBox(3.0F, -17.0F, -10.0F, 1.0F, 20.0F, 22.0F, 0.0F, false);
        knee_skin4.setTextureOffset(320, 57).addBox(-2.0F, 12.0F, -13.0F, 4.0F, 4.0F, 27.0F, 0.0F, false);

        thigh_skin4 = new ModelRenderer(this);
        thigh_skin4.setRotationPoint(1.0F, -14.0F, 0.0F);
        knee_skin4.addChild(thigh_skin4);
        setRotationAngle(thigh_skin4, 0.0F, 0.0F, -0.6981F);
        thigh_skin4.setTextureOffset(35, 73).addBox(0.75F, -13.0F, -13.0F, 1.0F, 13.0F, 26.0F, 0.0F, false);

        belly_skin4 = new ModelRenderer(this);
        belly_skin4.setRotationPoint(2.0F, -10.0F, 0.0F);
        thigh_skin4.addChild(belly_skin4);
        setRotationAngle(belly_skin4, 0.0F, 0.0F, -0.8727F);
        belly_skin4.setTextureOffset(319, 178).addBox(1.0F, -35.0F, -28.0F, 1.0F, 19.0F, 55.0F, 0.0F, false);
        belly_skin4.setTextureOffset(305, 71).addBox(0.5F, -16.0F, -20.0F, 2.0F, 14.0F, 40.0F, 0.0F, false);

        rimtop_skin4 = new ModelRenderer(this);
        rimtop_skin4.setRotationPoint(4.5F, -34.0F, 0.0F);
        belly_skin4.addChild(rimtop_skin4);
        setRotationAngle(rimtop_skin4, 0.0F, 0.0F, 2.9671F);
        rimtop_skin4.setTextureOffset(319, 178).addBox(-1.5F, -8.0F, -34.0F, 1.0F, 8.0F, 68.0F, 0.0F, false);

        panel_skin4 = new ModelRenderer(this);
        panel_skin4.setRotationPoint(-6.5F, -4.0F, 1.0F);
        rimtop_skin4.addChild(panel_skin4);
        setRotationAngle(panel_skin4, 0.0F, 0.0F, 3.0718F);
        panel_skin4.setTextureOffset(319, 188).addBox(-6.55F, 3.0F, -27.0F, 1.0F, 14.0F, 51.0F, 0.0F, false);
        panel_skin4.setTextureOffset(319, 184).addBox(-6.55F, 17.0F, -19.0F, 1.0F, 14.0F, 36.0F, 0.0F, false);
        panel_skin4.setTextureOffset(319, 178).addBox(-6.55F, 31.0F, -13.0F, 1.0F, 12.0F, 23.0F, 0.0F, false);

        skelly4 = new ModelRenderer(this);
        skelly4.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_4.addChild(skelly4);
        

        rib4 = new ModelRenderer(this);
        rib4.setRotationPoint(-68.0F, -49.0F, -2.0F);
        skelly4.addChild(rib4);
        setRotationAngle(rib4, 0.0F, 0.0F, -0.2618F);
        rib4.setTextureOffset(370, 107).addBox(1.0F, -1.0F, -2.0F, 54.0F, 5.0F, 8.0F, 0.0F, false);
        rib4.setTextureOffset(325, 77).addBox(0.0F, -3.0F, 1.0F, 55.0F, 4.0F, 2.0F, 0.0F, false);

        rib_bolts_4 = new ModelRenderer(this);
        rib_bolts_4.setRotationPoint(68.0F, 49.0F, 2.0F);
        rib4.addChild(rib_bolts_4);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_4.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        foot4 = new ModelRenderer(this);
        foot4.setRotationPoint(-8.0F, 0.0F, -2.0F);
        skelly4.addChild(foot4);
        foot4.setTextureOffset(354, 84).addBox(-47.0F, -5.0F, -3.0F, 31.0F, 4.0F, 10.0F, 0.0F, false);
        foot4.setTextureOffset(419, 78).addBox(-52.0F, -6.0F, -4.0F, 7.0F, 6.0F, 12.0F, 0.0F, false);
        foot4.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, 5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot4.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot4.setTextureOffset(302, 0).addBox(-16.0F, -40.0F, -0.5F, 4.0F, 33.0F, 5.0F, 0.0F, false);
        foot4.setTextureOffset(333, 84).addBox(-59.0F, -46.0F, -2.0F, 40.0F, 4.0F, 8.0F, 0.0F, false);
        foot4.setTextureOffset(349, 84).addBox(-63.5F, -50.25F, -2.5F, 19.0F, 8.0F, 9.0F, 0.0F, false);
        foot4.setTextureOffset(353, 72).addBox(-7.75F, -80.0F, -3.0F, 4.0F, 20.0F, 10.0F, 0.0F, false);
        foot4.setTextureOffset(339, 74).addBox(-65.0F, -52.0F, 0.5F, 12.0F, 12.0F, 3.0F, 0.0F, false);
        foot4.setTextureOffset(160, 75).addBox(-9.0F, -77.4F, -4.0F, 4.0F, 3.0F, 12.0F, 0.0F, false);

        footslope4 = new ModelRenderer(this);
        footslope4.setRotationPoint(8.0F, -2.0F, 1.5F);
        foot4.addChild(footslope4);
        

        foot_r7 = new ModelRenderer(this);
        foot_r7.setRotationPoint(-41.9844F, -2.6072F, 5.0F);
        footslope4.addChild(foot_r7);
        setRotationAngle(foot_r7, 0.0F, 0.0F, -0.2182F);
        foot_r7.setTextureOffset(399, 102).addBox(-18.5F, -5.5F, -6.0F, 38.0F, 6.0F, 3.0F, 0.0F, false);

        foot_r8 = new ModelRenderer(this);
        foot_r8.setRotationPoint(-39.5F, -3.5F, 0.5F);
        footslope4.addChild(foot_r8);
        setRotationAngle(foot_r8, 0.0F, 0.0F, -0.2182F);
        foot_r8.setTextureOffset(386, 210).addBox(-14.5F, -3.5F, -4.0F, 12.0F, 5.0F, 8.0F, 0.0F, false);
        foot_r8.setTextureOffset(387, 194).addBox(-2.5F, -3.5F, -4.0F, 25.0F, 8.0F, 8.0F, 0.0F, false);

        leg4 = new ModelRenderer(this);
        leg4.setRotationPoint(-21.0F, -4.0F, 0.0F);
        foot4.addChild(leg4);
        setRotationAngle(leg4, 0.0F, 0.0F, 0.4363F);
        leg4.setTextureOffset(430, 61).addBox(0.0F, -12.0F, 0.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        leg4.setTextureOffset(51, 93).addBox(-16.0F, -65.0F, 0.0F, 4.0F, 10.0F, 4.0F, 0.0F, false);

        thigh4 = new ModelRenderer(this);
        thigh4.setRotationPoint(-8.0F, -32.0F, 0.0F);
        foot4.addChild(thigh4);
        setRotationAngle(thigh4, 0.0F, 0.0F, -0.7854F);
        thigh4.setTextureOffset(46, 91).addBox(-9.0F, -26.0F, 0.0F, 9.0F, 27.0F, 4.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-17.0F, -32.0F, 0.0F, 2.0F, 39.0F, 4.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-15.0F, 0.0F, 1.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-15.0F, -9.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-15.0F, -18.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-20.0F, -23.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-20.0F, -14.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-20.0F, -5.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-20.0F, -30.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh4.setTextureOffset(46, 91).addBox(-15.0F, -30.0F, 1.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);

        floorpipes4 = new ModelRenderer(this);
        floorpipes4.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_4.addChild(floorpipes4);
        setRotationAngle(floorpipes4, 0.0F, 0.5236F, 0.0F);
        floorpipes4.setTextureOffset(200, 102).addBox(-31.6F, -4.0F, 0.4F, 6.0F, 2.0F, 13.0F, 0.0F, false);
        floorpipes4.setTextureOffset(309, 70).addBox(-31.0F, -43.0F, -2.0F, 2.0F, 20.0F, 2.0F, 0.0F, false);
        floorpipes4.setTextureOffset(375, 69).addBox(-31.0F, -23.0F, -2.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        floorpipes4.setTextureOffset(317, 372).addBox(-32.0F, -24.0F, 1.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        floorpipes4.setTextureOffset(443, 91).addBox(-31.0F, -20.0F, 2.0F, 2.0F, 19.0F, 2.0F, 0.0F, false);
        floorpipes4.setTextureOffset(58, 67).addBox(-36.0F, -27.0F, -9.0F, 19.0F, 5.0F, 5.0F, 0.0F, false);
        floorpipes4.setTextureOffset(58, 67).addBox(-41.0F, -46.0F, -9.0F, 5.0F, 19.0F, 5.0F, 0.0F, false);
        floorpipes4.setTextureOffset(204, 15).addBox(-42.0F, -28.0F, -10.0F, 8.0F, 7.0F, 7.0F, 0.0F, false);
        floorpipes4.setTextureOffset(204, 15).addBox(-42.0F, -31.0F, -10.0F, 7.0F, 3.0F, 7.0F, 0.0F, false);
        floorpipes4.setTextureOffset(204, 15).addBox(-34.25F, -28.5F, -10.5F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        floorpipes4.setTextureOffset(204, 15).addBox(-42.5F, -34.0F, -10.5F, 8.0F, 3.0F, 8.0F, 0.0F, false);

        front_rail4 = new ModelRenderer(this);
        front_rail4.setRotationPoint(-88.0F, 0.0F, 2.0F);
        side_4.addChild(front_rail4);
        setRotationAngle(front_rail4, 0.0F, 0.5236F, 0.0F);
        front_rail4.setTextureOffset(345, 32).addBox(14.7F, -46.85F, 11.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        front_railbolt4 = new ModelRenderer(this);
        front_railbolt4.setRotationPoint(88.0F, 0.0F, -2.0F);
        front_rail4.addChild(front_railbolt4);
        front_railbolt4.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 69.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt4.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 17.15F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt4.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 44.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt4.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 30.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt4.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 57.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        rail_topbevel4 = new ModelRenderer(this);
        rail_topbevel4.setRotationPoint(16.0F, -50.0F, 43.0F);
        front_rail4.addChild(rail_topbevel4);
        setRotationAngle(rail_topbevel4, 0.0F, 0.0F, 0.8727F);
        rail_topbevel4.setTextureOffset(331, 39).addBox(1.6F, -1.0F, -32.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        rail_underbevel4 = new ModelRenderer(this);
        rail_underbevel4.setRotationPoint(16.0F, -42.0F, 43.0F);
        front_rail4.addChild(rail_underbevel4);
        setRotationAngle(rail_underbevel4, 0.0F, 0.0F, -0.9599F);
        rail_underbevel4.setTextureOffset(328, 37).addBox(-0.075F, -1.55F, -35.0F, 2.0F, 4.0F, 69.0F, 0.0F, false);

        rotor_gasket4 = new ModelRenderer(this);
        rotor_gasket4.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_4.addChild(rotor_gasket4);
        setRotationAngle(rotor_gasket4, 0.0F, 0.5236F, 0.0F);
        rotor_gasket4.setTextureOffset(304, 52).addBox(-15.0F, -70.0F, -4.0F, 2.0F, 10.0F, 8.0F, 0.0F, false);
        rotor_gasket4.setTextureOffset(352, 63).addBox(-14.0F, -79.0F, -4.0F, 2.0F, 6.0F, 8.0F, 0.0F, false);
        rotor_gasket4.setTextureOffset(136, 96).addBox(-17.0F, -76.9F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        rotor_gasket4.setTextureOffset(297, 77).addBox(-17.0F, -67.0F, -5.0F, 2.0F, 10.0F, 10.0F, 0.0F, false);

        floor_trim4 = new ModelRenderer(this);
        floor_trim4.setRotationPoint(-86.0F, -0.75F, 44.0F);
        side_4.addChild(floor_trim4);
        setRotationAngle(floor_trim4, 0.0F, 0.5236F, 0.0F);
        floor_trim4.setTextureOffset(292, 18).addBox(43.0F, -2.0F, -22.0F, 8.0F, 2.0F, 54.0F, 0.0F, false);
        floor_trim4.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -14.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim4.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 13.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim4.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -5.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim4.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 4.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim4.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 21.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);

        side_5 = new ModelRenderer(this);
        side_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        structure.addChild(side_5);
        setRotationAngle(side_5, 0.0F, 2.0944F, 0.0F);
        

        skin5 = new ModelRenderer(this);
        skin5.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_5.addChild(skin5);
        setRotationAngle(skin5, 0.0F, 0.5236F, 0.0F);
        

        floor_skin5 = new ModelRenderer(this);
        floor_skin5.setRotationPoint(0.0F, 0.0F, 0.0F);
        skin5.addChild(floor_skin5);
        

        floorboards5 = new ModelRenderer(this);
        floorboards5.setRotationPoint(0.0F, 0.0F, 0.0F);
        floor_skin5.addChild(floorboards5);
        setRotationAngle(floorboards5, 3.1416F, 0.0F, 0.0F);
        floorboards5.setTextureOffset(214, 162).addBox(-46.0F, 0.25F, -23.0F, 12.0F, 1.0F, 45.0F, 0.0F, false);
        floorboards5.setTextureOffset(318, 76).addBox(-34.0F, 0.5F, -17.0F, 10.0F, 2.0F, 34.0F, 0.0F, false);
        floorboards5.setTextureOffset(285, 73).addBox(-24.25F, 1.5F, -13.0F, 3.0F, 4.0F, 26.0F, 0.0F, false);

        leg_skin5 = new ModelRenderer(this);
        leg_skin5.setRotationPoint(-23.0F, -3.0F, 0.0F);
        floor_skin5.addChild(leg_skin5);
        setRotationAngle(leg_skin5, 0.0F, 0.0F, -1.2217F);
        leg_skin5.setTextureOffset(319, 178).addBox(0.0F, 0.0F, -12.0F, 20.0F, 1.0F, 24.0F, 0.0F, false);

        knee_skin5 = new ModelRenderer(this);
        knee_skin5.setRotationPoint(16.0F, -4.0F, 0.0F);
        leg_skin5.addChild(knee_skin5);
        setRotationAngle(knee_skin5, 0.0F, 0.0F, 1.2217F);
        knee_skin5.setTextureOffset(329, 88).addBox(1.0F, -16.0F, -9.0F, 4.0F, 4.0F, 20.0F, 0.0F, false);
        knee_skin5.setTextureOffset(319, 178).addBox(3.0F, -17.0F, -10.0F, 1.0F, 20.0F, 22.0F, 0.0F, false);
        knee_skin5.setTextureOffset(320, 57).addBox(-2.0F, 12.0F, -13.0F, 4.0F, 4.0F, 27.0F, 0.0F, false);

        thigh_skin5 = new ModelRenderer(this);
        thigh_skin5.setRotationPoint(1.0F, -14.0F, 0.0F);
        knee_skin5.addChild(thigh_skin5);
        setRotationAngle(thigh_skin5, 0.0F, 0.0F, -0.6981F);
        thigh_skin5.setTextureOffset(35, 73).addBox(0.75F, -13.0F, -13.0F, 1.0F, 13.0F, 26.0F, 0.0F, false);

        belly_skin5 = new ModelRenderer(this);
        belly_skin5.setRotationPoint(2.0F, -10.0F, 0.0F);
        thigh_skin5.addChild(belly_skin5);
        setRotationAngle(belly_skin5, 0.0F, 0.0F, -0.8727F);
        belly_skin5.setTextureOffset(319, 178).addBox(1.0F, -35.0F, -28.0F, 1.0F, 19.0F, 55.0F, 0.0F, false);
        belly_skin5.setTextureOffset(305, 71).addBox(0.5F, -16.0F, -20.0F, 2.0F, 14.0F, 40.0F, 0.0F, false);

        rimtop_skin5 = new ModelRenderer(this);
        rimtop_skin5.setRotationPoint(4.5F, -34.0F, 0.0F);
        belly_skin5.addChild(rimtop_skin5);
        setRotationAngle(rimtop_skin5, 0.0F, 0.0F, 2.9671F);
        rimtop_skin5.setTextureOffset(319, 178).addBox(-1.5F, -8.0F, -34.0F, 1.0F, 8.0F, 68.0F, 0.0F, false);

        panel_skin5 = new ModelRenderer(this);
        panel_skin5.setRotationPoint(-6.5F, -4.0F, 1.0F);
        rimtop_skin5.addChild(panel_skin5);
        setRotationAngle(panel_skin5, 0.0F, 0.0F, 3.0718F);
        panel_skin5.setTextureOffset(319, 188).addBox(-6.55F, 3.0F, -27.0F, 1.0F, 14.0F, 51.0F, 0.0F, false);
        panel_skin5.setTextureOffset(319, 184).addBox(-6.55F, 17.0F, -19.0F, 1.0F, 14.0F, 36.0F, 0.0F, false);
        panel_skin5.setTextureOffset(319, 178).addBox(-6.55F, 31.0F, -13.0F, 1.0F, 12.0F, 23.0F, 0.0F, false);

        skelly5 = new ModelRenderer(this);
        skelly5.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_5.addChild(skelly5);
        

        rib5 = new ModelRenderer(this);
        rib5.setRotationPoint(-68.0F, -49.0F, -2.0F);
        skelly5.addChild(rib5);
        setRotationAngle(rib5, 0.0F, 0.0F, -0.2618F);
        rib5.setTextureOffset(370, 107).addBox(1.0F, -1.0F, -2.0F, 54.0F, 5.0F, 8.0F, 0.0F, false);
        rib5.setTextureOffset(325, 77).addBox(0.0F, -3.0F, 1.0F, 55.0F, 4.0F, 2.0F, 0.0F, false);

        rib_bolts_5 = new ModelRenderer(this);
        rib_bolts_5.setRotationPoint(68.0F, 49.0F, 2.0F);
        rib5.addChild(rib_bolts_5);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_5.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        foot5 = new ModelRenderer(this);
        foot5.setRotationPoint(-8.0F, 0.0F, -2.0F);
        skelly5.addChild(foot5);
        foot5.setTextureOffset(354, 84).addBox(-47.0F, -5.0F, -3.0F, 31.0F, 4.0F, 10.0F, 0.0F, false);
        foot5.setTextureOffset(419, 78).addBox(-52.0F, -6.0F, -4.0F, 7.0F, 6.0F, 12.0F, 0.0F, false);
        foot5.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, 5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot5.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot5.setTextureOffset(302, 0).addBox(-16.0F, -40.0F, -0.5F, 4.0F, 33.0F, 5.0F, 0.0F, false);
        foot5.setTextureOffset(333, 84).addBox(-59.0F, -46.0F, -2.0F, 40.0F, 4.0F, 8.0F, 0.0F, false);
        foot5.setTextureOffset(349, 84).addBox(-63.5F, -50.25F, -2.5F, 19.0F, 8.0F, 9.0F, 0.0F, false);
        foot5.setTextureOffset(353, 72).addBox(-7.75F, -80.0F, -3.0F, 4.0F, 20.0F, 10.0F, 0.0F, false);
        foot5.setTextureOffset(339, 74).addBox(-65.0F, -52.0F, 0.5F, 12.0F, 12.0F, 3.0F, 0.0F, false);
        foot5.setTextureOffset(160, 75).addBox(-9.0F, -77.4F, -4.0F, 4.0F, 3.0F, 12.0F, 0.0F, false);

        footslope5 = new ModelRenderer(this);
        footslope5.setRotationPoint(8.0F, -2.0F, 1.5F);
        foot5.addChild(footslope5);
        

        foot_r9 = new ModelRenderer(this);
        foot_r9.setRotationPoint(-41.9844F, -2.6072F, 5.0F);
        footslope5.addChild(foot_r9);
        setRotationAngle(foot_r9, 0.0F, 0.0F, -0.2182F);
        foot_r9.setTextureOffset(399, 102).addBox(-18.5F, -5.5F, -6.0F, 38.0F, 6.0F, 3.0F, 0.0F, false);

        foot_r10 = new ModelRenderer(this);
        foot_r10.setRotationPoint(-39.5F, -3.5F, 0.5F);
        footslope5.addChild(foot_r10);
        setRotationAngle(foot_r10, 0.0F, 0.0F, -0.2182F);
        foot_r10.setTextureOffset(386, 210).addBox(-14.5F, -3.5F, -4.0F, 12.0F, 5.0F, 8.0F, 0.0F, false);
        foot_r10.setTextureOffset(387, 194).addBox(-2.5F, -3.5F, -4.0F, 25.0F, 8.0F, 8.0F, 0.0F, false);

        leg5 = new ModelRenderer(this);
        leg5.setRotationPoint(-21.0F, -4.0F, 0.0F);
        foot5.addChild(leg5);
        setRotationAngle(leg5, 0.0F, 0.0F, 0.4363F);
        leg5.setTextureOffset(430, 61).addBox(0.0F, -12.0F, 0.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        leg5.setTextureOffset(51, 93).addBox(-16.0F, -65.0F, 0.0F, 4.0F, 10.0F, 4.0F, 0.0F, false);

        thigh5 = new ModelRenderer(this);
        thigh5.setRotationPoint(-8.0F, -32.0F, 0.0F);
        foot5.addChild(thigh5);
        setRotationAngle(thigh5, 0.0F, 0.0F, -0.7854F);
        thigh5.setTextureOffset(46, 91).addBox(-9.0F, -26.0F, 0.0F, 9.0F, 27.0F, 4.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-17.0F, -32.0F, 0.0F, 2.0F, 39.0F, 4.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-15.0F, 0.0F, 1.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-15.0F, -9.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-15.0F, -18.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-20.0F, -23.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-20.0F, -14.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-20.0F, -5.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-20.0F, -30.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh5.setTextureOffset(46, 91).addBox(-15.0F, -30.0F, 1.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);

        floorpipes5 = new ModelRenderer(this);
        floorpipes5.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_5.addChild(floorpipes5);
        setRotationAngle(floorpipes5, 0.0F, 0.5236F, 0.0F);
        floorpipes5.setTextureOffset(200, 102).addBox(-31.6F, -4.0F, 0.4F, 6.0F, 2.0F, 13.0F, 0.0F, false);
        floorpipes5.setTextureOffset(58, 67).addBox(-31.0F, -44.0F, 4.0F, 5.0F, 40.0F, 5.0F, 0.0F, false);
        floorpipes5.setTextureOffset(204, 15).addBox(-31.5F, -20.0F, 3.5F, 6.0F, 8.0F, 6.0F, 0.0F, false);
        floorpipes5.setTextureOffset(204, 15).addBox(-32.5F, -17.5F, 2.5F, 8.0F, 3.0F, 8.0F, 0.0F, false);
        floorpipes5.setTextureOffset(203, 434).addBox(-27.2F, -17.4F, -6.0F, 4.0F, 7.0F, 7.0F, 0.0F, false);
        floorpipes5.setTextureOffset(80, 429).addBox(-26.6F, -18.0F, -6.6F, 4.0F, 8.0F, 8.0F, 0.0F, false);
        floorpipes5.setTextureOffset(210, 114).addBox(-25.6F, -4.0F, -3.6F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        floorpipes5.setTextureOffset(345, 50).addBox(-25.0F, -10.0F, -3.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);

        front_rail5 = new ModelRenderer(this);
        front_rail5.setRotationPoint(-88.0F, 0.0F, 2.0F);
        side_5.addChild(front_rail5);
        setRotationAngle(front_rail5, 0.0F, 0.5236F, 0.0F);
        front_rail5.setTextureOffset(345, 32).addBox(14.7F, -46.85F, 11.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        front_railbolt5 = new ModelRenderer(this);
        front_railbolt5.setRotationPoint(88.0F, 0.0F, -2.0F);
        front_rail5.addChild(front_railbolt5);
        front_railbolt5.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 69.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt5.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 17.15F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt5.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 44.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt5.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 30.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt5.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 57.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        rail_topbevel5 = new ModelRenderer(this);
        rail_topbevel5.setRotationPoint(16.0F, -50.0F, 43.0F);
        front_rail5.addChild(rail_topbevel5);
        setRotationAngle(rail_topbevel5, 0.0F, 0.0F, 0.8727F);
        rail_topbevel5.setTextureOffset(331, 39).addBox(1.6F, -1.0F, -32.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        rail_underbevel5 = new ModelRenderer(this);
        rail_underbevel5.setRotationPoint(16.0F, -42.0F, 43.0F);
        front_rail5.addChild(rail_underbevel5);
        setRotationAngle(rail_underbevel5, 0.0F, 0.0F, -0.9599F);
        rail_underbevel5.setTextureOffset(328, 37).addBox(-0.075F, -1.55F, -35.0F, 2.0F, 4.0F, 69.0F, 0.0F, false);

        rotor_gasket5 = new ModelRenderer(this);
        rotor_gasket5.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_5.addChild(rotor_gasket5);
        setRotationAngle(rotor_gasket5, 0.0F, 0.5236F, 0.0F);
        rotor_gasket5.setTextureOffset(304, 52).addBox(-15.0F, -70.0F, -4.0F, 2.0F, 10.0F, 8.0F, 0.0F, false);
        rotor_gasket5.setTextureOffset(352, 63).addBox(-14.0F, -79.0F, -4.0F, 2.0F, 6.0F, 8.0F, 0.0F, false);
        rotor_gasket5.setTextureOffset(136, 96).addBox(-17.0F, -76.9F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        rotor_gasket5.setTextureOffset(297, 77).addBox(-17.0F, -67.0F, -5.0F, 2.0F, 10.0F, 10.0F, 0.0F, false);

        floor_trim5 = new ModelRenderer(this);
        floor_trim5.setRotationPoint(-86.0F, -0.75F, 44.0F);
        side_5.addChild(floor_trim5);
        setRotationAngle(floor_trim5, 0.0F, 0.5236F, 0.0F);
        floor_trim5.setTextureOffset(292, 18).addBox(43.0F, -2.0F, -22.0F, 8.0F, 2.0F, 54.0F, 0.0F, false);
        floor_trim5.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -14.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim5.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 13.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim5.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -5.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim5.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 4.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim5.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 21.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);

        side_6 = new ModelRenderer(this);
        side_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        structure.addChild(side_6);
        setRotationAngle(side_6, 0.0F, 1.0472F, 0.0F);
        

        skin6 = new ModelRenderer(this);
        skin6.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_6.addChild(skin6);
        setRotationAngle(skin6, 0.0F, 0.5236F, 0.0F);
        

        floor_skin6 = new ModelRenderer(this);
        floor_skin6.setRotationPoint(0.0F, 0.0F, 0.0F);
        skin6.addChild(floor_skin6);
        

        floorboards6 = new ModelRenderer(this);
        floorboards6.setRotationPoint(0.0F, 0.0F, 0.0F);
        floor_skin6.addChild(floorboards6);
        setRotationAngle(floorboards6, 3.1416F, 0.0F, 0.0F);
        floorboards6.setTextureOffset(214, 162).addBox(-46.0F, 0.25F, -23.0F, 12.0F, 1.0F, 45.0F, 0.0F, false);
        floorboards6.setTextureOffset(318, 76).addBox(-34.0F, 0.5F, -17.0F, 10.0F, 2.0F, 34.0F, 0.0F, false);
        floorboards6.setTextureOffset(285, 73).addBox(-24.25F, 1.5F, -13.0F, 3.0F, 4.0F, 26.0F, 0.0F, false);

        leg_skin6 = new ModelRenderer(this);
        leg_skin6.setRotationPoint(-23.0F, -3.0F, 0.0F);
        floor_skin6.addChild(leg_skin6);
        setRotationAngle(leg_skin6, 0.0F, 0.0F, -1.2217F);
        leg_skin6.setTextureOffset(319, 178).addBox(0.0F, 0.0F, -12.0F, 20.0F, 1.0F, 24.0F, 0.0F, false);

        knee_skin6 = new ModelRenderer(this);
        knee_skin6.setRotationPoint(16.0F, -4.0F, 0.0F);
        leg_skin6.addChild(knee_skin6);
        setRotationAngle(knee_skin6, 0.0F, 0.0F, 1.2217F);
        knee_skin6.setTextureOffset(329, 88).addBox(1.0F, -16.0F, -9.0F, 4.0F, 4.0F, 20.0F, 0.0F, false);
        knee_skin6.setTextureOffset(319, 178).addBox(3.0F, -17.0F, -10.0F, 1.0F, 20.0F, 22.0F, 0.0F, false);
        knee_skin6.setTextureOffset(320, 57).addBox(-2.0F, 12.0F, -13.0F, 4.0F, 4.0F, 27.0F, 0.0F, false);

        thigh_skin6 = new ModelRenderer(this);
        thigh_skin6.setRotationPoint(1.0F, -14.0F, 0.0F);
        knee_skin6.addChild(thigh_skin6);
        setRotationAngle(thigh_skin6, 0.0F, 0.0F, -0.6981F);
        thigh_skin6.setTextureOffset(35, 73).addBox(0.75F, -13.0F, -13.0F, 1.0F, 13.0F, 26.0F, 0.0F, false);

        belly_skin6 = new ModelRenderer(this);
        belly_skin6.setRotationPoint(2.0F, -10.0F, 0.0F);
        thigh_skin6.addChild(belly_skin6);
        setRotationAngle(belly_skin6, 0.0F, 0.0F, -0.8727F);
        belly_skin6.setTextureOffset(319, 178).addBox(1.0F, -35.0F, -28.0F, 1.0F, 19.0F, 55.0F, 0.0F, false);
        belly_skin6.setTextureOffset(305, 71).addBox(0.5F, -16.0F, -20.0F, 2.0F, 14.0F, 40.0F, 0.0F, false);

        rimtop_skin6 = new ModelRenderer(this);
        rimtop_skin6.setRotationPoint(4.5F, -34.0F, 0.0F);
        belly_skin6.addChild(rimtop_skin6);
        setRotationAngle(rimtop_skin6, 0.0F, 0.0F, 2.9671F);
        rimtop_skin6.setTextureOffset(319, 178).addBox(-1.5F, -8.0F, -34.0F, 1.0F, 8.0F, 68.0F, 0.0F, false);

        panel_skin6 = new ModelRenderer(this);
        panel_skin6.setRotationPoint(-6.5F, -4.0F, 1.0F);
        rimtop_skin6.addChild(panel_skin6);
        setRotationAngle(panel_skin6, 0.0F, 0.0F, 3.0718F);
        panel_skin6.setTextureOffset(319, 188).addBox(-6.55F, 3.0F, -27.0F, 1.0F, 14.0F, 51.0F, 0.0F, false);
        panel_skin6.setTextureOffset(319, 184).addBox(-6.55F, 17.0F, -19.0F, 1.0F, 14.0F, 36.0F, 0.0F, false);
        panel_skin6.setTextureOffset(319, 178).addBox(-6.55F, 31.0F, -13.0F, 1.0F, 12.0F, 23.0F, 0.0F, false);

        skelly6 = new ModelRenderer(this);
        skelly6.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_6.addChild(skelly6);
        

        rib6 = new ModelRenderer(this);
        rib6.setRotationPoint(-68.0F, -49.0F, -2.0F);
        skelly6.addChild(rib6);
        setRotationAngle(rib6, 0.0F, 0.0F, -0.2618F);
        rib6.setTextureOffset(370, 107).addBox(1.0F, -1.0F, -2.0F, 54.0F, 5.0F, 8.0F, 0.0F, false);
        rib6.setTextureOffset(325, 77).addBox(0.0F, -3.0F, 1.0F, 55.0F, 4.0F, 2.0F, 0.0F, false);

        rib_bolts_6 = new ModelRenderer(this);
        rib_bolts_6.setRotationPoint(68.0F, 49.0F, 2.0F);
        rib6.addChild(rib_bolts_6);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, 1.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-59.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-19.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-39.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-29.75F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        rib_bolts_6.setTextureOffset(117, 104).addBox(-49.5F, -50.25F, -3.6F, 2.0F, 1.0F, 2.0F, 0.0F, false);

        foot6 = new ModelRenderer(this);
        foot6.setRotationPoint(-8.0F, 0.0F, -2.0F);
        skelly6.addChild(foot6);
        foot6.setTextureOffset(354, 84).addBox(-47.0F, -5.0F, -3.0F, 31.0F, 4.0F, 10.0F, 0.0F, false);
        foot6.setTextureOffset(419, 78).addBox(-52.0F, -6.0F, -4.0F, 7.0F, 6.0F, 12.0F, 0.0F, false);
        foot6.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, 5.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot6.setTextureOffset(151, 117).addBox(-52.25F, -5.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        foot6.setTextureOffset(302, 0).addBox(-16.0F, -40.0F, -0.5F, 4.0F, 33.0F, 5.0F, 0.0F, false);
        foot6.setTextureOffset(333, 84).addBox(-59.0F, -46.0F, -2.0F, 40.0F, 4.0F, 8.0F, 0.0F, false);
        foot6.setTextureOffset(349, 84).addBox(-63.5F, -50.25F, -2.5F, 19.0F, 8.0F, 9.0F, 0.0F, false);
        foot6.setTextureOffset(353, 72).addBox(-7.75F, -80.0F, -3.0F, 4.0F, 20.0F, 10.0F, 0.0F, false);
        foot6.setTextureOffset(339, 74).addBox(-65.0F, -52.0F, 0.5F, 12.0F, 12.0F, 3.0F, 0.0F, false);
        foot6.setTextureOffset(160, 75).addBox(-9.0F, -77.4F, -4.0F, 4.0F, 3.0F, 12.0F, 0.0F, false);

        footslope6 = new ModelRenderer(this);
        footslope6.setRotationPoint(8.0F, -2.0F, 1.5F);
        foot6.addChild(footslope6);
        

        foot_r11 = new ModelRenderer(this);
        foot_r11.setRotationPoint(-41.9844F, -2.6072F, 5.0F);
        footslope6.addChild(foot_r11);
        setRotationAngle(foot_r11, 0.0F, 0.0F, -0.2182F);
        foot_r11.setTextureOffset(399, 102).addBox(-18.5F, -5.5F, -6.0F, 38.0F, 6.0F, 3.0F, 0.0F, false);

        foot_r12 = new ModelRenderer(this);
        foot_r12.setRotationPoint(-39.5F, -3.5F, 0.5F);
        footslope6.addChild(foot_r12);
        setRotationAngle(foot_r12, 0.0F, 0.0F, -0.2182F);
        foot_r12.setTextureOffset(386, 210).addBox(-14.5F, -3.5F, -4.0F, 12.0F, 5.0F, 8.0F, 0.0F, false);
        foot_r12.setTextureOffset(387, 194).addBox(-2.5F, -3.5F, -4.0F, 25.0F, 8.0F, 8.0F, 0.0F, false);

        leg6 = new ModelRenderer(this);
        leg6.setRotationPoint(-21.0F, -4.0F, 0.0F);
        foot6.addChild(leg6);
        setRotationAngle(leg6, 0.0F, 0.0F, 0.4363F);
        leg6.setTextureOffset(430, 61).addBox(0.0F, -12.0F, 0.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        leg6.setTextureOffset(51, 93).addBox(-16.0F, -65.0F, 0.0F, 4.0F, 10.0F, 4.0F, 0.0F, false);

        thigh6 = new ModelRenderer(this);
        thigh6.setRotationPoint(-8.0F, -32.0F, 0.0F);
        foot6.addChild(thigh6);
        setRotationAngle(thigh6, 0.0F, 0.0F, -0.7854F);
        thigh6.setTextureOffset(46, 91).addBox(-9.0F, -26.0F, 0.0F, 9.0F, 27.0F, 4.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-17.0F, -32.0F, 0.0F, 2.0F, 39.0F, 4.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-15.0F, 0.0F, 1.0F, 6.0F, 4.0F, 2.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-15.0F, -9.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-15.0F, -18.0F, 1.0F, 6.0F, 3.0F, 2.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-20.0F, -23.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-20.0F, -14.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-20.0F, -5.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-20.0F, -30.0F, 0.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
        thigh6.setTextureOffset(46, 91).addBox(-15.0F, -30.0F, 1.0F, 6.0F, 6.0F, 2.0F, 0.0F, false);

        floorpipes6 = new ModelRenderer(this);
        floorpipes6.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_6.addChild(floorpipes6);
        setRotationAngle(floorpipes6, 0.0F, 0.5236F, 0.0F);
        floorpipes6.setTextureOffset(200, 102).addBox(-31.6F, -4.0F, 0.4F, 6.0F, 2.0F, 13.0F, 0.0F, false);
        floorpipes6.setTextureOffset(309, 70).addBox(-31.0F, -43.0F, -2.0F, 2.0F, 20.0F, 2.0F, 0.0F, false);
        floorpipes6.setTextureOffset(375, 69).addBox(-31.0F, -23.0F, -2.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        floorpipes6.setTextureOffset(317, 372).addBox(-32.0F, -24.0F, 1.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        floorpipes6.setTextureOffset(443, 91).addBox(-31.0F, -20.0F, 2.0F, 2.0F, 19.0F, 2.0F, 0.0F, false);
        floorpipes6.setTextureOffset(58, 67).addBox(-31.0F, -44.0F, 7.0F, 5.0F, 40.0F, 5.0F, 0.0F, false);
        floorpipes6.setTextureOffset(58, 67).addBox(-36.0F, -27.0F, -9.0F, 19.0F, 5.0F, 5.0F, 0.0F, false);
        floorpipes6.setTextureOffset(58, 67).addBox(-41.0F, -46.0F, -9.0F, 5.0F, 19.0F, 5.0F, 0.0F, false);
        floorpipes6.setTextureOffset(204, 15).addBox(-42.0F, -28.0F, -10.0F, 8.0F, 7.0F, 7.0F, 0.0F, false);
        floorpipes6.setTextureOffset(204, 15).addBox(-42.0F, -31.0F, -10.0F, 7.0F, 3.0F, 7.0F, 0.0F, false);
        floorpipes6.setTextureOffset(204, 15).addBox(-34.25F, -28.5F, -10.5F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        floorpipes6.setTextureOffset(204, 15).addBox(-42.5F, -34.0F, -10.5F, 8.0F, 3.0F, 8.0F, 0.0F, false);
        floorpipes6.setTextureOffset(204, 15).addBox(-31.5F, -20.0F, 6.5F, 6.0F, 8.0F, 6.0F, 0.0F, false);
        floorpipes6.setTextureOffset(204, 15).addBox(-32.5F, -17.5F, 5.5F, 8.0F, 3.0F, 8.0F, 0.0F, false);

        front_rail6 = new ModelRenderer(this);
        front_rail6.setRotationPoint(-88.0F, 0.0F, 2.0F);
        side_6.addChild(front_rail6);
        setRotationAngle(front_rail6, 0.0F, 0.5236F, 0.0F);
        front_rail6.setTextureOffset(345, 32).addBox(14.7F, -46.85F, 11.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        front_railbolt6 = new ModelRenderer(this);
        front_railbolt6.setRotationPoint(88.0F, 0.0F, -2.0F);
        front_rail6.addChild(front_railbolt6);
        front_railbolt6.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 69.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt6.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 17.15F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt6.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 44.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt6.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 30.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        front_railbolt6.setTextureOffset(137, 109).addBox(-73.55F, -45.7F, 57.9F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        rail_topbevel6 = new ModelRenderer(this);
        rail_topbevel6.setRotationPoint(16.0F, -50.0F, 43.0F);
        front_rail6.addChild(rail_topbevel6);
        setRotationAngle(rail_topbevel6, 0.0F, 0.0F, 0.8727F);
        rail_topbevel6.setTextureOffset(331, 39).addBox(1.6F, -1.0F, -32.0F, 2.0F, 4.0F, 64.0F, 0.0F, false);

        rail_underbevel6 = new ModelRenderer(this);
        rail_underbevel6.setRotationPoint(16.0F, -42.0F, 43.0F);
        front_rail6.addChild(rail_underbevel6);
        setRotationAngle(rail_underbevel6, 0.0F, 0.0F, -0.9599F);
        rail_underbevel6.setTextureOffset(328, 37).addBox(-0.075F, -1.55F, -35.0F, 2.0F, 4.0F, 69.0F, 0.0F, false);

        rotor_gasket6 = new ModelRenderer(this);
        rotor_gasket6.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_6.addChild(rotor_gasket6);
        setRotationAngle(rotor_gasket6, 0.0F, 0.5236F, 0.0F);
        rotor_gasket6.setTextureOffset(304, 52).addBox(-15.0F, -70.0F, -4.0F, 2.0F, 10.0F, 8.0F, 0.0F, false);
        rotor_gasket6.setTextureOffset(352, 63).addBox(-14.0F, -79.0F, -4.0F, 2.0F, 6.0F, 8.0F, 0.0F, false);
        rotor_gasket6.setTextureOffset(136, 96).addBox(-17.0F, -76.9F, -3.0F, 2.0F, 2.0F, 6.0F, 0.0F, false);
        rotor_gasket6.setTextureOffset(297, 77).addBox(-17.0F, -67.0F, -5.0F, 2.0F, 10.0F, 10.0F, 0.0F, false);

        floor_trim6 = new ModelRenderer(this);
        floor_trim6.setRotationPoint(-86.0F, -0.75F, 44.0F);
        side_6.addChild(floor_trim6);
        setRotationAngle(floor_trim6, 0.0F, 0.5236F, 0.0F);
        floor_trim6.setTextureOffset(292, 18).addBox(43.0F, -2.0F, -22.0F, 8.0F, 2.0F, 54.0F, 0.0F, false);
        floor_trim6.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -14.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim6.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 13.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim6.setTextureOffset(136, 116).addBox(45.0F, -2.25F, -5.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim6.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 4.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
        floor_trim6.setTextureOffset(136, 116).addBox(45.0F, -2.25F, 21.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(SteamConsoleTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        
        tile.getControl(HandbrakeControl.class).ifPresent(brake -> {
            this.lever_f1_rotate_z.rotateAngleZ = (float)Math.toRadians(brake.isFree() ? 20 : 170);
        });
        
        tile.getControl(RandomiserControl.class).ifPresent(rand -> {
            this.globe_rotate_y.rotateAngleY = (float) Math.toRadians((rand.getAnimationProgress() * 720) + 45);
        });
        
        tile.getControl(FacingControl.class).ifPresent(face -> {
            float rot = WorldHelper.getAngleFromFacing(face.getDirection());
            rot -= 360.0F * face.getAnimationProgress();
            this.rotation_crank_rotate_y.rotateAngleY = (float) Math.toRadians(rot);
        });
        
        tile.getControl(DoorControl.class).ifPresent(doorControl -> {
            tile.getDoor().ifPresent(door -> {
                float rot = door.getOpenState() == EnumDoorState.CLOSED ? 0 : 180;
                rot += 180 * doorControl.getAnimationProgress();
                this.door_crank_rotate_y.rotateAngleY = (float) Math.toRadians(rot);
            });
        });
        
        tile.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
            this.needle_a1_rotate_y.rotateAngleY = (float)Math.toRadians(sys.isControlActivated() ? 45 : -45);
        });
        
        tile.getControl(ThrottleControl.class).ifPresent(throttle -> {
            this.leaver_b1_rotate_z.rotateAngleZ = (float) Math.toRadians(80 - (throttle.getAmount() * 150));
            if (throttle.getAmount() < 0.5F) {
                this.glow_red_throttle.setBright(1F);
                this.glow_yellow_throttle.setBright(0F);
                this.glow_green_throttle.setBright(0F);
            }
            if (throttle.getAmount() >= 0.5F) {
                this.glow_red_throttle.setBright(1F);
                this.glow_yellow_throttle.setBright(1F);
                this.glow_green_throttle.setBright(0F);
            }
            if (throttle.getAmount() == 1F) {
                this.glow_red_throttle.setBright(1F);
                this.glow_yellow_throttle.setBright(1F);
                this.glow_green_throttle.setBright(1F);
            }
        });
        
        tile.getControl(IncModControl.class).ifPresent(inc -> {
            this.cord_slider_rotate_z.rotateAngleZ = (float) Math.toRadians(4 + (-9 * (inc.index / (float)IncModControl.COORD_MODS.length)));
        });
        
        tile.getControl(LandingTypeControl.class).ifPresent(land -> {
            float rotMiddle = land.getLandType() == EnumLandType.UP ? -1 : 10;
            float rotSide = land.getLandType() == EnumLandType.UP ? 10 : -1;
            
            this.sliderknob_c3_rotate_z.rotateAngleZ = this.sliderknob_c1_rotate_z.rotateAngleZ = (float)Math.toRadians(rotSide);
            this.sliderknob_c2_rotate_z.rotateAngleZ = (float) Math.toRadians(rotMiddle);
        });
        
        tile.getControl(RefuelerControl.class).ifPresent(refuel -> {
            this.refueler_button_rotate_z.rotateAngleZ = (float) Math.toRadians(refuel.isRefueling() ? 0.75 : 0);
        });
        
        tile.getControl(CommunicatorControl.class).ifPresent(coms -> {
            this.radio_needle_rotate_y.rotateAngleY = (float) Math.toRadians(11.5 - Math.cos(coms.getAnimationTicks() * 0.1) * 15);
        });
        
        tile.getControl(TelepathicControl.class).ifPresent(tele -> {
            this.scrying_glass_rotate_y.rotateAngleY = (float) Math.toRadians(-80 + Math.cos(tele.getAnimationTicks() * 0.5) * -20);
        });
        
        this.glow.setBright(1.0F);
        
        this.glow_core.setBright(1F);
        this.glow_frame.setBright(1F);
        this.glow_glass_b1.setBright(1F);
        this.glow_glass_b2.setBright(1F);
        this.glow_glass_e1.setBright(1F);
        this.glow_glass_e2.setBright(1F);
        this.glow_glass_e3.setBright(1F);
        
        this.glow_globe.setBright(1F);
        this.glow_lamp_a1.setBright(1F);
        this.glow_lamp_a2.setBright(1F);
        this.glow_lamp_c1.setBright(1F);
        this.glow_lamp_c2.setBright(1F);
        this.glow_meterglass_a1.setBright(1F);
        this.glow_panel_e.setBright(1F);
        this.glow_radio.setBright(1F);
        
        int glowTime = (int)(tile.getWorld().getGameTime() % 120);
        
        this.redlamp_e3.setBright(glowTime > 30 ? 1F : 0F);
        this.redlamp_e4.setBright(glowTime < 30 ? 1F : 0F);
        
        this.refueler_button_rotate_z.setBright(1F);
        this.glass.setBright(1F);
        
        glow.render(matrixStack, buffer, packedLight, packedOverlay);
        controls.render(matrixStack, buffer, packedLight, packedOverlay);
        structure.render(matrixStack, buffer, packedLight, packedOverlay);
        
        matrixStack.push();
        
        float cos = (float)Math.cos(MathHelper.lerp(Minecraft.getInstance().getRenderPartialTicks(), tile.prevFlightTicks, tile.flightTicks) * 0.1);
        
        matrixStack.translate(0, 0.5 - (cos * 0.5), 0);
        
        
        float timeToRotate = 10;
        float loopTime = 60;
        float rotTime = MathHelper.lerp(Minecraft.getInstance().getRenderPartialTicks(), tile.prevFlightTicks, tile.flightTicks) % loopTime;
        float threshold = loopTime - timeToRotate;
        if(rotTime >= threshold) {
            
            float angle = ((threshold - rotTime) / timeToRotate) * 180;
            
            this.hourflip_rotate_x.rotateAngleX = (float)Math.toRadians(angle);
        }
        rotor.render(matrixStack,buffer, packedLight, packedOverlay);
        matrixStack.pop();
    }
}