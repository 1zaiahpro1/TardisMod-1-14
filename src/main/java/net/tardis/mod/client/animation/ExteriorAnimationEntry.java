package net.tardis.mod.client.animation;

import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.client.animation.IExteriorAnimation.IAnimSpawn;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

//public class ExteriorAnimationEntry<T extends ExteriorAnimation> extends ForgeRegistryEntry<ExteriorAnimationEntry<T>> {
public class ExteriorAnimationEntry extends ForgeRegistryEntry<ExteriorAnimationEntry> {

	private IAnimSpawn<ExteriorAnimation> spawn;
    private String displayKey;

    public ExteriorAnimationEntry(IAnimSpawn<ExteriorAnimation> spawn) {
		this.spawn = spawn;
    }

    public ExteriorAnimationEntry() {

    }

    public ExteriorAnimation create(ExteriorTile tile) {
		return spawn.create(this, tile);
	}
    
    public String getTranslationKey() {
    	if (this.displayKey == null) {
    		this.displayKey = Util.makeTranslationKey("exterior_animation", this.getRegistryName());
    	}
    	return this.displayKey;
    }

    public TranslationTextComponent getDisplayName() {
        return new TranslationTextComponent(this.getTranslationKey());
    }

}
