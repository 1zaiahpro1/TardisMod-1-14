package net.tardis.mod.traits;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
/** Template to implement on BlockEntities and Blocks to make traits affect landing if these blocks are detected.*/
public interface IAffectLandingTrait {

    /**
     * Return the passed in position if it should not affect landing
     * @param world
     * @param pos
     * @return
     */
    BlockPos redirect(World world, BlockPos pos);
}
