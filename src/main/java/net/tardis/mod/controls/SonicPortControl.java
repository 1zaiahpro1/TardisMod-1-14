package net.tardis.mod.controls;

import java.util.HashSet;
import java.util.Set;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.items.TItems;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class SonicPortControl extends BaseControl{

	public SonicPortControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		return EntitySize.flexible(0.25F, 0.25F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote()) {
			if(player.getHeldItemMainhand().getItem() == TItems.SONIC.get() && console.getSonicItem().isEmpty()) {
				console.setSonicItem(player.getHeldItemMainhand());
				player.setHeldItem(Hand.MAIN_HAND, ItemStack.EMPTY);
				console.getSonicItem().getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
					Set<Schematic> uniqueSchematics = new HashSet<>();
					uniqueSchematics.addAll(cap.getSchematics());
					for(Schematic s : uniqueSchematics) {
						if (s.onConsumedByTARDIS(console, player))
						    cap.getSchematics().remove(s);
					}
				});
			}
			else if(player.getHeldItemMainhand().isEmpty() && !console.getSonicItem().isEmpty()) {
				InventoryHelper.spawnItemStack(console.getWorld(), player.getPosX(), player.getPosY(), player.getPosZ(), console.getSonicItem());
				console.setSonicItem(ItemStack.EMPTY);
			}
		}
		return true;
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(0, 6 / 16.0, 11 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(0.4405431448823669, 0.59375, 0.2563013906978433);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(0.000084, 0.46875, 0.925973550627678);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(0.812, 0.419, -0.104);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(-0.568, 0.531, 0.18);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.5, 0.425, 0.30095038457786805);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(-0.6386732096349401, 0.4375, 0.3380022588333057);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(-0.7754584326851953, 0.46875, 0.11659741289809078);
		
		return new Vector3d(-11 / 16.0, 6 / 16.0, 6 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return console.getSonicItem() != null ? SoundEvents.ENTITY_ITEM_FRAME_ADD_ITEM : SoundEvents.ENTITY_ITEM_FRAME_REMOVE_ITEM;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		
	}

}
