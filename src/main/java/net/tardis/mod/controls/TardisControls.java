package net.tardis.mod.controls;

import java.util.HashMap;
import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
/** Deprecated, used when we did not have Forge's custom Deferred Register*/
@Deprecated
public class TardisControls {

	private static HashMap<ResourceLocation, Supplier<AbstractControl>> REGISTRY = new HashMap<ResourceLocation, Supplier<AbstractControl>>();
	
	/*
	 * Register any controls anytime during loading, should be done on both sides
	 */
	public static void register(ResourceLocation loc, Supplier<AbstractControl> sup) {
		REGISTRY.put(loc, sup);
	}
	
	public static void register(String name, Supplier<AbstractControl> sup) {
		register(new ResourceLocation(Tardis.MODID, name), sup);
	}
	
	public static Supplier<AbstractControl> getControl(ResourceLocation key) {
		try {
			if(!REGISTRY.containsKey(key))
				throw new Exception("No such control");
			return REGISTRY.get(key);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
