package net.tardis.mod.energy;

import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.EnergyStorage;
/** Forge Energy implementation for piping energy between Tardis exterior/interior*/
public class TardisEnergy extends EnergyStorage implements INBTSerializable<CompoundNBT>{

	public TardisEnergy(int capacity) {
		super(capacity);
	}
	
	public TardisEnergy(int capacity, int maxTransfer) {
		super(capacity, maxTransfer);
	}
	
	@Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putInt("energy", getEnergyStored());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.energy = (nbt.getInt("energy"));
    }

    protected void onEnergyChanged() {

    }

}
