package net.tardis.mod.config;



import java.util.List;

import net.tardis.mod.Tardis;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.Lists;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.tardis.mod.constants.TardisConstants;

/**
 * Created by Swirtzly
 * on 24/03/2020 @ 21:39
 */


//Making a global config
public class TConfig {

    public static final Common COMMON;
    public static final ForgeConfigSpec COMMON_SPEC;
    public static Client CLIENT;
    public static ForgeConfigSpec CLIENT_SPEC;
    
    public static final Server SERVER;
    public static final ForgeConfigSpec SERVER_SPEC;

    static {
        final Pair<Common, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Common::new);
        COMMON = specPair.getLeft();
        COMMON_SPEC = specPair.getRight();
        
        Pair<Client, ForgeConfigSpec> specClientPair = new ForgeConfigSpec.Builder().configure(Client::new);
        CLIENT_SPEC = specClientPair.getRight();
        CLIENT = specClientPair.getLeft();
        
        final Pair<Server, ForgeConfigSpec> specServerPair = new ForgeConfigSpec.Builder().configure(Server::new);
        SERVER = specServerPair.getLeft();
        SERVER_SPEC = specServerPair.getRight();
    }
    
    public static class Client {
        public ForgeConfigSpec.BooleanValue openVMEmptyHand;
        public ForgeConfigSpec.BooleanValue interiorShake;
        public ForgeConfigSpec.BooleanValue playToolNotificationSounds;
        public BooleanValue enableBoti;
        public ForgeConfigSpec.ConfigValue<List<? extends String>> botiBlacklistedBlocks;
        public ForgeConfigSpec.ConfigValue<List<? extends String>> botiBlacklistedEntities;
        public BooleanValue showMoreMachineTooltips;
        public ForgeConfigSpec.BooleanValue displayOptifineWarning;
        
        
        public Client(ForgeConfigSpec.Builder builder) {
            builder.push("Client Settings");
            
            enableBoti = builder.comment("Toggle whether to render the Bigger On the Inside effect on the Tardis Interior Door and Exterior block", "Server Owners: BOTI Packets cannot be disabled on server side because they need to be sent to allow BOTI to update"
                , "BOTI packets will only be processed on the server if the portal needs to update.")
                .translation("config.tardis.enableBoti")
                .define("enableBoti", true);
            
            botiBlacklistedBlocks = builder.translation("config.tardis.botiBlacklistedBlocks")
                    .comment("List block ids that will be blacklisted from rendering in the Bigger On the Inside Effect", "Example: minecraft:tall_grass", "Seperate every entry except the last one with commas","To blacklist all blocks from a mod, use YourModId:*")
                    .defineList("botiBlacklistedBlocks", Lists.newArrayList(""), String.class::isInstance);
            
            botiBlacklistedEntities = builder.translation("config.tardis.botiBlacklistedEntities")
                    .comment("List entity ids that will be blacklisted from rendering in the Bigger On the Inside Effect", "Example: minecraft:ender_dragon", "Seperate every entry except the last one with commas","To blacklist all entities from a mod, use YourModId:*")
                    .defineList("botiBlacklistedEntities", Lists.newArrayList(""), String.class::isInstance);
            
            showMoreMachineTooltips = builder.translation("config.tardis.show_more_machine_tooltips")
                    .comment("Define if machine tooltips should show more information. E.g. Show Progress in ticks")
                    .define("showMoreMachineTooltips", false);
            
            playToolNotificationSounds = builder.translation("config.tardis.play_tool_notification_sound")
                    .comment("Define if tools such as the Sonic Screwdriver will play their sounds when notifying the player", "Example: Sonic playing sound when it detects a Rift Chunk")
                    .define("playToolNotificationSounds", true);
            
            this.displayOptifineWarning = builder.translation("config.tardis.disable_optifine_warning")
                    .comment("Display the Optifine Warning message when logging in", "Use Optifine at your own risk...")
                    .define("displayOptifineWarning", true);
            
            builder.push("Vortex Manipulator");
            openVMEmptyHand = builder.comment("Toggles whether the Vortex Manipulator in the inventory will open its GUI if the player right clicks with an empty hand")
                              .translation("config.tardis.openVMEmptyHand")
                              .define("openVMEmptyHand", false);
            builder.pop();
            
            builder.push("Tardis Client Settings");
            interiorShake = builder.comment("Toggles whether to shake the camera during flight or when the exterior is hit")
                              .translation("config.tardis.interiorShake")
                              .define("interiorShake", true);
            builder.pop();
            
            builder.pop();
        }
    }
    
    public static class Server {
    //Tardis Specific
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedDimTypes;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> dimsToHandleTardisVoidTeleport;
    public ConfigValue<List<? extends String>> tardisDimEntitySpawnBlacklist;
    public ConfigValue<Integer> controlTime;
    //entitiesToKill is replaced by interior_change_kill entity type tag
//    public ForgeConfigSpec.ConfigValue<List<? extends String>> entitiesToKill;
    public ConfigValue<Integer> interiorChangeProcessTime;
    public ConfigValue<Integer> interiorChangeCooldownTime;
    public ConfigValue<Integer> interiorChangeArtronUse;
    public ConfigValue<Integer> tardisAbandonmentTimer;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> botiBlacklistedBlocks;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> botiBlacklistedEntities;
    
    public ConfigValue<Float> FEtoAURate;
    public ConfigValue<Integer> exteriorEnergyTransferRate;
    public ConfigValue<Integer> transductionEffectiveRange;
    public ConfigValue<Integer> transductionEnergyMaxCapacity;
    public ConfigValue<Integer> roundelTapEnergyTransferRate;
    public ConfigValue<Integer> collisionRange;
    public ConfigValue<Float> artronPylonRiftDrainRate;
    public ConfigValue<Float> artronCollectorBatteryChargeRate;
    public ConfigValue<Integer> transductionDrainAmount;
   // public ConfigValue<Integer> telepathicSearchRadius;
    
    //VM Specific
    public ForgeConfigSpec.BooleanValue toggleVMWhitelistDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> whitelistedVMDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedVMDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedVMDimTypes;
    public ConfigValue<Integer> vmTeleportRange;
    public ConfigValue<Integer> vmCooldownTime;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> vmSideEffects;
    public ForgeConfigSpec.ConfigValue<List<? extends Integer>> vmSideEffectTime;
    public ConfigValue<Integer> vmBaseFuelUsage;
    public ConfigValue<Double> vmFuelUsageMultiplier;
    public ConfigValue<Integer> vmFuelUsageTime;
    
    /** Sonic Start */
    
    //Entity
    public ForgeConfigSpec.BooleanValue detonateCreeper;
    public ForgeConfigSpec.BooleanValue shearSheep;
    public ForgeConfigSpec.BooleanValue dismantleSkeleton;
    public ForgeConfigSpec.BooleanValue inkSquid;

    //Block
    public ForgeConfigSpec.BooleanValue detonateTnt;
    public ForgeConfigSpec.BooleanValue redstoneLamps;
    public ForgeConfigSpec.BooleanValue openDoors;
    public ForgeConfigSpec.BooleanValue openTrapDoors;
    //public ForgeConfigSpec.BooleanValue toggleRedstone;

    //Tardis
    public ForgeConfigSpec.BooleanValue coordinateTardis;

    //Laser
//    public ForgeConfigSpec.BooleanValue laserFire;
    
    /** Sonic End */
    
    /** Mob Entity Configurations */
    public ForgeConfigSpec.BooleanValue specialWeaponsAlwaysExplode;
    public ForgeConfigSpec.BooleanValue dalekBlockGrief;
    public ForgeConfigSpec.BooleanValue dalekImmuneToProjectiles;
    public ForgeConfigSpec.BooleanValue dalekTargetAllLiving;
    public ForgeConfigSpec.BooleanValue dalekTargetHostileOnly;
    public ForgeConfigSpec.BooleanValue dalekTargetAnimals;
    public ForgeConfigSpec.BooleanValue dalekTargetVillagers;
    public ForgeConfigSpec.BooleanValue dalekTargetPlayers;
    public ForgeConfigSpec.ConfigValue<Double> dalekShieldDisableChance;
    public ConfigValue<Integer> specialWeaponExplosionRadius;


        public Server(ForgeConfigSpec.Builder builder) {
        
        builder.push("Tardis Configurations");
        blacklistedDims = builder.translation("config.tardis.blacklistedDims")
                .comment("List of Dimension IDs Tardis cannot travel to, everything else is allowed","Seperate every entry except the last one with commas")
                .defineList("blacklistedDims", Lists.newArrayList("minecraft:the_end"), String.class::isInstance);
        blacklistedDimTypes = builder.translation("config.tardis.blacklisted_dimension_types")
                .comment("List of DimensionType IDs the Tardis cannot travel to.", "DimensionTypes can be thought of as a group of Dimensions.", "Mods can categorise many Dimensions under one DimensionType.", 
                        "Example: Dimension IDs of rftools:my_custom_dimension and rftools:my_other_dimension might share the same DimensionType ID of rftools:fixed_day" , "Seperate every entry except the last one with commas")
                .defineList("blacklistedDimTypes", Lists.newArrayList("rftoolsdim:fixed_day", "rftools:fixed_night", "hyperbox:hyperbox"), String.class::isInstance);
//        telepathicSearchRadius =  builder.translation("config.tardis.telepathicSearchRadius")
//                .comment("Sets the search radius for the Telepathic Circuit Biome/Structure Search", "NOTICE: This is limited to 500 blocks, anymore will cause performance issues")
//                .define("telepathicSearchRadius", 500, Integer.class::isInstance);
        collisionRange = builder.translation("config.tardis.collision")
                .comment("The minimum between two Tardises before a Time Ram event will occur", "If two Tardises are closer than this number, they are considered 'colliding'","When they collide, a Time Ram event is initiated")
                .define("collisionRange", 16, Integer.class::isInstance);
        dimsToHandleTardisVoidTeleport = builder.translation("config.tardis.dims_handle_tardis_void_teleport")
                .comment("List of Dimension IDs which cause the Tardis Exterior to be teleported to the Overworld if the exterior goes below the minimum height of this dimension.","Seperate every entry except the last one with commas")
                .defineList("dimsToHandleTardisVoidTeleport", Lists.newArrayList("tardis:space"), String.class::isInstance);
        exteriorEnergyTransferRate  = builder.translation("config.tardis.exterior_energy_transfer_rate")
                .comment("Define how much Forge Energy the Tardis exterior will pull from the Tardis interior's internal energy buffer")
                .define("exteriorEnergyTransferRate", 10, Integer.class::isInstance);
        tardisAbandonmentTimer = builder.translation("config.tardis.tardis_abandonment_timer")
                .comment("Define Minecraft days before a Tardis will lose mood from being abandoned by its pilot", "Abandonment occurs if there are no players inside a Tardis interior dimension.")
                .define("tardisAbandonmentTimer", 1, Integer.class::isInstance);
        tardisDimEntitySpawnBlacklist = builder.translation("config.tardis.blacklisted_entity_spawns")
                .comment("List of entity type IDs which cannot spawn in the Tardis","Seperate every entry except the last one with commas", "Example. minecraft:enderman", 
                        "Add an asterisk after the mod id to blacklist all entity types from the specified mod id.", "Example. rats:*")
                .defineList("tardisDimEntitySpawnBlacklist", Lists.newArrayList("rats:demon_rat", "minecraft:enderman"), String.class::isInstance);
        botiBlacklistedBlocks = builder.translation("config.tardis.botiBlacklistedBlocks")
                .comment("List of block ids that will be blacklisted from included in the Bigger On the Inside Portal", "Example: minecraft:tall_grass", "Seperate every entry except the last one with commas","To blacklist all blocks from a mod, use YourModId:*")
                .defineList("botiBlacklistedBlocks", Lists.newArrayList(""), String.class::isInstance);
        
        botiBlacklistedEntities = builder.translation("config.tardis.botiBlacklistedEntities")
                .comment("List of entity ids that will be blacklisted from being included in the Bigger On the Inside Portal", "Example: minecraft:ender_dragon", "Seperate every entry except the last one with commas","To blacklist all entities from a mod, use YourModId:*")
                .defineList("botiBlacklistedEntities", Lists.newArrayList(""), String.class::isInstance);

        controlTime = builder.translation(TConfig.createTranslationKey("flight_event_time"))
                .comment("The amount of time in seconds at full throttle before missing a flight event")
                .define("flight_event_time", 3, Integer.class::isInstance);

        builder.pop();
        
//        builder.push("Entities to Kill when changing TARDIS interior");
//        this.entitiesToKill = builder.translation("config.tardis.entities_to_kill")
//                .comment("List the IDs of the entities you want to kill when the Tardis interior is changed")
//                .defineList("things_to_kill", Lists.newArrayList(""), String.class::isInstance);
//        builder.pop();
        
        builder.push("Interior Change Configuration");
        interiorChangeProcessTime =  builder.translation("config.tardis.interiorChangeProcessTime")
                .comment("Sets the seconds of time needed before the Tardis finishes changing its interior")
                .define("interiorChangeProcessTime", 300, Integer.class::isInstance);
        interiorChangeCooldownTime =  builder.translation("config.tardis.interiorChangeCooldownTime")
                .comment("Sets the seconds of cooldown time between each changing of the Tardis Interior")
                .define("interiorChangeCooldownTime", 60, Integer.class::isInstance);
        interiorChangeArtronUse = builder.translation("config.tardis.interiorChangeArtronUse")
                .comment("Sets the amount of Artron fuel required between each changing of the Tardis Interior")
                .define("interiorChangeArtronUse", 100, Integer.class::isInstance);
        builder.pop();
        
        builder.push("Machines Configuration");
//        FEtoAURate = builder.translation("config.tardis.machines.conversionRate")
//                .comment("Defines how much Forge Energy is needed for 1 Artron Unit","Used to balance out the Artron Converter Block", "This is a float value, allows for decimals")
//                .define("FEtoAURate", 1000F, Float.class::isInstance);
        artronPylonRiftDrainRate = builder.translation("config.tardis.machines.artron_pylon_rift_drain_rate")
                .comment("Define how much artron is taken from a Rift Chunk each second by the Artron Pylon Machine")
                .define("artronPylonRiftDrainRate", 10F, Float.class::isInstance);
        artronCollectorBatteryChargeRate = builder.translation("config.tardis.machines.artron_collector_battery_charge_rift")
                .comment("Define how much Artron Units is taken from the Artron Collector each second to charge Artron Batteries")
                .define("artronCollectorBatteryChargeRate", 50F, Float.class::isInstance);
        transductionDrainAmount = builder.translation("config.tardis.machines.transduction_drain_amount")
                .comment("Define how much Artron Units is taken from the Transduction Barrier machine after it blocks a Tardis from landing")
                .define("transductionDrainAmount", 500, Integer.class::isInstance);
        transductionEnergyMaxCapacity = builder.translation("config.tardis.machines.transduction_energy_max_capacity")
                .comment("Define the max Forge Energy Capacity for the Transduction Barrier")
                .define("transductionEnergyMaxCapacity", 10000, Integer.class::isInstance);
        transductionEffectiveRange = builder.translation("config.tardis.machines.transduction_effective_range")
                .comment("Define the radius around a Transduction Barrier block which Tardises cannot land within")
                .define("transductionEffectiveRange", 32, Integer.class::isInstance);
        roundelTapEnergyTransferRate = builder.translation("config.tardis.machines.roundel_tap_energy_transfer_rate")
                .comment("Define how much Forge Energy is transferred to and from the Roundel Tap")
                .define("roundelTapEnergyTransferRate", 10, Integer.class::isInstance);
        builder.pop();
        
        builder.push("Vortex Manipulator (VM) Configurations");
        
        builder.push("Travel Configurations");
        toggleVMWhitelistDims = builder.translation("config.tardis.vm.toggleVMDimWhitelist")
        .comment("Toggle whether to use the Dimension Whitelist. By default, uses the Blacklist")
        .define("toggleVMWhitelistDims", false);
        whitelistedVMDims = builder.translation("config.tardis.vm.whitelistedDims")
                .comment("List of Dimension IDs the VM can travel inside, anything else is not allowed","Seperate every entry except the last one with commas")
                .defineList("whitelistedVMDims", Lists.newArrayList("minecraft:overworld","minecraft:the_nether","minecraft:the_end"), String.class::isInstance);
        blacklistedVMDims = builder.translation("config.tardis.vm.blacklistedDims")
                .comment("List of Dimension IDs the VM cannot travel inside, everything else is allowed","Seperate every entry except the last one with commas")
                .defineList("blacklistedVMDims", Lists.newArrayList("modID:dimensionName"), String.class::isInstance);
        vmTeleportRange = builder.translation("config.tardis.vm.teleportRange")
                .comment("Sets the maximum Teleport Range the Vortex Manipulator can teleport to")
                .define("vmTeleportRange", 5000, Integer.class::isInstance);
        builder.pop();
        
        builder.push("Limitations");
        vmCooldownTime = builder.translation("config.tardis.vm.cooldownTime")
                .comment("Sets the seconds of cooldown time needed between uses of the Vortex Manipulator")
                .define("vmCooldownTime", 10, Integer.class::isInstance);
        vmSideEffects = builder.translation("config.tardis.vm.sideEffects")
                .comment("Sets the effect side effects experienced after using the Vortex Manipulator")
                .defineList("vmSideEffects", Lists.newArrayList("minecraft:nausea","minecraft:blindness","minecraft:weakness"), String.class::isInstance);
        vmSideEffectTime = builder.translation("config.tardis.vm.sideEffectTime")
                .comment("Sets the duration in seconds for each effect in the above value","The order of the numbers should match each the order of effect in vmSideEffects.")
                .defineList("vmSideEffectTime", Lists.newArrayList(5,5,10),Integer.class::isInstance);
        builder.pop();
        
        builder.push("Fuel Control");
        vmBaseFuelUsage =  builder.translation("config.tardis.vm.baseFuelUsage")
                .comment("Sets the base value that scales for how much fuel is used by the VM"
                        ,"Fuel Usage = YourInputValue + (YourMultiplier * TeleportDistance)"
                        ,"E.g. If you want the fuel usage to be 20 charge per 100 blocks, you would set the base value to 20 + (0.005 * 100)")
                .define("vmBaseFuelUsage", 5,Integer.class::isInstance);
        vmFuelUsageMultiplier =  builder.translation("config.tardis.vm.fuelUsageMultiplier")
                .comment("Sets the figure that scales how much fuel is used by the VM per amount of seconds set by user"
                        ,"Fuel Usage = YourBaseInputValue + (YourMultiplier * TeleportDistance)"
                        ,"E.g. If you want the fuel usage to be 20 charge per 100 blocks, you would set the base value to 20 + (0.005 * 100)")
                .define("vmFuelUsageMultiplier", 0.1, Double.class::isInstance);
        vmFuelUsageTime = builder.translation("config.tardis.vm.fuelUsageTime")
                .comment("Sets the seconds of time that must elapse before the VM uses up fuel")
                .define("vmFuelUsageTime", 1, Integer.class::isInstance);
        builder.pop();
        
        builder.pop();
        
        
        builder.push("Sonic Screwdriver");
        builder.push("Block Interactions");
        detonateTnt = builder.translation("config.tardis.detonate_tnt").comment("Toggle whether sonics can detonate TNT").define("detonate_tnt", true);
        redstoneLamps = builder.translation("config.tardis.redstone_lamps").comment("Toggle whether sonics can toggle Lamps").define("redstone_lamps", true);
        openDoors = builder.translation("config.tardis.open_doors").comment("Toggle whether sonics can open doors").define("open_doors", true);
        openTrapDoors = builder.translation("config.tardis.open_trapdoors").comment("Toggle whether sonics can open Trap doors").define("open_trapdoors", true);
        //toggleRedstone = builder.translation("config.tardis.toggle_redstone").comment("Toggle whether enable/disable redstone").define("redstone", true);
        builder.pop();

        builder.push("Entity Interactions");
        detonateCreeper = builder.translation("config.tardis.detonate_creeper").comment("Toggle whether sonics can detonate Creepers").define("detonate_creeper", true);
        shearSheep = builder.translation("config.tardis.shear_sheep").comment("Toggle whether sonics can Shear Sheep").define("shear_sheep", true);
        dismantleSkeleton = builder.translation("config.tardis.dismantle_skeleton").comment("Toggle whether sonics can dismantle Skeleton like entities").define("dismantle_skeletons", true);
        inkSquid = builder.translation("config.tardis.ink_squid").comment("Toggle whether sonics can cause Squids to squirt ink").define("ink_squid", true);
        builder.pop();

        builder.push("Tardis");
        coordinateTardis = builder.translation("config.tardis.coordinate_tardis").comment("Toggle whether sonics can tell the Tardis where to land").define("coordinate_tardis", true);
        builder.pop();

//        builder.push("Laser");
//        laserFire = builder.translation("config.tardis.laser_fire").comment("Toggle where the Laser setting burns things").define("laser_fire", true);
//        builder.pop();
        
        builder.pop(); //End Sonic Configurations

        builder.push("Mob Entity Configurations");
        
        builder.push("Daleks");
        this.dalekBlockGrief = builder.translation("config.tardis.dalek.dalek_block_grief").comment("If certain Daleks should be able grief blocks when Gamerule mobGriefing is enabled", "If false, other mobs can still grief blocks, but Daleks will not.").define("dalekBlockGrief", true);
        this.dalekImmuneToProjectiles = builder.translation("config.tardis.dalek.dalek_immune_projectile").comment("If Daleks will always be immune to projectile damage", "If false, Daleks will only be immune to projectiles in Hard Mode").define("dalekImmuneToProjectiles", true);
        this.specialWeaponsAlwaysExplode = builder.translation("config.tardis.dalek_special_weapons_always_explode").comment("If Special Weapon Dalek lasers should always be explosive on impact", "If false, lasers will only make explosions if it hits a block").define("specialWeaponsAlwaysExplode", true);
        this.specialWeaponExplosionRadius = builder.translation("config.tardis.dalek.special_weapon_explosion_radius").comment("Radius of the Special Weapon Dalek Explosion, between 1 and 100", "Does not accept decimal points").defineInRange("specialWeaponExplosionRadius", 3, 1, 100);
        this.dalekTargetAllLiving = builder.translation("config.tardis.dalek.dalek_target_all").comment("Determine if Daleks should target all living entities.", "If this is false, you can fine tune the targeting, such as hostiles only, or blacklist animals.").define("dalekTargetAllLiving", true);
        this.dalekTargetHostileOnly = builder.translation("config.tardis.dalek.dalek_target_hostile_only").comment("If Daleks should only target other hostile mobs.", "Requires dalekTargetAllLiving to be false.").define("dalekTargetHostileOnly", false);
        this.dalekTargetAnimals = builder.translation("config.tardis.dalek.dalek_target_animals").comment("If Daleks should target 'animal' entites such as cows and sheep.", "Requires dalekTargetAllLiving and dalekTargetHostileOnly to be false.").define("dalekTargetAnimals", true);
        this.dalekTargetVillagers = builder.translation("config.tardis.dalek.dalek_target_villagers").comment("If Daleks should target Villager entities.", "Requires dalekTargetAllLiving and dalekTargetHostileOnly to be false.").define("dalekTargetVillagers", true);
        this.dalekTargetPlayers = builder.translation("config.tardis.dalek.dalek_target_players").comment("If Daleks should target Player entities.", "Requires dalekTargetAllLiving and dalekTargetHostileOnly to be false").define("dalekTargetPlayers", true);
        this.dalekShieldDisableChance = builder.translation("config.tardis.dalek.dalek_disable_shield_chance").comment("Chance to disable shields").defineInRange("dalekShieldDisableChance", 0.1D, 0.0D, 1D);
        builder.pop();
        
        builder.pop(); //End Mob Configurations

        }
    }
    
    public static class Common{
        public ConfigValue<Integer> tardisSpawnProbability;
        public ConfigValue<List<? extends String>> tardisSpawnDimBlacklist;
        public ConfigValue<Integer> xionCrystalSpawnChance;
        public ConfigValue<Integer> cinnabarOreSpawnChance;
        
        public ConfigValue<Integer> crashedStructureSpawnChance;
        public ConfigValue<Integer> crashedStructureSeperation;
        public ConfigValue<Integer> crashedStructureSpacing;
        
        public ConfigValue<Integer> spaceStationMissionSpawnChance;
        public ConfigValue<Integer> spaceStationMissionSeperation;
        public ConfigValue<Integer> spaceStationMissionSpacing;
        
        public ConfigValue<Integer> dalekSaucerSpawnChance;
        public ConfigValue<Integer> dalekSaucerSeperation;
        public ConfigValue<Integer> dalekSaucerSpacing;
        
        public ConfigValue<Integer> abandonedSpaceshipSpawnChance;
        public ConfigValue<Integer> abandonedSpaceshipSeperation;
        public ConfigValue<Integer> abandonedSpaceshipSpacing;
        
        public ConfigValue<Integer> moonLanderSpawnChance;
        public ConfigValue<Integer> moonLanderSeperation;
        public ConfigValue<Integer> moonLanderSpacing;
        
        public ConfigValue<Integer> observatorySpawnWeightPlains;
        public ConfigValue<Integer> observatorySpawnWeightDesert;
        public ConfigValue<Integer> observatorySpawnWeightSavanna;
        
        public ConfigValue<Boolean> dalekJoinVillageRaid;
        public ConfigValue<List<? extends Integer>> dalekVillageRaidSpawnCount;
        
        public Common(ForgeConfigSpec.Builder builder) {
            builder.push("World Generation");
            builder.comment("Requires Server Restart to take effect.", "If you don't restart, unknown side effects can occur");
            
            this.tardisSpawnProbability = builder.translation("config.tardis.tardis_spawn_chance")
                    .comment("The probability that a Broken Tardis Exterior will spawn in a valid position", "This must be a whole number, no decimals")
                    .comment("This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("tardisSpawnProbability", 10);
            
            this.tardisSpawnDimBlacklist = builder.translation("config.tardis.tardis_spawn_dim_blacklist")
                    .comment("The list of dimension ids that a Broken Tardis Exterior cannot spawn in", "Example. tardis:moon", 
                            "Add an asterisk after the mod id to blacklist all dimensions.", "Example. tardis:*")
                    .defineList("tardisSpawnDimBlacklist", Lists.newArrayList("twilightforest:twilightforest", "twilightforest:skylight_forest", "regen:*"), String.class::isInstance);
            
            this.cinnabarOreSpawnChance = builder.translation("config.tardis.cinnabar_ore_spawn_chance")
                    .comment("The chance for Cinnabar Ore to spawn in the world", "This must be a whole number, no decimals"
                    , "The final spawn chance is equal to 1 divided by this config value", "Example: Value = 2, Spawn Chance = 1/2"
                    , "Use 1 to always generate this ore, 1001 to disable generation")
                    .defineInRange("cinnabarOreSpawnChance", 2, 1, 1001);

            this.xionCrystalSpawnChance = builder.translation("config.tardis.xion_crystal_spawn_chance")
                    .comment("The chance for Xion Crystals to spawn in the world", "This option does not allow decimal points"
                    , "This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("xionCrystalSpawnChance", 30);
            
            builder.push("Crashed Structure");
            this.crashedStructureSpawnChance = builder.translation(TardisConstants.Translations.STRUCTURE_GEN_CHANCE)
                    .comment("The chance for Crashed Structures such as Spaceships or Shuttles, to spawn in the world", "This option does not allow decimal points")
                    .comment("This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("crashedStructureSpawnChance", 10);
            this.crashedStructureSeperation = builder.translation(TardisConstants.Translations.STRUCTURE_SEPERATION)
                    .comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points")
                    .define("crashedStructureSeperation", 60);
            this.crashedStructureSpacing = builder.translation(TardisConstants.Translations.STRUCTURE_SPACING)
                    .comment("The chunk radius around a potential spawn location in which this structure can spawn.", "Structure Spacing value must be larger than Structure Seperation", "This option does not allow decimal points")
                    .define("crashedStructureSpacing", 100);
            builder.pop();
            
            builder.push("Space Station");
            this.spaceStationMissionSpawnChance = builder.translation(TardisConstants.Translations.STRUCTURE_GEN_CHANCE)
                    .comment("The chance for a Space Station Structure and its mission, to spawn in the world", "This option allows does allow decimal points", "This structure only spawns in the Space Dimension"
                    , "This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("spaceStationMissionSpawnChance", 5);
            this.spaceStationMissionSeperation = builder.translation(TardisConstants.Translations.STRUCTURE_SEPERATION)
                    .comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points")
                    .define("spaceStationMissionSeperation", 40);
            this.spaceStationMissionSpacing = builder.translation(TardisConstants.Translations.STRUCTURE_SPACING)
                    .comment("The chunk radius around a potential spawn location in which this structure can spawn.", "Structure Spacing value must be larger than Structure Seperation","This option does not allow decimal points")
                    .define("spaceStationMissionSpacing", 100);
            builder.pop();
            
            builder.push("Dalek Saucer");
            this.dalekSaucerSpawnChance = builder.translation(TardisConstants.Translations.STRUCTURE_GEN_CHANCE)
                    .comment("The chance for a Dalek Saucer Structure to spawn in the world", "This option does not allow decimal points"
                    , "This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("dalekSaucerSpawnChance", 3);
            this.dalekSaucerSeperation = builder.translation(TardisConstants.Translations.STRUCTURE_SEPERATION)
                    .comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points")
                    .define("dalekSaucerSeperation", 50);
            this.dalekSaucerSpacing = builder.translation(TardisConstants.Translations.STRUCTURE_SPACING)
                    .comment("The chunk radius around a potential spawn location in which this structure can spawn.", "Structure Spacing value must be larger than Structure Seperation", "This option does not allow decimal points")
                    .define("dalekSaucerSpacing", 200);
            builder.pop();
            
            builder.push("Abandoned Spaceship");
            this.abandonedSpaceshipSpawnChance = builder.translation(TardisConstants.Translations.STRUCTURE_GEN_CHANCE)
                    .comment("The chance for Abandoned Spaceship Structure, to spawn in the world", "This option does not allow decimal points"
                    , "This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("abandonedSpaceshipSpawnChance", 8);
            this.abandonedSpaceshipSeperation = builder.translation(TardisConstants.Translations.STRUCTURE_SEPERATION)
                    .comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points")
                    .define("abandonedSpaceshipSeperation", 20);
            this.abandonedSpaceshipSpacing = builder.translation(TardisConstants.Translations.STRUCTURE_SPACING)
                    .comment("The chunk radius around a potential spawn location in which this structure can spawn.", "Structure Spacing value must be larger than Structure Seperation", "This option does not allow decimal points")
                    .define("abandonedSpaceshipSpacing", 70);
            builder.pop();
            
            builder.push("Moon Lander");
            this.moonLanderSpawnChance = builder.translation(TardisConstants.Translations.STRUCTURE_GEN_CHANCE)
                    .comment("The chance for Moon Lander Structure to spawn in the world", "This option does not allow decimal points", "This structure only spawns in the Moon Dimension"
                    , "This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("moonLanderSpawnChance", 35);
            this.moonLanderSeperation = builder.translation(TardisConstants.Translations.STRUCTURE_SEPERATION)
                    .comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points")
                    .define("moonLanderSeperation", 10);
            this.moonLanderSpacing = builder.translation(TardisConstants.Translations.STRUCTURE_SPACING)
                    .comment("The chunk radius around a potential spawn location in which this structure can spawn.", "Structure Spacing value must be larger than Structure Seperation", "This option does not allow decimal points")
                    .define("moonLanderSpacing", 15);
            builder.pop();
            
            builder.push("Observatory"); //Observatory start
            
            builder.push("Plains");
            this.observatorySpawnWeightPlains =  builder.translation(TardisConstants.Translations.VILLAGE_STRUCTURE_WEIGHT)
                    .comment("The spawn weight of the village street which can spawn this structure in villages.", "Higher weight means number of times it can spawn, so more chance of spawning", "Cannot contain decimal points")
                    .defineInRange("observatorySpawnWeightPlains", 7, 1, 50);
            builder.pop();
            
            builder.push("Desert");
            this.observatorySpawnWeightDesert =  builder.translation(TardisConstants.Translations.VILLAGE_STRUCTURE_WEIGHT)
                    .comment("The spawn weight of the village street which can spawn this structure in villages.", "Higher weight means number of times it can spawn, so more chance of spawning", "Cannot contain decimal points")
                    .defineInRange("observatorySpawnWeightDesert", 7, 1, 50);
            builder.pop();
            
            builder.push("Savanna");
            this.observatorySpawnWeightSavanna =  builder.translation(TardisConstants.Translations.VILLAGE_STRUCTURE_WEIGHT)
                    .comment("The spawn weight of the village street which can spawn this structure in villages.", "Higher weight means number of times it can spawn, so more chance of spawning", "Cannot contain decimal points")
                    .defineInRange("observatorySpawnWeightSavanna", 6, 1, 50);
            builder.pop();
            
            builder.pop(); //End Observatory
            
            builder.pop(); //End World Generation
            
            builder.push("Entity Spawns");
            builder.comment("Requires Server Restart to take effect.", "If you don't restart, unknown side effects can occur");
            this.dalekJoinVillageRaid = builder.translation("config.tardis.dalek_join_village_raid")
                    .comment("Toggle if Daleks should spawn in each Village Raid Wave")
                    .define("dalekJoinVillageRaid", true);
            this.dalekVillageRaidSpawnCount = builder.translation("config.tardis.dalek_village_raid_spawn_count")
                    .comment("Number of Daleks that should spawn in each Village Raid Wave", "The index of the number determines which raid you are setting"
                    , "E.g. A list of {0,1,3,5,6,7,8} means the 1st wave will contain 0 Daleks, 2nd wave contains 1 Dalek, 3rd wave contains 3 Daleks, etc.")
                    .defineList("dalekVillageRaidSpawnCount", Lists.newArrayList(0,1,3,5,6,7,8), Integer.class::isInstance);
            builder.pop();
        }
        
    }

    public static String createTranslationKey(String trans){
        return "config." + Tardis.MODID + "." + trans;
    }

}
