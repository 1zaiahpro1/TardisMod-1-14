package net.tardis.mod.containers;

/**
 * Created by 50ap5ud5
 * on 27 May 2020 @ 4:31:04 pm
 */
public interface IContainerUtil {
	int getNumRows();
}
