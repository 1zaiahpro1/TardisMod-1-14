package net.tardis.mod.containers.slot;

import java.util.function.Predicate;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class EngineSlot extends SlotItemHandler{

	Direction panel;
	Predicate<ItemStack> filter;
	
	public EngineSlot(Direction panel, IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		this(stack -> true, panel, itemHandler, index, xPosition, yPosition);
	}
	
	public EngineSlot(Predicate<ItemStack> filter, Direction panel, IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
		this.panel = panel;
		this.filter = filter;
	}

	public Direction getPanelSide() {
	    return this.panel;
	}
	
	public void setPanelSide(Direction dir) {
        this.panel = dir;
    }
	
	public void setFilter(Predicate<ItemStack> filter) {
		this.filter = filter;
	}
	
	@Override
	public boolean isItemValid(ItemStack stack) {
		return this.filter.test(stack);
	}


}
