package net.tardis.mod.containers;

import javax.annotation.Nullable;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.containers.slot.FilteredSlot;
import net.tardis.mod.containers.slot.SlotItemHandlerFiltered;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.tileentities.machines.NeutronicSpectrometerTile;

public class SpectrometerContainer extends BlockEntityContextContainer<NeutronicSpectrometerTile> {

    protected SpectrometerContainer(@Nullable ContainerType<?> type, int id) {
        super(type, id);
    }

    public SpectrometerContainer(int id){
        this(TContainers.SPECTROMETER.get(), id);
    }

    /** Server Only constructor */
    public SpectrometerContainer(int id, PlayerInventory inv, NeutronicSpectrometerTile tile){
        this(id);
        init(inv, tile);
    }

    /** Client Only constructor */
    public SpectrometerContainer(int id, PlayerInventory inv, PacketBuffer buf){
        this(id);

        TileEntity te = inv.player.world.getTileEntity(buf.readBlockPos());

        if(te instanceof NeutronicSpectrometerTile)
            init(inv, (NeutronicSpectrometerTile)te);

    }

    public void init(PlayerInventory inv, NeutronicSpectrometerTile tile){
        this.blockEntity = tile;


        this.addSlot(new SlotItemHandler(tile.getInventory(), 0, 41, 27));
        this.addSlot(new SlotItemHandlerFiltered(tile.getInventory(), 1, 147, 47, stack -> stack.getItem() instanceof SonicItem));

        TInventoryHelper.addPlayerInvContainer(this, inv, 0, -2);

    }

    @Override
    public void updateProgressBar(int id, int data) {
        super.updateProgressBar(id, data);
    }

    public float getProgress() {
        return blockEntity == null ? 0 : blockEntity.getProgress();
    }

    public float getDownloadProgress() {
        return blockEntity == null ? 0 : blockEntity.getDownloadProgress();
    }

    public boolean hasSchematics(){
        return blockEntity == null  ? false : blockEntity.hasSchematics();
    }
}
