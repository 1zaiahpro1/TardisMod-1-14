package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.constants.TardisConstants.Part.PartType;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.misc.TooltipProviderItem;
import net.tardis.mod.properties.Prop;

/**
 * Base item for Subsystems and Upgrades
 * Created by Swirtzly on 22/08/2019 @ 20:15
 * Modified by 50ap5ud5 on 12/08/2020 @ 12:51
 */
public class TardisPartItem extends TooltipProviderItem {

	protected PartType type;
    protected boolean requiredForFlight;
    protected boolean requiresRepair;
    protected TranslationTextComponent dependentItem;
    protected boolean isDisabled = false;
    
    public TardisPartItem(Item.Properties prop, PartType type, boolean requiredForFlight, boolean requiresRepair, TranslationTextComponent dependentItem, boolean isDisabled) {
        super(prop);
        this.type = type;
        this.requiredForFlight = requiredForFlight;
        this.requiresRepair = requiresRepair;
        this.dependentItem = dependentItem;
        this.isDisabled = isDisabled;
        this.setShowTooltips(true);
        this.setHasStatisticsTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

    public TardisPartItem(Item.Properties prop, PartType type, boolean requiredForFlight, boolean requiresRepair, TranslationTextComponent dependentItem) {
        this(prop, type, requiredForFlight, requiresRepair, dependentItem, false);
    }
    
    public TardisPartItem(Item.Properties prop, PartType type, boolean requiredForFlight, boolean requiresRepair, boolean isDisabled) {
        this(prop, type, requiredForFlight, requiresRepair, new TranslationTextComponent(""), isDisabled);
    }
    
    public TardisPartItem(Item.Properties prop, PartType type, boolean requiredForFlight, boolean requiresRepair) {
        this(prop, type, requiredForFlight, requiresRepair, false);
    }
    
    public TardisPartItem(PartType type, boolean requiredForFlight, boolean requiresRepair, TranslationTextComponent dependentItem, boolean isDisabled) {
        this(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE).maxDamage(250), type, requiredForFlight, requiresRepair, dependentItem, isDisabled);
    }
    
    public TardisPartItem(PartType type, boolean requiredForFlight, boolean requiresRepair, TranslationTextComponent dependentItem) {
        this(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE).maxDamage(250), type, requiredForFlight, requiresRepair, dependentItem, false);
    }

    public TardisPartItem(PartType type, boolean requiredForFlight, boolean requiresRepair) {
        this(type, requiredForFlight, requiresRepair, new TranslationTextComponent(""));
    }

    @Override
	public void createStatisticTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
    	String typeName = this.type.toString().toLowerCase();
        String typeFriendlyName = typeName.substring(0, 1).toUpperCase() + typeName.substring(1);
        StringTextComponent panelName = new StringTextComponent(this.type == PartType.SUBSYSTEM ? "Components" : "Upgrades");
        tooltip.add(new TranslationTextComponent("tooltip.part.type").appendSibling(new StringTextComponent(typeFriendlyName).mergeStyle(TextFormatting.LIGHT_PURPLE)));

        if (type != PartType.UPGRADE)
            tooltip.add(new TranslationTextComponent("tooltip.part.flight.required").appendSibling(new StringTextComponent(String.valueOf(this.requiredForFlight)).mergeStyle(TextFormatting.LIGHT_PURPLE)));

        tooltip.add(new TranslationTextComponent("tooltip.part.repair.required").appendSibling(new StringTextComponent(String.valueOf(this.requiresRepair)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        if (this.type == PartType.UPGRADE && !this.dependentItem.getKey().isEmpty()) {
            tooltip.add(new TranslationTextComponent("tooltip.part.dependency").appendSibling(dependentItem != null ? dependentItem.mergeStyle(TextFormatting.LIGHT_PURPLE) : new StringTextComponent("None").mergeStyle(TextFormatting.LIGHT_PURPLE)));
        }
        tooltip.add(new TranslationTextComponent("tooltip.part.engine_panel").appendSibling(panelName.mergeStyle(this.type == PartType.SUBSYSTEM ? TextFormatting.BLUE : TextFormatting.GREEN)));   
	}

	@Override
	public void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
		tooltip.add(TardisConstants.Prefix.TOOLTIP_ITEM_DESCRIPTION.deepCopy().appendSibling(new TranslationTextComponent("tooltip.part." + this.getRegistryName().getPath() + ".description").mergeStyle(TextFormatting.GRAY)));
	}

	@Override
	public void createDefaultTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
		if (this.isDisabled) {
        	tooltip.add(TardisConstants.Translations.TOOLTIP_DISABLED_ITEM.mergeStyle(TextFormatting.RED));
        }
	}
}
