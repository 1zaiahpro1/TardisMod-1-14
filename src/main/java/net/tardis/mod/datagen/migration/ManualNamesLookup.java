package net.tardis.mod.datagen.migration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.datagen.TardisManualGen.ManualObjectType;
import net.tardis.mod.datagen.TardisManualGen.PageType;
import net.tardis.mod.helper.Helper;

public class ManualNamesLookup {
    
    private static List<ManualMigrationWrapper> migrationObjects = new ArrayList<>();
    
    public static void createMigrationObjects() {
    	//TODO: Add a chapter and Cover Page for these
    	
        //ARS Migration
        List<ResourceLocation> arsMerge = Lists.newArrayList(Helper.createOldManualRL("ars/ars"), Helper.createOldManualRL("ars/ars2"), Helper.createOldManualRL("ars/ars3"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("ars/ars"), Helper.createRL("ars/ars_intro"), arsMerge);
        List<ResourceLocation> arsUsageMerge = Lists.newArrayList(Helper.createOldManualRL("ars/ars4"), Helper.createOldManualRL("ars/ars5"), Helper.createOldManualRL("ars/ars6"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("ars/ars4"), Helper.createRL("ars/ars_usage"), arsUsageMerge);
        
        //Controls Migration
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls"), Helper.createRL("flight/controls/controls_intro"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls2"), Helper.createRL("flight/controls/dimension"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls3"), Helper.createRL("flight/controls/facing"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls4"), Helper.createRL("flight/controls/handbrake"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls5"), Helper.createRL("flight/controls/throttle"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls6"), Helper.createRL("flight/controls/coordinate_increment"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls7"), Helper.createRL("flight/controls/vertical_land_type"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls8"), Helper.createRL("flight/controls/randomizer"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls9"), Helper.createRL("flight/controls/refueler"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls10"), Helper.createRL("flight/controls/axis_controls"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls11"), Helper.createRL("flight/controls/fast_return"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls12"), Helper.createRL("flight/controls/telepathic"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls13"), Helper.createRL("flight/controls/stabilizers"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls14"), Helper.createRL("flight/controls/sonic_port"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls15"), Helper.createRL("flight/controls/communicator"));
        migrateToNormalPage(Helper.createOldManualRL("controls/flight_controls16"), Helper.createRL("flight/controls/door_switch"));
        
        //Energy Migration
        migrateToNormalPage(Helper.createOldManualRL("energy/energy"), Helper.createRL("energy/energy_intro"));
        List<ResourceLocation> energyMerge = Lists.newArrayList(Helper.createOldManualRL("energy/energy2"), Helper.createOldManualRL("energy/energy3"), Helper.createOldManualRL("energy/energy3"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("energy/energy2"), Helper.createRL("energy/artron_energy"), energyMerge);
        
        List<ResourceLocation> forgeEnergyMerge = Lists.newArrayList(Helper.createOldManualRL("energy/energy6"), Helper.createOldManualRL("energy/energy7"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("energy/energy6"), Helper.createRL("energy/forge_energy"), forgeEnergyMerge);
        //Exclude energy8 as the Artron Converter has been removed
        
        //Events/structures
        migrateToNormalPage(Helper.createOldManualRL("events/event"), Helper.createRL("structures/structure_intro"));
        List<ResourceLocation> structuresMerge = Lists.newArrayList(Helper.createOldManualRL("events/event_broken_timeship"), Helper.createOldManualRL("events/event_broken_timeship2"),Helper.createOldManualRL("events/event_broken_timeship3"),Helper.createOldManualRL("events/event_broken_timeship4"),Helper.createOldManualRL("events/event_broken_timeship5"),Helper.createOldManualRL("events/event_broken_timeship6"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("events/event_broken_timeship"), Helper.createRL("structures/structure_broken_timeship"), structuresMerge);
        List<ResourceLocation> shipMerge = Lists.newArrayList(Helper.createOldManualRL("events/event_crashed_ship"), Helper.createOldManualRL("events/event_crashed_ship2"),Helper.createOldManualRL("events/event_crashed_ship3"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("events/event_crashed_ship"), Helper.createRL("structures/structure_crashed_ship"), shipMerge);
        
        //Flight Controls
        migrateToNormalPage(Helper.createOldManualRL("flight/flight"), Helper.createRL("flight/flight_mode_intro"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight2"), Helper.createRL("flight/flight_mode/flight_mode_types"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight3"), Helper.createRL("flight/flight_mode/unstabilized_flight"));
        List<ResourceLocation> stabilisedMerge = Lists.newArrayList(Helper.createOldManualRL("flight/flight4"), Helper.createOldManualRL("flight/flight5"),Helper.createOldManualRL("flight/flight6"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("flight/flight4"), Helper.createRL("flight/flight_mode/stabilized_flight"), stabilisedMerge);
        migrateToNormalPage(Helper.createOldManualRL("flight/flight6a"), Helper.createRL("flight/flight_mode/vortex_limbo"));
        
        //Flight Hazards
        migrateToNormalPage(Helper.createOldManualRL("flight/flight6b"), Helper.createRL("flight/flight_event/flight_event_intro"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight7"), Helper.createRL("flight/flight_event/vortex_scrap"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight8"), Helper.createRL("flight/flight_event/time_wind"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight9"), Helper.createRL("flight/flight_event/spatial_drift"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight10"), Helper.createRL("flight/flight_event/artron_flow"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight11"), Helper.createRL("flight/flight_event/exterior_bulkhead"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight12"), Helper.createRL("flight/flight_event/dimensional_drift"));
        migrateToNormalPage(Helper.createOldManualRL("flight/flight13"), Helper.createRL("flight/flight_event/vertical_displacement"));
        
        List<ResourceLocation> timeRam = Lists.newArrayList(Helper.createOldManualRL("flight/flight14"), Helper.createOldManualRL("flight/flight15"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("flight/flight14"), Helper.createRL("flight/flight_event/time_ram"), timeRam);
        List<ResourceLocation> timeRamInstigate = Lists.newArrayList(Helper.createOldManualRL("flight/flight16"), Helper.createOldManualRL("flight/flight17"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("flight/flight16"), Helper.createRL("flight/flight_event/time_ram_instigate"), timeRamInstigate);
        List<ResourceLocation> timeRamReceive = Lists.newArrayList(Helper.createOldManualRL("flight/flight18"), Helper.createOldManualRL("flight/flight19"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("flight/flight18"), Helper.createRL("flight/flight_event/time_ram_receive"), timeRamReceive);
        migrateToNormalPage(Helper.createOldManualRL("flight/flight20"), Helper.createRL("flight/flight_event/artron_pocket"));
        
        //Emotion
        List<ResourceLocation> emotionIntro = Lists.newArrayList(Helper.createOldManualRL("maintenance/maintenance_emotion"), Helper.createOldManualRL("maintenance/maintenance_emotion2"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("maintenance/maintenance_emotion"), Helper.createRL("emotion/emotion_intro"), emotionIntro);
        List<ResourceLocation> mood = Lists.newArrayList(Helper.createOldManualRL("maintenance/maintenance_emotion3"), Helper.createOldManualRL("maintenance/maintenance_emotion4"),Helper.createOldManualRL("maintenance/maintenance_emotion5_a"),Helper.createOldManualRL("maintenance/maintenance_emotion6"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("maintenance/maintenance_emotion3"), Helper.createRL("emotion/mood"), mood);
        List<ResourceLocation> loyalty = Lists.newArrayList(Helper.createOldManualRL("maintenance/maintenance_emotion7"), Helper.createOldManualRL("maintenance/maintenance_emotion8"),Helper.createOldManualRL("maintenance/maintenance_emotion9"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("maintenance/maintenance_emotion7"), Helper.createRL("emotion/loyalty"), loyalty);
        
        //Engine - to rewrite, so will not port
        
        //Subsystems - to rewrite, so will not port
        
        //Upgrades, to rewrite, so will not port
        
        //Protocols
        migrateToNormalPage(Helper.createOldManualRL("protocols/monitor_protocols"), Helper.createRL("protocols/protocol_intro"));
        migrateToNormalPage(Helper.createOldManualRL("protocols/monitor_protocols2"), Helper.createRL("protocols/change_exterior"));
        List<ResourceLocation> interiorProtocol = Lists.newArrayList(Helper.createOldManualRL("protocols/monitor_protocols3"), Helper.createOldManualRL("protocols/monitor_protocols4"),Helper.createOldManualRL("protocols/monitor_protocols4a"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("protocols/monitor_protocols3"), Helper.createRL("protocols/change_interior"), interiorProtocol);
        migrateToNormalPage(Helper.createOldManualRL("protocols/monitor_protocols5"), Helper.createRL("protocols/change_console"));
        List<ResourceLocation> securityProtocol = Lists.newArrayList(Helper.createOldManualRL("protocols/monitor_protocols6"), Helper.createOldManualRL("protocols/monitor_protocols7"),Helper.createOldManualRL("protocols/monitor_protocols7a"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("protocols/monitor_protocols6"), Helper.createRL("protocols/security/security_submenu"), securityProtocol);
        List<ResourceLocation> interiorProperties = Lists.newArrayList(Helper.createOldManualRL("protocols/monitor_protocols8"), Helper.createOldManualRL("protocols/monitor_protocols9"),Helper.createOldManualRL("protocols/monitor_protocols9a"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("protocols/monitor_protocols8"), Helper.createRL("protocols/interior/interior_properties"), interiorProperties);
        List<ResourceLocation> exteriorProperties = Lists.newArrayList(Helper.createOldManualRL("protocols/monitor_protocols10"), Helper.createOldManualRL("protocols/monitor_protocols11"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("protocols/monitor_protocols10"), Helper.createRL("protocols/exterior/exterior_properties"), exteriorProperties);
        migrateToNormalPage(Helper.createOldManualRL("protocols/monitor_protocols12"), Helper.createRL("protocols/waypoint_screen"));
        
        //Security
        migrateToNormalPage(Helper.createOldManualRL("security/security"), Helper.createRL("security/security_intro"));
        List<ResourceLocation> security = Lists.newArrayList(Helper.createOldManualRL("security/security2"), Helper.createOldManualRL("security/security3"));
        migrateAndMergeToNormalPage(Helper.createOldManualRL("security/security2"), Helper.createRL("security/admin_functions"), security);
    }
    
    public static List<ManualMigrationWrapper> getMigrationObjects(){
    	return migrationObjects;
    }
    
    public static Map<ResourceLocation, ResourceLocation> getMappingsWithoutMerging(){
        Map<ResourceLocation, ResourceLocation> locations = new HashMap<>();
        for (ManualMigrationWrapper wrapper : migrationObjects) {
            if (!wrapper.requiresMerging())
                locations.put(wrapper.getOldLoc(), wrapper.getNewLoc());
        }
        return locations;
    }
    
    public static Map<ResourceLocation, List<ResourceLocation>> mappingWithMerging(){
        Map<ResourceLocation, List<ResourceLocation>> locations = new HashMap<>();
        for (ManualMigrationWrapper wrapper : migrationObjects) {
            if (wrapper.requiresMerging())
                locations.put(wrapper.getOldLoc(), wrapper.getPathsToMerge());
        }
        return locations;
    }
    /**
     *  Create a mapping object for a manual page
     * @param oldLoc - the old page's resource location. Under tardis:manual/chapters. Use {@link Helper#createOldManualRL(String)} for easy helper
     * @param newLoc - the new page's resource locaiton. You can use a tardis:mypath without needing the manual prefix, so using {@link Helper#createRL(String)} is fine
     * @param convertedType
     */
    public static void migrate(ResourceLocation oldLoc, ResourceLocation newLoc, ManualObjectType convertedType) {
        ManualMigrationWrapper wrapper = new ManualMigrationWrapper(oldLoc, newLoc, convertedType);
        migrationObjects.add(wrapper);
    }
    
    public static void migrateAndMerge(ResourceLocation oldLoc, ResourceLocation newLoc, ManualObjectType convertedType, List<ResourceLocation> pathsToMerge) {
        ManualMigrationWrapper wrapper = new ManualMigrationWrapper(oldLoc, newLoc, convertedType);
        wrapper.setOldPathsToMerge(pathsToMerge);
        migrationObjects.add(wrapper);
    }
    
    public static void migrateToPageType(ResourceLocation oldLoc, ResourceLocation newLoc, PageType pageType) {
        ManualMigrationWrapper wrapper = new ManualMigrationWrapper(oldLoc, newLoc, ManualObjectType.PAGE);
        wrapper.setPageType(pageType);
        migrationObjects.add(wrapper);
    }
    
    public static void migrateAndMergeToPageType(ResourceLocation oldLoc, ResourceLocation newLoc, PageType pageType, List<ResourceLocation> pathsToMerge) {
        ManualMigrationWrapper wrapper = new ManualMigrationWrapper(oldLoc, newLoc, ManualObjectType.PAGE);
        wrapper.setOldPathsToMerge(pathsToMerge);
        wrapper.setPageType(pageType);
        migrationObjects.add(wrapper);
    }
    
    public static void migrateToNormalPage(ResourceLocation oldLoc, ResourceLocation newLoc) {
        ManualMigrationWrapper wrapper = new ManualMigrationWrapper(oldLoc, newLoc, ManualObjectType.PAGE);
        migrationObjects.add(wrapper);
    }
    
    public static void migrateAndMergeToNormalPage(ResourceLocation oldLoc, ResourceLocation newLoc, List<ResourceLocation> pathsToMerge) {
        ManualMigrationWrapper wrapper = new ManualMigrationWrapper(oldLoc, newLoc, ManualObjectType.PAGE);
        wrapper.setOldPathsToMerge(pathsToMerge);
        migrationObjects.add(wrapper);
    }
    
    public static class ManualMigrationWrapper{
        private List<ResourceLocation> oldPathsToMerge = new ArrayList<>();
        private ResourceLocation oldLoc;
        private ResourceLocation newLoc;
        private ManualObjectType newType;
        private PageType pageType = PageType.NORMAL;
        
        public ManualMigrationWrapper(ResourceLocation oldLoc, ResourceLocation newLoc, ManualObjectType newType) {
            this.newLoc = newLoc;
            this.oldLoc = oldLoc;
            this.newType = newType;
        }
        
        public ResourceLocation getOldLoc() {
            return this.oldLoc;
        }
        
        public ResourceLocation getNewLoc() {
            return this.newLoc;
        }
        
        public ManualObjectType getConvertedObjectType() {
            return this.newType;
        }
        
        public ManualMigrationWrapper setPageType(PageType pageType) {
            this.pageType = pageType;
            return this;
        }
        
        public PageType getPageType() {
            return this.pageType;
        }
        
        public ManualMigrationWrapper setOldPathsToMerge(List<ResourceLocation> oldPathsToMerge) {
            this.oldPathsToMerge = oldPathsToMerge;
            return this;
        }
        
        public List<ResourceLocation> getPathsToMerge(){
            return this.oldPathsToMerge;
        }
        
        public boolean requiresMerging() {
            return !this.oldPathsToMerge.isEmpty();
        }
    }

}
