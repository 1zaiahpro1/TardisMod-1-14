package net.tardis.mod.registries;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.rng.RarityPool;
import net.tardis.mod.traits.AgoraphobicTrait;
import net.tardis.mod.traits.ArachnophobiaTrait;
import net.tardis.mod.traits.ClaustrophobicTrait;
import net.tardis.mod.traits.ColdBloodedTrait;
import net.tardis.mod.traits.JealousTrait;
import net.tardis.mod.traits.MurderousTrait;
import net.tardis.mod.traits.PeaceTrait;
import net.tardis.mod.traits.PyroTrait;
import net.tardis.mod.traits.TardisTrait;
import net.tardis.mod.traits.TardisTraitType;
import net.tardis.mod.traits.WetTrait;

public class TraitRegistry {

	public static final RarityPool<TardisTraitType> RARITIES = new RarityPool<>();

    public static final DeferredRegister<TardisTraitType> TRAITS = DeferredRegister.create(TardisTraitType.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<TardisTraitType>> TRAIT_REGISTRY = TRAITS.makeRegistry("trait", () -> new RegistryBuilder<TardisTraitType>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<TardisTraitType> MURDEROUS = TRAITS.register("murderous", () -> setupTrait(MurderousTrait::new, test -> test.getTraitClass() == PeaceTrait.class));
	public static final RegistryObject<TardisTraitType> PEACEFUL = TRAITS.register("peaceful", () -> setupTrait(PeaceTrait::new, test -> test.getTraitClass() == MurderousTrait.class));
	public static final RegistryObject<TardisTraitType> WET = TRAITS.register("wet", () -> setupTrait(WetTrait::new));
	public static final RegistryObject<TardisTraitType> ARACHOPHOBIA = TRAITS.register("arachnophobia", () -> setupTrait(ArachnophobiaTrait::new));
	public static final RegistryObject<TardisTraitType> PYRO = TRAITS.register("pyro", () -> setupTrait(PyroTrait::new));
	public static final RegistryObject<TardisTraitType> COLDBLOODED = TRAITS.register("cold_blood", () -> setupTrait(ColdBloodedTrait::new));
	public static final RegistryObject<TardisTraitType> JEALOUS = TRAITS.register("jealous", () -> setupTrait(JealousTrait::new));
	public static final RegistryObject<TardisTraitType> CLAUSTROPHOBIC = TRAITS.register("claustrophobic", () -> setupTrait(ClaustrophobicTrait::new, test -> test.getTraitClass() == AgoraphobicTrait.class));
	public static final RegistryObject<TardisTraitType> AGORAPHOBIC = TRAITS.register("agoraphobic", () -> setupTrait(AgoraphobicTrait::new, test -> test.getTraitClass() == ClaustrophobicTrait.class));
    
	public static TardisTraitType setupTrait(Function<TardisTraitType, TardisTrait> supplier, Predicate<TardisTraitType> test){
		TardisTraitType type = new TardisTraitType(supplier, test);
		return type;
	}
	
	public static TardisTraitType setupTrait(Function<TardisTraitType, TardisTrait> supplier) {
		return setupTrait(supplier, test -> false);
	}

	public static void registerRarities(){

		RARITIES.addChance(50, MURDEROUS.get());

	}

}
