package net.tardis.mod.registries;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.schematics.types.ConsoleSchematicType;
import net.tardis.mod.schematics.types.ExteriorSchematicType;
import net.tardis.mod.schematics.types.InteriorSchematicType;
import net.tardis.mod.schematics.types.SchematicType;

import java.util.function.Supplier;

public class SchematicTypes {

    public static final DeferredRegister<SchematicType> SCHEMATIC_TYPES = DeferredRegister.create(SchematicType.class, Tardis.MODID);
    public static final Supplier<IForgeRegistry<SchematicType>> REGISTRY = SCHEMATIC_TYPES.makeRegistry("schematic_types", () -> new RegistryBuilder<SchematicType>().setMaxID(Integer.MAX_VALUE - 1));


    public static final RegistryObject<InteriorSchematicType> INTERIOR = SCHEMATIC_TYPES.register("interior", InteriorSchematicType::new);
    public static final RegistryObject<ExteriorSchematicType> EXTERIOR = SCHEMATIC_TYPES.register("exterior", ExteriorSchematicType::new);
    public static final RegistryObject<ConsoleSchematicType> CONSOLE = SCHEMATIC_TYPES.register("console", ConsoleSchematicType::new);


}
