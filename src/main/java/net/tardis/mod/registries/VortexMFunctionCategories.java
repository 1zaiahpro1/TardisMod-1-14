package net.tardis.mod.registries;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.vm.VortexMFunctionCategory;

public class VortexMFunctionCategories {

	public static final DeferredRegister<VortexMFunctionCategory> FUNCTION_CATEGORIES = DeferredRegister.create(VortexMFunctionCategory.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<VortexMFunctionCategory>> FUNCTION_CATEGORY_REGISTRY = FUNCTION_CATEGORIES.makeRegistry("vm_function_category", () -> new RegistryBuilder<VortexMFunctionCategory>().setMaxID(Integer.MAX_VALUE - 1));
    
    public static final RegistryObject<VortexMFunctionCategory> TELEPORT = FUNCTION_CATEGORIES.register("teleport", () -> new VortexMFunctionCategory());
    public static final RegistryObject<VortexMFunctionCategory> DIAGNOSTIC = FUNCTION_CATEGORIES.register("diagnostic", () -> new VortexMFunctionCategory());
    public static final RegistryObject<VortexMFunctionCategory> MAINTENANCE = FUNCTION_CATEGORIES.register("maintenance", () -> new VortexMFunctionCategory());
    
    public static VortexMFunctionCategory getCategoryFromIndex(int index) {
    	List<VortexMFunctionCategory> categories = Lists.newArrayList(FUNCTION_CATEGORY_REGISTRY.get().getValues());
    	return categories.get(index);
    }
}
