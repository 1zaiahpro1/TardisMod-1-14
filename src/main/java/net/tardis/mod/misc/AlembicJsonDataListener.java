package net.tardis.mod.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.apache.logging.log4j.Logger;

import com.google.gson.JsonElement;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.recipe.AlembicRecipe;

public class AlembicJsonDataListener extends GenericJsonDataListener<AlembicRecipe>{

	public AlembicJsonDataListener(String folderName, Class<AlembicRecipe> dataClass, Logger logger,
	        Function<JsonElement, AlembicRecipe> mapper) {
		super(folderName, dataClass, logger, mapper);
	}

	@Override
	public Map<ResourceLocation, AlembicRecipe> mapValues(Map<ResourceLocation, JsonElement> inputs,
	        Function<JsonElement, AlembicRecipe> mapper) {
        Map<ResourceLocation, AlembicRecipe> newMap = new HashMap<>();
		inputs.forEach((key, input) -> {newMap.put(key, mapper.apply(input).setRegistryId(key)); this.logger.info("Added: {}", key.toString());});
		return newMap;
	}


	
	

}
