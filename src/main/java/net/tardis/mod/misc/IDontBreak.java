package net.tardis.mod.misc;
/** Interface to stop inherited block classes from breaking. This is handled in Block break events in CommonEvents*/
public interface IDontBreak {

}
