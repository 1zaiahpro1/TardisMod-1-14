package net.tardis.mod.cap;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.artron.IArtronItemStackBattery;
import net.tardis.mod.boti.BotiHandler;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.boti.stores.TileStore;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.energy.TardisEnergy;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.AttunementHandler;
import net.tardis.mod.misc.InteriorEffectsHandler;
import net.tardis.mod.misc.TardisNames;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BOTIEntityMessage;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.network.packets.BOTITileMessage;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class TardisWorldCapability implements ITardisWorldData {

    public PanelInventory northInv;
    public PanelInventory eastInv;
    public PanelInventory southInv;
    public PanelInventory westInv;
    private World world;
    private TardisEnergy power = createEnergyCap();
    private ItemStackHandler itemBuffer;
    private String name;
    private BotiHandler botiHandler;
    private AttunementHandler attunementHandler;
    private InteriorEffectsHandler effectsHandler;

    private WorldShell shell;

    public TardisWorldCapability(World world) {
        this.northInv = new PanelInventory(Direction.NORTH, 8);
        this.eastInv = new PanelInventory(Direction.EAST, 5);
        this.southInv = new PanelInventory(Direction.SOUTH, 10);
        this.westInv = new PanelInventory(Direction.WEST, 10);
        this.itemBuffer = new ItemStackHandler(9);
        this.world = world;
        this.botiHandler = new BotiHandler(this, TardisConstants.BOTI_SHELL_RADIUS);
        this.attunementHandler = new AttunementHandler(() -> eastInv, this.world);
        this.effectsHandler = new InteriorEffectsHandler(this.world);

    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.put("north_inv", northInv.serializeNBT());
        tag.put("east_inv", eastInv.serializeNBT());
        tag.put("south_inv", southInv.serializeNBT());
        tag.put("west_inv", westInv.serializeNBT());
        tag.put("power", this.power.serializeNBT());
        tag.put("item_buffer", this.itemBuffer.serializeNBT());
        if(this.name != null)
        	tag.putString("name", this.name);
        this.attunementHandler.serialize(tag);
        tag.put("effects_handler", this.effectsHandler.serializeNBT());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        northInv.deserializeNBT(tag.getList("north_inv", Constants.NBT.TAG_COMPOUND));
        eastInv.deserializeNBT(tag.getList("east_inv", Constants.NBT.TAG_COMPOUND));
        southInv.deserializeNBT(tag.getList("south_inv", Constants.NBT.TAG_COMPOUND));
        westInv.deserializeNBT(tag.getList("west_inv", Constants.NBT.TAG_COMPOUND));
        power.deserializeNBT(tag.getCompound("power"));
        this.itemBuffer.deserializeNBT(tag.getCompound("item_buffer"));
        if(tag.contains("name"))
        	this.name = tag.getString("name");
        else this.name = TardisNames.getRandomName(world.rand);
        this.attunementHandler.deserialize(tag);
        this.effectsHandler.deserializeNBT(tag.getCompound("effects_handler"));
    }

    @Override
    public PanelInventory getEngineInventoryForSide(Direction dir) {
        switch (dir) {
            case EAST:
                return eastInv;
            case SOUTH:
                return southInv;
            case WEST:
                return westInv;
            default:
                return northInv;
        }
    }

    @Override
    public void tick() {


        float maxTransfer = 0.1F;
        //Artron charging
        if (!world.isRemote) {
            TardisHelper.getConsoleInWorld(this.world).ifPresent(console -> {
                PanelInventory inv = this.getEngineInventoryForSide(Direction.EAST);
                //Loop only through charging slots
                for(int i = 0; i < 4; ++i){
                    ItemStack stack = inv.getStackInSlot(i);
                    if(stack.getItem() instanceof IArtronItemStackBattery){
                        float transfer = console.getArtron() > maxTransfer ? maxTransfer : console.getArtron();
                        float accepted = ((IArtronItemStackBattery)stack.getItem()).charge(stack, transfer);
                        console.setArtron(console.getArtron() - accepted);
                    }
                }
            });
        }
        
        if (!world.isRemote) {

            TardisHelper.getConsoleInWorld(world).ifPresent(console -> {
        		if(console.isInFlight())
        		    this.attunementHandler.tick(console);
        		else{
                    if (world.getGameTime() % 20 == 0)
                        this.buildBoti();
                }
            });
        }

        //Client only shit
        if(world.isRemote) {
            if(this.shell != null)
                this.shell.tick(world.isRemote);
        }
        this.effectsHandler.tick();


        if(!world.isRemote && this.shell != null){
            TardisHelper.getConsoleInWorld(world).ifPresent(tile ->{
                this.sendBotiEntities(world.getServer().getWorld(tile.getCurrentDimension()), tile.getCurrentLocation(), this.getOffset(tile));
            });
        }

    }

    @Override
    public TardisEnergy getEnergyCap() {
        return this.power;
    }
    
    private TardisEnergy createEnergyCap() {
    	return new TardisEnergy(Integer.MAX_VALUE);
    }

    @Override
    public ItemStackHandler getItemBuffer() {
        return this.itemBuffer;
    }

    @Override
    public AttunementHandler getAttunementHandler() {
        return this.attunementHandler;
    }

    @Override
    public InteriorEffectsHandler getInteriorEffectsHandler() {
        return this.effectsHandler;
    }

    @Override
    public WorldShell getBotiWorld() {
        return this.shell;
    }

    @Override
    public void setBotiWorld(WorldShell shell) {
        this.shell = shell;
    }

    @Override
    public BOTIMessage createMessage(WorldShell shell) {
        return new BOTIMessage(shell);
    }

    public void buildBoti() {
    	
        TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
        	
        	ServerWorld exteriorWorld = this.world.getServer().getWorld(tile.getCurrentDimension());

        	this.botiHandler.setExcludedTilePos(tile.getCurrentLocation().up());

        	this.botiHandler.updateBoti(exteriorWorld, tile.getCurrentLocation(), this.getOffset(tile), false);

        	//Set portal direction
            this.shell.setPortalDirection(tile.getTrueExteriorFacingDirection());

            Network.sendToAllInWorld(new BOTIMessage(this.shell), (ServerWorld) this.world);
            sendBotiEntities(exteriorWorld, tile.getCurrentLocation(), getOffset(tile));
            for(TileStore tileStore : this.botiHandler.getTileStores()){
                Network.sendToAllInWorld(new BOTITileMessage(tileStore), (ServerWorld) this.world);
            }
        });

    }

    public BlockPos getOffset(ConsoleTile tile){
        return BlockPos.ZERO.offset(tile.getTrueExteriorFacingDirection(), (int)(TardisConstants.BOTI_SHELL_RADIUS/(double)2));
    }

    public void sendBotiEntities(ServerWorld exteriorWorld, BlockPos currentPos, BlockPos offset){
        this.botiHandler.gatherEntities(exteriorWorld, currentPos, offset);
        for(EntityStorage store : this.botiHandler.getEntityStores()){
            Network.sendToAllInWorld(new BOTIEntityMessage(store), (ServerWorld) this.world);
        }
    }

	@Override
	public String getTARDISName() {

        if(this.name == null || this.name.isEmpty())
            this.name = TardisNames.getRandomName(world.rand);

		return name;
	}

}
