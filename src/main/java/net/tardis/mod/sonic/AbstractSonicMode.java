package net.tardis.mod.sonic;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.cap.items.sonic.ISonic;
import net.tardis.mod.cap.items.sonic.SonicCapability;
import net.tardis.mod.helper.PlayerHelper;

import java.util.ArrayList;

public abstract class AbstractSonicMode extends ForgeRegistryEntry<AbstractSonicMode> {

    private TranslationTextComponent translation;

    public boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos) {
		return false;
    }

    public boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic) {
    	return false;
    }

    public TranslationTextComponent getTranslation() {
        if(this.translation == null){
            this.translation = new TranslationTextComponent("sonic.mode." + getRegistryName().getPath());
        }
        return this.translation;
    }
    
    public ItemStack getItemDisplayIcon() {
        return new ItemStack(Items.GRASS_BLOCK);
    }

    public String getDescriptionLangKey() {
        return "sonic.mode." + getRegistryName().getPath() + ".desc";
    }

    public ArrayList<TranslationTextComponent> getAdditionalInfo() {
        return new ArrayList<>();
    }

    public boolean hasAdditionalInfo() {
    	return false;
    }

    public void updateHeld(PlayerEntity playerEntity, ItemStack stack) {
    	
    }

    public boolean consumeCharge(float amount, ItemStack stack) {
        return true;
    }

    public double getReachDistance() {
        return 15D;
    }

    public void processSpecialBlocks(PlayerInteractEvent.RightClickBlock event) {

    }

    public void processSpecialEntity(PlayerInteractEvent.EntityInteract event) {

    }

    public boolean handleDischarge(Entity entity, ItemStack stack, float dischargeAmount) {
        ISonic data = SonicCapability.getForStack(stack).orElseGet(null);
        float currentCharge = data.getCharge();
        if (dischargeAmount > data.getCharge()) {
            if (entity instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) entity;
                PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.tardis.not_enough_charge", dischargeAmount - data.getCharge()), true);
            }
            return false;
        } else {
            data.setCharge(currentCharge - dischargeAmount);
            return true;
        }
    }

    public void inventoryTick(PlayerEntity player, ItemStack sonic){}
}