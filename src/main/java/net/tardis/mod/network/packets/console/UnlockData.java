package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.UnlockManager;

public class UnlockData implements ConsoleData {

    public UnlockManager manager;

    public UnlockData(UnlockManager manager) {
        this.manager = manager;
    }

    @Override
    public void applyToConsole(ConsoleTile tile, Supplier<NetworkEvent.Context> context) {
        if(manager != null)
            tile.getUnlockManager().merge(this.manager);
    }

    @Override
    public void serialize(PacketBuffer buf) {
        buf.writeCompoundTag(manager.serializeNBT());
    }

    @Override
    public void deserialize(PacketBuffer buf) {
        this.manager = new UnlockManager(null);
        this.manager.deserializeNBT(buf.readCompoundTag());
    }

    @Override
    public DataType getDataType() {
        return DataTypes.UNLOCK;
    }
}
