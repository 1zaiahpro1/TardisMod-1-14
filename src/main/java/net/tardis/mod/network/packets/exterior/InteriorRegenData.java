package net.tardis.mod.network.packets.exterior;

import net.minecraft.network.PacketBuffer;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class InteriorRegenData extends ExteriorData {

    boolean isRegening;

    public InteriorRegenData(boolean isInteriorRegenerating) {
        super(Type.INTERIOR_REGEN);
        this.isRegening = isInteriorRegenerating;
    }

    public InteriorRegenData(Type type) {
        super(type);
    }

    @Override
    public void encode(PacketBuffer buf) {
        buf.writeBoolean(this.isRegening);
    }

    @Override
    public void decode(PacketBuffer buf) {
        this.isRegening = buf.readBoolean();
    }

    @Override
    public void apply(ExteriorTile object) {
        object.setInteriorRegenerating(this.isRegening);
    }
}
