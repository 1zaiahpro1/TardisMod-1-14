package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.network.packets.exterior.ExteriorData;

import java.util.function.Supplier;

public class ExteriorDataMessage {

    public BlockPos pos;
    public ExteriorData data;

    public ExteriorDataMessage(BlockPos pos, ExteriorData data) {
        this.pos = pos;
        this.data = data;
    }

    public static void encode(ExteriorDataMessage mes, PacketBuffer buf){
        buf.writeBlockPos(mes.pos);
        buf.writeEnumValue(mes.data.getType());
        mes.data.encode(buf);
    }

    public static ExteriorDataMessage decode(PacketBuffer buf){
        BlockPos pos = buf.readBlockPos();
        ExteriorData data = buf.readEnumValue(ExteriorData.Type.class).provide();
        data.decode(buf);
        return new ExteriorDataMessage(pos, data);
    }

    public static void handle(ExteriorDataMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> ClientPacketHandler.handleExteriorData(mes.pos, mes.data));
        context.get().setPacketHandled(true);
    }
}
