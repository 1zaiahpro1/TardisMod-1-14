package net.tardis.mod.network.packets;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.helper.BlockPosHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.TelepathicUtils.SearchType;
import net.tardis.mod.tileentities.ConsoleTile;

public class TelepathicMessage {
	
    private static final TranslationTextComponent VALID = new TranslationTextComponent("message.tardis.telepathic.success");
    private static final String NOT_FOUND = "message.tardis.telepathic.not_found";
    private String name;
    private SearchType type;
    
    public TelepathicMessage(SearchType type, String key) {
        this.name = key;
        this.type = type;
    }
    
    public static void encode(TelepathicMessage mes, PacketBuffer buf) {
        buf.writeInt(mes.type.ordinal());
        buf.writeString(mes.name, 64);
    }
    
    public static TelepathicMessage decode(PacketBuffer buf) {
        return new TelepathicMessage(SearchType.values[buf.readInt()], buf.readString(64));
    }
    
    public static void handle(TelepathicMessage mes, Supplier<NetworkEvent.Context> cont) {
    	cont.get().enqueueWork(() -> {
            TileEntity te = cont.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile) {
                ConsoleTile console = (ConsoleTile) te;
                ServerWorld world = console.getWorld().getServer().getWorld(console.getDestinationDimension());
                if (world != null) {
                    BlockPos dest = console.getDestinationPosition();
                    PlayerEntity player = cont.get().getSender();
//                    int configRadius = TConfig.CONFIG.telepathicSearchRadius.get();
                    int rangeLimit = 500;
                    int rangeLimitChunks = (int)((byte)((double)rangeLimit/(double)16) << 2);
//                    int searchRadius = configRadius >= 500 ? rangeLimit : configRadius; //Use config value if not above 500 blocks
                    switch (mes.type) {
					case BIOME:
						List<Biome> list = Lists.newArrayList();
						RegistryKey<Biome> biomeKey = RegistryKey.getOrCreateKey(Registry.BIOME_KEY, new ResourceLocation(mes.name));
						Biome destBiome = world.getServer().getDynamicRegistries().getRegistry(Registry.BIOME_KEY).getOrThrow(biomeKey);
						list.add(destBiome);
						BiomeProvider biomeProvider = world.getChunkProvider().getChunkGenerator().getBiomeProvider();
						BlockPos biome = biomeProvider.findBiomePosition(dest.getX(), dest.getY(), dest.getZ(), rangeLimitChunks, 1, list::contains, world.rand, true);
						if(biome != null) {
                    		console.setDestination(console.getDestinationDimension(), biome.up(64));
                    		player.sendStatusMessage(VALID, true);
                    		
                    	}
						else player.sendStatusMessage(new TranslationTextComponent(NOT_FOUND, mes.type.toString().toUpperCase() ,rangeLimit), true);
						break;
					case SPAWN:
						BlockPos spawn = world.getSpawnPoint();
                		if(spawn != null) {
                			console.setDestination(console.getDestinationDimension(), console.randomizeCoords(spawn, 25));
                			player.sendStatusMessage(VALID, true);
                		}
						break;
					case STRUCTURE:
						int structureSearchRange = 256;
						int structureSearchRangeChunks = (int)(((double)structureSearchRange / (double)16));
						RegistryKey<StructureFeature<?, ?>> structureKey = RegistryKey.getOrCreateKey(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY, new ResourceLocation(mes.name));
						Structure<?> structureToSearch = world.getServer().getDynamicRegistries().getRegistry(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY).getOrThrow(structureKey).field_236268_b_;
                		BlockPos pos = world.getStructureLocation(structureToSearch, console.getDestinationPosition(), structureSearchRangeChunks, false);
                		if(pos != null) {
                			BlockPos landSpot;
                			List<BlockPos> possibleLandSpots = BlockPosHelper.getFilteredBlockPositionsInStructure(pos, world, world.getStructureManager(), structureToSearch, TBlocks.landing_pad.get());
                			if (!possibleLandSpots.isEmpty()) {
                				int index = world.rand.nextInt(possibleLandSpots.size());
                				landSpot = possibleLandSpots.get(index);
                			}
                			else 
                			    landSpot = pos;
                			console.setDestination(console.getDestinationDimension(), landSpot);
                			player.sendStatusMessage(VALID, true);
                		}
                		else player.sendStatusMessage(new TranslationTextComponent(NOT_FOUND, mes.type.toString().toUpperCase(), structureSearchRange), true);
						break;
					default:
						break;
                    }
                }
            }
        });
        cont.get().setPacketHandled(true);
    }




}
