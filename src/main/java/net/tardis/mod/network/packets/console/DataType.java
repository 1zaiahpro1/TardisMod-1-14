package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

public class DataType{
	
	private Supplier<ConsoleData> data;
	private String registryId;
	
	DataType(Supplier<ConsoleData> data){
		this.data = data;
	}
	
	public Supplier<ConsoleData> getConsoleData() {
		return this.data;
	}
	
	public String getRegistryId() {
		return this.registryId;
	}
	
	public DataType setRegistryId(String registryId) {
		this.registryId = registryId;
		return this;
	}
	
}
