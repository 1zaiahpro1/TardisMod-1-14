package net.tardis.mod.sounds;

import java.util.function.Supplier;

import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.tileentities.ConsoleTile;
/** Default implementation of {@linkplain AbstractSoundScheme}
 * <br> Inherit this class for most use cases*/
public class SoundSchemeBase extends AbstractSoundScheme {

	private Supplier<SoundEvent> land = () -> TSounds.TARDIS_LAND.get();
	private Supplier<SoundEvent> takeoff = () -> TSounds.TARDIS_TAKEOFF.get();
	private Supplier<SoundEvent> fly = () -> TSounds.TARDIS_FLY_LOOP.get();
	
	public SoundSchemeBase(Supplier<SoundEvent> takeoff, Supplier<SoundEvent> land, Supplier<SoundEvent> fly) {
		this.land = land;
		this.takeoff = takeoff;
		this.fly = fly;
	}
	
	public SoundSchemeBase() {}
	
	@Override
	public void playFlightLoop(ConsoleTile console) {
		console.getWorld().playSound(null, console.getPos(), fly.get(), SoundCategory.BLOCKS, 0.25F, 1F);
	}

	@Override
	public void playInteriorTakeOff(ConsoleTile console) {
		console.getWorld().playSound(null, console.getPos(), takeoff.get(), SoundCategory.BLOCKS, 0.5F, 1F);
	}

	@Override
	public void playExteriorTakeOff(ConsoleTile console) {
		ServerWorld world = console.getWorld().getServer().getWorld(console.getCurrentDimension());
		if(world != null)
			world.playSound(null, console.getCurrentLocation(), takeoff.get(), SoundCategory.BLOCKS, 0.5F, 1F);
	}

	@Override
	public void playInteriorLand(ConsoleTile console) {
		console.getWorld().playSound(null, console.getPos(), land.get(), SoundCategory.BLOCKS, 0.5F, 1F);
	}

	@Override
	public void playExteriorLand(ConsoleTile console) {
		ServerWorld world = console.getWorld().getServer().getWorld(console.getDestinationDimension());
		if(world != null)
			world.playSound(null, console.getDestinationPosition(), land.get(), SoundCategory.BLOCKS, 0.5F, 1F);
	}

	@Override
	public int getLoopTime() {
		return 32;
	}

	@Override
	public int getLandTime() {
		return 200;
	}

	@Override
	public int getTakeoffTime() {
		return 200;
	}

	@Override
	public void playTakeoffSounds(ConsoleTile console) {
		this.playInteriorTakeOff(console);
		this.playExteriorTakeOff(console);
	}

	@Override
	public void playLandSounds(ConsoleTile console) {
		this.playExteriorLand(console);
		this.playInteriorLand(console);
	}

	@Override
	public void playInteriorLandAfter(ConsoleTile console) {
		console.getWorld().playSound(null, console.getPos(), TSounds.REACHED_DESTINATION.get(), SoundCategory.BLOCKS, 1F, 1F);
	}

}
