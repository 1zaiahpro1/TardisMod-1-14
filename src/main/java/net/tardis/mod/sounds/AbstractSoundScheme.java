package net.tardis.mod.sounds;

import net.minecraft.util.SoundEvent;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.tileentities.ConsoleTile;
/** Template class for Sound Schemes
 * <br> Sound Schemes are a logic class that allows for multiple {@linkplain SoundEvent}(s) to be grouped togethor and played during specific parts of the Tardis flying sequence*/
public abstract class AbstractSoundScheme extends ForgeRegistryEntry<AbstractSoundScheme> {
    protected String translationKey;
    
    /** Define what sound sound be played in a loop, as a background sound during flight*/
    public abstract void playFlightLoop(ConsoleTile console);
    public abstract void playInteriorTakeOff(ConsoleTile console);
    public abstract void playExteriorTakeOff(ConsoleTile console);
    public abstract void playInteriorLand(ConsoleTile console);
    public abstract void playExteriorLand(ConsoleTile console);
    abstract void playInteriorLandAfter(ConsoleTile console);
    
    /** Define what sounds to play during Tardis Takeoff*/
    public abstract void playTakeoffSounds(ConsoleTile console);
    /** Define what sounds to play during Tardis Landing*/
    public abstract void playLandSounds(ConsoleTile console);
    
    /**
     * The duration of time to pass before looping the sound event again
     * <br> Should be zero only if you enjoy / by zero errors
     */
    public abstract int getLoopTime();
    /** Get the time needed to play the landing sound*/
    public abstract int getLandTime();
    /** Get the time needed to play the takeoff sound*/
    public abstract int getTakeoffTime();
    
    public String getTranslationKey() {
        if (this.translationKey == null) {
            this.translationKey = Util.makeTranslationKey("sound_scheme", this.getRegistryName());
        }
        return this.translationKey;
    }
    
    public TranslationTextComponent getDisplayName() {
        return new TranslationTextComponent(this.getTranslationKey());
    }
}
