package net.tardis.mod.world.structures;

import java.util.List;
import java.util.Map;
import java.util.Random;

import com.google.common.collect.ImmutableMap;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.monster.PillagerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Mirror;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.gen.feature.structure.StructurePiece;
import net.minecraft.world.gen.feature.structure.TemplateStructurePiece;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.loottables.TardisLootTables;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.schematics.Schematics;
import net.tardis.mod.tileentities.ShipComputerTile;

public class DalekShipStructurePieces {

    private static final ResourceLocation DALEK_SHIP = new ResourceLocation(Tardis.MODID, "space/dalek_mothership");

    private static final Map<ResourceLocation, BlockPos> OFFSET = ImmutableMap.of(DALEK_SHIP, BlockPos.ZERO);


    public static void start(TemplateManager templateManager, BlockPos pos, Rotation rotation, List<StructurePiece> pieceList, Random random) {
        int x = pos.getX();
        int z = pos.getZ();
        BlockPos rotationOffSet = new BlockPos(0, 0, 0).rotate(rotation);
        BlockPos blockpos = rotationOffSet.add(x, pos.getY(), z);
        pieceList.add(new DalekShipStructurePieces.Piece(templateManager, DALEK_SHIP, blockpos, rotation));
    }

    public static void start(TemplateManager templateManager, BlockPos pos, Rotation rotation, List<StructurePiece> pieceList, ResourceLocation templateRL) {
        int x = pos.getX();
        int z = pos.getZ();
        BlockPos rotationOffSet = new BlockPos(0, 0, 0).rotate(rotation);
        BlockPos blockpos = rotationOffSet.add(x, pos.getY(), z);
        pieceList.add(new DalekShipStructurePieces.Piece(templateManager, templateRL, blockpos, rotation));
    }

    public static class Piece extends TemplateStructurePiece {
        private final ResourceLocation resourceLocation;
        private final Rotation rotation;

        public Piece(TemplateManager templateManagerIn, ResourceLocation resourceLocationIn, BlockPos pos, Rotation rotationIn) {
            super(TStructures.Structures.DALEK_SHIP_PIECE, 0);
            this.resourceLocation = resourceLocationIn;
            BlockPos blockpos = DalekShipStructurePieces.OFFSET.get(resourceLocation);
            this.templatePosition = pos.add(blockpos.getX(), blockpos.getY(), blockpos.getZ());
            this.rotation = rotationIn;
            this.setupPiece(templateManagerIn);
        }

        public Piece(TemplateManager templateManagerIn, CompoundNBT tagCompound) {
            super(TStructures.Structures.DALEK_SHIP_PIECE, tagCompound);
            this.resourceLocation = new ResourceLocation(tagCompound.getString("Template"));
            this.rotation = Rotation.valueOf(tagCompound.getString("Rot"));
            this.setupPiece(templateManagerIn);
        }

        private void setupPiece(TemplateManager templateManager) {
            Template template = templateManager.getTemplateDefaulted(this.resourceLocation);
            PlacementSettings placementsettings = (new PlacementSettings()).setRotation(this.rotation).setMirror(Mirror.NONE);
            this.setup(template, this.templatePosition, placementsettings);
        }

        /**
         * (abstract) Helper method to read subclass data from NBT
         */
        @Override
        protected void readAdditional(CompoundNBT tagCompound) {
            super.readAdditional(tagCompound);
            tagCompound.putString("Template", this.resourceLocation.toString());
            tagCompound.putString("Rot", this.rotation.name());
        }

        @Override
        protected void handleDataMarker(String function, BlockPos pos, IServerWorld worldIn, Random rand, MutableBoundingBox sbb) {
        	if ("schematic".equals(function)) {
        		TileEntity te = worldIn.getTileEntity(pos.down());
        		if (te instanceof ShipComputerTile) {
       			    ShipComputerTile computer = (ShipComputerTile) te;
                    computer.setSchematic(this.getRandomSchematic(worldIn.getRandom(), this.getPossibleInteriorSchematics()).getId());
                    computer.setLootTable(TardisLootTables.SPACESTATION_DRONE);
                    worldIn.removeBlock(pos, false);
        		}
        	}
        	if ("schematic_exterior".equals(function)) {
        		TileEntity te = worldIn.getTileEntity(pos.down());
        		if (te instanceof ShipComputerTile) {
        			 ShipComputerTile computer = (ShipComputerTile) te;
                     computer.setSchematic(this.getRandomSchematic(worldIn.getRandom(), this.getPossibleExteriorSchematics()).getId());
                     computer.setLootTable(TardisLootTables.CRASHED_SHIP);
                     worldIn.removeBlock(pos, false);
        		}
        	}
        	if ("airlock_loot".equals(function)) {
        		Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/desert_pyramid"));
        	}
        	if ("bridge_loot".equals(function)) {
        		//dalek ship loot
        		Helper.addLootToComputerBelow(worldIn, pos, TardisLootTables.DALEK_SHIP);
        	}
        	if ("bridge_loot_alt".equals(function)) {
        		Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/end_city_treasure"));
        	}
        	if("bridge_entrance_loot".equals(function)) {
        		Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/bastion_treasure"));
        	}
        	if("cell_loot".equals(function)) {
        		//ruined portal
        		Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/ruined_portal"));
        	}
        	if("outer_misc_loot".equals(function)) {
        		//misc
        		Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/woodland_mansion"));
        	}
        	if("outer_corridor_loot".equals(function)) {
        		Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/simple_dungeon"));
        	}
            if("outer_corridor_loot_alt".equals(function)) {
            	Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/bastion_other"));
        	}
            if("transmat_loot".equals(function)) {
            	//loot at transmat room in ship
            	Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/bastion_hoglin_stable"));
            }
            if("turret_loot".equals(function)) {
            	Helper.addLootToComputerBelow(worldIn, pos, new ResourceLocation("minecraft:chests/woodland_mansion"));
            }
            if ("dalek_spawn".equals(function)) {
                final BlockPos spawnPos = pos.toImmutable();
            	DalekEntity dalek = TEntities.DALEK.get().create(worldIn.getWorld());
            	dalek.setPosition(spawnPos.getX() + 0.5, spawnPos.getY() + 1, spawnPos.getZ() + 0.5);
            	dalek.onInitialSpawn(worldIn.getWorld(), worldIn.getDifficultyForLocation(spawnPos), SpawnReason.STRUCTURE, null, null);
            	worldIn.addEntity(dalek);
            	worldIn.removeBlock(pos, false);
            }
            if ("illager_spawn".equals(function)) {
           	    final BlockPos spawnPos = pos.toImmutable();
           	    PillagerEntity pillager = EntityType.PILLAGER.create(worldIn.getWorld());
           	    pillager.setPosition(spawnPos.getX() + 0.5, spawnPos.getY() + 1, spawnPos.getZ() + 0.5);
           	    pillager.onInitialSpawn(worldIn.getWorld(), worldIn.getDifficultyForLocation(spawnPos), SpawnReason.PATROL, null, null);
           	    pillager.setItemStackToSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY); //Disarm them
           	    worldIn.addEntity(pillager);
           	    worldIn.removeBlock(pos, false);
            }
        	
        }
        
        public Schematic[] getPossibleExteriorSchematics(){
            return new Schematic[]{
            		Schematics.Exteriors.POLICE_BOX, Schematics.Exteriors.POLICE_BOX_MODERN
            };
        }
        
        public Schematic[] getPossibleInteriorSchematics(){
            return new Schematic[]{
                    Schematics.Interiors.TOYOTA, Schematics.Interiors.CORAL
            };
        }

        public Schematic getRandomSchematic(Random rand, Schematic[] schematics){
            return schematics[rand.nextInt(schematics.length)];
        }

    }
}
