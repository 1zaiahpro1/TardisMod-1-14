package net.tardis.mod.entity.hostile.dalek.types;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.entity.hostile.dalek.weapons.DalekSpecialWeapon;
import net.tardis.mod.sounds.TSounds;

public class SpecialType extends DalekType {

    private SoundEvent[] specialFireSounds,specialAmbientSounds;

    @Override
    protected void setupWeapons(DalekEntity entity) {
    	this.setWeapon(new DalekSpecialWeapon(entity));
    }

    @Override
	public double getMovementSpeed() {
		return 0.5D;
	}

	@Override
	public double getAttacksPerSecond() {
		return 0.6D;
	}

	@Override
	public double getAttackDamage() {
		return 12D;
	}

	@Override
	public double getMaxHealth() {
		return 120D;
	}

	@Override
	public Vector3d getLaserColour() {
		return new Vector3d(0.5D, 0.5D, 0);
	}

	@Override
	public DamageSource getDamageSource(LivingEntity entity) {
		return TDamageSources.causeTardisMobDamage(TDamageSources.DALEK_SPECIAL, entity).setExplosion();
	}

	@Override
    public SoundEvent getAmbientSound(DalekEntity dalekEntity) {
        if (specialAmbientSounds == null) {
            specialAmbientSounds = new SoundEvent[]{TSounds.DALEK_SW_AIM.get(), TSounds.DALEK_HOVER.get()};
        }
        return specialAmbientSounds[(int) (System.currentTimeMillis() % specialAmbientSounds.length)];
    }

    @Override
    public SoundEvent getFireSound(DalekEntity dalekEntity) {
        if (specialFireSounds == null) {
            specialFireSounds = new SoundEvent[]{TSounds.DALEK_SW_FIRE.get(), TSounds.DALEK_EXTERMINATE.get()};
        }
        return specialFireSounds[(int) (System.currentTimeMillis() % specialFireSounds.length)];
    }
}
