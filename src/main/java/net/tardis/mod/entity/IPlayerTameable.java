package net.tardis.mod.entity;

import java.util.UUID;

public interface IPlayerTameable {
	
	boolean isTamed();
	boolean isSitting();
	void setSitting(boolean isSitting);
	UUID getOwner();
	void setOwner(UUID owner);

}
