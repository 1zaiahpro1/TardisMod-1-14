package net.tardis.mod.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.GameRules;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.blocks.misc.VoxelShapeDirectional;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.helper.VoxelShapeUtils;

public class SteamGrateBlock extends Block {

    public static final VoxelShapeDirectional SHAPE = new VoxelShapeDirectional(createShape());

    public SteamGrateBlock(Properties properties) {
        super(properties);
        this.setDefaultState(getDefaultState().with(BlockStateProperties.UP, false));
    }

    public static VoxelShape createShape() {
        return Block.makeCuboidShape(0, 0, 3, 16, 1, 13);
    }
    
    public static VoxelShape createShapeUp() {
        return Block.makeCuboidShape(0, 16, 3, 16, 15, 13);
    }

    @Override
    public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        super.animateTick(stateIn, worldIn, pos, rand);
        boolean isUp = stateIn.get(BlockStateProperties.UP);
        int randomTickSpeed = worldIn.getGameRules().getInt(GameRules.RANDOM_TICK_SPEED);
        int ticksBeforeAction = randomTickSpeed > 3 && randomTickSpeed < 300 ? 300 - randomTickSpeed : 300;
        if (worldIn.getGameTime() % ticksBeforeAction == 0) {
            for (int i = 0; i < 300; ++i) {
                double horSpeed = (i / 300.0) * 0.2;
                worldIn.addParticle(ParticleTypes.CLOUD, pos.getX() + 0.3, pos.getY() + (isUp ? 0.5 : 0.1) + (i / 300.0) * 2, pos.getZ() + 0.5, ((rand.nextDouble() * horSpeed) - (horSpeed) / 2.0), 0.05, ((rand.nextDouble() * horSpeed) - (horSpeed) / 2.0));
            }
            ClientHelper.playSteamSoundOnClient(worldIn, pos);
        }
    }
    

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
    	return state.hasProperty(BlockStateProperties.HORIZONTAL_FACING) ? (state.hasProperty(BlockStateProperties.UP) 
    			?  (state.get(BlockStateProperties.UP)  == true ? VoxelShapeUtils.rotateHorizontal(createShapeUp(), state.get(BlockStateProperties.HORIZONTAL_FACING)) : SHAPE.getFor(state.get(BlockStateProperties.HORIZONTAL_FACING))) : SHAPE.getFor(state.get(BlockStateProperties.HORIZONTAL_FACING))): SHAPE.getFor(Direction.NORTH);
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		Direction direction = context.getFace();
        Direction horizontalFacing = context.getPlayer().getHorizontalFacing();
        return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, horizontalFacing.getOpposite()).with(BlockStateProperties.UP, direction.getOpposite() == Direction.DOWN ? false : true);
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(BlockStateProperties.HORIZONTAL_FACING, BlockStateProperties.WATERLOGGED, BlockStateProperties.UP);
	}

}
