package net.tardis.mod.blocks.monitor;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.misc.VoxelShapeDirectional;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.properties.Prop;

public class MonitorDynamicBlock extends NotSolidTileBlock implements IARS{

	public static final VoxelShapeDirectional SHAPE = new VoxelShapeDirectional(VoxelShapes.create(0, 0, 0.8125, 1, 1, 1));
	
    public MonitorDynamicBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.HORIZONTAL_FACING);
    }

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return SHAPE.getFor(state.get(BlockStateProperties.HORIZONTAL_FACING));
	}

}
