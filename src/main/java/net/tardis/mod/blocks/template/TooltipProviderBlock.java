package net.tardis.mod.blocks.template;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.misc.IItemTooltipProvider;
import net.tardis.mod.properties.Prop;
/** Template block class to allow for block item tooltips to be handled more consistently
 * <br> The tooltip logic is opt-in by default
 * <p> To enable this logic, call shouldShowTooltips and set to true
 * <br> If we want to allow for statistical tooltips to be shown, call setHasStatisticsTooltips and set to true
 * <br> If we want to allow for description tooltips to be shown, call setHasDescriptionTooltips and set to true*/
public class TooltipProviderBlock extends Block implements IItemTooltipProvider{
    
    protected boolean shouldShowTooltips = false;
    protected boolean hasStatisticsTooltips = false;
    protected boolean hasDescriptionTooltips = false;

    public TooltipProviderBlock(Properties properties, boolean showTooltips) {
        super(properties);
        this.shouldShowTooltips = showTooltips;
    }
    
    public TooltipProviderBlock(Properties properties) {
        this(properties, false);
    }
    
    public TooltipProviderBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }
    
    @Override
    public void addInformation(ItemStack stack, IBlockReader world, List<ITextComponent> tooltip, ITooltipFlag flag) {
        super.addInformation(stack, world, tooltip, flag);
        if (this.shouldShowTooltips()) {
            this.createDefaultTooltips(stack, world, tooltip, flag);
            if (this.hasStatisticsTooltips()) {
                tooltip.add(TardisConstants.Translations.TOOLTIP_HOLD_SHIFT);
                if (Screen.hasShiftDown()) {
                    tooltip.clear();
                    tooltip.add(0, this.getTranslatedName());
                    createStatisticTooltips(stack, world, tooltip, flag);
                }    
            }
            if (this.hasDescriptionTooltips()) {
            	tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
                if (Screen.hasControlDown()) {
                    tooltip.clear();
                    tooltip.add(0, this.getTranslatedName());
                    createDescriptionTooltips(stack, world, tooltip, flag);
                }
            }
        }
    }

    @Override
    public void createStatisticTooltips(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        
    }

    @Override
    public void createDescriptionTooltips(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        
    }
    
    @Override
    public void createDefaultTooltips(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        
    }

    @Override
    public boolean shouldShowTooltips() {
        return this.shouldShowTooltips;
    }

    @Override
    public void setShowTooltips(boolean showTooltips) {
        this.shouldShowTooltips = showTooltips;
    }

    @Override
    public boolean hasStatisticsTooltips() {
        return this.hasStatisticsTooltips;
    }

    @Override
    public void setHasStatisticsTooltips(boolean hasStatisticsTooltips) {
        this.hasStatisticsTooltips = hasStatisticsTooltips;
    }

    @Override
    public boolean hasDescriptionTooltips() {
        return this.hasDescriptionTooltips;
    }

    @Override
    public void setHasDescriptionTooltips(boolean hasDescriptionTooltips) {
        this.hasDescriptionTooltips = hasDescriptionTooltips;
    }
}
