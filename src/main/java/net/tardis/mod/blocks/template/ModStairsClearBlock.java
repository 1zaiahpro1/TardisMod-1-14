package net.tardis.mod.blocks.template;


import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.StairsBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.ars.IARS;

import java.util.function.Supplier;

public class ModStairsClearBlock extends StairsBlock implements IARS {

    public ModStairsClearBlock(Properties prop, Supplier<BlockState> state, SoundType sound, float hardness, float resistance) {
        super(state, prop.sound(sound).hardnessAndResistance(hardness, resistance).setSuffocates((blockState, reader, pos) -> false));
    }


    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }

}
