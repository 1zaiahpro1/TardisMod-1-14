package net.tardis.mod.blocks;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface ICrewTask {

    void setUsing(boolean used, World world, BlockPos pos);
    boolean isBeingUsed(World world, BlockPos pos);

}
