package net.tardis.mod.helper;

import java.util.LinkedList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.minecart.MinecartEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.network.play.server.SPlayEntityEffectPacket;
import net.minecraft.network.play.server.SPlayerAbilitiesPacket;
import net.minecraft.network.play.server.SServerDifficultyPacket;
import net.minecraft.network.play.server.SSetExperiencePacket;
import net.minecraft.network.play.server.SSetPassengersPacket;
import net.minecraft.potion.EffectInstance;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.server.TicketType;
import net.minecraft.world.storage.IWorldInfo;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.entity.TEntities;
/**
 * Modified by 50ap5ud5 17th September 2021 
 * @author LotuxPunk on 28th July 2019
 */
public class TeleportUtil {
	
    public static Entity teleportEntity(Entity entity, ServerWorld dimension, double xCoord, double yCoord, double zCoord, float yaw, float pitch){
        if (entity == null || entity.world.isRemote)
            return entity;
        MinecraftServer server = entity.getServer();
        ServerWorld sourceDim = entity.getServer().getWorld(entity.getEntityWorld().getDimensionKey());

        if (!entity.isBeingRidden() && !entity.isPassenger()){
            return handleEntityTeleport(entity, server, sourceDim, dimension, xCoord, yCoord, zCoord, yaw, pitch);
        }
        else {
        	final Entity rootEntity = entity.getLowestRidingEntity();
        	handleEntityTeleport(rootEntity, server, sourceDim, dimension, xCoord, yCoord, zCoord, yaw, pitch);
        	
//            final PassengerHelper passengerHelper = new PassengerHelper(rootEntity);
//            final PassengerHelper rider = passengerHelper.getPassenger(entity);
//            if (rider == null) {
//                return entity;
//            }
//            passengerHelper.teleport(server, sourceDim, dimension, xCoord, yCoord, zCoord, yaw, pitch);
//            passengerHelper.remountRiders();
//            passengerHelper.updateClients();
            return rootEntity;
        }
    }

    public static Entity teleportEntity(Entity entity, ServerWorld dimension, double xCoord, double yCoord, double zCoord) {
        return teleportEntity(entity, dimension, xCoord, yCoord, zCoord, entity.rotationYaw, entity.rotationPitch);
    }

    private static Entity handleEntityTeleport(Entity entity, MinecraftServer server, ServerWorld sourceDim, ServerWorld targetDim, double xCoord, double yCoord, double zCoord, float yaw, float pitch) {
        if (entity == null || entity.world.isRemote()) {
            return entity;
        }
        if (entity.getType() == ForgeRegistries.ENTITIES.getValue(EntityType.ENDER_DRAGON.getRegistryName()) || entity.getType() == TEntities.TARDIS.get() || entity.getType() == TEntities.DISPLAY_TARDIS.get()) { //Prevent Ender dragon from teleporting inside the interior and destroying it
            return entity;
        }
        boolean interDimensional = !sourceDim.getDimensionKey().getLocation().equals(targetDim.getDimensionKey().getLocation());
        if (!interDimensional) {
        	//Handle Elytra flying
            if (!(entity instanceof LivingEntity) || !((LivingEntity)entity).isElytraFlying()) {
            	entity.setMotion(entity.getMotion().mul(1.0D, 0.0D, 1.0D));
            	entity.setOnGround(true);
            }
            if (entity instanceof ServerPlayerEntity) {
                ServerPlayerEntity player = (ServerPlayerEntity)entity;
                if(player.getEntityWorld().getDimensionKey() == targetDim.getDimensionKey()) {
                    player.connection.setPlayerLocation(xCoord, yCoord, zCoord, yaw, pitch);
                }
            }
            else {
                entity.setLocationAndAngles(xCoord, yCoord, zCoord, yaw, pitch);
                entity.setRotationYawHead(yaw);
            }
            return entity;
        }
        //Assume interdimensional teleport from now on
        if (interDimensional && !ForgeHooks.onTravelToDimension(entity, targetDim.getDimensionKey())) {
            return entity;
        }
        if (entity instanceof ServerPlayerEntity) {
            return teleportPlayerInterdimensional((ServerPlayerEntity)entity, server, sourceDim, targetDim, xCoord, yCoord, zCoord, yaw, pitch);
        }
        return teleportEntityInterdimensional(entity, server, sourceDim, targetDim, xCoord, yCoord, zCoord, yaw, pitch);
    }

    private static Entity teleportEntityInterdimensional(Entity entity, MinecraftServer server, ServerWorld sourceDim, ServerWorld targetDim, double xCoord, double yCoord, double zCoord, float yaw, float pitch) {
        final ServerWorld sourceWorld = sourceDim;
        final ServerWorld targetWorld = targetDim;

        Entity old = entity;
        old.detach();
        
        Entity newEntity = entity.getType().create(targetWorld);
        if (old.isAlive() && entity instanceof MinecartEntity) {
        	old.remove(true); //equivalent of old.removed = true;
        	old.changeDimension(targetWorld);
        	old.revive(); //equivalent of old.removed = false;
        }
        if (old instanceof FishingBobberEntity) { //Explicit handling of fishing rod because it doesn't have an entity type, hence it will cause an NPE
            FishingBobberEntity bobber = (FishingBobberEntity)old;
            old.remove(true);
            ServerPlayerEntity player = (ServerPlayerEntity)bobber.func_234606_i_(); //bobber#getAngler
            newEntity = new FishingBobberEntity(player, targetWorld, 0, 0); //Use serverside constructor. Fix critical serverside stackoverflow issue caused when fishing bobber is spawn on the exterior door
        }
        
        sourceWorld.removeEntity(old);
        sourceWorld.updateEntity(old);
        if (newEntity != null) {
            newEntity.copyDataFromOld(old);
            newEntity.setLocationAndAngles(xCoord, yCoord, zCoord, yaw, pitch);
            targetWorld.addFromAnotherDimension(newEntity);
            targetWorld.updateEntity(newEntity);
        }
        else {
            System.err.println("Error teleporting entity: " + old);
            System.err.println("Entity that was teleported: " + newEntity);
        }
        sourceWorld.resetUpdateEntityTick();
        targetWorld.resetUpdateEntityTick();
        
        //Handle Elytra flying
        if (!(newEntity instanceof LivingEntity) || !((LivingEntity)newEntity).isElytraFlying()) {
        	newEntity.setMotion(newEntity.getMotion().mul(1.0D, 0.0D, 1.0D));
        	newEntity.setOnGround(true);
        }

        return newEntity;
    }

    private static PlayerEntity teleportPlayerInterdimensional(ServerPlayerEntity player, MinecraftServer server, ServerWorld sourceDim, ServerWorld destination, double xCoord, double yCoord, double zCoord, float yaw, float pitch) {
    	ChunkPos chunkpos = new ChunkPos(new BlockPos(xCoord, yCoord, zCoord));
        destination.getChunkProvider().registerTicket(TicketType.POST_TELEPORT, chunkpos, 1, player.getEntityId());
    	//Force load the target world if it's a Tardis dimension.
        //Handles cases where player didn't right click exterior and tries to enter (exterior force loads chunks if right clicked)
        //If this was not done, if the console wasn't loaded the door cannot be found
        WorldHelper.preLoadTardisInteriorChunks(destination.getWorldServer(), true);
        if (player.isSleeping()) {
            player.wakeUp();
        }
        ServerWorld destinationServerWorld = destination;
       
        player.teleport(destinationServerWorld, xCoord, yCoord, zCoord, yaw, pitch);

        //Handle player active potion effects
        for(EffectInstance effectinstance : player.getActivePotionEffects()) {
            player.connection.sendPacket(new SPlayEntityEffectPacket(player.getEntityId(), effectinstance));
        }
        //Handle player experience not getting received on client after interdimensional teleport
        IWorldInfo worldinfo = player.world.getWorldInfo();
        player.connection.sendPacket(new SSetExperiencePacket(player.experience, player.experienceTotal, player.experienceLevel));
        player.connection.sendPacket(new SPlayerAbilitiesPacket(player.abilities));//Keep abilities like creative mode flight height etc.
        player.connection.sendPacket(new SServerDifficultyPacket(worldinfo.getDifficulty(), worldinfo.isDifficultyLocked()));

        return player;
    }

    public static Entity getHighestRidingEntity(final Entity mount) {
        Entity entity;
        for (entity = mount; entity.getPassengers().size() > 0; entity = entity.getPassengers().get(0)) {}
        return entity;
    }


    public static class PassengerHelper {
        public Entity entity;
        public LinkedList<PassengerHelper> passengers;
        public double offsetX;
        public double offsetY;
        public double offsetZ;

        public PassengerHelper(final Entity entity) {
            this.passengers = new LinkedList<PassengerHelper>();
            this.entity = entity;
            if (entity.isPassenger()) {
                this.offsetX = entity.getPosX() - entity.getRidingEntity().getPosX();
                this.offsetY = entity.getPosY() - entity.getRidingEntity().getPosY();
                this.offsetZ = entity.getPosZ() - entity.getRidingEntity().getPosZ();
            }
            for (final Entity passenger : entity.getPassengers()) {
                this.passengers.add(new PassengerHelper(passenger));
            }
        }

        public void teleport(final MinecraftServer server, final ServerWorld sourceDim, final ServerWorld targetDim, final double xCoord, final double yCoord, final double zCoord, final float yaw, final float pitch) {
            this.entity.removePassengers();
            for (final PassengerHelper passenger : this.passengers) {
            	handleEntityTeleport(passenger.entity, server, sourceDim, targetDim, xCoord, yCoord, zCoord, yaw, pitch);
            }
            this.entity = handleEntityTeleport(this.entity, server, sourceDim, targetDim, xCoord, yCoord, zCoord, yaw, pitch);
        }

        public void remountRiders() {
            if (this.entity.isPassenger()) {
                this.entity.setLocationAndAngles(this.entity.getPosX() + this.offsetX, this.entity.getPosY() + this.offsetY, this.entity.getPosZ() + this.offsetZ, this.entity.rotationYaw, this.entity.rotationPitch);
            }
            for (final PassengerHelper passenger : this.passengers) {
                passenger.entity.startRiding(this.entity, true);
                passenger.remountRiders();
            }
        }

        public void updateClients() {
            if (this.entity instanceof ServerPlayerEntity) {
                this.updateClient((ServerPlayerEntity)this.entity);
            }
            for (final PassengerHelper passenger : this.passengers) {
                passenger.updateClients();
            }
        }

        private void updateClient(final ServerPlayerEntity playerMP) {
            if (this.entity.isBeingRidden()) {
                playerMP.connection.sendPacket(new SSetPassengersPacket(this.entity));
            }
            for (final PassengerHelper passenger : this.passengers) {
                passenger.updateClients();
            }
        }

        public PassengerHelper getPassenger(final Entity passenger) {
            if (this.entity == passenger) {
                return this;
            }
            for (final PassengerHelper rider : this.passengers) {
                final PassengerHelper re = rider.getPassenger(passenger);
                if (re != null) {
                    return re;
                }
            }
            return null;
        }
    }
}
