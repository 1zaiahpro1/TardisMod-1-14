package net.tardis.mod.potions;

import net.minecraft.potion.Effect;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

public class TardisPotions {
	
	public static final DeferredRegister<Effect> EFFECTS = DeferredRegister.create(ForgeRegistries.POTIONS, Tardis.MODID);

	public static final RegistryObject<Effect> MERCURY = EFFECTS.register("mercury", () -> new MercuryEffect(0xFFFFFF));

}
