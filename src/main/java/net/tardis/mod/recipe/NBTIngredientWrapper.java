package net.tardis.mod.recipe;

import net.minecraft.item.ItemStack;
import net.minecraftforge.common.crafting.NBTIngredient;
/** Wrapper class for a {@linkplain NBTIngredient}.
 * <br> DO NOT USE THIS FOR FIELDS IN A RECIPE, because there is no way to cast this to {@linkplain NBTIngredient} during deserialisation/serialisation
 * */
public class NBTIngredientWrapper extends NBTIngredient {
	/** DO NOT USE THIS FOR FIELDS IN A RECIPE*/
	public NBTIngredientWrapper(ItemStack stack) {
        super(stack);
    }

    public static NBTIngredientWrapper fromItemStack(ItemStack stack) {
        return new NBTIngredientWrapper(stack);
    }
}